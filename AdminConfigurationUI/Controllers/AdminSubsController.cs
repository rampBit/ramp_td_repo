﻿using System.Collections.Generic;

using System.Web.Mvc;
using RampGroup.Config.Utility;
using System.Data;
using Common.Repository.SQLAzureDBUtility;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Security.AccessControl;
using System.Security.Principal;
using System.DirectoryServices;
using System.Text;
using System;
using AdminConfigurationUI;
using AzureActiveDirectoryUtility;
using BusinessObjects;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using FileUtility;
using System.Web;
using BlobBusinessLogic;
using System.Xml.Linq;
using System.Configuration;

namespace BlobStorageClientGUI.Controllers
{
    public class AdminSubsController : Controller
    {
        // GET: AdminSubs
        public string eXception = null;
        public async Task<JsonResult> FindUser()
        {
            //    ViewBag.Name = ClaimsPrincipal.Current.FindFirst(ClaimTypes.Name).Value;
            //ViewBag.ObjectId = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value;
            //ViewBag.GivenName = ClaimsPrincipal.Current.FindFirst(ClaimTypes.GivenName).Value;
            //ViewBag.Surname = ClaimsPrincipal.Current.FindFirst(ClaimTypes.Surname).Value;
            //ViewBag.UPN = ClaimsPrincipal.Current.FindFirst(ClaimTypes.Upn).Value;

            return Json("Success", JsonRequestBehavior.AllowGet);
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SubmitMyBlob(BlobStorageAccount blobStorageSettings)
        {
            //new BlobStorageAccount(blobStorageSettings.AccountKey, blobStorageSettings.AccountName, blobStorageSettings.BlobStorageContainerName);
            //var blobStorageSettingsstr = BlobStorageAccount.Settings;
            //string strView = CreateXMLBlobStorage(blobStorageSettingsstr);
            var res = AdminRepository.Instance.AdminBlobSettingSConfigurations(blobStorageSettings);
            if (res == 0)
            {
                bool result = true;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                bool resulerr = false;
                return Json(resulerr, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult GetViewDetails(string Productname)
        {
            AdminRepository _repo = new AdminRepository();
            var details = _repo.GetExchangeOnlineDetails(Productname);
            
            return Json(details, JsonRequestBehavior.AllowGet);
              
        }
        [HttpPost]
        public ActionResult GetViewActive()
        {
            AdminRepository _repo = new AdminRepository();
            var details = _repo.GetActiveDirectoryDetails();

            return Json(details, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult GetBlobSettings()
        {
            AdminRepository _repo = new AdminRepository();
            var details = _repo.GetBlobSettings();

            return Json(details, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult GetViewSharePointDetails(string ProductName)
        {
            AdminRepository _repo = new AdminRepository();
            var details = _repo.GetViewSharePointDetails(ProductName);

            return Json(details, JsonRequestBehavior.AllowGet);

        }
        
        public ActionResult YourActionMethodName()
        {
            var result = new { Success = "False", Message = "Error Message" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetActiveDirectoryGroupsNames()
        {
            AzureADRepository _azureADRepository = new AzureADRepository();
            return View();
        }
        [HttpPost]
        public ActionResult SubmitActive(ActiveDirectorySettings _activeDirectorySettings)
        {
            var res = AdminRepository.Instance.SaveActiveDirectorySettings(_activeDirectorySettings);
            if (res == 0)
            {
                bool result = true;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                bool resulerr = false;
                return Json(resulerr, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SubmitMyExchange(BusinessObjects.ExchangeServerSettings adminSubscription)
        {

            var res = AdminRepository.Instance.AdminExchangeSettingSConfigurations(adminSubscription);
            if (res == 0)
            {
                bool result = true;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                bool resulerr = false;
                return Json(resulerr, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public ActionResult GetSMTPView(string ProductName)
        {

            var res = EmailQueueRepository.Instance.GetSMTPSetting(ProductName);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
    

        [HttpPost]
        public ActionResult SubmitSMTP(BusinessObjects.SMTPSetting SMTPSettings)
        {

            var res = EmailQueueRepository.Instance.saveSMTPSettings(SMTPSettings);
            if (res == 0)
            {
                bool result = true;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                bool resulerr = false;
                return Json(resulerr, JsonRequestBehavior.AllowGet);
            }

        }

        BaseRepository<AdminRepository> n;
        public ActionResult GetSourceBlobSettingsDetails()
        {
            string SourceBlobSettingsDetailsSp = "usp_GetSourceBlobSettingsDetails";
            List<string> Source = new List<string>();
            using (DBHelper db = new DBHelper(n.SqlAzureConnectionString()))
            {
                DataSet ds = db.ExecDataSetProc(SourceBlobSettingsDetailsSp);

                foreach (DataRow d in ds.Tables[0].Rows)
                {
                    Source.Add(d["AliasName"].ToString());
                }
            }
            return Json(Source, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult GetProductNames()
        {
            try
            {
                //  List<BlobSettingDetails> blobsettings = new List<BlobSettingDetails>();
                AdminRepository _repo = new AdminRepository();
                var details = _repo.GetProductsNames();
                if (details != null)
                    return Json(details, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                eXception = ex.Message;
            }
            return Json(eXception, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveRulesandRoles(List<GroupNames> FinalList)
        {

            AdminRepository _repo = new AdminRepository();
            var details = _repo.SaveGroupsandRules(FinalList);
            if (details == 0)
            {
                bool result = true;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                bool resulerr = false;
                return Json(resulerr, JsonRequestBehavior.AllowGet);
            }

        }

        [Route("GetGroupNames/{GroupName}")]
        [HttpGet]
        public ActionResult GetGroupNames()
        {
            //string GroupName
            try
            {
                AzureADRepository _azureADRepository = new AzureADRepository();
                var GroupNames = _azureADRepository.getAllADGroups();//GroupName
                if (GroupNames != null)
                {
                    return Json(GroupNames, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                eXception = ex.Message;
            }
            return Json(eXception, JsonRequestBehavior.AllowGet);
        }
        [Route("GetUserNames/{selectedSourceAlias}")]
        [HttpPost]
        public ActionResult GetUserNames(string selectedSourceAlias)
        {
            selectedSourceAlias = "AdminADgroup";

            try
            {
                AzureADRepository _azureADRepository = new AzureADRepository();
                var GroupNames = _azureADRepository.getAllUsersNames(selectedSourceAlias);
                if (GroupNames != null)
                {
                    return Json(GroupNames, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                eXception = ex.Message;
            }
            return Json(eXception, JsonRequestBehavior.AllowGet);
        }
        public List<FileObject> ProcessUplodedFiles(string xmlPath,Stream fileContent)
        {
            List<FileObject> selectedFiles = new List<FileObject>();
            
            selectedFiles.Add(new FileObject { FileName = FileService.GetFileName(xmlPath), Content = FileService.GetBytesFromStream(fileContent) });
           
            return selectedFiles;
        }
        [HttpPost]
        public ActionResult SubmitDatabaseSetting(BusinessObjects.DatabaseServerSettings databaseSettings)
        {
            var xmlPath = string.Empty;
           XmlDocument xmlDoc = new XmlDocument();
            XmlSerializer xmlSerializer = new XmlSerializer(databaseSettings.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, databaseSettings);
                xmlStream.Position = 0;
                xmlDoc.Load(xmlStream);
                xmlPath = FileService.GetXmlPathForBlobStorage();
               
                xmlDoc.Save(xmlPath);
                // xmlDoc.Save(xmlStream);
                xmlStream.Close();
                var ctntXml = xmlDoc.InnerXml;
            }

            #region Upload XMl to BLobstorage
            try
            {
                FileStream fileStream = new FileStream(xmlPath, FileMode.Open, FileAccess.Read);
                var selectedFiles = ProcessUplodedFiles(xmlPath,fileStream);
                BlobRequest request = new BlobRequest();
               
                String[] accountValues = (ConfigHelper.GetString("StoarageaccountConfig", string.Empty)).Split(';');
                var tmpc = new BlobStorageContainer
                {
                    ContainerName = accountValues[5].Replace("Containername=", "")
                };
                request.BlobStorageContainers = new List<BlobStorageContainer>();
                request.BlobStorageContainers.Add(tmpc);
                request.SourceBlob = new BlobConfig
                {
                    AccountKey = accountValues[0].Replace("AccountKey=",""),
                    AccountName = accountValues[1].Replace("AccountName=", ""),
                    BlobStorageContainerName= accountValues[2].Replace("BlobStorageContainerName=", ""),
                    DefaultEndpointsProtocol = accountValues[3].Replace("DefaultEndpointsProtocol=", ""),
                    Domain= accountValues[4].Replace("Domain=", ""),
                };
                
                var blobService = new BlobLogic(request);
                tmpc.Blobs = selectedFiles;
                var resp = blobService.UploadXmlBlob(Path.GetFileName(xmlPath).ToString(),FileService.GetBytes(xmlPath));
                return Json(resp, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                eXception = ex.Message;
            }
            #endregion

            return Json(xmlPath, JsonRequestBehavior.AllowGet);
            ///return View();

        }

        [HttpPost]

        public ActionResult SaveUnattenedFolderSettings(SPUnAttendedApplyRulesSettings UnAttendedApplyRulesSettings)
        {
            var result = SharePointRepository.Instance.saveSPUnAttendedApplyRulesSettings(UnAttendedApplyRulesSettings);
            if (result == 0)
            {             
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                bool resulerr = false;
                return Json(resulerr, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public ActionResult GetRoleandGroups()
        {
            var result = AdminRepository.Instance.GetRolesandGroups();
            if (result !=null)
            {
                
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                bool resulerr = false;
                return Json(resulerr, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SubmitSharePointOnlineSetting(BusinessObjects.SharePointOnlineSettings sharepointSettings)
        {
            var details = AdminRepository.Instance.SharePointOnlineSettingsConfigurations(sharepointSettings);
            if (details == 0)
            {
                bool result = true;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                bool resulerr = false;
                return Json(resulerr, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult getSPUnAttendedApplyRulesSettingsInfo()
        {
            var Getresult = SharePointRepository.Instance.getSPUnAttendedApplyRulesSettingsInfo();
          
            return Json(Getresult, JsonRequestBehavior.AllowGet);         
        }
        [Route("DeleteFolderSettings/{deleteID}")]
        [HttpPost]
        public ActionResult DeleteFolderSettings(int deleteID)
        {
            var deleterecord= SharePointRepository.Instance.DeleteUnattendedFolder(deleteID);
            return Json(deleterecord, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult GetSPFileTransferURL()
        {
            var SPfileTrnsUrl = SharePointRepository.Instance.GetSharePointSettingsDetailsSP();
            return Json(SPfileTrnsUrl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //EmailQueueRepository
        public ActionResult GetSelectEXcRulesets(string ProductName)
        {
            var RulesetsAppliedEx = EmailQueueRepository.Instance.getExchangeRuleSet(ProductName);
            return Json(RulesetsAppliedEx, JsonRequestBehavior.AllowGet);
        }
    }
}
