﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessObjects;
namespace BlobStorageClientGUI.Models
{
    public class Admin
    {
       public String SubcriptionID { get; set; }
        public String ResourceGroup { get; set; }
        public String DataBaseName { get; set; }
    }
}