﻿using BusinessObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlobStorageClientUI.Models
{
    public class BlobViewModel
    {
        public BlobRequest blobRequest { get; set; }

        [Required(ErrorMessage = "Please select file.")]
        [Display(Name = "Browse File")]
        public HttpPostedFileBase[] files { get; set; }
        public string FileUrl { get; set; }
    }
}