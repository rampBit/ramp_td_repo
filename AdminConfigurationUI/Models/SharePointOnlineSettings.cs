﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminConfigurationUI.Models
{
    public class SharePointOnlineSettings
    {
        public string SPFileTransferServiceURL { get; set; }
        public string SPAdminUserID { get; set; }
        public string SPAdminPassword { get; set; }
        public string RedactedDocument { get; set; }
        public string SiteURL { get; set; }
        public bool IsActive { get; set; }
    }
}