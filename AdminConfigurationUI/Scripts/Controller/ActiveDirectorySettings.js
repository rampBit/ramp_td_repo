﻿app.controller('ActiveDirectorySettings', ['$scope', '$http', '$routeParams', '$location', 'toastr',
       function ActiveDirectorySettings($scope, $http, $routeParams, $location, toastr,esterlineServices) {


           $scope.ViewActive = function () {
               $http({
                   method: 'POST',
                   url: '/AdminSubs/GetViewActive',
                   cache: false,

               }).then(function successCallback(response) {
                   $scope.ApplicationID = response.data[0].ApplicationID;
                   $scope.RedirectURL = response.data[0].RedirectURL;
                   $scope.ClientID = response.data[0].ClientID;
                   $scope.ClientSecret = response.data[0].ClientSecret;
                   $scope.TenantID = response.data[0].TenantID;
                   $scope.TenantName = response.data[0].TenantName;
                   $scope.AuthString = response.data[0].AuthString;

               })
           }
           $scope.Discard = function (ApplicationID, RedirectURL, ClientID, ClientSecret, TenantID, TenantName, AuthString) {
               $scope.ApplicationID = null;
               $scope.RedirectURL = null;
               $scope.ClientID = null;
               $scope.ClientSecret = null;
               $scope.TenantID = null;
               $scope.TenantName = null;
               $scope.AuthString = null;
           }
           $scope.Protocol = "https";
           $scope.SubmitMyActive = function (ApplicationID, RedirectURL, ClientID, ClientSecret,TenantID, TenantName, AuthString) {
               var _activeDirectorySettings = {
                   ApplicationID: ApplicationID,
                   RedirectURL: RedirectURL,
                   ClientID: ClientID,
                   ClientSecret: ClientSecret,
                   TenantID: TenantID,
                   TenantName: TenantName,
                   AuthString: AuthString,
                   IsActive: true
               };
               $http({
                   method: 'POST',
                   url: '/AdminSubs/SubmitActive',
                   cache: false,
                   data: { _activeDirectorySettings: _activeDirectorySettings }
               }).then(function successCallback(response) {
                   if (response.data == true) {

                       toastr.success("Saved Successfully");
                   }
                   else {
                       toastr.error("Not Saved ");
                   }
               }, function errorCallback(response) {
                   var demo = 0;
               });
           }
       }]);