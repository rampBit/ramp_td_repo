﻿app.controller('AzureSQLSettingsController', ['$scope', '$http', '$routeParams', '$location', 'toastr',
       function AzureSQLSettingsController($scope, $http, $routeParams, $location, toastr) {

           $scope.Protocol = "https";
           $scope.SubmitAzureSQL = function (Server, UserName, Password, DatabaseName) {
               var AzureSQlSettings = {
                   DatabaseServerName: Server,
                   DatabaseUserName: UserName,
                   DatabasePassword: Password,
                   DatabaseName: DatabaseName
               };
               $http({
                   method: 'POST',
                   url: '/AdminSubs/SubmitDatabaseSetting',
                   cache: false,
                   data: { databaseSettings: AzureSQlSettings }
               }).then(function successCallback(response) {
                   toastr.success("Saved Successfully");
               }, function errorCallback(response) {
                   var demo = 0;
               });
           }
       }]);