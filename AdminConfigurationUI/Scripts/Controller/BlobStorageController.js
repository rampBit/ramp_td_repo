﻿app.controller('BlobStorageController', ['$scope', '$http', '$routeParams', '$location', 'toastr',
       function BlobStorageController($scope, $http, $routeParams, $location, toastr,esterlineServices) {


           $scope.ViewBlobSettings = function () {
               $http({
                   method: 'POST',
                   url: '/AdminSubs/GetBlobSettings',
                   cache: false,

               }).then(function successCallback(response) {
                  
                   $scope.blobDetailsList = response.data;
                   $scope.currentPage = 0;
                   $scope.pageSize = 10;
                   $scope.numberOfPages = function () {
                       return Math.ceil($scope.blobDetailsList.length / $scope.pageSize);
                   }
                   
               })
           }
           $scope.onDetailsRowSelect = function (item) {
               $scope.AliasName = item.AliasName;
               $scope.AccountKey = item.AccountKey;
               $scope.AccountName = item.AccountName;
               $scope.BlobType = item.BlobType;
               $scope.Domain = item.Domain;
               $scope.Protocol = item.Protocol;
           }
           $scope.Discard = function (AliasName, AccountKey, AccountName, BlobType, Domain, Protocol) {
               $scope.AliasName = null;
               $scope.AccountKey = null;
               $scope.AccountName = null;
               $scope.BlobType = null;
               $scope.Domain = null;
               $scope.Protocol = null;
           }

           $scope.Protocol = "https";
           $scope.SubmitMyBlob = function (AliasName, AccountKey, AccountName, BlobType, Domain,Protocol) {
               var blobStorageSettings = {
                   AliasName: AliasName,
                   AccountKey: AccountKey,
                   AccountName: AccountName,
                   ProductName: "BlobStorageSettings",
                   BlobType: BlobType,
                   Domain: Domain,
                   Protocol: Protocol
               };
               $http({
                   method: 'POST',
                   url: '/AdminSubs/SubmitMyBlob',
                   cache: false,
                   data: { blobStorageSettings: blobStorageSettings }
               }).then(function successCallback(response) {

                   if (response.data == true) {

                       toastr.success("Saved Successfully");
                   }
                   else {
                       toastr.error("Not Saved ");
                   }
               }, function errorCallback(response) {
                   var demo = 0;
               });
           }
       }]);

app.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});