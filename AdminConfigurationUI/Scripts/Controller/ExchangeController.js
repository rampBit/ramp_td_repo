﻿app.controller('ExchangeController', ['$scope', '$http', '$routeParams', '$location', 'toastr',
       function ExchangeController($scope, $http, $routeParams,fileUploadService, $location, toastr, esterlineServices)  {
           $scope.Ex = {};
           $scope.options = [{ ProductName: "Exchange Settings", value: "ExchangeSettings" }, { ProductName: "Exchange OnPrem Settings", value: "ExchangeOnPremSettings" }];
           $scope.ProductName = $scope.options[0];
           $scope.text = 'enter email'; 
           $scope.word = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
           $scope.EwsUrl = "https://outlook.office365.com/EWS/Exchange.asmx";
           $scope.MailCount = "50";
           $scope.Protocal = "https"

           $scope.GetView = function (Product) {
               $http({
                   method: 'POST',
                   url: '/AdminSubs/GetViewDetails/?ProductName=' + Product.value,
                   cache: false,

               }).then(function successCallback(response) {
                   $scope.Ex.EmailID = response.data[0].AccountName;
                   $scope.Ex.Password = response.data[0].AccountKey;
                   $scope.Ex.Domain = response.data[0].Domain;
                   $scope.Ex.EwsUrl = response.data[0].EWSUrl;
                   $scope.Ex.MailCount = response.data[0].MailCount;
                   $scope.Ex.Protocal = response.data[0].Protocol;
               });
               $http({
                   method: 'POST',
                   url: '/AdminSubs/GetSelectEXcRulesets?ProductName=' + Product.value,
                   cache: false,

               }).then(function successCallback(responses) {
                   var responseRuleSet = responses.data.split(',');
                   //_.each(responseRuleSet, function (item) {
                   //    $scope.getListRuleSetsFromTDServerList.name == { name: item }
                   //});
                   $scope.example14model = [];
                   
                   _.each(responseRuleSet, function (repsRL) {
                       var tempItem = {};
                       _.each($scope.getListRuleSetsFromTDServerList, function (selectRL) {
                           if (repsRL == selectRL.name) {
                               tempItem.id = selectRL.id;
                               $scope.example14model.push(tempItem)
                           }
                       });
                   });
                   //_.each($scope.getListRuleSetsFromTDServerList, function (items) {
                   //    $scope.example14model.name = items.name;
                   //});
               });
           }
       
           $scope.Discard = function (EmailID, Password, Domain, EwsUrl, MailCount, Protocal) {
               $scope.EmailID = null;
               $scope.Password = null;
               $scope.Domain = null;
               $scope.EwsUrl = null;
               $scope.MailCount = null;
               $scope.Protocal = null;
           }

           $scope.SubmitMyExchange = function (Ex) {
               var Ruleset="";
             
               _.each($scope.example14model, function (item1) {
                   _.each($scope.getListRuleSetsFromTDServerList, function (item2) {
                       if (item1.id == item2.id) {
                           item1.name = item2.name;
                           if (Ruleset == "") { Ruleset = item1.name; } else { Ruleset = Ruleset +","+item1.name}
                       }
                   })
               })
               var sd = $scope.ProductName;
               //EmailID, Password, Domain, EwsUrl, MailCount, Protocal
               var adminSubscription = {
                   AccountKey: Ex.Password,
                   AccountName: Ex.EmailID,
                   Domain: Ex.Domain,
                   EwsUrl: Ex.EwsUrl,
                   MailCount: Ex.MailCount,
                   ProductName: $scope.ProductName.value,
                   Protocal: Ex.Protocal,
                   RulesetDescription: Ruleset
               };
               $http({
                   method: 'POST',
                   url: '/AdminSubs/SubmitMyExchange',
                   cache: false,
                   data: { adminSubscription: adminSubscription }
               }).then(function successCallback(response) {

                   if (response.data == true) {
                       toastr.success("Saved Succesfully");
                   }
                   else {
                       toastr.error("Not Saved ");
                   }
               }, function errorCallback(response) {
                   var demo = 0;
               });
           }
          
           $scope.GetFileTransferURL = function () {
               var url = "AdminSubs/GetSPFileTransferURL";
               var resp =
                   null;
               $http({
                   method: 'GET',
                   url: url,
                   cache: false,

               }).then(function successCallback(response) {
                   $scope.SpFLTRSpFLTRbaseURL = response.data[0].SPFileTransferServiceURL;
                   if ($scope.SpFLTRSpFLTRbaseURL !=null) {
                       $scope.GetListRuleSetsFromTDServer();
                   }
               });
             
               
           };
           $scope.GetListRuleSetsFromTDServer = function () {
               var url = $scope.SpFLTRSpFLTRbaseURL + "/getListRuleSetsFromTDServer";

               $http({
                   method: 'GET',
                   url: url,
                   contentType: "application/json", 
                   cache: false,
                   
               }).then(function successCallback(response) {
                   $scope.getListRuleSetsFromTDServerList = response.data.RulesetMessageList.messageList.message;
                   $scope.example14data = $scope.getListRuleSetsFromTDServerList;
               });
               //var response = $http({
               //    method: 'GET',
               //    url: url,
               //    contentType: "application/json", // Not to set any content header           
               //});
               
               //fileUploadService.GetRuleSetListAPI(url).success(function (response) {
                  
               //})

           }
           $scope.example14model = [];
           $scope.example14settings = {
               scrollableHeight: '400px',
               scrollable: true,
               enableSearch: true
           };
           //$scope.example14data = $scope.getListRuleSetsFromTDServerList;
           $scope.example2settings = {
               displayProp: 'id'
           };
           angular.element(document).ready(function () {
               $scope.SpFLTRSpFLTRbaseURL = {};
               $scope.GetFileTransferURL();
              
           });
       }]);