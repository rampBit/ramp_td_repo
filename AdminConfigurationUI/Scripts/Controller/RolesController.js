﻿app.controller('RulesetsController', ['$scope', '$http', '$routeParams', '$location', 'toastr',
       function RulesetsController($scope, $http, $routeParams, $location, toastr, esterlineServices) {
           $scope.RolesList = null;
           //Declaring the function to load data from database
           $scope.fillRolesList = function () {
               $http({
                   method: 'GET',
                   url: 'https:localhost:6091/BlobStorage/AdminSubs/GetRRoles',
                   data: {}
               }).success(function (result) {
                   $scope.RolesList = result.data;
               });
           };
           //Calling the function to load the data on pageload
           $scope.fillRolesList();
       }]);

