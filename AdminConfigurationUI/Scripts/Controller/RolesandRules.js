﻿
//var localurl = "http://localhost:61092/api/TeraDActFileSericeAPI";
//var baseURL = "";
//"http://teradactfiletransferserviceapi.azurewebsites.us/api/TeraDActFileSericeAPI";
app.controller('RolesandRules', ['$scope', '$http', '$routeParams', 'fileUploadService', '$location', 'toastr', '$q',
       function RolesandRules($scope, $http, $routeParams, fileUploadService, $location, toastr, $q) {

           $scope.GetProductNames = function () {
               var url = "AdminSubs/GetProductNames";
               fileUploadService.GetProductsNames(url).success(function (response) {
                   $scope.ProductName = response;
               }).error(function (response) {
               });
           }
           $scope.GetPolicy = function () {
               ///getAccountPolicy
               fileUploadService.getPolicy($scope.SpFLTRSpFLTRbaseURL + "/getAccountPolicy").success(function (response) {
                   $scope.GridPolicy = response;
               }).error(function (response) {
               });

           }
           $scope.GetTDPolicies = function () {
               $scope.TDPolicies = {};
               fileUploadService.getPolicy($scope.SpFLTRSpFLTRbaseURL + "/Checkaccountpolicy").success(function (response) {
                   $scope.TDPolicies = response;
               }).error(function (response) {
               });
           }

           $scope.onPolicyRowSelect = function (item) {
               $scope.GlobalObj = {};
               $scope.GlobalObj.PolicyName = item.PolicyName;
               $scope.GlobalObj.PolicyType = item.PolicyType;
               $scope.GlobalObj.PolicyKey = item.PolicyKey;
               $scope.GlobalObj.PolicyValue = item.PolicyValue;

               ////$scope.Pattern = item.Pattern;
               //$scope.RuleSetNamedisabled = true;
           }
           $scope.SaveADirectives = function () {

               _.each($scope.GridObject, function (item) {
                   item.PolicyType = "--" + item.PolicyType;
                   item.MasterPolicyType = null;
                   item.PolicyTypeID = null;
               });

               fileUploadService.updatePolicy($scope.SpFLTRSpFLTRbaseURL + "/updateAccountPolicy", $scope.GridObject).success(function (response) {
                   toastr.success("Saved Succesfully");
               }).error(function (response) {
                   toastr.error(response);
               });

           }
           $scope.CreateNew = function () {
               $scope.label = null;
               $scope.replacementText = null;
               $scope.RuleSetName = null;
               $scope.Type = null;
               $scope.Pattern = null;
               $scope.RuleSetNamedisabled = false;

           }

           $scope.SaveRuleSettings = function (item) {
               var listRdRuleset = [];
               var RuleSettings = {};
               if (item == null) {
                   RuleSettings = {
                       label: null,
                       replacementText: null,
                       RuleSetName: angular.element(document.getElementById("txtFilename")).val(),///.split('.'))[0],
                       Type: null,
                       Pattern: null,
                       TotalRLString: FileContents,
                       NewRuleSetName: null
                   };
               }
               else if (item != null && item != undefined) {
                   if(item.NewRuleSetName ==undefined || item.NewRuleSetName==null)
                       item.NewRuleSetName=""
                   RuleSettings = {
                       label: null,
                       replacementText: null,
                       RuleSetName: item.NewRuleSetName,
                       Type: null,
                       Pattern: null,
                       TotalRLString: item.TotalRLString,
                       NewRuleSetName: item.RuleSetName
                   };
               };

               //$scope.SpFLTRSpFLTRbaseURL
               if (RuleSettings != {}) {
                  
                  // var x = { TDRuleSetDirectiveInfo: RuleSettings };
                   listRdRuleset.push(RuleSettings);
                   fileUploadService.updateRuleSettings($scope.SpFLTRSpFLTRbaseURL + '/createORUpdateTDRuleDirectiveInfo', listRdRuleset).success(function (response) {
                       toastr.success(response.toString());
                       $scope.GetRuleListGridView();
                   }).error(function (response) {
                       console.log(response);
                   });
               }
               else
                   toastr.error("Invalid Ruleset Values");
           }
           $scope.GroupNameSearch = function () {
               var url = "AdminSubs/GetGroupNames";
               //AdminSubs/GetGroupNames/?GroupName=USer;GroupName
               fileUploadService.GetGroupNames(url).success(function (response) {
                   $scope.Groupresponse = response;
                   $scope.GetRoleDirectives();
               }).error(function (response) {
               });
           }


           $scope.groupselected = function (selectedList) {
               $scope.displayGroupName = selectedList;
               $scope.showdisplayGroupName = true;
               //v
               $scope.FinalList = [];
               _.each(selectedList, function (item) {
                   var obj = {
                       Name: item,
                       rulesList: angular.copy($scope.rulesList)
                   }
                   $scope.FinalList.push(obj);
               })


           }

           $scope.GetUserDetails = function (selectedSourceAlias) {
               var url = "AdminSubs/GetUserNames/selectedSourceAlias";
               fileUploadService.GetUserNames(url).success(function (response) {
                   $scope.Groupresponse = response;
               }).error(function (response) {
               });
           }

           $scope.getSPUnAttendedApplyRulesSettingsInfo = function () {
               var url = "AdminSubs/getSPUnAttendedApplyRulesSettingsInfo";
               fileUploadService.getSPUnAttendedApplyRulesSettings(url).success(function (response) {
                   $scope.getFolderSettingsGrid = response;
               })

           }



           $scope.GetRuleset = function () {
               var url = $scope.SpFLTRSpFLTRbaseURL + "/getListRuleSetsFromTDServer";
               fileUploadService.GetRuleset(url).success(function (response) {
                   $scope.report = response;
                   $scope.rulesList = $scope.report.data.RulesetMessageList.messageList.message;

                   //v
                   _.each($scope.rulesList, function (item) {
                       item.checked = false;
                   })
               }).error(function (response) {
               });

           }
          
           $scope.GetRuleListGridView = function () {
               //$scope.SpFLTRSpFLTRbaseURL
               var url = $scope.SpFLTRSpFLTRbaseURL + "/getTDRuleDirectiveInfo";
               fileUploadService.GetRuleList(url).success(function (response) {
                   $scope.Grid = response;

                   _.each($scope.Grid, function (item) {
                       item.NewRuleSetName = item.RuleSetName;
                   });
                   //$scope.Grid = $scope.Grids;
                   //$scope.Grid.oldRuleSetName = response.RuleSetName;
                   //$scope.Grid = response;
               })
           }


           $scope.GetAllsites = function () {
               if ($scope.SpFLTRSpFLTRbaseURL == undefined) $scope.GetFileTransferURL();
               var url = $scope.SpFLTRSpFLTRbaseURL + "/getAllSites";
               fileUploadService.getAllSitesList(url).success(function (response) {
                   $scope.GetSitesDropdownList = response;
               })

           }

           $scope.GetRoleandGroups = function () {
               var url = "AdminSubs/GetRoleandGroups/";
               $scope.List = [];
               fileUploadService.GetRoleandGroupsList(url).success(function (response) {
                   $scope.getRoleSettings = response;

                   _.each($scope.getRoleSettings, function (item) {
                       $scope.rulelist = item.RuleSetName;
                       item.GroupDetail = {};
                       item.GroupDetail.DisplayName = item.GroupName;
                       var splits = $scope.rulelist.split(",");
                       _.each($scope.GetRoleResponse, function (item2) {
                           var exitloop = false;
                           _.each(splits, function (item3) {
                               if (exitloop == false) {
                                   if (item2 == item3) {
                                       var obj = { name: item2, isChecked: true };
                                       item[item2] = obj;
                                       exitloop = true;
                                   }
                                   else {
                                       var tempObj = { name: item2, isChecked: false };

                                       item[item2] = tempObj;
                                   }
                               }
                           })
                       })

                   })
                   $scope.Groupresponse = $scope.getRoleSettings;
               })
           }


           $scope.GetTargetAllsites = function () {
               if ($scope.SpFLTRSpFLTRbaseURL == undefined) $scope.GetFileTransferURL();
               var url = $scope.SpFLTRSpFLTRbaseURL + "/getAllSites";
               fileUploadService.getAllSitesList(url).success(function (response) {

                   $scope.GetSitesTargetDropdownList = response;
                   $scope.MasterTargetDropdownList = response;
               })

           }

           $scope.selectSitesforDocmentLibraryList = function (selectedSPHostURL) {
               $scope.TargetselectedSPHostURL = null;
               $scope.GetSitesTargetDropdownList = [];
               $scope.GetSitesTargetDropdownList = angular.copy($scope.MasterTargetDropdownList);

               $scope.GetSitesTargetDropdownList = _.filter($scope.GetSitesTargetDropdownList, function (temp) {
                   return angular.lowercase(temp) != angular.lowercase(selectedSPHostURL);
               });
               //_.each($scope.GetSitesTargetDropdownList, function (temp)
               //{
               //    if(angular.lowercase(temp)==angular.lowercase(selectedSPHostURL))
               //    {
               //        $scope.GetSitesTargetDropdownList.splice(temp, 1);
               //    }
               //});


               //URL?QuerystringName=QueryStringvalue
               var url = $scope.SpFLTRSpFLTRbaseURL + "/getDocmentLibraryList" + "/?SPHostURL=" + selectedSPHostURL;
               fileUploadService.getDocmentLibraryList(url).success(function (response) {
                   $scope.DocmentLibraryList = response;
                   $scope.TargetDocmentLibraryList = response;
                   $scope.MatserDocmentLibraryList = response;

               })
           }

           $scope.DeleteFolder = function (item) {

               var url = "AdminSubs/DeleteFolderSettings" + "/?deleteID=" + item.ID;
               fileUploadService.deleteFolderSettings(url).success(function (response) {
                   $scope.getSPUnAttendedApplyRulesSettingsInfo();
               })
           }
           $scope.TargetselectSitesforDocmentLibraryList = function (TargetselectedSPHostURL) {
               //URL?QuerystringName=QueryStringvalue
               var url = $scope.SpFLTRSpFLTRbaseURL + "/getDocmentLibraryList" + "/?SPHostURL=" + TargetselectedSPHostURL;
               fileUploadService.getDocmentLibraryList(url).success(function (response) {
                   $scope.TargetDocmentLibraryList = response;
                   $scope.MatserDocmentLibraryList = response;

               })
           }

           $scope.GetFolderList = function (selectedSPHostURL, selecteddocLibName) {
               var url = $scope.SpFLTRSpFLTRbaseURL + "/getFolderList" + "/?SPHostURL=" + selectedSPHostURL + "/&docLibName=" + selecteddocLibName;
               fileUploadService.GetFolderList(url).success(function (response) {
                   $scope.GetFolderListList = response;


               })
           }
           $scope.TagetGetFolderList = function (TargetselectedSPHostURL, TargetSelectDocmentLibraryList) {
               var url = $scope.SpFLTRSpFLTRbaseURL + "/getFolderList" + "/?SPHostURL=" + TargetselectedSPHostURL + "/&docLibName=" + TargetSelectDocmentLibraryList;
               fileUploadService.GetFolderList(url).success(function (response) {
                   $scope.TarGetFolderListList = response;
                   $scope.MaterGetFolderListList = response;


               })
           }
           $scope.GetListRuleSetsFromTDServer = function () {
               var url = $scope.SpFLTRSpFLTRbaseURL + "/getListRuleSetsFromTDServer";
               fileUploadService.GetRuleSetListAPI(url).success(function (response) {
                   $scope.getListRuleSetsFromTDServerList = response.RulesetMessageList.messageList.message;
                   $scope.example14data = $scope.getListRuleSetsFromTDServerList;
               })

           }

           $scope.onDetailsRowSelect = function (item) {
               $scope.label = item.label;
               $scope.replacementText = item.replacementText;
               $scope.RuleSetName = item.RuleSetName;
               $scope.Type = item.Type;
               $scope.Pattern = item.Pattern;
               $scope.RuleSetNamedisabled = true;
           }
           $scope.GetRoleDirectives = function () {
               var url = $scope.SpFLTRSpFLTRbaseURL + "/getAllRoleDirectives";
               fileUploadService.GetRoleDirectives(url).success(function (response) {
                   $scope.GetRoleResponse = response;
                   _.each($scope.Groupresponse, function (item) {
                       _.each($scope.GetRoleResponse, function (item2) {
                           var tempObj = { name: item2, isChecked: false };

                           item[item2] = tempObj;
                       })
                   });

               }).error(function (response) { });

           }

           $scope.groupNameChecked = function () {

           }



           $scope.GroupsandNames = function (FinalList) {
               var saveObj = $scope.Groupresponse;

               var _List = angular.copy(saveObj);
               $scope.saveList = [];
               _.each(_List, function (item) {
                   var groupName = item.GroupDetail.DisplayName;
                   item.list = [];
                   _.each(item, function (itm) {

                       if (itm !== null && itm !== undefined) {
                           if (itm.name) {
                               item.list.push(itm);
                           }
                       }

                   })

                   var childSaveLst = angular.copy(_.filter(item.list, { isChecked: true }));
                   var mainSaveList = { Name: groupName, rulesList: childSaveLst }

                   $scope.saveList.push(mainSaveList);
               })





               var url = "AdminSubs/SaveRulesandRoles";
               fileUploadService.SaveRulesandRoles(url, $scope.saveList).success(function (response) {
                   if (response == true) {

                       toastr.success("Saved Successfully");
                   }
                   else {
                       toastr.error("Not Saved ");
                   }

                   $scope.report = response;
               });
               //var filteredContainers = _.filter($scope.rulesList, function (temp3) {
               //    return _.filter(temp3.name, { Selected: true }).length > 0;
               //});


           }

            $scope.tempDropDownObj = {};

           $scope.saveSourceandTarget = function (selectedSPHostURL, SelectDocmentLibraryList, selectedFolderNames, TargetselectedSPHostURL, TargetSelectDocmentLibraryList, TargetselectedFolderNames, TargetRuleset) {
               var ruleSetString;
               var selectedMultiList = $scope.example14model;

               _.each(selectedMultiList, function(item1){
                   _.each($scope.getListRuleSetsFromTDServerList, function (item2) {
                       if (item1.id == item2.id) {
                           item1.name = item2.name;
                       }
                   })
               })

               _.each(selectedMultiList, function (truleString) {
                   if (ruleSetString == undefined) ruleSetString = truleString.name; else ruleSetString = ruleSetString + "," + truleString.name;
               });
               var UnAttendedApplyRulesSettings = {

                   SourceSiteURL: selectedSPHostURL,
                   SourceDocLib: SelectDocmentLibraryList,
                   SourceFolder: selectedFolderNames,
                   TargetSiteURL: TargetselectedSPHostURL,
                   TargetDocLib: TargetSelectDocmentLibraryList,
                   TargetFolder: TargetselectedFolderNames,
                   AppliedRuleSets: ruleSetString
               };

               if (UnAttendedApplyRulesSettings.SourceSiteURL == null || UnAttendedApplyRulesSettings.SourceDocLib == null
                   || UnAttendedApplyRulesSettings.TargetSiteURL == null || UnAttendedApplyRulesSettings.TargetDocLib == null || UnAttendedApplyRulesSettings.AppliedRuleSets == "" || UnAttendedApplyRulesSettings.AppliedRuleSets == undefined) {
                   toastr.error("Please select the required values");
                   return false;
               }
               $http({
                   method: 'POST',
                   url: '/AdminSubs/SaveUnattenedFolderSettings',
                   cache: false,
                   data: { UnAttendedApplyRulesSettings: UnAttendedApplyRulesSettings }
               }).then(function successCallback(response) {

                   if (response.status == 200) {

                       toastr.success("Saved Successfully");
                       $scope.tempDropDownObj = {};
                       
                       $scope.example14model = [];
                   }

               });

           }

           $scope.onFolderDetailsRowSelect = function (item) {

               $scope.selectedSPHostURL = item.SourceSiteURL;
               $scope.SelectDocmentLibraryList = item.SourceDocLib;
               $scope.selectedFolderNames = item.SourceFolder;
               $scope.TargetselectedSPHostURL = item.TargetSiteURL;
               $scope.TargetSelectDocmentLibraryList = item.TargetDocLib;
               $scope.TargetselectedFolderNames = item.TargetFolder;
               $scope.TargetRuleset = item.TargetSiteURL;

           }

           $scope.GetFileTransferURL = function () {
               $scope.SpFLTRSpFLTRbaseURL = {};
               var url = "AdminSubs/GetSPFileTransferURL";
               fileUploadService.GetFLTransfrURL(url).success(function (response) {
                   var x = response;
                   $scope.SpFLTRSpFLTRbaseURL = response[0].SPFileTransferServiceURL;
                   if (response[0].SPFileTransferServiceURL != null && response[0].SPFileTransferServiceURL != undefined) {
                       $scope.GetRuleset();
                       $scope.GetPolicy();
                       $scope.GetRuleListGridView();
                       $scope.GetAllsites();
                       $scope.GetTargetAllsites();
                       $scope.GetListRuleSetsFromTDServer();
                       $scope.GetTDPolicies();
                   }
                   // return response[0].SPFileTransferServiceURL;
               }).error(function (response) {
               });
           };


           $scope.EnableGrid = function (flag) {
               if (flag == 1) { $scope.GetFlag = false; $scope.viewFlag = true; $scope.hideFlag = false; }
               else { $scope.GetFlag = true; $scope.viewFlag = false; $scope.hideFlag = true; }
           };
           $scope.AppendtoGrid = function () {

               $scope.GridPolicy.push($scope.GlobalObj);

               _.each($scope.GridPolicy, function (eItem) {
                   eItem.PolicyName = 'Redact';
               });
               $scope.GridObject = {};
               $scope.GridObject = $scope.GridPolicy;
               $scope.GlobalObj = {};
           };
           
         
           angular.element(document).ready(function () {
               $scope.EnableGrid(0);
                   $scope.GetFileTransferURL();
                   $scope.GroupNameSearch();
                   //$scope.GetRuleset();
                   //$scope.GetPolicy();
                   //$scope.GetRuleListGridView();
                   //$scope.GetAllsites();
                   //$scope.GetTargetAllsites();
                   //$scope.GetListRuleSetsFromTDServer();
               });
           
           ////multiselect

           $scope.example14model = [];
           $scope.example14settings = {
               scrollableHeight: '400px',
               scrollable: true,
               enableSearch: true
           };
           //$scope.example14data = $scope.getListRuleSetsFromTDServerList;
           $scope.example2settings = {
               displayProp: 'id'
           };
       }]);




