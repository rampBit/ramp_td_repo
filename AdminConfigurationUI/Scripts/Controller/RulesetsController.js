﻿app.controller('RulesetsController', ['$scope', '$http', '$routeParams', '$location', 'toastr',
       function RulesetsController($scope, $http, $routeParams, $location, toastr, esterlineServices) {
           $scope.RulesetList = null;
           //Declaring the function to load data from database
           $scope.fillRulesetList = function () {
               $http({
                   method: 'GET',
                   url: 'https:localhost:6091/BlobStorage/AdminSubs/GetRulesetNames',
                   data: {}
               }).success(function (result) {
                   $scope.RulesetList = result.data;
               });
           };
           //Calling the function to load the data on pageload
           $scope.fillRulesetList();
       }]);

