﻿app.controller('SMTPController', ['$scope', '$http', '$routeParams', '$location', 'toastr',
       function SMTPController($scope, $http, $routeParams, $location, toastr, esterlineServices) {
           $scope.TargetName = "STARTTLS/smtp.office365.com";
           $scope.options = [{ ProductName: "Exchange Online Settings", value: "ExchangeSettings" }, { ProductName: "Exchange OnPremSettings", value: "ExchangeOnPremSettings" }];


           $scope.ProductName = $scope.options[0];

           $scope.Enablefalse = function () {
               if ($scope.EnableSSLType == false) {
                   return true;
               }
               else
                   return false;
           }
           $scope.Enabletrue = function () {
               if ($scope.EnableSSLType == true) {
                   return true;
               }
               else
                   return false;
           }
           $scope.GetView = function (ProductName) {
               $http({
                   method: 'GET',
                   url: '/AdminSubs/GetSMTPView?ProductName=' + ProductName.value,
                   cache: false,

               }).then(function successCallback(response) {

                   //$scope.ProductName = response.data.ProductName;
                   $scope.Port = response.data.Port;
                   $scope.Host = response.data.Host;
                   $scope.EnableSSLType = response.data.EnableSsl;
                   $scope.TargetName = response.data.TargetName;

               })


           }

           $scope.SubmitMySMTP = function (ProductName, Port, Host, EnableSSLType, TargetName) {
               ProductNamesettings = ProductName.value;

               var SMTPSettings = {
                   ProductName: ProductNamesettings,
                   Port: Port,
                   Host: Host,
                   EnableSsl: EnableSSLType,
                   TargetName: TargetName
               };
               $http({
                   method: 'POST',
                   url: '/AdminSubs/SubmitSMTP',
                   cache: false,
                   data: { SMTPSettings: SMTPSettings }
               }).then(function successCallback(response) {

                   if (response.data == true) {

                       toastr.success("Saved Successfully");
                   }
                   else {
                       toastr.error("Not Saved ");
                   }
               }, function errorCallback(response) {
                   var demo = 0;
               });
           }
       }]);

app.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});