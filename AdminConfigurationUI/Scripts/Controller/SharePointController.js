﻿app.controller('SharePointController', ['$scope', '$http', '$routeParams', '$location', 'toastr',
   
function SharePointController($scope, $http, $routeParams, fileUploadService, toastr, esterlineServices) {
    $scope.sp = {}; $scope.options = [{ ProductName: "SharePoint Settings", value: "SharePointSettings" }, { ProductName: "SharePoint OnPrem Settings", value: "SharePointOnPremSettings" }];
    $scope.SPProductType = $scope.options[0];
    $scope.ViewSharePointDetails = function (Product) {
        $http({
            method: 'POST',
            url: '/AdminSubs/GetViewSharePointDetails/?ProductName=' + Product.value,
            cache: false,
           // data: { ProductName: Product }
        }).then(function successCallback(response) {
            $scope.sp = response.data[0];
            console.log(response.data[0] +"/r/n" + $scope.sp);
        })
    }

    $('#ddlRuleSet').multiselect({
        includeSelectAllOption: true,
        maxHeight: 250,
        buttonWidth: 150,
        numberDisplayed: 3,
        SelectedText: 'selected',
        nonSelectedText: 'Select RuleSet'
    });

    $scope.Discard = function (SPFileTransferServiceURL, SPAdminUserID, SPAdminPassword, AdminCentarlSiteURL, SharedDocumentLibName) {
        $scope.SPFileTransferServiceURL = null;
        $scope.SPAdminUserID = null;
        $scope.SPAdminPassword = null;
        $scope.AdminCentarlSiteURL = null;
        $scope.SharedDocumentLibName = null;
    }

    $scope.RedactedDocument = "TeraDActRedactedDocuments";


       
    //$scope.SPFileTransferServiceURL ="http://teradactfiletransferserviceapi.azurewebsites.us/api/TeraDActFileSericeAPI/ProcessFile/";
    $scope.SubmitMySharePoint = function (SP) {
        var sharepointSettings = {
            SPFileTransferServiceURL: SP.SPFileTransferServiceURL,
            SPAdminUserID: SP.SPAdminUserID,
            SPAdminPassword: SP.SPAdminPassword,
            AdminCentarlSiteURL: SP.AdminCentarlSiteURL,
            SharedDocumentLibraryName: SP.SharedDocumentLibraryName,
            ProductName: $scope.SPProductType.value,
            Domain:SP.Domain
        };
        $http({
            method: 'POST',
            url: '/AdminSubs/SubmitSharePointOnlineSetting',
            cache: false,
            data: { sharepointSettings: sharepointSettings }
        }).then(function successCallback(response) {

            if (response.data == true) {
                toastr.success("Saved Succesfully");
            }
            else {
                toastr.error("Not Saved ");
            }
        }, function errorCallback(response) {
            // alert(response.data);
        });
    }
}]);