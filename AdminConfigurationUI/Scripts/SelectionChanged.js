﻿function OnSelectionchange() {
    if ($('#SelectSourceTarget').val() === "Blob_To_Blob") {
        GetAdminBlobAccount();
        $('#source_to_destination').css("display", "");
        $('#Local_to_Blob').css("display", "none");       
        //Visible only for blob to blob and for non-redact case.
    } else if ($('#SelectSourceTarget').val() === "Non_Blob") {
        $('#source_to_destination').css("display", "none");
        $('#Redact_To_Blob').css("display", "none");
        $('#Non_Blob').css("display", "");
        $('#Local_to_Blob').css("display", "");
       
        //Hidden for others.
    }
    else {
        $('#source_to_destination').css("display", "none");
        $('#Non_Blob').css("display", "none");
        $('#Redact_To_Blob').css("display", "");
        $('#Local_to_Blob').css("display", "");
    }
    
}

function GetAdminBlobAccount()
{
    $.ajax({
        url: "BlobUpload/GetBlobAccount",  // Url to send request to
        data: "",  // Send the data to the server
        type: "GET",    // Make it a POST request
        dataType: "json", // Expect back JSON
        contentType: 'application/json; charset=utf-8',
        success: function (json) {
            $('#source_accountkey').val(json.SourceBlob.AccountKey);
            $('#source_accountname').val(json.SourceBlob.AccountName);
            $('#source_blobcontainer').val(json.SourceBlob.BlobStorageContainerName);
        }
    });
}