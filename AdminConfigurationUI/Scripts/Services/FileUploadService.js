﻿app.service('fileUploadService', ['$http',  function ($http) {

    var Service = {};
    Service.UploadFiles = function (url,FileData)
    {
        var response = $http({
            method: 'POST',
            url: url,
            data: FileData,
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        })
        return response;
    }
    Service.BlopbToBlobFileTransfer = function (url, SourceToTargetTransfer) {
        var response = $http({
            method: 'POST',
            url: url,
            contentType: "application/json", // Not to set any content header  
            data: JSON.stringify(SourceToTargetTransfer),
        })
        return response;
    }

    Service.deleteFolderSettings = function (url) {
        var response = $http({
            method: 'POST',
            url: url,
            contentType: "application/json", // Not to set any content header           
        })
        return response;
    }
    Service.getSPUnAttendedApplyRulesSettings = function (url) {
        var response = $http({
            method: 'GET',
            url: url,
            contentType: "application/json", // Not to set any content header           
        })
        return response;
    }
    Service.getAllSitesList = function (url) {
        var response = $http({
            method: 'GET',
            url: url,
            contentType: "application/json", // Not to set any content header           
        })
        return response;
    }

    Service.getDocmentLibraryList = function (url) {
        var response = $http({
            method: 'GET',
            url: url,
            contentType: "application/json", // Not to set any content header           
        })
        return response;
    }
    
    Service.GetFolderList = function (url) {
        var response = $http({
            method: 'GET',
            url: url,
            contentType: "application/json", // Not to set any content header           
        })
        return response;
    }
    Service.GetRuleSetListAPI = function (url) {
        var response = $http({
            method: 'GET',
            url: url,
            contentType: "application/json", // Not to set any content header           
        })
        return response;
    }
    Service.GetRoleandGroupsList = function (url) {
        var response = $http({
            method: 'GET',
            url: url,
            contentType: "application/json", // Not to set any content header           
        })
        return response;
    }
    Service.Setup = function (url) {
        var response = $http({
            method: 'POST',
            url: url,
            contentType: "application/json", // Not to set any content header           
        })
        return response;
    }
    Service.GetProductsNames = function (url) {
        var response = $http({
            method: 'POST',
            url: url,
            contentType: "application/json", // Not to set any content header           
        })
        return response;
    }
    Service.GetGroupNames = function (url) {
        var response = $http({
            method: 'GET',
            url: url,
            contentType: "application/json", // Not to set any content header           
        })
        return response;
    }
    Service.getPolicy = function (url) {
        var response = $http({
            method: 'GET',
            url: url,
            contentType: "application/json", // Not to set any content header           
        })
        return response;
    }
    Service.updatePolicy = function (url, policyinfo) {
        var response = $http({
            method: 'POST',
            url: url,
            contentType: "application/json", // Not to set any content header  
            data: JSON.stringify(policyinfo)
        })
        return response;
    }
    Service.updateRuleSettings = function (url, RuleSettings) {
        var response = $http({
            method: 'POST',
            url: url,
            contentType: "application/json", // Not to set any content header  
            data: RuleSettings
        })
        return response;
    }
    Service.GetUserNames = function (url) {
        var response = $http({
            method: 'POST',
            url: url,
            contentType: "application/json", // Not to set any content header  
            
        })
        return response;
    }
    Service.SaveRuleset = function (url, FinalList) {
        var res = HttpContext.Current.Response;
        var req = HttpContext.Current.Request;
        res.AppendHeader("Access-Control-Allow-Origin", req.Headers["Origin"]);
        res.AppendHeader("Access-Control-Allow-Credentials", "true");
        res.AppendHeader("Access-Control-Allow-Headers", "Content-Type, X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Date, X-Api-Version, X-File-Name");
        res.AppendHeader("Access-Control-Allow-Methods", "POST,GET,PUT,PATCH,DELETE,OPTIONS");

        // ==== Respond to the OPTIONS verb =====
        if (req.HttpMethod == "OPTIONS") {
            res.StatusCode = 200;
            res.End();
        }
        var response = $http({
            method: 'POST',
            url: url,
            contentType: "application/json", // Not to set any content header  
            data: JSON.stringify(FinalList),
        })
        return response;
    }

    Service.SaveRulesandRoles = function (url,FinalList) {
        var response = $http({
            method: 'POST',
            url: url,
            contentType: "application/json",
            data: JSON.stringify(FinalList),
        })
        return response;
    }


    Service.GetRoleDirectives = function (url) {
        var response = $http({
            method: 'GET',
            url: url,
            contentType: "application/json", // Not to set any content header  
        })
        return response;
    }
    
    Service.GetRuleList = function (url) {
        var response = $http({
            method: 'GET',
            url: url,
            contentType: "application/json", // Not to set any content header  
        })
        return response;
    }
    Service.GetRuleset = function (url) {
        var response = $http({
            method: 'GET',
            url: url,
            contentType: "application/json", // Not to set any content header  
        })
        return response;
    }

    Service.GetFLTransfrURL = function (url) {
        var response = $http({
            method: 'GET',
            url: url,
            contentType: "application/json", // Not to set any content header  
        })
        return response;
    }
   
    return Service;
}]);