/// <autosync enabled="true" />
/// <reference path="ai.0.15.0-build58334.js" />
/// <reference path="app/app.js" />
/// <reference path="blobtoblobupload.js" />
/// <reference path="bootstrap.js" />
/// <reference path="controller/accountdirective.js" />
/// <reference path="controller/activedirectorysettings.js" />
/// <reference path="controller/azuresqlsettingscontroller.js" />
/// <reference path="controller/blobstoragecontroller.js" />
/// <reference path="controller/exchangecontroller.js" />
/// <reference path="controller/rolesandrules.js" />
/// <reference path="controller/rolescontroller.js" />
/// <reference path="controller/rulesetscontroller.js" />
/// <reference path="Controller/SharePointController.js" />
/// <reference path="fileuploadcontroller.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="lib/angular.js" />
/// <reference path="lib/angular-route.js" />
/// <reference path="lib/angular-toastr.min.js" />
/// <reference path="lib/angular-toastr.tpls.js" />
/// <reference path="lib/bootstrap.min.js" />
/// <reference path="lib/bootstrap-3.3.2.min.js" />
/// <reference path="lib/jquery.multiselect.js" />
/// <reference path="lib/jquery-3.1.1.min.js" />
/// <reference path="lib/moment.min.js" />
/// <reference path="lib/tinymce.js" />
/// <reference path="lib/ui-bootstrap-tpls-2.3.0.js" />
/// <reference path="lib/underscore.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="respond.js" />
/// <reference path="selectionchanged.js" />
/// <reference path="services/fileuploadservice.js" />
/// <reference path="toastr.js" />
