﻿using BusinessObjects;
using Common.Repository.SQLAzureDBUtility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureActiveDirectoryUtility
{
    public class AzureADRepository : BaseRepository<AzureADRepository>
    {
        private GraphRequests _GR = null;
     
        public AzureADRepository()
        {
             if (null == AADConstants.settings)   
                getTenantConstants();
            _GR = new AzureActiveDirectoryUtility.GraphRequests(GraphRequestsMode.AppMode);
            
        }
        //public AzureADRepository(GraphRequestsMode mode)
        //{
        //    if (null == AADConstants.settings)
        //        getTenantConstants();
        //    _GR = new AzureActiveDirectoryUtility.GraphRequests(mode);

        //}

        private void getTenantConstants()
        {
            const string getAzureADSettings = "getAzureADSettings";
            AADConstants AADConstants = new AADConstants();
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlDataReader reader = db.ExecDataReaderProc(getAzureADSettings);

                var Res = from Dr in reader.Enumerate()
                          select new BusinessObjects.AADConstants
                          {
                              AuthString = Dr["AuthString"] as string,
                              ClientId = Dr["ClientID"] as string,
                              ClientSecret = Dr["ClientSecret"] as string,
                              ResourceUrl = Dr["ResourceUrl"] as string,
                              TenantId = Dr["TenantID"] as string,
                              TenantName = Dr["TenantName"] as string
                          };
                AADConstants = Res.First();
                AADConstants.settings = AADConstants;
            }
        }

        public List<AADGroup> getAllADGroups()
        {
            //string SearchString = ""
            var t1 = _GR.GetAllGroups();
            t1.Wait();
            return t1.Result;

        }
        public List<AADGroup> getAllADGroups(string SPLoggedUserName)
        {
            //string SearchString = ""
            var t1 = _GR.GetAllGroupsbyEmailID(SPLoggedUserName);
            t1.Wait();
            return t1.Result;

        }
        

        public void getAllAADRoles(string UserEmailID)
        {
            var t1 = _GR.getAllAADRoles(UserEmailID);
            t1.Wait();
           // return t1.Result;
        }

        public List<MemberDetail> getAllUsersNames(string GroupName)
        {
            _GR = new GraphRequests(GraphRequestsMode.UserMode);
            var t2 = _GR.GetMembersFromGroup(GroupName);
            t2.Wait();
            return t2.Result;
        }
 
    }
}
