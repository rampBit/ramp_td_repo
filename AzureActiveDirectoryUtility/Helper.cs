﻿using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AzureActiveDirectoryUtility
{
    public static class StringExtensions
    {
        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }
    }
    public class Helper
    {
        public static string ExtractErrorMessage(Exception exception)
        {
            List<string> errorMessages = new List<string>();


            string tabs = "\n";
            while (exception != null)
            {
                string requestIdLabel = "requestId";
                if (exception is DataServiceClientException &&
                    exception.Message.Contains(requestIdLabel))
                {
                    Dictionary<string, object> odataError =
                        new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(exception.Message);
                    odataError = (Dictionary<string, object>)odataError["odata.error"];
                    errorMessages.Insert(0, "\nRequest ID: " + odataError[requestIdLabel]);
                    errorMessages.Insert(1, "Date: " + odataError["date"]);
                }

                tabs += "    ";
                errorMessages.Add(tabs + exception.Message);
                exception = exception.InnerException;
            }

            return string.Join("\n", errorMessages);
        }
        public static string GetRandomString(int length = 32)
        {
            //because GUID can't be longer than 32
            return Guid.NewGuid().ToString("N").Substring(0, length > 32 ? 32 : length);
        }
    }
}
