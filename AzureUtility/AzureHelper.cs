﻿
using Common.Repository.SQLAzureDBUtility;

namespace AzureUtility
{
    public class AzureHelper  
    {
        private static AzureHelper currentInstance = null;
        public static AzureHelper Instance { get { if (currentInstance == null) currentInstance = new AzureUtility.AzureHelper(); return currentInstance; } }


        //public ResourceGroup CreateOrUpdateResourceGroup(ResourceManagementClient resourceMgmtClient, string subscriptionId, string resourceGroupName, string resourceGroupLocation)
        // {
        //     ResourceGroup resourceGroupParameters = new ResourceGroup()
        //     {
        //         Location = resourceGroupLocation,
        //     };
        //     resourceMgmtClient.SubscriptionId = subscriptionId;
        //     ResourceGroup resourceGroupResult = resourceMgmtClient.ResourceGroups.CreateOrUpdate(resourceGroupName, resourceGroupParameters);
        //     return resourceGroupResult;
        // }
        //public string SqlAzureConnectionString()
        //{
        //    return $"Data Source = {DBSettings.Settings.DataSource}; Initial Catalog = {DBSettings.Settings.InitialCatalog}; User ID = {DBSettings.Settings.UserID}; Password = {DBSettings.Settings.Password}";

        //}
        //public ServerGetResponse CreateOrUpdateServer(SqlManagementClient sqlMgmtClient, string resourceGroupName, string serverLocation, string serverName, string serverAdmin, string serverAdminPassword)
        //{
        //    ServerCreateOrUpdateParameters serverParameters = new ServerCreateOrUpdateParameters()
        //    {
        //        Location = serverLocation,
        //        Properties = new ServerCreateOrUpdateProperties()
        //        {
        //            AdministratorLogin = serverAdmin,
        //            AdministratorLoginPassword = serverAdminPassword,
        //            Version = "12.0"
        //        }
        //    };
        //    ServerGetResponse serverResult = sqlMgmtClient.Servers.CreateOrUpdate(resourceGroupName, serverName, serverParameters);
        //    return serverResult;
        //}


        //public FirewallRuleGetResponse CreateOrUpdateFirewallRule(SqlManagementClient sqlMgmtClient, string resourceGroupName, string serverName, string firewallRuleName, string startIpAddress, string endIpAddress)
        //{
        //    FirewallRuleCreateOrUpdateParameters firewallParameters = new FirewallRuleCreateOrUpdateParameters()
        //    {
        //        Properties = new FirewallRuleCreateOrUpdateProperties()
        //        {
        //            StartIpAddress = startIpAddress,
        //            EndIpAddress = endIpAddress
        //        }
        //    };
        //    FirewallRuleGetResponse firewallResult = sqlMgmtClient.FirewallRules.CreateOrUpdate(resourceGroupName, serverName, firewallRuleName, firewallParameters);
        //    return firewallResult;
        //}



        //public DatabaseCreateOrUpdateResponse CreateOrUpdateDatabase(SqlManagementClient sqlMgmtClient, string resourceGroupName, string serverName, string databaseName, string databaseEdition, string databasePerfLevel)
        //{
        //    // Retrieve the server that will host this database
        //    Server currentServer = sqlMgmtClient.Servers.Get(resourceGroupName, serverName).Server;

        //    // Create a database: configure create or update parameters and properties explicitly
        //    DatabaseCreateOrUpdateParameters newDatabaseParameters = new DatabaseCreateOrUpdateParameters()
        //    {
        //        Location = currentServer.Location,
        //        Properties = new DatabaseCreateOrUpdateProperties()
        //        {
        //            CreateMode = DatabaseCreateMode.Default,
        //            Edition = databaseEdition,
        //            RequestedServiceObjectiveName = databasePerfLevel
        //        }
        //    };
        //    DatabaseCreateOrUpdateResponse dbResponse = sqlMgmtClient.Databases.CreateOrUpdate(resourceGroupName, serverName, databaseName, newDatabaseParameters);
        //    return dbResponse;
        //}



        //public string GetToken()
        //{
        //    AuthenticationContext ac = new AuthenticationContext(OAuthTokenSettings.Settings.Authority);
        //    ClientCredential clientCred = new ClientCredential(OAuthTokenSettings.Settings.AppID,
        //        OAuthTokenSettings.Settings.AppPassword);
        //    var authenticationResult = ac.AcquireTokenAsync(
        //         OAuthTokenSettings.Settings.Resource, clientCred);
        //    authenticationResult.Wait();
        //    return authenticationResult.Result.AccessToken;
        //}
        //public DatabaseCreateOrUpdateResponse CreateOrUpdateDatabase(string _subscriptionId, string resourceGroupName, string databaseName)
        //{
        //    // Retrieve the server that will host this database

        //    SqlManagementClient sqlMgmtClient = new SqlManagementClient(new TokenCloudCredentials(_subscriptionId, GetToken()));
        //    Server currentServer = sqlMgmtClient.Servers.Get(resourceGroupName, DBTeraDActConfiguration.Settings.ServerName).Server;

        //    // Create a database: configure create or update parameters and properties explicitly
        //    DatabaseCreateOrUpdateParameters newDatabaseParameters = new DatabaseCreateOrUpdateParameters()
        //    {
        //        Location = currentServer.Location,
        //        Properties = new DatabaseCreateOrUpdateProperties()
        //        {
        //            CreateMode = DatabaseCreateMode.Default,
        //            //  Edition = databaseEdition,
        //            // RequestedServiceObjectiveName = databasePerfLevel
        //        }
        //    };
        //    DatabaseCreateOrUpdateResponse dbResponse = sqlMgmtClient.Databases.CreateOrUpdate(resourceGroupName, DBTeraDActConfiguration.Settings.ServerName, DBTeraDActConfiguration.Settings.DataBase, newDatabaseParameters);
        //    return dbResponse;
        //}

        //public HttpResponseMessage InvokeRestCallbyToken(string BearerTokenID, string RestURL)
        //{
        //    HttpClient client = new HttpClient();
        //    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get,
        //        RestURL);
        //    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", BearerTokenID);
        //    var response = client.SendAsync(request);
        //    response.Wait();
        //    return response.Result;

        //}

        //public void SaveRuleSetsToDB()
        //{
        //    string path = @"D:\RuleSet1.txt";
        //    string fileName = Path.GetFileName(path);
        //    string filetype = Path.GetExtension(path);
        //    byte[] contentType = FileService.GetBytes(path);
        //    string constr = "Data Source = sqldbteradactserver.database.windows.net; Initial Catalog = sqlteradactdb; User ID = sqladmin; Password = ptg@1234"; //ConfigurationManager.ConnectionStrings[DBTeraDActConfiguration.Settings.Connectionconstr].ConnectionString;
        //    using (SqlConnection con = new SqlConnection(constr))
        //    {
        //        string query = "insert into FileStorage(FileContent, FileType, FileDescription) values (@FileContent, @FileType, @FileDescription)";
        //        using (SqlCommand cmd = new SqlCommand(query))
        //        {
        //            cmd.Connection = con;
        //            cmd.Parameters.AddWithValue("@FileContent", contentType);
        //            cmd.Parameters.AddWithValue("@FileType", filetype);
        //            cmd.Parameters.AddWithValue("@FileDescription", "RuleSets");
        //            con.Open();
        //            cmd.ExecuteNonQuery();
        //            con.Close();
        //        }
        //    }
        //}
       
        public byte[] ReadRuleSetsFromDB()
        {

            return new EmailQueueRepository().ReadRuleSetsFromDB();



        }
      

    }
}
