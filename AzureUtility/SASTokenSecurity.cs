﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure;
using RampGroup.Config.Utility;
namespace AzureUtility
{
  public class SASTokenSecurity
    {
       public static string GetAccountSASToken()
        {

            string ConnectionString = BlobStorageAccount.Settings.BlobStorageConnectionString;         
            // To create the account SAS, you need to use your shared key credentials. Modify for your account.
          //  const string ConnectionString = "DefaultEndpointsProtocol=https;AccountName=teradact;AccountKey=cIzXgKJdvM+IBKfk6K3RrzRj2b0z5Uhq1Q4alpiAOLi++bqpEB9ud+3kvxSn1JCKj6iRkCJ8Qs9rQEc8vdBpfg==";
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConnectionString);

            //// Create a new access policy for the account.
            //SharedAccessAccountPolicy policy = new SharedAccessAccountPolicy()
            //{
            //    Permissions = SharedAccessAccountPermissions.Read | SharedAccessAccountPermissions.Write | SharedAccessAccountPermissions.List,
            //    Services = SharedAccessAccountServices.Blob | SharedAccessAccountServices.File,
            //    ResourceTypes = SharedAccessAccountResourceTypes.Service,
            //    SharedAccessExpiryTime = DateTime.UtcNow.AddHours(24),
            //    Protocols = SharedAccessProtocol.HttpsOnly
            //};


            //// Return the SAS token.
            //return storageAccount.GetSharedAccessSignature(policy);

            return "";
        }

        //This method that generates the shared access signature for 
        //the container and returns the signature URI.
       public static string GetContainerSasUri(CloudBlobContainer container)
        {
            //Set the expiry time and permissions for the container.
            //In this case no start time is specified, so the shared access signature becomes valid immediately.
            SharedAccessBlobPolicy sasConstraints = new SharedAccessBlobPolicy();
            sasConstraints.SharedAccessExpiryTime = DateTime.UtcNow.AddHours(24);
            sasConstraints.Permissions = SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.List;

            //Generate the shared access signature on the container, setting the constraints directly on the signature.
            string sasContainerToken = container.GetSharedAccessSignature(sasConstraints);

            //Return the URI string for the container, including the SAS token.
            return container.Uri + sasContainerToken;
        }

    }
}
