﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using RampGroup.Config.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureUtility
{
    public class StdQueue : BaseMessage
    {
        public static CloudQueue redactemailsqueue;
        public static CloudQueue sendmailqueue;

        protected CloudQueue queue;
        public  static CloudQueue createQueue(string QueueName)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(BlobStorageAccount.Settings.BlobStorageConnectionString);
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            CloudQueue queue = queueClient.GetQueueReference(QueueName);
            queue.CreateIfNotExists();
            return queue;
        }
        public StdQueue(CloudQueue queue)
        {
            this.queue = queue;
        }

        //public void AddMessage(T message)
        //{
        //    CloudQueueMessage msg =
        //    new CloudQueueMessage(message.ToBinary());
        //    queue.AddMessage(msg);
        //}
        public void AddMessage(string message)
        {
            CloudQueueMessage msg =
            new CloudQueueMessage(message);
            queue.AddMessage(msg);
        }
        public void AddMessage(byte[] message)
        {
            CloudQueueMessage msg =
            new CloudQueueMessage(message);
            queue.AddMessage(msg);
        }

        public void DeleteMessage(CloudQueueMessage msg)
        {
            queue.DeleteMessage(msg);
        }

        public CloudQueueMessage GetMessage()
        {
            return queue.GetMessage(TimeSpan.FromSeconds(120));
        }
    }
}
