﻿using BusinessObjects;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using RampGroup.Config.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlobAPI
{
    public static class BlobHelper
    {
        private static string BuildblobStorageConnectionString(BlobConfig _request)
        {
          // _request.DefaultEndpointsProtocol = BlobStorageAccount.BlobStorageAccount.DefaultEndpointsProtocol.ToString();


            return $"DefaultEndpointsProtocol={_request.DefaultEndpointsProtocol.ToLower()};AccountName={_request.AccountName.ToLower()};AccountKey={_request.AccountKey};EndpointSuffix={_request.Domain.ToLower()}";



        }
        public static CloudBlobContainer GetSourceBlobContainer(BlobConfig config)
        {
           
            var blobStorageAccount = CloudStorageAccount.Parse(BuildblobStorageConnectionString(config));
            var blobClient = blobStorageAccount.CreateCloudBlobClient();
            return blobClient.GetContainerReference(config.BlobStorageContainerName);
         
        }

        //public static CloudBlobContainer GetFilesfromSourceFolder(BlobRequest config)
        //{
           
        //    var blobStorageAccount = CloudStorageAccount.Parse(BuildblobStorageConnectionString(config.SourceBlob));
        //    var blobClient = blobStorageAccount.CreateCloudBlobClient();
        //    var dirEctory = blobClient.GetContainerReference(config.SourceBlob.BlobStorageContainerName);
        //    var dir = dirEctory.GetDirectoryReference(config.BlobStorageContainers[0].BlobDirectoryList[0].DirectoryName);
        //    var folders = dir.ListBlobs().ToList();
        //    foreach (var folder in folders)
        //    {
        //      var Containers=((Microsoft.WindowsAzure.Storage.Blob.CloudBlob)folder).Name.ToString();
        //    }
        //    //get(request.SourceBlob.BlobStorageContainerName + "/" + request.BlobStorageContainers[0].BlobDirectoryList[0].DirectoryName.ToString());
        //    return blobClient.GetContainerReference(config.SourceBlob.BlobStorageContainerName);

        //}

        public static CloudBlobContainer GetTargetBlobContainer(BlobConfig config)
        {
           // var blobStorageContainerName = BlobStorageAccount.Settings.BlobStorageContainerName;
            var blobStorageAccount = CloudStorageAccount.Parse(BuildblobStorageConnectionString(config));
            var blobClient = blobStorageAccount.CreateCloudBlobClient();
            return blobClient.GetContainerReference(config.BlobStorageContainerName);
        }
        //private static string BuildblobStorageConnectionString(BlobConfig _request) {
        //    return $"DefaultEndpointsProtocol={_request.DefaultEndpointsProtocol};AccountName={_request.AccountName};AccountKey={_request.AccountKey}";
        //}
        public static CloudBlobClient GetBlobClient(BlobConfig config)
        {
            var blobStorageAccount = CloudStorageAccount.Parse(BuildblobStorageConnectionString(config));
           return  blobStorageAccount.CreateCloudBlobClient();
        }

    }
}
