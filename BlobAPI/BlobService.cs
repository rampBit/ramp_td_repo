﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using System.IO;
using BusinessObjects;
using RampGroup.MX.Core.Authentication;
using RampGroup.Logging;
using TeraDActEventHandler;
using Microsoft.WindowsAzure.Storage.Blob;

using RampGroup.Config.Utility;
using System.Net.Mime;
using FileUtility;
using Microsoft.WindowsAzure.Storage.DataMovement;
using System.Collections;

namespace BlobAPI
{
    public sealed class BlobService : IBlobService
    {
        public LogHelper _logHelper = null;

        public BlobService(BlobRequest request)
        {
            _logHelper = new LogHelper();
            this.request = request;
        }
        private void setPermission(CloudBlobContainer container)
        {
            try
            {


                container.SetPermissionsAsync(new BlobContainerPermissions
                {
                    PublicAccess = BlobContainerPublicAccessType.Blob
                });
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public BlobRequest request { get; set; }

        public FileObject DownloadBlob(string blobName)
        {
            var blobContainer = BlobHelper.GetSourceBlobContainer(request.SourceBlob); ;
            var blob = blobContainer.GetBlockBlobReference(blobName);
            if (!String.IsNullOrEmpty(blobName))
            {
                var ms = new MemoryStream();
                blob.DownloadToStreamAsync(ms).Wait();

                var fileName = blob.Uri.Segments.Last();
                var download = new FileObject
                {
                    Content = FileService.GetBytesFromStream(ms),
                    FileName = fileName,

                    //  FileLength = blob.Properties.Length,
                    ContentType = blob.Properties.ContentType,
                    OperationStatus = OperationStatus.Succeded
                };

                return download;
            }

            return null;
        }

        public string UploadBlob(byte[] content, string FileName)
        {
            try
            {
                // TransferManager.Configurations.BlockSize = 4 * 1024 * 1024; //4MB

                var container = BlobHelper.GetSourceBlobContainer(request.SourceBlob);
                //Create a new container, if it does not exist
                if (!container.Exists())
                {
                    container.CreateAsync().Wait();
                    // 
                }

                CloudBlockBlob destinationBlob = container.GetBlockBlobReference(FileName);
                // UploadOptions options = new UploadOptions();

                //SingleTransferContext context = new SingleTransferContext();
                //context.SetAttributesCallback = (destination) =>
                //{
                //    CloudBlob destBlob = destination as CloudBlob;

                //};

                var c = destinationBlob.UploadFromByteArrayAsync(content, 0, content.Length);
                _logHelper.Info($"{(FileName)} successflly uploaded.");
                setPermission(container);
                return destinationBlob.Uri.ToString();

            }
            catch (Exception ex)
            {
                //_logHelper.Error(ex);

            }
            return null;
        }


        public IEnumerable<IListBlobItem> DownloadBlob(BlobStorageContainer oneContainer)
        {
            //BlobStorageAccount.Settings.BlobStorageContainerName = request.SourceBlob.BlobStorageContainerName.ToString();
            var container = BlobHelper.GetSourceBlobContainer(request.SourceBlob);
            bool IsDirectoryListing = false;
            if (container == null)
                return null;
            #region Directory listing
            List<IListBlobItem> directoryBlobList = new List<IListBlobItem>();
            if (oneContainer.BlobDirectoryList != null)
                foreach (BlobDirectory bDir in oneContainer.BlobDirectoryList)
                {
                    IsDirectoryListing = true;
                    CloudBlobDirectory dir = container.GetDirectoryReference(bDir.DirectoryName);
                    var blobs = dir.ListBlobs(useFlatBlobListing: false);
                    if (blobs.Count() > 0)
                        directoryBlobList.AddRange(blobs);
                    //  directoryBlobList.AddRange(container.GetDirectoryReference(bDir.DirectoryName).ListBlobs(useFlatBlobListing:false));
                    //directoryBlobList.AddRange(container.ListBlobs(useFlatBlobListing:false));
                }

            if (IsDirectoryListing) return directoryBlobList;
            #endregion
            return container.ListBlobs(useFlatBlobListing: false);
        }
        public void BlobToBlob(List<FileObject> redactedFiles, string ContainerName = "")
        {
            CloudBlobContainer sourceContainer = BlobHelper.GetSourceBlobContainer(request.SourceBlob);

            foreach (BlobConfig config in request.TargetBlobs)
            {
                try
                {
                    if (string.IsNullOrEmpty(config.BlobStorageContainerName) && config.AccountName != request.SourceBlob.AccountName)
                        config.BlobStorageContainerName = ContainerName;  //will create with same name

                    CloudBlobContainer targetContainer = BlobHelper.GetSourceBlobContainer(config);
                    targetContainer.CreateIfNotExists();
                    foreach (FileObject blob in redactedFiles)
                    {
                        var blobName = blob.FileName;
                        _logHelper.Info("Copying blob: " + blobName);
                        CloudBlockBlob sourceBlob = sourceContainer.GetBlockBlobReference(blobName);
                        CloudBlockBlob targetBlob = targetContainer.GetBlockBlobReference(blobName);
                        Task task = TransferManager.CopyAsync(sourceBlob, targetBlob, true /* isServiceCopy */);
                        task.Wait();
                        if (task.IsCompleted)
                            blob.OperationStatus = OperationStatus.Succeded;

                    }
                }
                catch (Exception ex)
                {
                    //_logHelper.Error(ex);

                }

            }
        }

        public IEnumerable<CloudBlobContainer> GetAllContainerList()
        {
            CloudBlobClient bClient = BlobHelper.GetBlobClient(request.SourceBlob);
            return bClient.ListContainers();
        }
    }

}
