﻿using BusinessObjects;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
namespace BlobAPI
{
    internal class BlobStorageUploadProvider : MultipartFileStreamProvider
    {
        public List<FileObject> Uploads { get; set; }
        private BlobRequest  _request { get; set; }

        public BlobStorageUploadProvider(BlobRequest request) : base(Path.GetTempPath())
        {
            Uploads = new List<FileObject>();
            _request = request;
        }
      
        //public override Task ExecutePostProcessingAsync()
        //{
        //    foreach (var fileData in FileData)
        //    {
        //        var fileName = Path.GetFileName(fileData.Headers.ContentDisposition.Name.Trim('"'));
        //        var blobContainer = BlobHelper.GetBlobContainer(_request);
        //        var blob = blobContainer.GetBlockBlobReference(fileName);

        //        blob.Properties.ContentType = fileData.Headers.ContentType.MediaType;

        //        using (var fs = File.OpenRead(fileData.LocalFileName))
        //        {
        //            blob.UploadFromStream(fs);
        //        }

        //        // Delete local file from disk
        //        File.Delete(fileData.LocalFileName);

        //        var fileUpload = new FileUpload
        //        {
        //            FileName = blob.Name,
        //            FileUrl = blob.Uri.AbsoluteUri,
        //            FileSizeInBytes = blob.Properties.Length
        //        };

        //        // Add uploaded blob to the list
        //        Uploads.Add(fileUpload);
        //    }

        //    return base.ExecutePostProcessingAsync();
        //}
    }
}
