﻿using BusinessObjects;
using Microsoft.WindowsAzure.Storage.Blob;
using RampGroup.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BlobAPI
{
    public interface IBlobService
    {
        string UploadBlob(byte[] content, string FileName);
        IEnumerable<IListBlobItem> DownloadBlob(BlobStorageContainer container);
        BlobRequest request { get; set; }
        void BlobToBlob(List<FileObject> redactedFiles, string ContainerName = "");
        IEnumerable<CloudBlobContainer> GetAllContainerList();
        FileObject DownloadBlob(string blobName);
    }
}
