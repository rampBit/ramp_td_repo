﻿using TeraDActEventHandler;
using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using FileUtility;
using System.Web;
using BlobAPI;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using Common.Repository.SQLAzureDBUtility;
using Microsoft.WindowsAzure.Storage;

namespace BlobBusinessLogic
{
    public class BlobLogic : IBlobLogic
    {


        public IBlobService _blobService
        {
            get;

        }

        public BlobRequest BlobRequest
        {
            get; set;
        }



        public BlobLogic(BlobRequest request) : base()
        {

            this.BlobRequest = request;
            _blobService = new BlobService(request);
        }


        public BlobResponse B2BUpload()
        {
            BlobResponse resp = new BlobResponse();
            var containerList = new List<BlobStorageContainer>();
            resp.BlobStorageContainers = containerList;
            foreach (var request in BlobRequest.BlobStorageContainers)
            {
                UploadBlobToBlob(request);
                containerList.Add(request);
                //_blobService.BlobToBlob(request.Blobs.Where(b => b.RedactionStatus == OperationStatus.Succeded).ToList(),
                //    request.ContainerName);
                //// _blobService.BlobToBlob(request.Blobs.ToList(),
                ////request.ContainerName);


            }
            return resp;


        }

        private void UploadBlobToBlob(BlobStorageContainer request)
        {
            EmailQueueRepository QueueRepo = new EmailQueueRepository();
            BlobQueue emqueue = new BlobQueue
            {
                RequestFilesCount = request.Blobs.Count(),
                Status = RequestStatus.Requested,

            };
            emqueue.BlobQueueID = QueueRepo.saveBlobQueue(emqueue, 'I');

            BlobRequest.SourceBlob.BlobStorageContainerName = request.ContainerName.ToLower();
            foreach (FileObject file in request.Blobs)
            {
                var tmpFile = _blobService.DownloadBlob(file.FileName);
                file.Content = tmpFile.Content;
            }
            var orgSoruceBlobConfig = new BlobConfig
            {
                AccountKey = BlobRequest.SourceBlob.AccountKey,
                AccountName = BlobRequest.SourceBlob.AccountName,
                Domain = BlobRequest.SourceBlob.Domain,
                DefaultEndpointsProtocol = BlobRequest.SourceBlob.DefaultEndpointsProtocol,
                BlobStorageContainerName = BlobRequest.SourceBlob.BlobStorageContainerName
            };
            foreach (FileObject file in request.Blobs)
            {
                foreach (BlobConfig target in BlobRequest.TargetBlobs)
                {
                    BlobRequest.SourceBlob.AccountKey = target.AccountKey;  // for diff storage acct in target..
                    BlobRequest.SourceBlob.AccountName = target.AccountName;
                    BlobRequest.SourceBlob.DefaultEndpointsProtocol = target.DefaultEndpointsProtocol;
                    BlobRequest.SourceBlob.Domain = target.Domain;
                    BlobRequest.SourceBlob.BlobStorageContainerName =
                       (string.IsNullOrEmpty(target.BlobStorageContainerName) ? request.ContainerName : target.BlobStorageContainerName).ToLower();
                    this.Redact(file);
                    if (file.RedactionStatus == OperationStatus.Failed)
                    {
                        emqueue.Status = RequestStatus.RedactionFailed; emqueue.FilesProcessedCount = 0;

                        QueueRepo.saveBlobQueue(emqueue, 'U');
                        //continue;
                    }

                    var res = _blobService.UploadBlob(file.Content, file.FileName);

                    if (res != null)
                        file.OperationStatus = OperationStatus.Succeded;


                }
            }
            BlobRequest.SourceBlob = orgSoruceBlobConfig;

            emqueue.Status = RequestStatus.BlobUploaded; emqueue.FilesProcessedCount =
                request.Blobs.Count(c => c.RedactionStatus == OperationStatus.Succeded);
            QueueRepo.saveBlobQueue(emqueue, 'U');
        }

        public BlobResponse DownloadBlob()
        {
            BlobResponse resp = new BlobResponse();
            var containerList = new List<BlobStorageContainer>();
            resp.BlobStorageContainers = containerList;
            foreach (BlobStorageContainer request in BlobRequest.BlobStorageContainers)
            {
                // var tmpcontainer = new BlobStorageContainer { ContainerName = request.ContainerName };
                containerList.Add(request);
                BlobRequest.SourceBlob.BlobStorageContainerName = request.ContainerName;
                var download = new List<FileObject>();
                var list = _blobService.DownloadBlob(request);

                List<FileObject> allBlobs = list.OfType<CloudBlockBlob>().Select(c => new FileObject { FileName = c.Name , FilePath= request.ContainerName }).ToList();
                var allPageBlobs = list.OfType<CloudPageBlob>().ToList().Select(c => new FileObject { FileName = c.Name, FilePath = request.ContainerName }).ToList();
                allBlobs.AddRange(allPageBlobs);
                //Random rand = new Random(100);

                var allBlobDirectoryList = list.OfType<CloudBlobDirectory>().Select(c => new BlobDirectory
                {
                    DirectoryName = c.Prefix,
                    ParentID = c.Parent.Uri.AbsolutePath,
                    DirID = c.Prefix + "/" + Guid.NewGuid().ToString("N")
            }).ToList();


                //foreach (CloudBlockBlob blob in allBlobs)
                //{
                //    download.Add(DownloadBlob(blob));
                //}

                if (request.BlobDirectoryList == null || request.BlobDirectoryList.Count == 0)
                {
                    request.Blobs = allBlobs;
                    request.BlobDirectoryList = allBlobDirectoryList;
                }
                else
                {
                    request.BlobDirectoryList.First().Blobs = allBlobs;
                    request.BlobDirectoryList.First().BlobDirectoryList = allBlobDirectoryList;
                }

            }
            // resp.Blobs = download;
            return resp; ;

        }

        public BlobResponse DownloadDirBlobs()
        {
            return DownloadBlob();
            //BlobResponse resp = new BlobResponse();
            //var containerList = new List<BlobStorageContainer>();
            //resp.BlobStorageContainers = containerList;
            //foreach (BlobStorageContainer request in BlobRequest.BlobStorageContainers)
            //{
            //    // var tmpcontainer = new BlobStorageContainer { ContainerName = request.ContainerName };
            //    containerList.Add(request);
            //    BlobRequest.SourceBlob.BlobStorageContainerName = request.ContainerName;
            //    var download = new List<FileObject>();
            //    var list = _blobService.DownloadBlob(request);

            //    List<FileObject> allBlobs = list.OfType<CloudBlockBlob>().Select(c => new FileObject { FileName = c.Name }).ToList();
            //    var allPageBlobs = list.OfType<CloudPageBlob>().ToList().Select(c => new FileObject { FileName = c.Name }).ToList();
            //    allBlobs.AddRange(allPageBlobs);
            //    Random rand = new Random(100);
                
            //    var allBlobDirectoryList = list.OfType<CloudBlobDirectory>().Select(c => new BlobDirectory {
            //        DirectoryName = c.Prefix,
            //        ParentID = c.Parent.Uri.AbsoluteUri,
            //        DirID=c.Prefix+"/"+(rand.Next(000000000, 999999999)).ToString()   
            //    }).ToList();
               
                

            ////foreach (CloudBlockBlob blob in allBlobs)
            ////{
            ////    download.Add(DownloadBlob(blob));
            ////}

            //if (request.BlobDirectoryList == null || request.BlobDirectoryList.Count == 0)
            //    {
            //        request.Blobs = allBlobs;
            //        request.BlobDirectoryList = allBlobDirectoryList;
            //    }
            //    else
            //    {
            //        request.BlobDirectoryList.First().Blobs = allBlobs;
            //        request.BlobDirectoryList.First().BlobDirectoryList = allBlobDirectoryList;
                   
            //    }
                
            //}
           
            //return resp;
        }
        private void Redact(FileObject file)
        {
            string nomatchesFound = "no matches found";
            string noredactionrequired = "No Redaction Required";
            TeraDActService.Logic.ITeraDActRest teradobj = new TeraDActService.Logic.TeraDActRest();
            var respemil = teradobj.RedactFilestoBlob(file);
            bool respStatus = true;
            if (respemil == null || respemil.FileStream == null || respemil.Content.Length==0) respStatus = false;
            if (respStatus)
            {
                string NonRedactedContent = respemil.Content==null? noredactionrequired:FileService.getStringFromBytes(respemil.Content);
                if (string.IsNullOrEmpty(NonRedactedContent) || NonRedactedContent.Contains(nomatchesFound) 
                    || NonRedactedContent.Contains(noredactionrequired)) respStatus = false;
            }
            if (respStatus)
            {
                file.RedactionStatus = OperationStatus.Succeded;
                file.Content = respemil.Content;
            }
        }

      

        public string UploadXmlBlob(string filename, byte[] content)
        {

            var res = _blobService.UploadBlob(content, filename);
            return string.Empty;
        }
        private void UploadBlob(BlobStorageContainer request, bool Isb2bRequest = false)
        {
            EmailQueueRepository QueueRepo = new EmailQueueRepository();
            BlobQueue emqueue = new BlobQueue
            {

                RequestFilesCount = request.Blobs.Count(),
                Status = RequestStatus.Requested,

            };
            emqueue.BlobQueueID = QueueRepo.saveBlobQueue(emqueue, 'I');

            BlobRequest.SourceBlob.BlobStorageContainerName = request.ContainerName.ToLower();



            foreach (FileObject file in request.Blobs)
            {

                this.Redact(file);
                if (file.RedactionStatus == OperationStatus.Failed)
                {
                    emqueue.Status = RequestStatus.RedactionFailed; emqueue.FilesProcessedCount = 0;

                    QueueRepo.saveBlobQueue(emqueue, 'U');
                    //continue;
                }

                var res = _blobService.UploadBlob(file.Content, file.FileName);

                if (res != null)
                    file.OperationStatus = OperationStatus.Succeded;
            }
            emqueue.Status = RequestStatus.BlobUploaded; emqueue.FilesProcessedCount = request.Blobs.Count(c => c.RedactionStatus == OperationStatus.Succeded);
            QueueRepo.saveBlobQueue(emqueue, 'U');
        }
        public BlobResponse UploadBlobs()//local to blob
        {
            BlobResponse resp = new BlobResponse();
            var containerList = new List<BlobStorageContainer>();
            resp.BlobStorageContainers = containerList;
            foreach (var request in BlobRequest.BlobStorageContainers)
            {
                UploadBlob(request);
                containerList.Add(request);
            }
            return resp;
        }



        public BlobResponse GetAllContainerList()
        {
            BlobResponse resp = new BlobResponse();
            resp.BlobStorageContainers = new List<BlobStorageContainer>();
            var cList = _blobService.GetAllContainerList().Select(c =>
                new BlobStorageContainer { ContainerName = c.Name }
                );
            resp.BlobStorageContainers.AddRange(cList);
            return resp;
        }
    }
}
