﻿using BlobAPI;
using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlobBusinessLogic
{
  public  interface IBlobLogic
    {
        IBlobService _blobService { get;  }
        BlobRequest BlobRequest { get; set; }
        BlobResponse DownloadBlob();
        BlobResponse DownloadDirBlobs();
        BlobResponse UploadBlobs();
        BlobResponse GetAllContainerList();
        BlobResponse B2BUpload();
    }
}
