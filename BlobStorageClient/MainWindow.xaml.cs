﻿using System.Windows;
using BlobStorageClient.ViewModelUtilities;

namespace BlobStorageClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainViewmodel VM = new MainViewmodel();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = VM;
        }
    }
}
