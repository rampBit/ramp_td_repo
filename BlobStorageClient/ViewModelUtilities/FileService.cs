﻿
using Microsoft.Win32;
using RampGroup.Config.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TeraDActService.Logic;
using System.Windows;
using System.Web;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.Windows.Forms;
using CustomEventHandler;
using TeraDActEventHandler;
using BusinessObjects;
using BlobBusinessLogic;

namespace BlobStorageClient.ViewModelUtilities
{
    public class FileService
    {


        private ITeraDActRest teradobj;
        private IBlobLogic blobService;
        #region propeties
        public List<string> Files { get; set; }

        #endregion
        public FileService()
        {
            this.Files = new List<string>();
            teradobj = new TeraDActRest();

        }

        public Stream OpenFile(string defaultExtension, string filter)
        {

            //===========FOR Selecting Files=============
            var openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.DefaultExt = defaultExtension;
            openFileDialog.Filter = filter;

            openFileDialog.Multiselect = true;//For multiple files selections
            bool? result = openFileDialog.ShowDialog();

            foreach (string filename in openFileDialog.FileNames)
            {
                Files.Add(filename);
            }
            return result.Value ? openFileDialog.OpenFile() : null;
        }

        public Stream UploadFile(string defaultExtension, string filter)
        {
            Microsoft.Win32.SaveFileDialog fd = new Microsoft.Win32.SaveFileDialog();
            fd.DefaultExt = defaultExtension;
            fd.Filter = filter;

            bool? result = fd.ShowDialog();

            return result.Value ? fd.OpenFile() : null;

        }

      
    }
}
