﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BlobStorageClient.ViewModelUtilities
{
    public class MainViewmodel : ViewModelBase
    {
        private string text;
        private List<string> files;
        public MainViewmodel()
        {
            UploadCommand = new RelayCommand(UploadFile);
            OpenFileCommand = new RelayCommand(OpenFile);
            //OpenFolderCommand = new RelayCommand(OpenFolder);
            this.Files = new List<string>();
        }

        #region Properties
        public List<string> Files { get { return files; } set { files = value;this.RaisePropertyChanged("Files"); } }
        public string Text
        {
            get
            {
                return this.text;
            }

            set
            {
                this.text = value;
                this.RaisePropertyChanged("Text");
            }
        }

        public ICommand OpenFileCommand
        {
            get;
            set;
        }

        public ICommand OpenFolderCommand
        {
            get;
            set;
        }

        public ICommand UploadCommand
        {
            get;
            set;
        }

        #endregion

        private void OpenFile()
        {
            FileDialogViewModel fdvm = new FileDialogViewModel();
            //fdvm.Extension = "*.txt";
            //fdvm.Filter = "Text documents (.txt)|*.txt";
            fdvm.OpenFileCommand.Execute(null);

            //====Code for streaming the files===========
            if (fdvm.Stream == null)
                return;
            using (StreamReader sr = new StreamReader(fdvm.Stream, Encoding.ASCII))
            {
                Text = sr.ReadToEnd();
            }
            //=======End=======
            this.Files = fdvm.Files;
        }

        private void OpenFolder()
        {
            FileDialogViewModel fdvm = new FileDialogViewModel();
            fdvm.OpenFolderCommand.Execute(null);

            if (fdvm.Stream == null)
                return;
            using (StreamReader sr = new StreamReader(fdvm.Stream, Encoding.ASCII))
            {
                Text = sr.ReadToEnd();
            }
            this.Files = fdvm.Files;
        }

        private void UploadFile()
        {
            FileDialogViewModel fdvm = new FileDialogViewModel();
            fdvm.Files = this.files;
            //fdvm.Extension = "*.txt";
            //fdvm.Filter = "Text documents (.txt)|*.txt";

            fdvm.UploadCommand.Execute(null);

            //====Code for streaming the files============
            if (fdvm.Stream == null)
                return;

            using (StreamWriter sw = new StreamWriter(fdvm.Stream, Encoding.ASCII))
            {
                sw.Write(this.Text.ToString(CultureInfo.InvariantCulture));
            }
            //=======End=======
        }
    }
}
