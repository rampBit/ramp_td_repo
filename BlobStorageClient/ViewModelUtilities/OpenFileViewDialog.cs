﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BlobStorageClient.ViewModelUtilities
{
    public class FileDialogViewModel : ViewModelBase
    {
        public FileDialogViewModel()
        {
            UploadCommand = new RelayCommand(UploadFile);
            OpenFileCommand = new RelayCommand(OpenFile);
            //OpenFolderCommand = new RelayCommand(OpenFolder);

            Files = new List<string>();
        }

        #region Properties
        public List<string> Files
        {
            get;
            set;
        }

        public Stream Stream
        {
            get;
            set;
        }

        public string Extension
        {
            get;
            set;
        }

        public string Filter
        {
            get;
            set;
        }
        public ICommand OpenFileCommand
        {
            get;
            set;
        }

        public ICommand OpenFolderCommand
        {
            get;
            set;
        }

        public ICommand UploadCommand
        {
            get;
            set;
        }

        #endregion

        private void OpenFile()
        {
            FileService fileServices = new FileService();
            Stream = fileServices.OpenFile(Extension, Filter);
            Files = fileServices.Files;
        }

        //private void OpenFolder()
        //{
        //    FileService fileServices = new FileService();
        //    Stream = fileServices.OpenFolder(Extension, Filter);
        //    Files = fileServices.Files;
        //}

        private void UploadFile()
        {
            FileService fileServices = new FileService();
            //Stream = fileServices.UploadFile(Extension, Filter);
            fileServices.Files = this.Files;
          //  fileServices.UploadtoBlob();
            Files = fileServices.Files;
        }
    }
}
