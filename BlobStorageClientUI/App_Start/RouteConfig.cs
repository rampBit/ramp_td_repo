﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BlobStorageClientUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "index",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "BlobUpload", action = "BlobUploadView", id = UrlParameter.Optional }
            );
            // routes.MapRoute(
            //    name: "BlobUpload",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "BlobUpload", action = "BlobUploadView", id = UrlParameter.Optional }
            //);
            // routes.MapRoute(
            //    name: "Rulesets",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "AdminSubs", action = "GetRulesetNames", id = UrlParameter.Optional }
            //);

            //      routes.MapRoute(
            //    name: "Rolesets",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "AdminSubs", action = "GetRRoles", id = UrlParameter.Optional }
            //);


        }
    }
}
