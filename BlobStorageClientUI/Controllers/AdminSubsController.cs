﻿using AzureUtility;
using BlobStorageClientGUI.Models;
using Microsoft.Azure.Management.Sql.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RampGroup.Config;
using RampGroup.Config.Utility;
using BusinessObjects;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml.Serialization;

using System.Web.Mvc.Filters;
using System.Xml;
using System.IO;
using FileUtility;
using System.Xml.Linq;

namespace BlobStorageClientGUI.Controllers
{
    public class AdminSubsController : Controller
    {
        // GET: AdminSubs


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SubmitMyBlob(BlobStorageAccount blobStorageSettings)
        {
            //var acoountkey = BlobStorageAccount.Settings.AccountKey;
            //var acoountkeyName = BlobStorageAccount.Settings.AccountName;
            var BaseSettingValue = BlobStorageAccount.Settings;
            new BlobStorageAccount(blobStorageSettings.AccountKey, blobStorageSettings.AccountName, blobStorageSettings.BlobStorageContainerName);
            var blobStorageSettingsstr = BlobStorageAccount.Settings;
            string strView = CreateXMLBlobStorage(blobStorageSettingsstr);
            return View();

        }
        public string CreateXMLBlobStorage(BlobStorageAccount YourClassObject)
        {
            XmlDocument xmlDoc = new XmlDocument();   //Represents an XML document, 
                                                      // Initializes a new instance of the XmlDocument class.          
            XmlSerializer xmlSerializer = new XmlSerializer(YourClassObject.GetType());
            // Creates a stream whose backing store is memory. 
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, YourClassObject);
                xmlStream.Position = 0;
                //Read the existing XML


                //Loads the XML document from the specified string.
                xmlDoc.Load(xmlStream);
                xmlDoc.Save(FileService.GetXmlPathForBlobStorage());
                return xmlDoc.InnerXml;
            }

        }
        public ActionResult SubmitMyExchange(ExchangeServerSettings adminSubscription)
        {
            // var EmailID = ExchangeServerSettings.Settings.EmailID;
            var BaseSettingValue = ExchangeServerSettings.Settings;
            new ExchangeServerSettings(adminSubscription.EmailID, adminSubscription.Password, adminSubscription.Domain, BaseSettingValue.ExchangeVersion, BaseSettingValue.IsApprovalEmail, BaseSettingValue.EWSUrl);
            var a = ExchangeServerSettings.Settings;
            string strView = CreateXML(a);
            //return Json(new { exchangeServerSettings = _baseSettings.getExchangeSettingss(adminSubscription) }, JsonRequestBehavior.AllowGet);
            return View(a);
        }
        public string CreateXML(ExchangeServerSettings YourClassObject)
        {
            XmlDocument xmlDoc = new XmlDocument();   //Represents an XML document, 
                                                      // Initializes a new instance of the XmlDocument class.          
            XmlSerializer xmlSerializer = new XmlSerializer(YourClassObject.GetType());
            // Creates a stream whose backing store is memory. 
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, YourClassObject);

                xmlStream.Position = 0;

                xmlDoc.Load(xmlStream);

                xmlDoc.Save(FileService.GetXmlPath());
                return xmlDoc.InnerXml;
            }

        }
        public ActionResult CheckConnectionString(AdminLogin adminSubscription)
        {
            string ConnectionString = "Data Source={0};Initial Catalog={1};User id={2};Password={3};";
            bool returnVal = true;
            SqlConnection con = new SqlConnection(string.Format(ConnectionString, adminSubscription.ServerName, adminSubscription.DataBaseName, adminSubscription.LoginID, adminSubscription.Password));
            try
            {
                con.Open();

                if (con.State != ConnectionState.Open)
                    returnVal = false;
                else
                    returnVal = true;

            }
            catch (Exception ex)
            {
                returnVal = false;
            }
            return Json(new { Connected = returnVal }, JsonRequestBehavior.AllowGet);
        }
        //public void ReadRuleSetsFromDB(int FileID)
        //{
        //    string constr = ConfigurationManager.ConnectionStrings[DBTeraDActConfiguration.Settings.Connectionconstr].ConnectionString;
        //    using (SqlConnection con = new SqlConnection(constr))
        //    {
        //        con.Open();
        //        string selectQuery = string.Format(@"Select FileContent From FileStorage Where FileID={0}", FileID);
        //        // Read File content from Sql Table 
        //        SqlCommand selectCommand = new SqlCommand(selectQuery, con);
        //        SqlDataReader reader = selectCommand.ExecuteReader();
        //        if (reader.Read())
        //        {
        //            byte[] fileData = (byte[])reader[0];
        //            // Write/Export File content into new text file
        //            File.WriteAllBytes(@"D:\Retrive_RuleSet.txt", fileData);
        //        }
        //    }
        //}
        public List<string> GetRulesetNames()
        {
            List<string> rules = new List<string>();

            string ConnectionString =
                //"devtdc.database.usgovcloudapi.net;Initial Catalog = teradact;Persist Security Info = False;User ID = devtdc@devtdc;Password =TD40iiap;MultipleActiveResultSets = False;Encrypt = True;TrustServerCertificate = False;Connection Timeout = 30";
                "Data Source = devtdc.database.usgovcloudapi.net; Initial Catalog = teradact; User ID = devtdc@devtdc; Password = TD40iiap";
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                string strquery = "select name from [dbo].[ruleset]";
                using (SqlCommand cmd = new SqlCommand(strquery, conn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        rules.Add(reader["name"].ToString());
                    }
                }
            }
            return rules;
        }
        public List<string> GetRRoles()
        {
            List<string> rules = new List<string>();

            string ConnectionString =
                //"devtdc.database.usgovcloudapi.net;Initial Catalog = teradact;Persist Security Info = False;User ID = devtdc@devtdc;Password =TD40iiap;MultipleActiveResultSets = False;Encrypt = True;TrustServerCertificate = False;Connection Timeout = 30";
                "Data Source = devtdc.database.usgovcloudapi.net; Initial Catalog = teradact; User ID = devtdc@devtdc; Password = TD40iiap";
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                string strquery = "SELECT name FROM [dbo].[role]";
                using (SqlCommand cmd = new SqlCommand(strquery, conn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        rules.Add(reader["name"].ToString());
                    }
                }
            }
            return rules;
        }


    }
}
