﻿using BusinessObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using TeraDActService.Logic;
using BlobBusinessLogic;
using CustomEventHandler;
using TeraDActEventHandler;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using BlobStorageClientUI.Models;
using FileUtility;
using RampGroup.Config.Utility;
using Common.Repository.SQLAzureDBUtility;
using System.Data;
using static Common.Repository.SQLAzureDBUtility.AdminRepository;

namespace BlobStorageClientUI.Controllers
{
    public class BlobUploadController : Controller
    {
        public string eXception = null;
        private static List<FileObject> downLoadedFiles = null;
        private IBlobLogic blobService;
        public List<string> Files { get; set; }
        // GET: BlobUpload
        [HttpGet]
        public ActionResult BlobUploadView()
        {

            return View("BlobUploadView");
        }
        //[HttpPost]
        //public ActionResult Setup()
        //{
        //    var resp = new BlobConfig
        //    {
        //        AccountKey = BlobStorageAccount.Settings.AccountKey,
        //        AccountName = BlobStorageAccount.Settings.AccountName,
        //        DefaultEndpointsProtocol = BlobStorageAccount.Settings.DefaultEndpointsProtocol
        //    };

        //    return Json(resp, JsonRequestBehavior.AllowGet);
        //}



        [HttpPost]
        public JsonResult GotoTarget(string Name = null, string key = null, string blobcontainer = null)
        {
            List<BlobRequest> blobrequest = new List<BlobRequest>();
            if (Session["BLBReq"] != null)
            {
                blobrequest = (List<BlobRequest>)Session["BLBReq"];
            }
            BlobRequest blobreq = new BlobRequest();
            blobreq.SourceBlob = new BlobConfig();
            blobreq.SourceBlob.AccountName = Name;
            blobreq.SourceBlob.AccountKey = key;
            blobreq.SourceBlob.BlobStorageContainerName = blobcontainer;
            blobrequest.Add(blobreq);

            Session["BLBReq"] = blobrequest;
            return Json(blobrequest, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UploadFilesWithRedaction()
        {

            try
            {
                var selectedFiles = ProcessUplodedFiles(Request.Files);
                BlobRequest request = new BlobRequest();
                var ContainerName = string.IsNullOrEmpty(Request.Form["BlobStorageContainerName"]) || Request.Form["BlobStorageContainerName"] == "undefined"
                    ? BlobStorageAccount.Settings.BlobStorageContainerName : Request.Form["BlobStorageContainerName"];
                var tmpc = new BlobStorageContainer
                {
                    ContainerName = ContainerName
                };
                request.BlobStorageContainers = new List<BlobStorageContainer>();
                request.BlobStorageContainers.Add(tmpc);

                request.SourceBlob = new BlobConfig
                {
                    AccountKey = string.IsNullOrEmpty(Request.Form["AccountKey"]) || Request.Form["AccountKey"] == "undefined"
                    ? BlobStorageAccount.Settings.AccountKey : Request.Form["AccountKey"],
                    AccountName = string.IsNullOrEmpty(Request.Form["AccountName"]) || Request.Form["AccountName"] == "undefined"
                    ? BlobStorageAccount.Settings.AccountKey : Request.Form["AccountName"],
                    BlobStorageContainerName = ContainerName,
                    DefaultEndpointsProtocol = Request.Form["DefaultEndpointsProtocol"],
                    Domain = Request.Form["Domain"]
                    //lstContainerinfo.Where(c => c.BlobStorageAccount.BlobType.Equals(BlobSettingType.Source.ToString())),

                    //BlobType.Equals(BlobSettingType.Source.ToString()))

                };
                var blobService = new BlobLogic(request);
                tmpc.Blobs = selectedFiles;
                var resp = blobService.UploadBlobs();
                return Json(resp, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                eXception = ex.Message;
            }
            return Json(eXception, JsonRequestBehavior.AllowGet);
        }
        public List<FileObject> ProcessUplodedFiles(HttpFileCollectionBase requestedFiles)
        {
            List<FileObject> selectedFiles = new List<FileObject>();
            HttpFileCollectionBase files = requestedFiles;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFileBase file = files[i];

                selectedFiles.Add(new FileObject { FileName = FileService.GetFileName(file.FileName), Content = FileService.GetBytesFromStream(file.InputStream) });
            }
            return selectedFiles;
        }


        [HttpPost]
        public JsonResult BlobToBlobTransfer(BlobRequest request)
        {
            BlobRequest newreq = new BlobRequest();
            newreq.SourceBlob = request.SourceBlob;
            newreq.TargetBlobs = request.TargetBlobs;
            foreach (var container in request.BlobStorageContainers)
            {
                if (newreq.BlobStorageContainers == null)
                {
                    newreq.BlobStorageContainers = new List<BlobStorageContainer>();
                    newreq.BlobStorageContainers.Add(container);
                }
                else
                {
                    var tmpcontainer = newreq.BlobStorageContainers.FirstOrDefault(c => c.ContainerName == container.ContainerName);
                    if (tmpcontainer != null)
                        tmpcontainer.Blobs.ToList().AddRange(container.Blobs);
                    else
                        newreq.BlobStorageContainers.Add(container);
                }
            }

            var blobService = new BlobLogic(newreq);
            var resp = blobService.B2BUpload();
            return Json(resp, JsonRequestBehavior.AllowGet);
        }
        private BlobResponse DownloadBlob(BlobRequest SourceToTargetTransfer)
        {


            blobService = new BlobLogic(SourceToTargetTransfer);
            return blobService.DownloadBlob();


        }
        private BlobResponse DownloadDirBlob(BlobRequest SourceToTargetTransfer)
        {


            blobService = new BlobLogic(SourceToTargetTransfer);
            return blobService.DownloadDirBlobs();


        }

        [HttpPost]
        public JsonResult GetDirectoryBlobItems(BlobRequest SourceToTargetTransfer)
        {
            //SourceToTargetTransfer.SourceBlob.DefaultEndpointsProtocol = SourceToTargetTransfer.SourceBlob.DefaultEndpointsProtocol;// BlobStorageAccount.BlobStorageAccount.DefaultEndpointsProtocol.ToString();
            var resp = DownloadDirBlob(SourceToTargetTransfer);

            //var fileNames = downLoadedFiles.Select(fn => fn.FileName).ToList();
            ////tmpFiles.ForEach(x => x.FileStream = null);
            return Json(resp.BlobStorageContainers, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetSourceBlobItems(BlobRequest SourceToTargetTransfer)
        {
            //SourceToTargetTransfer.SourceBlob.DefaultEndpointsProtocol = SourceToTargetTransfer.SourceBlob.DefaultEndpointsProtocol;// BlobStorageAccount.BlobStorageAccount.DefaultEndpointsProtocol.ToString();
            var resp = DownloadBlob(SourceToTargetTransfer);

            return Json(resp.BlobStorageContainers, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetSourceContainerList(BlobRequest request)
        {

            var blobService = new BlobLogic(request);
            request.SourceBlob.DefaultEndpointsProtocol = request.SourceBlob.DefaultEndpointsProtocol;//"https"
            request.TargetBlobs.Select(c => c.DefaultEndpointsProtocol = request.SourceBlob.DefaultEndpointsProtocol);
            var resp = blobService.GetAllContainerList();
            var data = resp.BlobStorageContainers.Select(c => c.ContainerName).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //public virtual string SqlAzureConnectionString()
        //{
        //    return ConfigurationManager.AppSettings["DBTeraDActConfiguration"];
        //}
        [HttpPost]
        public ActionResult GetSourceBlobSettingsDetails()
        {

            try
            {
                //  select new BlobSettingDetails
                List<BlobSettingDetails> blobsettings = new List<BlobSettingDetails>();
                AdminRepository _repo = new AdminRepository();
                var details = _repo.GetSourceTargetBlobSettingsDetails();
                if (details != null)
                    return Json(new
                    {
                        Source = details.Where(c => c.BlobType.Equals(BlobSettingType.Source.ToString())),
                        Target = details.Where(c => c.BlobType.Equals(BlobSettingType.Target.ToString()))
                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                eXception = ex.Message;
            }
            return Json(eXception, JsonRequestBehavior.AllowGet);
            #region commented

            //{
            //    BlobStorageAccount=BlobStorageAccount

            //};//_repo.GetSourceTargetBlobSettingsDetails();
            //blobsettings.Add(details);

            //Source = details.Where(c => c.BlobStorageAccount.BlobType.Equals(BlobSettingType.Source.ToString())),
            //Target = details.Where(c => c.BlobStorageAccount.BlobType.Equals(BlobSettingType.Target.ToString()))
            #endregion
        }

        enum BlobSettingType
        {
            Source,
            Target
        }
    }

}