﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlobStorageClientGUI.Models
{
    public class ExchangeServerSettingsModel
    {

        public string EmailID { get; set; }
        public string Password { get; set; }
        public string Domain { get; set; }
        public string ExchangeVersion { get; set; }
        public string IsApprovalEmail { get; set; }
        public int MailCount { get; set; }
        public int FromMinutes { get; set; }
        public string EWSUrl { get; internal set; }


    }
}