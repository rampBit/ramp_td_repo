﻿var app = angular.module('app', ['ngRoute', 'toastr', 'ui.bootstrap', 'ngDialog']);
app.constant('BaseUrl', function () {
    var url = Resources.BaseUrl;
    return {
        url: url
    };
});
app.config(['$routeProvider',  function ($routeProvider) {
    $routeProvider
       .when('/LocalToBlob', {
           templateUrl: '/Templates/LocalToBLobNew.html',
        controller: 'BlobOperationsController',
        title: 'Teradact',
       })
       .when('/BlobToBlob', {
           templateUrl: '/Templates/BlobtoBlobNew.html',
           controller: 'BlobOperationsController',
           title: 'Teradact',
       })
    .otherwise({
        templateUrl: '/Templates/LocalToBLobNew.html',
        controller: 'BlobOperationsController',
        title: 'Teradact',
    });
    
}]);

app.directive('folders', function ($rootScope, $window) {

    return {

        restrict: 'EA',
        controller: 'BlobOperationsController',
        template: '<div ng-repeat="Infolder in folders.BlobDirectoryList" style="margin: 5px 0;"> <span class="glyphicon glyphicon-chevron-right"></span><img src="Images/Folder3.JPG" style="width: 20px; margin-left: 6px;" /><span style="cursor: pointer;font-weight: bolder;" ng-click="isCollapsed=!isCollapsed;GetFolderFiles(Infolder,blobvalue,containerobj)"><span style="font-weight: bolder" ng-bind="Infolder.DirectoryNames"></span></span><div style="margin-left:30px" ng-if="Infolder.BlobDirectoryList != null || Infolder.Blobs != null" folders="Infolder" blobvalue="blobvalue" containerobj="containerobj"></div></div><div ng-repeat="file in folders.Blobs" style="margin: 5px 0;"> <input type="checkbox" ng-change="FolderFilesSelected(file)" ng-model="file.Selected" />&nbsp;<span ng-bind="file.FileNames"></span></div>',
        scope: {
            folders: '=',
            blobvalue: '=',
            containerobj: '='
        },
        link: function (scope, element, attr) {
            //scope.GetFolderFiles(DirectoryName, blobvalue, containerobj);
            //scope.GetFolderFiles = function (DirectoryName, blobrequest, container) {
            //    debugger;
            //};
            
            scope.FolderFilesSelected=function(file)
            {             
                if (file.Selected == true) {
                    //var demo = _.filter($rootScope.FileNames, { FileName: angular.lowercase(file.FileName) });
                   // if (demo.length == 0) {
                    //$rootScope.FileNames.push(file);
                   // $rootScope.$root.ArrayA.push(file);
                    //$window.TempArray.push(file);
                    SelectedFileNames.push(file);
                        //console.log(file.FileName);
                    //}
                }
                //if (file.Selected == false)
                //{
                //    scope.FileNames = scope.FileNames.filter(function (obj) {
                //        return obj.FileName !== file.FileName;
                //    });
                //}
            }
            console.log(scope.folders);
        }
    };

});