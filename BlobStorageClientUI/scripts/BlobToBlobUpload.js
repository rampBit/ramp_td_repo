﻿function BlobToBlobUpload() {
  
    var DefaultEndpointsProtocol = "https"
    var AccountName = $('#source_accountname').val()
    var AccountKey = $('#source_accountkey').val()
    var BlobStorageContainerName = $('#source_contatiner').val();
    var SourceBlob ={}
    if (AccountName && AccountKey && BlobStorageContainerName) {
         SourceBlob =  {
            "DefaultEndpointsProtocol": DefaultEndpointsProtocol,
            "AccountName": AccountName,
            "AccountKey": AccountKey,
            "BlobStorageContainerName": BlobStorageContainerName
        }
    }

    var TAccountName = $('#target_accountname').val();
    var TAccountKey = $('#target_accountkey').val();
    var TBlobStorageContainerName = $('#target_blobcontainer').val()
    var TargetBlobs=[];
    if (TAccountName && TAccountKey && TBlobStorageContainerName) {
        var targetBlobs =  {
            "DefaultEndpointsProtocol": DefaultEndpointsProtocol,
            "AccountName": TAccountName,
            "AccountKey": TAccountKey,
            "BlobStorageContainerName": TBlobStorageContainerName

        }
        TargetBlobs.push(targetBlobs);
    }
    if (TargetBlobs && SourceBlob)
    {
        var SourceToTargetTransfer = { "TargetBlobs": TargetBlobs, "SourceBlob": SourceBlob }
    }
    $.ajax({  
        url: "BlobUpload/BlobToBlobTransfer",
        type: "POST",  
        contentType: "application/json", // Not to set any content header  
        data: JSON.stringify(SourceToTargetTransfer),
        success: function (result) {  
            toastr.success("Uploded sucessfully.");
            $('#UploadFiles').val('');
        },  
        error: function (err) {
            debugger;
            toastr.error(err.statusText);
            $('#UploadFiles').val('');
        }  
    });  
}

