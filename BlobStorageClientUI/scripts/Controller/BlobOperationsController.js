﻿app.controller('BlobOperationsController', ['$scope', '$http', '$routeParams', '$location', 'toastr', 'fileUploadService', 'ngDialog', '$rootScope', '$window',

function BlobOperationsController($scope, $http, $routeParams, $location, toastr, fileUploadService, ngDialog, $rootScope, $window) {
    $scope.Disable = true;
    $scope.isCollapsed = true;
    $scope.IsToDisplayLocalToBlob = false;
    $scope.IsToDisplayBlobToBlob = false;
    $scope.NonRedact_To_Blob = false;
    $scope.Currenturl = $location.$$url;
    //$rootScope.BlobRequest = {}

    $scope.BlobRequest = {}
    $scope.BlobRequest.Source = { AccountKey: "", AccountName: "", Domain: "", DefaultEndpointsProtocol: "https" };
    $scope.BlobRequest.Source.Target = [];
    $scope.BlobRequest.Source.Target.push({ AccountKey: "", AccountName: "", BlobStorageContainerName: "" })

    //$rootScope.BlobRequest.Source.Target = [];
    //$rootScope.BlobRequest.Source.Target.push({ AccountKey: "", AccountName: "", BlobStorageContainerName: "" })
    $scope.SelectedType = {};
    $scope.GetSourceTargetNames = function () {
        var url = "BlobUpload/GetSourceBlobSettingsDetails";
        fileUploadService.GetSourceDestinationServers(url).success(function (response) {
            $scope.SourceAliasNames = response.Source;
            $scope.TargetAliasNames = response.Target;
            $scope.DefaultEndpointsProtocol = "https";
            $scope.Protocol = "https";
            $scope.disabledUpload = true;
        }).error(function (response) {

        });
    }

    // upload files to store in blob with and with out redaction
    var UploadFileslocal_ui;

    $scope.EnableUploadB2B = function () {
        var count = 0;
        var objFolderDyn =angular.copy($scope.SourceContainerNames);
        _.each(objFolderDyn, function (item) {
            if (item.Blobs) {
                var temp1 = _.filter(item.Blobs, { Selected: true });
                count = count + temp1.length;
            }
        });
        if (count == 0 && objFolderDyn != undefined) {

            if (SelectedFileNames.length > 0) {
                var obj = {
                    ContainerName: SelectedFileNames[0].FilePath,
                    Blobs: SelectedFileNames
                };
                objFolderDyn.push(obj);
                _.each(objFolderDyn, function (itemTe) {
                    if (itemTe.Blobs) {
                        var temp1 = _.filter(itemTe.Blobs, { Selected: true });
                        count = count + temp1.length;
                    }
                });
            }

        }
        if ($scope.selectedTargetAlias) {
            if ($scope.selectedTargetAlias.length > 0 && count > 0) {
                return false;
            }
            else {
                return true;
            }
        }
        return true;

    }
    $scope.UploadLocalContainers = function (selection) {
        URL = 'BlobUpload/UploadFilesWithRedaction';
        $scope.TargetConfig.BlobStorageContainerName = (_.filter($scope.TargetContainerNames, { Selected: true })[0]).ContainerName;

        var fileData = $scope.FillFormaData();

        var rsult1 = fileUploadService.UploadFiles(URL, fileData).success(function (response) {

            $scope.StorageContainerStatus = response.BlobStorageContainers;


            ngDialog.openConfirm({
                template: 'Templates/BlobUploadStatusPopup.html',
                plain: false,
                scope: $scope,
                closeByDocument: false
            }).finally(function () {
                $scope.message = "";
            })

        }).error(function (errmsg) {



        });
    }
    $scope.EnableUpload = function (value) {
        if ($scope.TargetConfig != 'undefined' && value == true) {
            if ($scope.TargetConfig.BlobStorageContainerName != 'undefined') {
                return false;
            }

        }
        else
            return true;


    }
    //Enable GetContainers button

    $scope.EnableGetContainers = function (value) {

        if (value != 'undefined') {
            return false;
        }
        else
            return true;
    }
    //fill form data to post to server
    $scope.FillFormaData = function () {
        if (window.FormData !== undefined) {

            var fileUpload = $("#UploadFiles").get(0);
            var files = fileUpload.files;

            // Create FormData object  
            var fileData = new FormData();

            // Looping over all files and add it to FormData object  


            for (var i = 0; i < files.length; i++) {
                fileData.append(files[i].name, files[i]);
            }

            //fileData.append("ContainerName", UploadFileslocal_ui);

            for (var key in $scope.TargetConfig) {
                fileData.append(key, $scope.TargetConfig[key]);
            }
            return fileData;
        }
    }

    // Bolb to Blob transfer from source to target
    $scope.BlobToBlobUpload = function (blobrequest, selectedTargetAlias, targetcontainername) {

        var TargetObj = _.filter($scope.TargetAliasNames, function (item) {

            var count = 0;
            _.each(selectedTargetAlias, function (tmp) {
                //console.log("onUpload :" + item.AliasName);
                if (tmp.trim() == item.AliasName.trim()) {
                    count = count + 1;
                }
            })
            if (count > 0) {
                return true;
            } else {
                return false;
            }
        })

        $scope.TargetConfig = new Array();

        _.each(TargetObj, function (temp) {

            var demo = {
                DefaultEndpointsProtocol: "https",
                AccountName: temp.AccountName,
                AccountKey: temp.AccountKey,
                BlobStorageContainerName: targetcontainername,
                Domain: temp.Domain
            }
            $scope.TargetConfig.push(demo);
        });

        var tempblobs = angular.copy($scope.SourceContainerNames);
        var filteredContainers = tempblobs;

        //var filteredContainers = _.filter(tempblobs, function (temp3) {
        //    return _.filter(temp3.Blobs, { Selected: true }).length > 0;
        //});
        if (filteredContainers.length > 0) {
            _.each(filteredContainers, function (item) {
                item.Blobs = _.filter(item.Blobs, { Selected: true });
            })
            SelectedFileNames = _.filter(SelectedFileNames, { Selected: true });

            _.each(filteredContainers, function (temp4) {
                _.each(SelectedFileNames, function (temp5) {

                    if (temp5.FilePath == temp4.ContainerName) {
                        temp4.Blobs.push(temp5);

                    }
                    //else {
                    //    var obj = {
                    //        ContainerName: SelectedFileNames[0].FilePath,
                    //        Blobs: [temp5]

                    //    };
                    //    filteredContainers.push(obj);
                    //}
                  
                });
            });
        }

        filteredContainers = _.filter(filteredContainers, function (temp3) {
            return _.filter(temp3.Blobs, { Selected: true }).length > 0;
        });
        if (filteredContainers.length == 0) {

            if (SelectedFileNames.length > 0) {
                var obj = {
                    ContainerName: SelectedFileNames[0].FilePath,
                    Blobs: SelectedFileNames
                };
            }
            filteredContainers.push(obj);
        }

        var url = "BlobUpload/BlobToBlobTransfer";

        var SourceToTargetTransfer = {
            "TargetBlobs": $scope.TargetConfig, "SourceBlob": $scope.SourceConfig, BlobStorageContainers: filteredContainers
        }
        var result1 = fileUploadService.BlopbToBlobFileTransfer(url, SourceToTargetTransfer).success(function (response) {
            $scope.StorageContainerStatus = response.BlobStorageContainers;
            //$scope.SourceContainerNames = null; 
            toastr.success("Uploded sucessfully.");
            //angular.forEach($scope.SourceContainerNames, function (container) {
            //    _.each(container.Blobs, function (file) {
            //        file.Selected = false;
            //    });
            //});
            //angular.forEach($scope.TargetContainerNames, function (container) {
            //    container.Selected = false;
            //});
            //$scope.selectedTargetAlias = null;
            //$scope.EnableUploadB2B();

            ngDialog.openConfirm({
                template: 'Templates/BlobUploadStatusPopup.html',
                plain: false,
                scope: $scope,
                closeByDocument: false
            }).finally(function () {
                $scope.message = "";
            })

        }).error(function (errmsg) {

            toastr.error("Uploaded Failed");
        });

    }



    $scope.CheckonlyOneContainer = function (position) {
        angular.forEach($scope.SourceContainerNames, function (container, index) {
            if (position != index)
                container.Selected = false;
            else {
                container.name = true;
                $scope.TargetConfig.BlobStorageContainerName = $scope.TargetContainerNames[position].ContainerName;

            }
        });

    }

    $scope.CheckonlyOneContainerTarget = function (position) {
        angular.forEach($scope.TargetContainerNames, function (container, index) {
            if (position != index)
                container.Selected = false;
            else {
                container.name = true;
                $scope.TargetConfig.BlobStorageContainerName = $scope.TargetContainerNames[position].ContainerName;
                $scope.TargetContainerName = $scope.TargetConfig.BlobStorageContainerName;
            }
        });
    }
    $scope.Disable2 = function () {
        if (_.filter($scope.TargetContainerNames, { Selected: true }).length > 0) {
            return false;
        }
        else {
            return true;
        }
    }

    // Adding more targets blobs to transfer from source blob.
    $scope.AddDestination = function () {

        $rootScope.BlobRequest.Source.Target.push({ AccountKey: "", AccountName: "", DefaultEndpointsProtocol: "https", BlobStorageContainerName: "" })
    }

    //For getting the file names
    $scope.GetSourceContainers = function (blobrequest, selectedSourceAlias) {

        var Containers = _.filter($scope.SourceAliasNames, function (item) {
            return item.AliasName.trim() == selectedSourceAlias[0].trim();
        })


        if (Containers.length > 0) {
            blobrequest.Source.AccountKey = Containers[0].AccountKey;
            blobrequest.Source.AccountName = Containers[0].AccountName;
            blobrequest.Source.Domain = Containers[0].Domain;
            blobrequest.Source.Protocol = Containers[0].Protocol;
        }

        var url = "BlobUpload/GetSourceContainerList";

        var SourceBlob = null;
        if (blobrequest.Source.AccountKey && blobrequest.Source.AccountName && blobrequest.Source.Domain && blobrequest.Source.Protocol) {
            SourceBlob = { AccountKey: blobrequest.Source.AccountKey, AccountName: blobrequest.Source.AccountName, Domain: blobrequest.Source.Domain, DefaultEndpointsProtocol: blobrequest.Source.Protocol };
            $scope.SourceConfig = SourceBlob;
        }

        var request = {
            SourceBlob: $scope.SourceConfig,
            TargetBlobs: null,
            BlobStorageContainers: null
        }

        fileUploadService.BlopbToBlobFileTransfer(url, request).success(function (response) {
            $scope.SourceContainerNames = new Array();
            var fileNameStringList = response;
            _.each(fileNameStringList, function (temp) {
                var obj = {
                    ContainerName: temp,
                    Selected: false
                }
                $scope.SourceContainerNames.push(obj);
            });


        }).error(function (errmsg) {

            toastr.error(errmsg.statusText);
        });
    }

    $scope.GetBlobSourceContainers = function (blobrequest, selectedSourceAlias) {

        var Containers = _.filter($scope.SourceAliasNames, function (item) {
            return item.AliasName.trim() == selectedSourceAlias.trim();
        })


        if (Containers.length > 0) {
            blobrequest.Source.AccountKey = Containers[0].AccountKey;
            blobrequest.Source.AccountName = Containers[0].AccountName;
            blobrequest.Source.Domain = Containers[0].Domain;
            blobrequest.Source.Protocol = Containers[0].Protocol;
        }

        var url = "BlobUpload/GetSourceContainerList";

        var SourceBlob = null;
        if (blobrequest.Source.AccountKey && blobrequest.Source.AccountName && blobrequest.Source.Domain && blobrequest.Source.Protocol) {
            SourceBlob = { AccountKey: blobrequest.Source.AccountKey, AccountName: blobrequest.Source.AccountName, Domain: blobrequest.Source.Domain, DefaultEndpointsProtocol: blobrequest.Source.Protocol };
            $scope.SourceConfig = SourceBlob;
        }

        var request = {
            SourceBlob: $scope.SourceConfig,
            TargetBlobs: null,
            BlobStorageContainers: null
        }

        fileUploadService.BlopbToBlobFileTransfer(url, request).success(function (response) {
            $scope.SourceContainerNames = new Array();
            var fileNameStringList = response;
            _.each(fileNameStringList, function (temp) {
                var obj = {
                    ContainerName: temp,
                    Selected: false
                }
                $scope.SourceContainerNames.push(obj);
            });

        }).error(function (errmsg) {

            toastr.error(errmsg.statusText);
        });
    }

    $scope.GetContainerFiles = function (container, blobrequest) {
        var url = "BlobUpload/GetSourceBlobItems";
        var BlobStorageContainerobj = {
            BlobDirectoryList: null,
            Blobs: null,
            ContainerName: container.ContainerName,
        };
        var BlobStorageContainerList = new Array();
        BlobStorageContainerList.push(BlobStorageContainerobj);

        var request = {
            SourceBlob: $scope.SourceConfig,
            TargetBlobs: $scope.TargetConfig,
            BlobStorageContainers: BlobStorageContainerList

        }
        fileUploadService.BlopbToBlobFileTransfer(url, request).success(function (response) {
            var FilesResponce = response;
            _.each(FilesResponce, function (item) {
                _.each($scope.SourceContainerNames, function (temp) {
                    if (temp.ContainerName == item.ContainerName) {
                        temp.BlobDirectoryList = item.BlobDirectoryList;
                        _.each(temp.BlobDirectoryList, function (temp3) {
                            temp3.Selected = false;
                        })
                        temp.Blobs = item.Blobs;
                        _.each(temp.Blobs, function (temp2) {
                            temp2.Selected = false;
                        })
                    }
                })
            })

        }).error(function (errmsg) {

            toastr.error(errmsg.statusText);
        });
    }

    $scope.GetFolderFiles = function (DirectoryName, BlobRequest, container) {
        var url = "BlobUpload/GetDirectoryBlobItems";
        var BlobStorageContainerobj = {
            BlobDirectoryList: [DirectoryName],
            Blobs: null,
            ContainerName: container.ContainerName,
        };
        var BlobStorageContainerList = new Array();
        BlobStorageContainerList.push(BlobStorageContainerobj);

        var results = {
            AccountKey: BlobRequest.Source.AccountKey,
            AccountName: BlobRequest.Source.AccountName,
            Domain: BlobRequest.Source.Domain,
            DefaultEndpointsProtocol: BlobRequest.Source.Protocol
        }
        var request = {
            SourceBlob: results,
            TargetBlobs: $scope.TargetConfig,
            BlobStorageContainers: BlobStorageContainerList
        }
        fileUploadService.BlopbToBlobFileTransfer(url, request).success(function (response) {
            var directoryList = response[0].BlobDirectoryList[0].BlobDirectoryList;

            var blobsList = response[0].BlobDirectoryList[0].Blobs;

            DirectoryName.BlobDirectoryList = directoryList;
            DirectoryName.Blobs = blobsList;
            _.each(DirectoryName.BlobDirectoryList, function (items) {
                if ((items.DirectoryName.split('/'))[(items.DirectoryName).split('/').length - 1] != "")
                    items.DirectoryNames = ((items.DirectoryName).split('/'))[(items.DirectoryName).split('/').length - 1];
                else
                    items.DirectoryNames = ((items.DirectoryName).split('/'))[(items.DirectoryName).split('/').length - 2];

            });
            _.each(DirectoryName.Blobs, function (item) {
                item.FileNames = ((item.FileName).split('/'))[(item.FileName).split('/').length - 1];

            });



        }).error(function (errmsg) {

            toastr.error(errmsg.statusText);
        });
    }
    $scope.FolderFilesSelected = function (fileName) {
        $scope.FileName = new Array();
        if (fileName) {
            var demo = 1;
        }
    }
    $scope.GetTargetContainerFiles = function (container, blobrequest) {
        var url = "BlobUpload/GetSourceBlobItems";
        var BlobStorageContainerobj = {
            BlobDirectoryList: null,
            Blobs: null,
            ContainerName: container.ContainerName,
        };
        var BlobStorageContainerList = new Array();
        BlobStorageContainerList.push(BlobStorageContainerobj);
        $scope.TargetContainerName = container.ContainerName;
        var request = {
            SourceBlob: $scope.TargetConfig,
            TargetBlobs: null,
            BlobStorageContainers: BlobStorageContainerList

        }


        fileUploadService.BlopbToBlobFileTransfer(url, request).success(function (response) {
            var FilesResponce = response;
            _.each(FilesResponce, function (item) {
                _.each($scope.TargetContainerNames, function (temp) {
                    if (temp.ContainerName == item.ContainerName) {

                        temp.Blobs = item.Blobs;
                        _.each(temp.Blobs, function (temp2) {
                            temp2.Selected = false;
                        })
                    }
                })
            })
            $scope.Disable = false;
        }).error(function (errmsg) {

            toastr.error(errmsg.statusText);
        });
    }
    $scope.GetSourceTargetNames();


    $scope.GetTargetContainers = function (blobrequest, selectedTargetAlias) {
        var targetlist = _.filter($scope.TargetAliasNames, function (item) {
            return item.AliasName.trim() == selectedTargetAlias[0].trim();
        })

        if (targetlist.length > 0) {

            blobrequest.Source.AccountKey = targetlist[0].AccountKey;
            blobrequest.Source.AccountName = targetlist[0].AccountName;
            blobrequest.Source.Domain = targetlist[0].Domain;
            blobrequest.Source.Protocol = targetlist[0].Protocol;
        }

        var url = "BlobUpload/GetSourceContainerList";

        var TargetBlob = null;
        if (blobrequest.Source.AccountKey && blobrequest.Source.AccountName && blobrequest.Source.Domain) {
            TargetBlob = { AccountKey: blobrequest.Source.AccountKey, AccountName: blobrequest.Source.AccountName, Domain: blobrequest.Source.Domain, DefaultEndpointsProtocol: blobrequest.Source.Protocol };
            $scope.TargetConfig = TargetBlob;
        }

        var request = {
            SourceBlob: TargetBlob,
            TargetBlobs: null,
            BlobStorageContainers: null
        }

        fileUploadService.BlopbToBlobFileTransfer(url, request).success(function (response) {
            $scope.TargetContainerNames = new Array();
            var fileNameStringList = response;
            _.each(fileNameStringList, function (temp) {
                var obj = {
                    ContainerName: temp,
                    Selected: false
                }
                $scope.TargetContainerNames.push(obj);
            });
            //toastr.success("Uploded sucessfully.");

        }).error(function (errmsg) {

            toastr.error(errmsg.statusText);
        });
    }
}
]);



