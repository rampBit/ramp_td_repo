﻿function Upload(selection) {  
    debugger;
    var URL;
    if (selection === 1)
    {
        URL = 'BlobUpload/UploadFilesWithRedaction';
      
    }
    else {
       
        URL = 'BlobUpload/UploadFiles';
      
    }
   
    // Checking whether FormData is available in browser  
    if (window.FormData !== undefined) {  
  
        var fileUpload = $("#UploadFiles").get(0);  
        var files = fileUpload.files;  
              
        // Create FormData object  
        var fileData = new FormData();  
  
        // Looping over all files and add it to FormData object  
        for (var i = 0; i < files.length; i++) {  
            fileData.append(files[i].name, files[i]);  
        }  
              
        // Adding one more key to FormData object  
      
  
        $.ajax({  
            url: URL,
            type: "POST",  
            contentType: false, // Not to set any content header  
            processData: false, // Not to process data  
            data: fileData,  
            success: function (result) {  
                toastr.success("Uploded sucessfully.");
            },  
            error: function (err) {
                debugger;
                toastr.error(err.statusText);
                $('#UploadFiles').val('');
            }  
        });  
    } else {  
        alert("FormData is not supported.");  
    }  
} 