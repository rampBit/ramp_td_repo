﻿app.service('fileUploadService', ['$http',  function ($http) {

    var Service = {};
    Service.UploadFiles = function (url, FileData) {
        var response = $http({
            method: 'POST',
            url: url,
            data:FileData,
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        })
        return response;
    }
    Service.BlopbToBlobFileTransfer = function (url, SourceToTargetTransfer) {

        var response = $http({
            method: 'POST',
            url: url,
            contentType: "application/json", // Not to set any content header  
            data: JSON.stringify(SourceToTargetTransfer),
        })
        return response;
    }
    Service.GetSourceDestinationServers = function (url)
    {
        var response = $http({
            method: 'POST',
            url: url,
            contentType: "application/json", // Not to set any content header           
        })
        return response;
    }
    Service.Setup=function(url)
    {
        var response = $http({
            method: 'POST',
            url: url,
            contentType: "application/json", // Not to set any content header           
        })
        return response;
    }
    return Service;
}]);