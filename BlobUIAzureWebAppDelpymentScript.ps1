#call the Deploy fn through Powershell and pass a paramater "BasePath" "Path of Published files from Web application"
function deploy($basePath,$appServiceName){
 setAzureSubscription
$zipPath= createPackage $basePath 
 CreateNEWWebApp $zipPath $appServiceName
}

function createPackage($basePath){
  $zipPath = $basePath + ".zip"
  Add-Type -Assembly System.IO.Compression.FileSystem 
  $compressionLevel = [System.IO.Compression.CompressionLevel]::Optimal
    [System.IO.Compression.ZipFile]::CreateFromDirectory($basePath, $zipPath,$compressionLevel,$false) 
  Write-Host "$(Get-Date �f $timeStampFormat) - Zip Package Created " -foregroundcolor "green"
  return $zipPath
   
}

function CreateNEWWebApp($zipPath,$appServiceName){
 New-AzureWebsite -Name $appServiceName -Location "USGov Iowa" 
 Publish-AzureWebsiteProject -name $appServiceName -Package $zipPath
 Write-Host "$(Get-Date �f $timeStampFormat) - Completed Web App Deployment " -foregroundcolor "green"
}

function setAzureSubscription(){
# Login-AzureRmAccount
 add-AzureRmAccount -Environment "AzureUSGovernment"
 $SubInfo = Get-AzureRmSubscription  
 Select-AzureRmSubscription -SubscriptionName $SubInfo.Name | Out-Null
# Select-AzureRmSubscription -SubscriptionName $SubInfo.SubscriptionName | Out-Null
 Write-Host "$(Get-Date �f $timeStampFormat) - Set Azure subscription" -$($SubInfo.Name) "Completed " -foregroundcolor "green"
}






