﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
   public class AdminLogin
    {
        public string SubscriptionID { get; set; }
        public string ResourceGroup { get; set; }
        public string LoginID { get; set; }
        public string ServerName { get; set; }
        public string DataBaseName { get; set; }
        public string ServerURL { get; set; }
        public string APIKey { get; set; }
        //public string RuleSetPath { get; set; }
        //public string RedactMailPath { get; set; }
        public string BaseURLRedactSuffix { get; set; }
        public string BaseURLRulesetSuffix { get; set; }
        public string RulesetXMLPathNode { get; set; }
        public string DefaultEndpointsProtocol { get; set; }
        public string AccountName { get; set; }
        public string AccountKey { get; set; }
        public string BlobStorageContainerName { get; set; }
        public string EndpointSuffix { get; set; }
        public string AppID { get; set; }
        public string AppPassword { get; set; }
        public string RedirectUri { get; set; }
        public string AppScopes { get; set; }
        public string TenantID { get; set; }
        public string Authority { get; set; }
        public string subscriptionname { get;  set; }
        public string Resource { get; set; }
        public string EmailID { get; set; }
        public string Password { get; set; }
        public string Domain { get; set; }
        public string ExchangeVersion { get; set; }
        public string IsApprovalEmail { get; set; }
        public int MailCount { get; set; }
        public int FromMinutes { get; set; }

    }
}
