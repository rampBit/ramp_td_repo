﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    public class AdminRequest
    {
        public AdminLogin AdminLogins { get; set; }

        public IList<AdminLogin> AdminLoginAuth { get; set; }
    }
}
