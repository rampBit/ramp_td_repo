﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
   public class AdminRequestAzure
    {
        public string SubscriptionID { get; set; }

        public string ResourceGroupName { get; set; }

        public string DataBaseName { get; set; }

    }
}
