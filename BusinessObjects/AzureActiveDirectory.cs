﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    public class MemberDetail
    {


        public Detail Detail { get;   set; }
        public object MemberType { get;   set; }
    }
    public enum MemberType
    {
        User,
        Group,
        Contact
    }
    public class AADGroup
    {

        public Detail GroupDetail { get; set; }
        public List<MemberDetail> Members { get;   set; }
    }
    public class Detail
    {

        public string Description { get;   set; }
        public string DisplayName { get;   set; }
        public string ObjectId { get;   set; }
        public string UserPrincipalName { get; set; }
            //    DisplayName = "newGroup" + Helper.GetRandomString(8),
            //    Description = "Best Group ever",
            //    MailNickname = "group" + Helper.GetRandomString(4),
            //    MailEnabled = false,
            //    SecurityEnabled = true


        }

    }
