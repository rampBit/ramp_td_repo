﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
  public  class BlobConfig
    {
        public string DefaultEndpointsProtocol { get; set; }
        public string AccountName { get; set; }
        public string AccountKey { get; set; }
        public string BlobStorageContainerName { get; set; }

        public string Domain { get; set; }
    }


   
}
