﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    public class BlobRequest
    {
        public BlobConfig SourceBlob { get; set; }
        
        public IList<BlobConfig> TargetBlobs { get; set; }  //in case multiple target if any
        public List<BlobStorageContainer> BlobStorageContainers { get; set; }

       // public IEnumerable<FileObject> selectedItem { get; set; }

    }

    public class BlobStorageContainer
    {
        public string DirID { get; set; }
        public List<BlobDirectory> BlobDirectoryList { get; set; }
        public IEnumerable<FileObject> Blobs { get; set; }
        public string ContainerName { get; set; }
    }

    public class BlobResponse
    {
        public List<BlobStorageContainer> BlobStorageContainers { get; set; }
    }
    public class BlobDirectory
    {
        public string DirID { get; set; }
        public string  ParentID { get; set; }
        public string DirectoryName { get; set; }
        public List<FileObject> Blobs { get; set; }
        public List<BlobDirectory> BlobDirectoryList { get; set; }
    }
}
