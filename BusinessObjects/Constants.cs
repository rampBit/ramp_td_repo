﻿namespace BusinessObjects
{
    public class AADConstants
    {
        public static AADConstants settings { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string TenantName { get; set; }
        public string TenantId { get; set; }
        public string AuthString { get; set; }
        public string ResourceUrl { get; set; }

    }
    public class AppModeConstants
    {

        public static string ClientId = AADConstants.settings.ClientId;  //"a4883a0e-d4d3-48e7-a294-2b1d6e46334d";
        public static string ClientSecret = AADConstants.settings.ClientSecret; //"T5b3Yiu8RMu1t2uG9TS6ftI/nBZatPkY+kYn8hMNYKA=";
        public static string TenantName = AADConstants.settings.TenantName;  //"teradactg.onmicrosoft.com";
        public static string TenantId = AADConstants.settings.TenantId; // "5a5fc493-a003-4f6e-a242-b2ab6f6a1546"; //https://login.windows.net/5a5fc493-a003-4f6e-a242-b2ab6f6a1546
        public static string AuthString = AADConstants.settings.AuthString + TenantName; // GlobalConstants.AuthString + TenantName;

    }

    public class UserModeConstants
    {
        public static string TenantId = AppModeConstants.TenantId;
        public static string ClientId = AppModeConstants.ClientId;
        public static string AuthString = AppModeConstants.AuthString + "common/";
    }

    public class GlobalConstants
    {
        public static string AuthString = AppModeConstants.AuthString; //"https://login.microsoftonline.com/";
        public static string ResourceUrl = "https://graph.windows.net";
        public const string GraphServiceObjectId = "00000002-0000-0000-c000-000000000000";
    }
    public enum GraphRequestsMode
    {
        AppMode,
        UserMode
    }
}
