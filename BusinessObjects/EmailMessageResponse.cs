﻿ 
using System;
using System.Collections.Generic;
 

namespace BusinessObjects
{
    [Serializable]
    public class EmailMessageResponse 
    {
        public string emailItemId { get; set; }
        public string subject { get; set; }
        public string fromAddress { get; set; }
        public List<string> senderName { get; set; }
        public DateTime SendDate { get; set; }
        public DateTime RecievedDate { get; set; }
        public long Size { get; set; }
        public string emailBody { get; set; }
        public byte[] Content { get; set; }
        public bool HasAttachments { get; set; }
        public bool IsAttachment { get; set; }
        public string ConversationTopic { get; set; }
        public List<string> ToAddress { get; set; }
        public List<string> CCAddress { get; set; }
        public List<string> BCCAddress { get; set; }
        public ExchangeMailboxType MailBoxtype { get; set; }
    }
    public enum ExchangeMailboxType:uint
    {
        Unknown = 0,
        OneOff = 1,
        Mailbox = 2,
        PublicFolder = 3,
        PublicGroup = 4,
        ContactGroup = 5,
        Contact = 6,
        GroupMailbox = 7
    }
}
