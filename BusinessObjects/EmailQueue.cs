﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    public class EmailQueue
    {
        public long? EmailQueueID { get; set; }
        public string MessageID { get; set; }
        public DateTime? RequestedTime { get; set; }
        public RequestStatus Status { get; set; }
        public DateTime? ProcessedTime { get; set; }
        public string ExchangeAttachementEmailUniqueID { get; set; }
        public int? RetryCount { get; set; }
        public ExchangeServerSettings ExchangeServerSettings { get; set; }
        public string EmailID { get; set; }
    }
    public class BlobQueue
    {
        public long? BlobQueueID { get; set; }
        public long? RequestFilesCount  { get; set; }
        public long? FilesProcessedCount { get; set; }
        public RequestStatus Status { get; set; }
        
        
    }

    public class SharePointQueue
    {
        public long? SharePointQueueID { get; set; }
        public long? RequestFilesCount { get; set; }
        public long? FilesProcessedCount { get; set; }
        public RequestStatus Status { get; set; }
        public string FileName { get; set; }
        public string SPListID { get; set; }
        public int? SPListItemID { get; set; }
        public string SPHostURL { get; set; }
        public ErrorSource EventSources { get; set; }
        public string  ActionType { get; set; }
    }


    public enum RequestStatus
    {
        Requested,
        FileDownloaded,
        Redacted,
        MailDelivered,
        RedactionFailed,
        MailUndelivered,
        FileDownloadedFailed,
        IPMUpdated,
        BlobUploaded,
        SharePointFileUpload,
        FileDeleted,
        FileNotDeleted
    }
    public enum TeraDActProductInfo
    {
        ExchangeOnlineService,
        ExchangeOnPremService,
        BlobService,
        SharePointOnlineService,
        SkypeForBusiness,
        AzureMarketPlace

    }
    public enum ErrorSource
    {
        ExchangeOnlineServiceWebJob,
        TeraDActServiceEmailWebJob,
        LocalToBlob,
        BlobToBlob,
        TeraDActFileTransferService,
        SharePointAttendedCustomAction,
        SharePointUnAttendedCustomAction,
        AdminUI,
        SkypeForBusiness,
        MailUtility,
        FileTransferService,
        AzureActiveDirectory,
        ContextMenuService,
        SharepointRedactServiceUnattendedWebJob,
    }
}
