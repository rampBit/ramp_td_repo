﻿
using System;
using System.Collections.Generic;


namespace BusinessObjects
{
    public class AttachmentSampleServiceResponse : ICloneable
    {
        public AttachmentSampleServiceResponse()
        {
        }

        public List<AttachmentDetails> attachments { get; set; }
        public int attachmentsProcessed { get; set; }
        public List<EmailMessageResponse> EmailMessageResponse { get; set; }

        public object Clone()
        {
            return (AttachmentSampleServiceResponse)this.MemberwiseClone();
        }
        public ExchangeServerSettings ExchangeServerSettings { get; set; }

    }
    public class AttachmentSampleServiceRequest
    {
        public string attachmentToken { get; set; }
        public string ewsUrl { get { return "https://outlook.office365.com/EWS/Exchange.asmx"; } }
        public string service { get; set; }
        public AttachmentDetails attachment { get; set; }
        public Guid RequestID { get; set; }


    }

    public class AttachmentDetails
    {
        public string attachmentType { get; set; }
        public string contentType { get; set; }
        public string id { get; set; }
        public bool isInline { get; set; }
        public bool IsItemAttachment { get; set; }
        public string name { get; set; }
        public int size { get; set; }

        public byte[] FileContent { get; set; }
        public string ParentID { get; set; }
        public string ExchangeAttachementEmailUniqueID { get; set; }
        public int? RetryCount { get; set; }
      
    }
}
