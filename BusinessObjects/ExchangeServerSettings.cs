﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    public class ExchangeServerSettings
    {
        public ExchangeServerSettings()
        {


        }
        public string EmailID { get; set; }
        public string Password { get; set; }
        public string Domain { get; set; }
        public string TenantName { get; set; }
        public string AliasName { get; set; }
        public string ExchangeVersion { get; set; }
        public string IsApprovalEmail { get; set; }
        public int MailCount { get; set; }
        public int FromMinute { get; set; }
        public string EWSUrl { get; set; }
        public string AccountKey { get; set; }
        public string AccountName { get; set; }
        public string BlobStorageContainerName { get; set; }
        public string Protocal { get; set; }
        public string ProductName { get; set; }
        public string BlobType { get; set; }
        public string RulesetDescription { get; set; }

    
        

    }
}
