﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    public class FileObject
    {
        public MemoryStream FileStream { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }

        
        public long FileLength { get; set; }
        public string FilePath { get; set; }
        public OperationStatus OperationStatus { get; set; }
        public Exception ExceptionDetails { get; set; }
        public byte[] Content { get; set; }
        public OperationStatus RedactionStatus { get; set; }
    }
    public enum OperationStatus
    {
        Failed, Succeded
    }
}
