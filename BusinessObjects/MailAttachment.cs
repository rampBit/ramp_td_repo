﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    [Serializable]
    public class MailAttachment
    {
        public byte[] AttachmentContent { get; set; }
        public string AttachmentName { get; set; }
        public string FileType { get; set; }

    }
}