﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    public class MainWindowViewModel
    {
        public MainWindowViewModel()
        {
            files = new List<string>();
        }
        public List<string> _files;
        public List<string> files
        {
            get
            {
                return _files;
            }
            set
            {
                _files = value;
            }
        }
    }
}
