﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    public class RestLogInfo
    {

        public DateTime StartedOn { get; set; }
        public string RequestMethod { get; set; }
        // log.FinishedOnUtc = DateTime.UtcNow;
        public string RequestUrl { get; set; }
        public string ResponseStatusCode { get; set; }
        public string ResponseData { get; set; }
    }
}
