﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    public class RolesandGroups
    {
        
        public List<GroupNames> GroupNames { get; set; }
    }
    public class GroupNames
    {
        public string Name { get; set; }
        public List<Ruleset> rulesList { get; set; }
    }
    public class Ruleset
    { 
        public string Name { get; set; }
        public int ID { get; set; }
        public string check { get; set; }
        public string Owner { get; set; }
        public string MappedLabel { get; set; }

    }
  
    public class GroupsAndRoles
    {
        public string GroupName { get; set; }
        public string RuleSetName { get;set;}
    }
}
