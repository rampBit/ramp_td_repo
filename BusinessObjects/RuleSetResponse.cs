﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    public class RuleSetResponse
    {
        public RulesetMessageList RulesetMessageList { get; set; }
    }
    public class RulesetMessageList
    {
        public messageList messageList { get; set; }
    }
    public class messageList
    {
        public List<message> message { get; set; }
    }
    public class message
    {
        public string id { get; set; }
        public string name { get; set; }
        public string owner { get; set; }
    }
    public class TDRuleSetDirectiveInfo
    {
        public string TotalRLString { get; set; }
        public string RuleSetName { get; set; }
        public string Type { get; set; }
        public string Pattern { get; set; }
        public string label { get; set; }
        public string replacementText { get; set; }

        public string NewRuleSetName { get; set; }

    }
}
