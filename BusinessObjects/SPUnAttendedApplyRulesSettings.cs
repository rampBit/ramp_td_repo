﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
  public  class SPUnAttendedApplyRulesSettings
    {
        public int ID { get; set; }
        public string SourceSiteURL { get; set; }
        public string SourceDocLib { get; set; }
        public string SourceFolder { get; set; }

        public string TargetSiteURL { get; set; }
        public string TargetDocLib { get; set; }
        public string TargetFolder { get; set; }
        public string AppliedRuleSets { get; set; }
        public bool? ISActive { get; set; }
    }
}
