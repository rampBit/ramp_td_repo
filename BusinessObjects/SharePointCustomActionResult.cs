﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
  public  class SharePointCustomActionResult
    {
        public string ApplyRulesetActionResult { get; set; }
        public object CheckAccountPolicyActionResult { get; set; }
        public string CheckActionResult { get; set; }
        public string CreateReviewActionResult { get; set; }
        public string CreateReviewSessionActionResult { get; set; }
        public List<string> DocmentLibraryListResult { get; set; }
        public object GetAccountPolicyActionResult { get; set; }
        public FileObject RedactActionResult { get; set; }
        public string ReleaseReviewActionResult { get; set; }
        public object ShareActionResult { get; set; }
        public object SharedReleaseActionResult { get; set; }
        public object SharedViewActionResult { get; set; }
        public object TDRuleDirectiveInfo { get; set; }
        public object UpdateAccountPolicyActionResult { get; set; }
    }
}
