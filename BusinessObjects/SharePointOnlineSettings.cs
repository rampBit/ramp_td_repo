﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    public class SharePointOnlineSettings
    {
       
        public string SPFileTransferServiceURL { get; set; }
        public string SPAdminUserID { get; set; }
        public string SPAdminPassword { get; set; }
        public string AdminCentarlSiteURL { get; set; }
        public bool IsActive { get; set; }
        public string SharedDocumentLibraryName { get; set; }

        public string ProductName { get; set; }

        public string Domain { get; set; }
    }
}
