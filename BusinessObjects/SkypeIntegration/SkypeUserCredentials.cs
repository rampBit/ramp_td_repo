﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects.SkypeIntegration
{
    public class SkypeUserCredentials
    {
        public string Username;
        public string Password;
        public SkypeUserCredentials(string authUser, string authPass)
        {
            Username = authUser;
            Password = authPass;
        }
    }
}
