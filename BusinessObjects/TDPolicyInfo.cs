﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
   
    public class TDPolicyInfo
    {

        public string PolicyName { get;   set; }

        public string PolicyType { get; set; }

        public string PolicyValue { get; set; }
        public string PolicyKey { get; set; }
        public string MasterPolicyType { get; set; }

        public string PolicyTypeID { get; set; }
        //public string pci { get; set; }
        //public String phi { get; set; }
        //public string pii { get; set; }
        //public string ranked { get; set; }

        //public string PCIType { get; set; }

        //public string PCIKey { get; set; }
        //public String PHIType { get; set; }

        //public String PHIKey { get; set; }


        //public string PIIType { get; set; }

        //public string PIIKey { get; set; }
        //public string RankedType { get; set; }

        //public string RankedKey { get; set; }


    }
}
