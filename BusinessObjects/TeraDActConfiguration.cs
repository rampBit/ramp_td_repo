﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    public class TeraDActConfiguration 
    {
        public static TeraDActConfiguration Settings { get; set; }
        public string ServerURL { get; set; }
        public string APIKey { get; set; }
       
        //public string RedactMailPath { get; set; }
        public string BaseURLRedactSuffix { get; set; }
        public string BaseURLRulesetSuffix { get; set; }
        public string RulesetXMLPathNode { get; set; }
        
      

    }
}
