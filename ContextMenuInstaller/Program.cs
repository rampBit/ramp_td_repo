﻿using BusinessObjects;
using SharepointFileTransferService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TeraDAct.SharepointFileTransferService;
using static SharepointFileTransferService.ContextMenuManager;

namespace ContextMenuInstaller
{
    class Program
    {
        private static SharePointOnlineSettings spvalues;



        static void Main(string[] args)
        {
            new Program().AddCustomAction();
            Console.WriteLine("Press Enter to exit");
            Console.ReadLine();
        }
        private void AddCustomAction()
        {

            //    var bgw = new BackgroundWorker();

            //bgw.DoWork += new DoWorkEventHandler(bgw_AddAction);
            //    bgw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgw_AddAction_End);
            //    bgw.ProgressChanged += new ProgressChangedEventHandler(BgwReportProgress);
            //    bgw.WorkerReportsProgress = true;
            //    bgw.WorkerSupportsCancellation = true;


            //    bgw.RunWorkerAsync();
            bgw_AddAction();


            // Console.ReadKey();

        }

        private void bgw_AddAction()
        {
            try
            {
                if (null == spvalues)
                    spvalues = FileTransferService.GetSharePointSettingsDetails()[0];

                IFileTransferService _service = new FileTransferService(spvalues.SPAdminUserID, spvalues.SPAdminPassword);
                SharepointContexRequest request = new SharepointContexRequest
                {
                    SPHostURL = spvalues.AdminCentarlSiteURL

                };
                _service.AdminCentarlSiteURL = spvalues.AdminCentarlSiteURL;
                var allSites = _service.getAllSites(request );
                IContextMenuManager _manager;

                var titles = new List<ShareActionTypeTitle>
                            {
                              new ShareActionTypeTitle {  Title =
                                  $"{ Constant.Teradact} Redact",
                                  ActionType ="Redact",
                                  Sequence=10001,
                                  PageName = "CustomActionContextMenu.aspx",
                                  SPFileTransferServiceURL=spvalues.SPFileTransferServiceURL
            },

                new ShareActionTypeTitle {  Title =
                                  $"{ Constant.Teradact} Check",
                                  ActionType ="Check",
                                   Sequence=10002,
                                   PageName = "CustomActionContextMenu.aspx",
                                    SPFileTransferServiceURL=spvalues.SPFileTransferServiceURL
            },
                 new ShareActionTypeTitle {  Title =
                                  $"{ Constant.Teradact} Review",
                                  ActionType ="CreateReview",
                                   Sequence=10003,
                                   PageName = "CustomActionContextMenu.aspx",
                                    SPFileTransferServiceURL=spvalues.SPFileTransferServiceURL
            },
                  new ShareActionTypeTitle {  Title =
                                  $"{ Constant.Teradact} Release",
                                  ActionType ="ReleaseReview",
                                   Sequence=10004,
                                   PageName = "CustomActionContextMenu.aspx",
                                    SPFileTransferServiceURL=spvalues.SPFileTransferServiceURL
            },
                  new ShareActionTypeTitle {  Title =
                                  $"{ Constant.Teradact} Share",
                                  ActionType ="Share",
                                   Sequence=10005,
                                   PageName = "ShareCustomAction.aspx",
                                    SPFileTransferServiceURL=spvalues.SPFileTransferServiceURL
            }
                            };
                ContextMenuManager.SiteCount = allSites.Count();

                foreach (var site in allSites)
                {

                    request.SPHostURL = site;

                    _manager = new ContextMenuManager(_service.getClientContext(request));
                    _manager.SPAppWebUrl = site;

                    if (_manager.DeleteAction())
                        _manager.AddCustomMenu(titles, false);

                }

            }
            catch
            {

                throw;
            }
            finally { Environment.Exit(0); }
        }

        private void BgwReportProgress(object sender, ProgressChangedEventArgs e)
        {

        }

        private void bgw_AddAction_End(object sender, RunWorkerCompletedEventArgs e)
        {
            Console.WriteLine("Done now...");
            Environment.Exit(0);

        }
    }
}
