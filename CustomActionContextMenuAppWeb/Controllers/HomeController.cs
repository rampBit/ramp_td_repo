﻿using CustomActionContextMenuAppWeb.Models;
using Microsoft.SharePoint.Client;
using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Mvc;


namespace TeraDAct.CustomActionContextMenuAppWeb.Controllers
{

    public class HomeController : Controller
    {
      
        public ActionResult Index()
        {
            //User spUser = null;

            //var spContext = SharePointContextProvider.Current.GetSharePointContext(HttpContext);

            //using (var clientContext = spContext.CreateUserClientContextForSPHost())
            //{
            //    if (clientContext != null)
            //    {
            //        spUser = clientContext.Web.CurrentUser;
            //        clientContext.Load(spUser, user => user.Title);
            //        clientContext.ExecuteQuery();
            //        ViewBag.UserName = spUser.Title;

            //    }
            //}



            return View("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult ApplyRulesetAction(string SPHostUrl,
           string SPListId, string SPListItemId)
        {

            ICusomActionHandler invoker = getCustomInvokeHandler(SPHostUrl, SPListId, SPListItemId);
               
            var result = invoker.ProcessCustomAction(SharePointRequestActionType.ApplyRuleset);
            
            return View();
        }
        public ActionResult CheckAction(string SPHostUrl,
          string SPListId, string SPListItemId)
        {



          //  ICusomActionHandler invoker = getCustomInvokeHandler(SPHostUrl, SPListId, SPListItemId);

          //  var result = invoker.ProcessCustomAction(SharePointRequestActionType.Check);
            return View();
        }

        public ActionResult CreateReviewAction(string SPHostUrl,
         string SPListId, string SPListItemId)
        {

            ICusomActionHandler invoker = getCustomInvokeHandler(SPHostUrl, SPListId, SPListItemId);
            var result = invoker.ProcessCustomAction(SharePointRequestActionType.CreateReview);
            return View();
        }

        public ActionResult ReleaseReviewAction(string SPHostUrl,
        string SPListId, string SPListItemId)
        {
            ICusomActionHandler invoker = getCustomInvokeHandler(SPHostUrl, SPListId, SPListItemId);
            var result = invoker.ProcessCustomAction(SharePointRequestActionType.ReleaseReview);
            return View();
        }


        public ActionResult RedactAction(string SPHostUrl,
            string SPListId, string SPListItemId)
        {
            ICusomActionHandler invoker = getCustomInvokeHandler(SPHostUrl, SPListId, SPListItemId);
            var result = invoker.ProcessCustomAction(SharePointRequestActionType.Redact);
          
            return View();
        }
        private CusomActionHandler getCustomInvokeHandler(string SPHostUrl, string SPListId,string SPListItemId)
        {
            
            CustomActionRequestedData request = new CustomActionRequestedData
            {
                SPHostUrl = SPHostUrl,
                SPListId = SPListId,
                SPListItemId = SPListItemId

            };
            return new CusomActionHandler(request);

        }
    }
}
