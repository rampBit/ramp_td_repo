﻿

using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TeraDAct.CustomActionContextMenuAppWeb;

namespace CustomActionContextMenuAppWeb.Models
{
    internal enum SharePointRequestActionType
    {
        Check,
        CreateReview,
        CreateReviewSession,
        ReleaseReview,
        Share,
        View,
        Download,
        ApplyRuleset,
        Redact,
    }
    internal class CusomActionHandler : ICusomActionHandler
    {
        private readonly string ActionMethodName = "ProcessFile";
        private readonly string ForwardSlash = "/";

        public CusomActionHandler(CustomActionRequestedData request)
        {
            
            this.customActionRequestedData = request;
             
        }

        public CustomActionRequestedData customActionRequestedData { get; private set; }

        public async Task<string> ProcessCustomAction(SharePointRequestActionType RequestActionType)
        {
           
            string ftURL = $"{Utilty.GetFileTransferServiceURL()}{ForwardSlash}{ActionMethodName}{ForwardSlash}";
            ftURL += $"?SPListId={this.customActionRequestedData.SPListId}&SPListItemId={this.customActionRequestedData.SPListItemId}&SPHostUrl={this.customActionRequestedData.SPHostUrl}&ActionType={RequestActionType.ToString()}";
          //  LogWriter.LogWrite(ftURL);
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(ftURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    var responseMessage = await client.GetAsync(ftURL);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                       return  responseMessage.Content.ReadAsStringAsync().Result;
                       
                    }

                }
                catch (Exception ex)
                {
                   // LogWriter.LogWrite(ex.Message);
                    throw ex;

                }
                return null;
            }
            

        }
    }

    internal interface ICusomActionHandler
    {
        Task<string> ProcessCustomAction (SharePointRequestActionType RequestActionType);
    }
}