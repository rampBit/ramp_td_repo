﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TeraDAct.CustomActionContextMenuAppWeb;

namespace CustomActionContextMenuAppWeb.Models
{
    internal class CustomActionRequestedData
    {

        public string SPListId { get; set; }
        public string SPListItemId { get; set; }
        public string SPHostUrl { get; set; }
    }
    internal static class Utilty
    {
        private static string ftURL;

        internal static string GetFileTransferServiceURL()
        {
            if (null != ftURL) return ftURL;
            try
            {
                string ConnectionString = ConfigurationManager.AppSettings["DBTeraDActConfiguration"];
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand("usp_GetSharePointSettingsDetails", con))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                        if (reader.Read())
                            ftURL = reader["SPFileTransferServiceURL"] as string;

                    }
                }
            }
            catch (Exception ex)
            {
                
              //  LogWriter.LogWrite(ex.Message);

            }
            return ftURL;

        }
    }
}