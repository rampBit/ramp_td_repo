﻿'use strict';
var baseURL = "http://teradactfiletransferserviceapi.azurewebsites.us/api/TeraDActFileSericeAPI/";

var response;
function openDialog(ActionType) {
    var options = SP.UI.$create_DialogOptions();
    options.title = ActionType;
    options.width = 600;
    options.height = 400;
    options.html = document.getElementById('message');

    SP.UI.ModalDialog.showModalDialog(options);
}

function OpenDialogReview(ActionType) {
    var options = {
        url: "http://52.205.42.150:8234/home.htm?sessionType=review&token=10fe345b-eeda-457f-aae2-2cadacfb8331#view=review&review=40",
        width: document.body.offsetWidth,
        height: document.body.offsetHeight,
        dialogReturnValueCallback: DialogCallback
    };
    SP.UI.ModalDialog.showModalDialog(options);
}
function DialogCallback(dialogResult, returnValue) {
     
    location.reload();
}
function InvokeAPI(url, successHandler, jsonData,method) {
   // var context = SP.ClientContext.get_current();
   // var request = new SP.WebRequestInfo();
   ////  request.set_headers({ "Accept": "application/json;odata=verbose" });
   // request.set_url(
   //   url
   //    );

   // request.set_method("GET");
   //   response = SP.WebProxy.invoke(context, request);
    // context.executeQueryAsync(success, errorHandler);

    $.ajax({
        url: "../_api/SP.WebProxy.invoke",
        type: "POST",
        data: JSON.stringify(
            {
                "requestInfo": {
                    "__metadata": { "type": "SP.WebRequestInfo" },
                    "Url": url,
                    "Method": method,
                    "Headers": {
                        "results": [{
                            "__metadata": { "type": "SP.KeyValue" },
                            "Key": "Accept",
                            "Value": "application/json;odata=verbose",
                            "ValueType": "Edm.String"
                        },
                            {
                                "__metadata": { "type": "SP.KeyValue" },
                                "Key": "Content-Type",
                                "Value": "application/json;odata=verbose",
                                "ValueType": "Edm.String"
                            }]
                    },
                    "Body": jsonData
                }
            }),
        headers: {
            "Accept": "application/json;odata=verbose",
            "Content-Type": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val()
        },
        success: successHandler,
        error: errorHandler
    });
   
}
function getAllSites() {
    InvokeAPI(baseURL + "getAllSites", success, null, "GET");
    function success(data) {
        if (data.d.Invoke.StatusCode == 200) {
            var responseData = JSON.parse(data.d.Invoke.Body);
            var ddlSiteList = $("[id*=ddlSiteList]");
            $.each(responseData, function (key, value) {
                ddlSiteList.append('<option value="' + value  + '">' + value + '</option>');
            });
            // ddlSiteList.val(-1);
            ddlSiteList.val(GetUrlKeyValue('SPHostUrl'));
            ddlSiteList.trigger('change');
        }
        else {

            var errordesc;
            errordesc = "<P>Status code: " +
                data.d.Invoke.StatusCode + "<br/>";

            alert(errordesc);
        }
    }
}
function getDocumentLibList(SPHostURL) {
    var url = baseURL + "getDocmentLibraryList?SPHostURL=" + SPHostURL;
    console.log(url);
    InvokeAPI(url, success, null, "GET");
     
    function success(data) {
        if (data.d.Invoke.StatusCode == 200) {
            var responseData = JSON.parse(data.d.Invoke.Body);
            console.log(responseData);
            
            var ddlDLList = $("[id*=ddlDLList]");
            ddlDLList.empty();
            $.each(responseData, function (key,value) {
                ddlDLList.append('<option value="' + value + '">' + value + '</option>');
            });
            ddlDLList.append('<option value="Other">' + "Other" + '</option>');
            ddlDLList.multiselect('rebuild');
        }
        else {
            var errordesc;
            errordesc = "<P>Status code: " +
                data.d.Invoke.StatusCode + "<br/>";
            alert(errordesc);
        }
    }
}
function getRuleSet() {
    InvokeAPI(baseURL+"getListRuleSetsFromTDServer", success, null, "GET");
    function success(data) {
        if (data.d.Invoke.StatusCode == 200) {
            var responseData = JSON.parse(data.d.Invoke.Body);
            var ddlruleset = $("[id*=ddlRuleSet]");
            $.each(responseData.RulesetMessageList.messageList.message, function (key, value) {
                ddlruleset.append('<option value="' + value.id + '">' + value.name + '</option>');
            });
            ddlruleset.multiselect('rebuild');
        }
        else
        {

            var errordesc;
            errordesc = "<P>Status code: " +
                data.d.Invoke.StatusCode + "<br/>";
             
            alert( errordesc);
        }
       
    }
    
    }
 
    function errorHandler() {
        alert(arguments[1]);
    }
    function dispCreateReviewOut(resp) {
        var msg = "Please perform first TeraDAct Check followed by Create Review and then Release Review Actions";
        if (resp != "") {
            var spl = resp.split(/\\r\\n/g);
            var link = spl[1];
            msg = "<a href='" + link + "'>Click here to check the reviewed Document .<br/> Session ID:" + spl[0] + "</a> ";
        }
        $("#message").html(msg);
        
    }
    function dispReleaseReviewOut(SPHostURL, RedactedDocumentLibraryName,resp) {

        var msg = "Please perform first TeraDAct Check followed by Create Review and then Release Review Actions";
        if (resp != "") {
            msg = "";
            var arr = RedactedDocumentLibraryName.split(',');
            $.each(arr, function (n, val) {
            var link = SPHostURL + "/" + val;
            msg += "<a href='" + link + "'>" + val + "   :Please check Redacted doucment here!</a><br/> ";
            
            });
        }
        
            $("#message").html(msg);
        
        
    }
    
function InvokeTeraDActCustomAction(teraDActrequest) {

     
    //var SPListId = "SPListId=" + teraDActrequest.SPListId;
    //var SPListItemId = "SPListItemId=" + teraDActrequest.SPListItemId;
    //var SPHostURL = "SPHostURL=" + teraDActrequest.SPHostURL;
    //var ActionType = "ActionType=" + teraDActrequest.ActionType;
    var actionurl = baseURL + "ProcessFile"; //+"?";
    //actionurl += SPListId + "&" + SPListItemId + "&" + SPHostURL + "&" + ActionType;
   console.log(actionurl);
    // InvokeAPI(baseURL + "getListRuleSetsFromTDServer", success, null, "GET");
    var data = JSON.stringify(teraDActrequest);
    console.log(data);
     
    InvokeAPI(actionurl  , success, data, "POST");
    function success(data) {
        var resp = data.d.Invoke.Body;
        resp = resp.replace(/"/g, "");

        console.log(data);
        switch (teraDActrequest.SharePointRequestActionType) {
            case "Check": {
                if(resp=="")
                    resp="RuleSet Applied Successfully."
                $('#message').text(resp); break;
            }
          case "CreateReview":
              {
                  
                  dispCreateReviewOut(resp);
                  break;
              }
            case "ReleaseReview":
            case "Redact":
                {

                    dispReleaseReviewOut(teraDActrequest.SourceRedactedSiteURL, teraDActrequest.RedactedDocumentLibraryName, resp);
                    break;
                }
            default: $('#message').text(resp);
        }
       
        openDialog(teraDActrequest.SharePointRequestActionType);
    }
    
}