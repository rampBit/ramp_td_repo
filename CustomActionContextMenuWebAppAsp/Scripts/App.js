﻿'use strict';

ExecuteOrDelayUntilScriptLoaded(initializePage, "sp.js");

function initializePage()
{
    var context = SP.ClientContext.get_current();
    var user = context.get_web().get_currentUser();
     
    // This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
    $(document).ready(function () {
        
        var spHostUrl = GetUrlKeyValue('SPHostUrl');
        
        test();

        
    });
    
    function test() {

        var context = SP.ClientContext.get_current();
        var request = new SP.WebRequestInfo();
        request.set_url(
            "http://teradactfiletransferserviceapi.azurewebsites.us/api/TeraDActFileSericeAPI/Hello"
            );
        request.set_method("GET");

        request.set_headers({ "Accept": "application/json;odata=verbose" });
        var response = SP.WebProxy.invoke(context, request);

        context.executeQueryAsync(successHandler, errorHandler);

        function successHandler() {

            if (response.get_statusCode() == 200) {
                // Load the OData source from the response.
                var responseData = JSON.parse(response.get_body());
                // log this to console so you can inspect it
                console.log(responseData)
            }
            else {
                var errordesc = "Status code: " + response.get_statusCode();
                errordesc += "\n" + response.get_body();
                console.log(errordesc)
            }
        }
        function errorHandler() {
            console.log(response.get_body());
        }
    }
    //function CallService() {
    //    var exec;
    //    var appweburl = GetUrlKeyValue("SPAppWebUrl");
    //    exec = new SP.RequestExecutor(appweburl);
    //    exec.executeAsync(
    //        {
    //            url: "http://teradactfiletransferserviceapi.azurewebsites.us/api/TeraDActFileSericeAPI/Hello",
    //            type: "GET",
    //            crossDomain: true,
    //            contentType: "application/json; charset=utf-8",
    //            dataType: "jsonp",
    //           // headers: { "Accept": "application/json; odata=verbose" },
    //            success: function (msg) {
    //                alert(msg);
    //            },
    //            error: function (data, errorCode, errorMessage) { alert('Error: ' + errorMessage); }
    //        });
    //}
    //function execCrossDomainRequest() {
    //    var context;
    //    var factory;
    //    var appContextSite;
    //    var appweburl = GetUrlKeyValue("SPAppWebUrl");
    //    context = new SP.ClientContext(appweburl);
    //    factory = new SP.ProxyWebRequestExecutorFactory(appweburl);
    //    context.set_webRequestExecutorFactory(factory);
    //    appContextSite = new SP.AppContextSite(context, hostweburl);

    //    var executor = new SP.RequestExecutor(appweburl);

    //    executor.executeAsync(
    //        {
    //            url:
    //               "http://teradactfiletransferserviceapi.azurewebsites.us/api/TeraDActFileSericeAPI/Hello",
    //            method: "GET",
    //            headers: { "Accept": "application/json; odata=verbose" },
    //            success: successHandler,
    //            error: errorHandler
    //        }
    //    );
    //}
   
    // This function prepares, loads, and then executes a SharePoint query to get the current users information
    function getUserName() {
        context.load(user);
        context.executeQueryAsync(onGetUserNameSuccess, onGetUserNameFail);
    }

    // This function is executed if the above call is successful
    // It replaces the contents of the 'message' element with the user name
    function onGetUserNameSuccess() {
        $('#message').text('Hello ' + user.get_title());
    }

    // This function is executed if the above call fails
    function onGetUserNameFail(sender, args) {
        alert('Failed to get user name. Error:' + args.get_message());
    }
}
