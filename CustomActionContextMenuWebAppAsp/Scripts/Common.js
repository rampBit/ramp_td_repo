﻿'use strict';
var baseURL = "http://teradactfiletransferserviceapi.azurewebsites.us/api/TeraDActFileSericeAPI/";
//http://teradactfiletransferserviceapi.azurewebsites.us/api/TeraDActFileSericeAPI/
var success = "success";
var response;
function openDialog(ActionType) {
    var options = SP.UI.$create_DialogOptions();
    options.title = ActionType;
    options.width = 600;
    options.height = 400;
    options.html = document.getElementById('message');

    SP.UI.ModalDialog.showModalDialog(options);
}

function OpenDialogReview(ActionType) {
    var options = {
        url: "http://52.205.42.150:8234/home.htm?sessionType=review&token=10fe345b-eeda-457f-aae2-2cadacfb8331#view=review&review=40",
        width: document.body.offsetWidth,
        height: document.body.offsetHeight,
        dialogReturnValueCallback: DialogCallback
    };
    SP.UI.ModalDialog.showModalDialog(options);
}
function DialogCallback(dialogResult, returnValue) {
     
    location.reload();
}

 
function InvokeAPI(url, successHandler, jsonData, method) {
     
    
    $.ajax({
        type: method,
        url: url,
        dataType: "json",
        data:jsonData,
        success: successHandler,
        error:errorHandler
    })

}

function getAllSites() {
    InvokeAPI(baseURL + "getAllSites", success, null, "GET");
    function success(res){
        var ddlSiteList = $("[id*=ddlSiteList]");
        $.each(res, function (key, value) {
            ddlSiteList.append('<option value="' + value + '">' + value + '</option>');
        });
        ddlSiteList.val(decodeURIComponent(getQueryStringParameter('SPHostUrl')));
      //  getDocumentLibList(decodeURIComponent(getQueryStringParameter('SPHostUrl')));
        //ddlSiteList.trigger('change');
        $('#ddlSiteList').trigger('change');
    }
    
}
function getDocumentLibList(SPHostURL) {
    var url = baseURL + "getDocmentLibraryList?SPHostURL=" + SPHostURL;
    InvokeAPI(url, success, null, "GET");

     function success(res){
        
        var ddlDLList = $("[id*=ddlDLList]");
        $.each(res, function (key, value) {
            ddlDLList.append('<option value="' + value + '">' + value + '</option>');
        });
        ddlDLList.append('<option value="Other">' + "Other" + '</option>');
        ddlDLList.multiselect('rebuild');
    }
     
   
}
function getRuleSet() {
    
    InvokeAPI(baseURL + "getListRuleSetsFromTDServer", success,null,"GET");


     
    function success(data) {
        console.log(data)
         
            var responseData =data;
            var ddlruleset = $("[id*=ddlRuleSet]");
            $.each(responseData.RulesetMessageList.messageList.message, function (key, value) {
                ddlruleset.append('<option value="' + value.id + '">' + value.name + '</option>');
            });
            ddlruleset.multiselect('rebuild');
        
    }
}
    function errorHandler() {
        alert(arguments[1]);
    }
    function dispCreateReviewOut(resp) {
        var msg = "Please perform first TeraDAct Check followed by Create Review and then Release Review Actions";
        if (resp != "") {
            var spl = resp.split(/\\r\\n/g);
            var link = spl[1];
            msg = "<a href='" + link + "'>Click here to check the reviewed Document .<br/> Session ID:" + spl[0] + "</a> ";
        }
        $("#message").html(msg);
        
    }
    function dispReleaseReviewOut(SPHostURL, RedactedDocumentLibraryName,resp) {

        var msg = "Please perform first TeraDAct Check followed by Create Review and then Release Review Actions";
        if (resp != "") {
            msg = "";
            var arr = RedactedDocumentLibraryName.split(',');
            $.each(arr, function (n, val) {
            var link = SPHostURL + "/" + val;
            msg += "<a href='" + link + "'>" + val + "   :Please check Redacted doucment here!</a><br/> ";
            
            });
        }
        
            $("#message").html(msg);
        
        
    }
    
function InvokeTeraDActCustomAction(teraDActrequest) {

     
    //var SPListId = "SPListId=" + teraDActrequest.SPListId;
    //var SPListItemId = "SPListItemId=" + teraDActrequest.SPListItemId;
    //var SPHostURL = "SPHostURL=" + teraDActrequest.SPHostURL;
    //var ActionType = "ActionType=" + teraDActrequest.ActionType;
    var actionurl = baseURL + "ProcessFile"; //+"?";
    //actionurl += SPListId + "&" + SPListItemId + "&" + SPHostURL + "&" + ActionType;
   console.log(actionurl);
    // InvokeAPI(baseURL + "getListRuleSetsFromTDServer", success, null, "GET");
    var data = JSON.stringify(teraDActrequest);
    console.log(data);
     
    InvokeAPI(actionurl, success, data, "POST");
    function success(data) {
        var resp = data ;
        resp = resp.replace(/"/g, "");
        
        console.log(data);
        switch (teraDActrequest.SharePointRequestActionType) {
            case "Check": {
                if(resp=="")
                    resp="RuleSet Applied Successfully."
                $('#message').text(resp); break;
            }
          case "CreateReview":
              {
                  
                  dispCreateReviewOut(resp);
                  break;
              }
            case "ReleaseReview":
            case "Redact":
                {

                    dispReleaseReviewOut(teraDActrequest.SourceRedactedSiteURL, teraDActrequest.RedactedDocumentLibraryName, resp);
                    break;
                }
            default: $('#message').text(resp);
        }
       
        openDialog(teraDActrequest.SharePointRequestActionType);
    }
    
}