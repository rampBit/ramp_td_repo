﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomEventHandler
{

    

    public class CustomEventArgs<T> : EventArgs
    {

        private T item;

        public CustomEventArgs(T item)
        {
            this.item = item;
        }

        public T Item
        {
            get { return item; }
        }
    }
    //public class CustomEventArgs<T> : EventArgs
    //{
    //    public CustomEventArgs(string s)
    //    {
    //        message = s;
    //    }
    //    private string message;

    //    public string Message
    //    {
    //        get { return message; }
    //        set { message = value; }
    //    }
    //}

    //public class CustomEventArgs : EventArgs
    //{
    //    public CustomEventArgs(string s)
    //    {
    //        message = s;
    //    }
    //    private string message;

    //    public string Message
    //    {
    //        get { return message; }
    //        set { message = value; }
    //    }
    //}


}


