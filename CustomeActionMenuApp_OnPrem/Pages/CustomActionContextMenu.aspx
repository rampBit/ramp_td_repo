﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-3.1.1.min.js"></script>
    <SharePoint:ScriptLink Name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
    <meta name="WebPartPageExpansion" content="full" />
    <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/prettify.css" />
    <link rel="Stylesheet" type="text/css" href="../Content/App.css" />
    <link rel="Stylesheet" type="text/css" href="../Content/jquery.multiselect.css" />
    <link rel="stylesheet" href="../Content/bootstrap-example.css" type="text/css" />
    <link rel="stylesheet" href="../Content/bootstrap-3.3.2.min.css" type="text/css" />
    <!-- Add your JavaScript to the following file -->
    <script type="text/javascript" src="../Scripts/bootstrap-3.3.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/CustomAction.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.multiselect.js"></script>
    <script type="text/javascript" src="../Scripts/Common.js"></script>
    <script type="text/javascript" src="../Scripts/prettify.js"></script>
   



</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<%--<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Page Title
</asp:Content>--%>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <%-- <div id="content">
        <iframe id="iframe" frameborder="0" height="400px" width="500px"
            src="http://52.205.42.150:8234/"></iframe>
     </div>--%>
     
   
        <div id="selectedAction" style="align-content:center; text-align:center;font-size:40px;"   ></div>
   
    <div class="row" style=" background: #d1e6e4; padding: 20px;" >
       
        <div class="col-md-3"></div>
        <div class="col-md-5" style="border: 1px solid;">
            <div class="row well " style="margin-bottom:0px;">
                <div class="col-md-12" id="divFileOperationMode">
                 
                    <div class="row">

                        <div class="col-md-3">
                        </div>
                        <div class="col-md-9">
                            <input type="checkbox" style="/* width: 200px; */" name="postage" value="Move" id="dltorg"><label for="dltorg" style="font-weight: 100; margin-bottom: 0; margin-left: 10px;"><strong>Delete Original After Redaction</strong></label>
                        </div>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="col-md-12" id="divSiteCollection">
                    <div class="row">
                        <div class="col-md-3">
                            
                            <h6><strong>Sites Collection</strong></h6>
                        </div>
                        <div class="col-md-9">
                            <select id="ddlSiteList"></select>
                        </div>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="col-md-12" id="divTarget">
                    <div class="row">
                        <div class="col-md-3">
                            <h6><strong>Document Library</strong></h6>
                        </div>
                        <%--<div class="col-md-6">
                            <h6> 
                                <select id="ddlSiteList"    ></select></h6>
                        </div>--%>
                        <div class="col-md-9">

                            <select id="ddlDLList" multiple="multiple" class="multiselect"></select>

                            <input class="AdmininputFieldSize format-label" type="text"
                                id="RedactedDocumentLibraryName" onkeypress="return event.keyCode != 13;">
                        </div>
                    </div>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="col-md-12" id="divRoles">
                    <div class="row">
                        <div class="col-md-3">
                            <h6><strong>Exemptions</strong></h6>
                        </div>
                        <div class="col-md-9">
                            <select id="ddlRoles" multiple="multiple" 
                                class="multiselect"></select>
                        </div>
                    </div>
                </div>

                <div class="clearfix">&nbsp;</div>
                <div class="col-md-12" id="divRuleSet">
                    <div class="row">
                        <div class="col-md-3">
                            <h6><strong>RuleSet</strong></h6>
                        </div>
                        <div class="col-md-9">
                            <select id="ddlRuleSet" multiple="multiple" class="multiselect"></select>
                        </div>
                    </div>
                </div>
               
                <div class="clearfix">&nbsp;</div>
                <div class="col-md-12">
                    <button class="btn btn-primary" type="button" id="btnOK">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div id="message">
            <!-- The following content will be replaced with the user name when you run the app - see App.js -->

        </div>
    </div>
    
   <%--<%-- <script type="text/javascript">
        url = parent.document.URL;
        document.write('<iframe src="http://example.com/mydata/page.php?url=' + url + '"></iframe>');
    </script>--%>
  
</asp:Content>
