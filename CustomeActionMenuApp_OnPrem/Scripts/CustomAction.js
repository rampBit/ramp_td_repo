﻿'use strict';

ExecuteOrDelayUntilScriptLoaded(initializePage, "sp.js");
// read URL parameters

function initializePage() {
    var userName;
    var context = SP.ClientContext.get_current();
    var user = context.get_web().get_currentUser();
    getUserName();
    //console.log(context);
    // This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
    $(document).ready(function () {
        //$("#sideNavBox").css("display", "none");
        //$("#contentBox").css("margin-left", "20px");
        //$("#s4-titlerow").css("display", "none");
        //$("#s4-ribbonrow").css("display", "none");
        //$("#suiteBarDelta").css("display", "none");

        $(".ms-siteicon-img").attr('src', '../Images/teradactlogo.jpg');

        $('#ddlRuleSet').multiselect({
            includeSelectAllOption: true,
            maxHeight: 250,
            buttonWidth: 150,
            numberDisplayed: 3,
            SelectedText: 'selected',
            nonSelectedText: 'Select RuleSet'
        });
        $('#ddlDLList').multiselect({

            enableFiltering: true,
            nonSelectedText: 'Document Library'

        });
        $('#ddlRoles').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            nonSelectedText: 'No Exemptions'

        });

        hideControl();

        $("#btnOK").click(function () {
            HandleCustomAction();
        });
    });
    $('#ddlSiteList').change(function () {

        getDocumentLibList($(this).val());

    });
    $('#ddlDLList').change(function () {

        var otherv = ($(this).val());
        if (otherv == "Other") $("#RedactedDocumentLibraryName").show();
        else if (otherv.indexOf("Other") >= 0) {
            $("#ddlDLList").val('');
            $("#ddlDLList option:selected").prop("selected", false);
            $("#ddlDLList").multiselect('refresh');
            $("#RedactedDocumentLibraryName").show();
        }
        else {
            $("#RedactedDocumentLibraryName").hide();
            $("#RedactedDocumentLibraryName").val('');

        }
    });
    function hideControl() {
        var atype = GetUrlKeyValue("ActionType");
        $('#selectedAction').html(atype + ' Action');
        $('#divFileOperationMode').hide();
        $('#divTarget').hide();
        $('#divSiteCollection').hide();
        $('#divRoles').hide();
        $("#RedactedDocumentLibraryName").hide();
        switch (atype) {
            case "Check":
                {
                    getRuleSet();
                    $('#ddlDLList').hide();
                    $('#ddlSiteList').hide();

                    break;
                }
            case "CreateReview":
                {
                    $('.row').hide();
                    setTimeout(function () { $('#btnOK').click() }, 100);
                    break;
                }
            case "ReleaseReview":
                {
                   // getRuleSet();
                    getAllSites();
                    getAllRoles();
                    $('#divFileOperationMode').show();
                    $('#divTarget').show();
                    $('#divSiteCollection').show();
                    $('#divRoles').show();
                    $('#divRuleSet').hide();
                    break;

                   
                }
            case "Redact":
                {
                    getRuleSet();
                    getAllSites();
                    getAllRoles();
                    $('#divFileOperationMode').show();
                    $('#divTarget').show();
                    $('#divSiteCollection').show();
                    $('#divRoles').show();
                    break;
                }
            case "Share":
                {

                    $('#divRuleSet').hide();
                    $('#ddlDLList').hide();
                    getAllSites();
                    $('#divSiteCollection').show();
                    var ddlSiteList = $("[id*=ddlSiteList]");


                    ddlSiteList.off('change');

                    break;
                }
            case "SharedView":
                {

                   $('.row').hide();
                   $('#divRuleSet').hide();
                   var intr = setInterval(function () {
                        
                       if (userName != undefined) {
                           setTimeout(function () { $('#btnOK').click() }, 100);
                           clearInterval(intr);
                       }
                   }, 1000);

                   
                    break;
                }
            case "SharedRelease":
                {
                    $('#divRuleSet').hide();
                    getAllSites();
                    $('#divFileOperationMode').show();
                    $('#divTarget').show();
                    $('#divSiteCollection').show();
                    break;
                }
        }

    }
     
    function getSelectedDocumentLibNames() {
        var selectedDL = $("#ddlDLList option:selected");
        var DLarray = [];
        selectedDL.each(function () {
            DLarray.push($(this).text());
        });
        if ($("#RedactedDocumentLibraryName").val() != "")
            DLarray.push($("#RedactedDocumentLibraryName").val());

        DLarray = DLarray.filter(function (elem) {
            return elem != 'Other';
        });
        return DLarray.join();
    }
    function getSelectedRuleSet() {
        var selectedruleset = $("#ddlRuleSet option:selected");
        var rulesetarray = [];
        selectedruleset.each(function () {
            rulesetarray.push($(this).text());
        });

        return rulesetarray.join();

    }
    function getSelectedRoles() {
        var selectedroles = $("#ddlRoles option:selected");  //$('#ddlRoles option').not(':selected'); 
        var rolesarray = [];
        selectedroles.each(function () {
            rolesarray.push($(this).text());
        });

        return rolesarray.join();

    }
    function getRedactedFileOperationMode() {

        return $("input:checkbox[name='postage']:checked").val();

    }
    function HandleCustomAction() {

        var request = {
            SPHostURL: GetUrlKeyValue('SPHostUrl'),
            SPListId: GetUrlKeyValue('SPListId'),
            SharePointRequestActionType: GetUrlKeyValue("ActionType"),
            SPListItemId: GetUrlKeyValue("SPListItemId"),
            RuleSetName: getSelectedRuleSet(),
            RedactedDocumentLibraryName: getSelectedDocumentLibNames(), //$('#RedactedDocumentLibraryName').val(),
            FileOperationMode: getRedactedFileOperationMode(),
            SourceRedactedSiteURL: $('#ddlSiteList').val(),
            UserName: userName.replace(/\\/g, 'backslash'),   //for onprem
            SPAppWebUrl: GetUrlKeyValue('SPAppWebUrl'),
            Roles: getSelectedRoles()
        };
        var atype = GetUrlKeyValue("ActionType");
        switch (atype) {
            case "Share": {
               // request.RedactedDocumentLibraryName = "SharedTeradactLib";
                break;
            }

        }
        InvokeTeraDActCustomAction(request);
    }
    //function getQueryStringParameter(param) {
    //    alert(document.URL);
    //    var params = document.URL.split("?")[1].split("&");
    //    var strParams = "";
    //    for (var i = 0; i < params.length; i = i + 1) {
    //        var singleParam = params[i].split("=");
    //        alert(singleParam[0]);
    //        if (singleParam[0] == param) {
    //            return singleParam[1];
    //        }
    //    }
    //}
    //function test() {

    //    var context = SP.ClientContext.get_current();
    //    var request = new SP.WebRequestInfo();
    //    var remoteAppUrl = $('.remoteAppUrlValue').val()
    //    alert(remoteAppUrl);
    //    request.set_url(
    //       remoteAppUrl + "/Hello"
    //        );
    //    request.set_method("GET");

    //    request.set_headers({ "Accept": "application/json;odata=verbose" });
    //    var response = SP.WebProxy.invoke(context, request);

    //    context.executeQueryAsync(successHandler, errorHandler);

    //    function successHandler() {

    //        if (response.get_statusCode() == 200) {
    //            // Load the OData source from the response.
    //            var responseData = JSON.parse(response.get_body());
    //            // log this to console so you can inspect it
    //            console.log(responseData)
    //        }
    //        else {
    //            var errordesc = "Status code: " + response.get_statusCode();
    //            errordesc += "\n" + response.get_body();
    //            console.log(errordesc)
    //        }
    //    }
    //    function errorHandler() {
    //        console.log(response.get_body());
    //    }
    //}
    //function CallService() {
    //    var exec;
    //    var appweburl = GetUrlKeyValue("SPAppWebUrl");
    //    exec = new SP.RequestExecutor(appweburl);
    //    exec.executeAsync(
    //        {
    //            url: "http://teradactfiletransferserviceapi.azurewebsites.us/api/TeraDActFileSericeAPI/Hello",
    //            type: "GET",
    //            crossDomain: true,
    //            contentType: "application/json; charset=utf-8",
    //            dataType: "jsonp",
    //           // headers: { "Accept": "application/json; odata=verbose" },
    //            success: function (msg) {
    //                alert(msg);
    //            },
    //            error: function (data, errorCode, errorMessage) { alert('Error: ' + errorMessage); }
    //        });
    //}
    //function execCrossDomainRequest() {
    //    var context;
    //    var factory;
    //    var appContextSite;
    //    var appweburl = GetUrlKeyValue("SPAppWebUrl");
    //    context = new SP.ClientContext(appweburl);
    //    factory = new SP.ProxyWebRequestExecutorFactory(appweburl);
    //    context.set_webRequestExecutorFactory(factory);
    //    appContextSite = new SP.AppContextSite(context, hostweburl);

    //    var executor = new SP.RequestExecutor(appweburl);

    //    executor.executeAsync(
    //        {
    //            url:
    //               "http://teradactfiletransferserviceapi.azurewebsites.us/api/TeraDActFileSericeAPI/Hello",
    //            method: "GET",
    //            headers: { "Accept": "application/json; odata=verbose" },
    //            success: successHandler,
    //            error: errorHandler
    //        }
    //    );
    //}

    // This function prepares, loads, and then executes a SharePoint query to get the current users information
    function getUserName() {

        context.load(user);
        context.executeQueryAsync(onGetUserNameSuccess, onGetUserNameFail);
    }

    // This function is executed if the above call is successful
    // It replaces the contents of the 'message' element with the user name
    function onGetUserNameSuccess() {
        userName = user.get_title();
    }

    // This function is executed if the above call fails
    function onGetUserNameFail(sender, args) {
        alert('Failed to get user name. Error:' + args.get_message());
    }
}
