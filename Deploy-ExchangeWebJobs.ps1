function execute(){
#$files=Get-ChildItem "C:\Users\hemanth\Downloads"
#setAzureSubscription
createPackage 'F:\'
#deploy
}

function createPackage($basePath){ 
   $path = '{0}/{1}' -f $basePath, "ExchangeWEBjob_DeployPackage"   
   $zipPath = '{0}/{1}' -f  $basePath,'ExchangeWEBjob_DeployPackage.zip'
   Add-Type -Assembly System.IO.Compression.FileSystem 
   $compressionLevel = [System.IO.Compression.CompressionLevel]::Optimal
    [System.IO.Compression.ZipFile]::CreateFromDirectory($path, $zipPath,
                                                          $compressionLevel, 
                                                          $false) 
    Write-Host "$(Get-Date �f $timeStampFormat) - Zip Package Created " -foregroundcolor "green"
    if($compressionLevel){
    return $zipPath;
    }
    else{
    return 0;
    }
}

function setAzureSubscription(){
    add-AzureAccount -Environment "AzureGovernment"
    Select-AzureSubscription -SubscriptionName "TeradactSubcription" | Out-Null
    Write-Host "$(Get-Date �f $timeStampFormat) - Set Azure subscription Completed " -foregroundcolor "green"
}
function WebjobDep($zipPath){
  New-AzureWebsiteJob -Name "ExchangeJobDeployment" -JobName Ver6TeradactWejob -JobType Continuous -JobFile $zipPath | Out-Null
}

function deploy(){
$basePath='F:\'
    #createpackage $basePath
    $zipPath= createpackage $basePath
    setAzureSubscription
    WebjobDep $zipPath
    #"F:\Release_Ver6TeradactWejob.zip"   
  
    Write-Host "$(Get-Date �f $timeStampFormat) - Completed Deployment " -foregroundcolor "green"
}
#Connect-SPOService -Url https://teradactcel-admin.sharepoint.com/ -credential admin4@teradactcel.onmicrosoft.com 



