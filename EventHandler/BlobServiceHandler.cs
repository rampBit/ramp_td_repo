﻿using BusinessObjects;
using CustomEventHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeraDActEventHandler
{

    public class BlobServiceHandler : IBlobServiceHandler
    {

        public event EventHandler BlobServiceEvent_BlobToBlobRedact;
        // public event EventHandler<CustomEventArgs<FileObject>> BlobServiceEvent_NonRedact;
        public event EventHandler BlobServiceEvent_Redact;
        private EventHandler handler = null;
        public event EventHandler BlobServiceEvent_GetSourceBlobItems;
        //  private EventHandler<CustomEventArgs<BlobConfig>> handler1 = null;
        public virtual void OnBlobServiceEvent_Redact()
        {
            handler = BlobServiceEvent_Redact;
            if (handler != null)
                handler(this, null);

        }
        public virtual void OnBlobServiceEvent_NonRedact(CustomEventArgs<FileObject> e)
        {
            //handler = BlobServiceEvent_NonRedact;
            //if (handler != null)
            //    handler(this, e);

        }
        public virtual void OnBlobServiceEvent_BlobToBlobRedact()
        {
            handler = BlobServiceEvent_BlobToBlobRedact;
            if (handler != null)
                handler(this, null);

        }

        public virtual void OnBlobServiceEvent_GetSourceBlobItems()
        {
            handler = BlobServiceEvent_GetSourceBlobItems;
            if (handler != null)
                handler(this, null);
        }
    }
}
