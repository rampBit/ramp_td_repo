﻿using BusinessObjects;
using CustomEventHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeraDActEventHandler
{

    public interface IBlobServiceHandler
    {

        event EventHandler BlobServiceEvent_Redact;
        // event EventHandler<CustomEventArgs<FileObject>> BlobServiceEvent_NonRedact;
        event EventHandler BlobServiceEvent_BlobToBlobRedact;
        event EventHandler BlobServiceEvent_GetSourceBlobItems;
    }
}