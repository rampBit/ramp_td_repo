﻿using BusinessObjects;
using CustomEventHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeraDActEventHandler
{
 public   class TeradactRedactEventHandler
    {
        public event EventHandler<CustomEventArgs<EmailMessageResponse>> RedactProcessing;
        private EventHandler<CustomEventArgs<EmailMessageResponse>> handler = null;

        public virtual void OnRedactProcessing(CustomEventArgs<EmailMessageResponse> e)
        {
            handler = RedactProcessing;
            if (handler != null)
                handler(this,e);

        }

    }
}
