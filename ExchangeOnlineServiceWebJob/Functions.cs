﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
//using Microsoft.Exchange.WebServices.Data;
using RampGroup.TeraDActBusinessLogic.ExchangeServiceRequests;
using BusinessObjects;
using Microsoft.WindowsAzure.Storage;
using System.Configuration;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using RampGroup.Config.Utility;
using System.Threading;
using AzureUtility;
using FileUtility;

using Common.Repository.SQLAzureDBUtility;

namespace ExchangeOnlineServiceWebJob
{

    public class Functions
    {
        
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        //public static void RedactThreadProc(string message)
        //{
        //    try
        //    {
        //        RedactProcess(message);

        //    }
        //    catch (Exception e)
        //    {
        //        Console.Error.WriteLine(">> Error: {0}", e);
        //    }
        //    finally
        //    {
        //        // release a slot for another thread
        //        semaphore_.Release();
        //    }
        //}
        //public static void RedactProcess(string message)
        //{

        //    Console.WriteLine("This is a web job invocation: Process Id: {0}, Thread Id: {1}.", System.Diagnostics.Process.GetCurrentProcess().Id, Thread.CurrentThread.ManagedThreadId);

        //    UserEmailMessageRequest req = new UserEmailMessageRequest();
        //    // EmailMessageResponse mail = JsonConvert.DeserializeObject<EmailMessageResponse>(message);

        //    req.ProcessWrite(message);  // for redacting




        //}
        //public static void ProcessQueueMessageForRedact([QueueTrigger(StringConstants.RedactEmailsQueue)] string message, TextWriter log)
        //{
        //    try
        //    {

        //        semaphore_.Wait();

        //        Thread thread = new Thread(() => RedactThreadProc(message));
        //        thread.Start();
        //        if (thread.IsAlive == false)
        //        {
        //            //send mail event calls
        //            Thread.Sleep(500);
        //            //var q = createQueue("sendmails");
        //            //q.AddMessage(new CloudQueueMessage(message));   //sending mail
        //            //UserEmailMessageRequest req = new UserEmailMessageRequest();
        //            //req.sendMail(message);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.Error.WriteLine(e);
        //    }


        //}

        //public static void ProcessQueueMessageForSendMail([QueueTrigger(StringConstants.SendEmailQueue)] string message, TextWriter log)
        //{
        //    try
        //    {
        //        UserEmailMessageRequest emailServiceRequest = new UserEmailMessageRequest();

        //        semaphore_.Wait();
        //        Thread thread = new Thread(() => emailServiceRequest.sendMail(message));
        //        thread.Start();

        //    }
        //    catch (Exception e)
        //    {
        //        Console.Error.WriteLine(e);
        //    }
        //    finally { semaphore_.Release(); }
        //}

        private const int MaxNumberOfThreads = 3;
        private static readonly SemaphoreSlim semaphore_ = new SemaphoreSlim(MaxNumberOfThreads, MaxNumberOfThreads);

        [NoAutomaticTriggerAttribute]
        public static async Task GetAllExchangeEmails()
        {
            while (true)
            {
                try
                {
                    UserEmailMessageRequest emailServiceRequest = new UserEmailMessageRequest();
                    AttachmentSampleServiceResponse allmail = null;
                    EmailQueueRepository _Repo = new EmailQueueRepository();

                    List<BusinessObjects.ExchangeServerSettings> settings = _Repo.getTenantEmail();
                    foreach (var setting in settings)
                    {
                        allmail = emailServiceRequest.ReadEmails(setting);
                        await Task.Run(() => AddtoQueue(allmail));
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogRepository.Instance.WriteErrorLog(ex , ErrorSource.ExchangeOnlineServiceWebJob);

                }
                string interval = ConfigurationManager.AppSettings["EmailPullingIntervalinSecond"] == null ? "60" : ConfigurationManager.AppSettings["EmailPullingIntervalinSecond"];
                Thread.Sleep(TimeSpan.FromSeconds(int.Parse(interval)));
            }
        }

        private static void AddtoQueue(AttachmentSampleServiceResponse allmail)
        {

            foreach (AttachmentDetails mail in allmail.attachments)
            {

                EmailQueueRepository QueueRepo = new EmailQueueRepository();
                EmailQueue emqueue = new EmailQueue
                {
                    MessageID = mail.id,
                    ExchangeAttachementEmailUniqueID = mail.ExchangeAttachementEmailUniqueID,
                    Status = RequestStatus.Requested,
                    ExchangeServerSettings = new BusinessObjects.ExchangeServerSettings
                    { EmailID = allmail.ExchangeServerSettings.EmailID }
                };

                QueueRepo.saveEmailQueue(emqueue);

                //moved to teradact webjob
            }
        }
    }
}
