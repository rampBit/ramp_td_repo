﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
//using Microsoft.Exchange.WebServices.Data;
using RampGroup.TeraDActBusinessLogic.ExchangeServiceRequests;
using BusinessObjects;
using System.Configuration;
using System.Threading;

using Common.Repository.SQLAzureDBUtility;

namespace ExchangeOnlineServiceWebJob
{

    public class Functions
    {
        
         

        private const int MaxNumberOfThreads = 3;
        private static readonly SemaphoreSlim semaphore_ = new SemaphoreSlim(MaxNumberOfThreads, MaxNumberOfThreads);

        [NoAutomaticTriggerAttribute]
        public static async Task GetAllExchangeEmails()
        {
            while (true)
            {
                try
                {
                 bool IsExchageOnPrem=bool.Parse(
                     ConfigurationManager.AppSettings["IsExchageOnPrem"] == null ? "true" :
                     ConfigurationManager.AppSettings["IsExchageOnPrem"]);

                    UserEmailMessageRequest emailServiceRequest = new UserEmailMessageRequest();
                    AttachmentSampleServiceResponse allmail = null;
                    EmailQueueRepository _Repo = new EmailQueueRepository();

                    List<BusinessObjects.ExchangeServerSettings> settings = _Repo.getTenantEmail(IsExchageOnPrem);
                    foreach (var setting in settings)
                    {
                        allmail = emailServiceRequest.ReadEmails(setting);
                        await Task.Run(() => AddtoQueue(allmail));
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogRepository.Instance.WriteErrorLog(ex , ErrorSource.ExchangeOnlineServiceWebJob);

                }
                string interval = ConfigurationManager.AppSettings["EmailPullingIntervalinSecond"] == null ? "60" : ConfigurationManager.AppSettings["EmailPullingIntervalinSecond"];
                Thread.Sleep(TimeSpan.FromSeconds(int.Parse(interval)));
            }
        }

        private static void AddtoQueue(AttachmentSampleServiceResponse allmail)
        {

            foreach (AttachmentDetails mail in allmail.attachments)
            {

                EmailQueueRepository QueueRepo = new EmailQueueRepository();
                EmailQueue emqueue = new EmailQueue
                {
                    MessageID = mail.id,
                    ExchangeAttachementEmailUniqueID = mail.ExchangeAttachementEmailUniqueID,
                    Status = RequestStatus.Requested,
                    ExchangeServerSettings = new BusinessObjects.ExchangeServerSettings
                    { EmailID = allmail.ExchangeServerSettings.EmailID }
                };

                QueueRepo.saveEmailQueue(emqueue);

                //moved to teradact webjob
            }
        }
    }
}
