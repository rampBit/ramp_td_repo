﻿using Microsoft.Azure.WebJobs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeOnlineServiceWebJob
{
    public abstract class WebJobBase
    {
        protected static JobHost GetJobHost(string webJobName)
        {
            JobHost host = new JobHost(GetConfiguration(webJobName));

            return host;
        }
        protected static JobHostConfiguration GetConfiguration(string webJobName)
        {
            JobHostConfiguration config = new JobHostConfiguration
            {
                DashboardConnectionString = "", //BlobStorageAccount.Settings.BlobStorageConnectionString,
                StorageConnectionString = ""// BlobStorageAccount.Settings.BlobStorageConnectionString
            };
            config.HostId = Guid.NewGuid().ToString("N");
           
            var host = new JobHost(config);
            return config;
        }

    }
}
