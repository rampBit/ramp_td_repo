﻿using Microsoft.Identity.Client;
using Microsoft.IdentityModel.Protocols;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Notifications;
using Microsoft.Owin.Security.OpenIdConnect;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Owin;
using RampGroup.Config.Utility;
using RampGroup.Logging;
using RampGroup.MX.Core;

using RampGroup.TeraDActBusinessLogic.ExchangeServiceRequests;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using TeraDActService.Logic;

using Microsoft.Azure;
using BlobBusinessLogic;
using TeraDActEventHandler;
using BusinessObjects;
using System.IO;
using CustomEventHandler;
using Common.Repository.SQLAzureDBUtility;
using System.Data.SqlClient;
using System.Data;
namespace ExchangeServiceTest
{
    class Program: BaseRepository<AdminRepository>
    {
        static void Main(string[] args)
        {

            Program _program = new Program();
          //  _program.GetSourceBlobSettingsDetails();
            //1.

            //BlobRequest request = new BlobRequest();
            //request.SourceBlob = new BlobConfig { DefaultEndpointsProtocol = "https", AccountName = "1234567890ajgx7abxnxun4", AccountKey = "EQ/Wh5AEnq+Lsy+9bKEhuFemFcIhTnAf4qGWdAfFsDbyTaqzKVq8Yv6DoD+vL9uHm2gALtMDP5JhB/zGKINpnA==", BlobStorageContainerName = "sourceblob" };
            //var blobService = new BlobLogic(  request);

            //var resp = blobService.GetAllContainerList();


            //// 2.

            //BlobRequest request = new BlobRequest();
            //request.SourceBlob = new BlobConfig
            //{
            //    DefaultEndpointsProtocol = "https",
            //    AccountName = "1234567890ajgx7abxnxun4",
            //    AccountKey = "EQ/Wh5AEnq+Lsy+9bKEhuFemFcIhTnAf4qGWdAfFsDbyTaqzKVq8Yv6DoD+vL9uHm2gALtMDP5JhB/zGKINpnA==",
            //};//sourceblob
            //request.BlobStorageContainers = new List<BlobStorageContainer>() {
            //     new BlobStorageContainer {  ContainerName=
            //     "sourceblob"},
            //       new BlobStorageContainer {  ContainerName=
            //     "redacted"}
            // };
            //var blobService = new BlobLogic( request);

            //var resp = blobService.DownloadBlob();


            // // 3. local upload

            BlobRequest request = new BlobRequest();
            request.SourceBlob = new BlobConfig
            {
                DefaultEndpointsProtocol = "https",
                AccountName = "1234567890ajgx7abxnxun4",
                AccountKey = "EQ/Wh5AEnq+Lsy+9bKEhuFemFcIhTnAf4qGWdAfFsDbyTaqzKVq8Yv6DoD+vL9uHm2gALtMDP5JhB/zGKINpnA==",

            };//
            var tmpc = new BlobStorageContainer
            {
                ContainerName =
                 "sourceblob"
            };
            request.BlobStorageContainers = new List<BlobStorageContainer>();
            request.BlobStorageContainers.Add(tmpc);

            var blobService = new BlobLogic(request);
            
            List<FileObject> uploadedfile = new List<FileObject>();
            using (StreamReader b = new StreamReader(@"F:\WorkingProjects\Teradact_new\Develop\XML Files\AdminSettings.xml"))
            {
                byte[] bytes = b.CurrentEncoding.GetBytes(b.ReadToEnd());
                uploadedfile.Add(new FileObject
                {
                    Content = bytes,
                    FileName = "AdminSettings.xml"
                });
            }
            tmpc.Blobs = uploadedfile;
            blobService.UploadBlobs();

            // 4. b2b upload
            // BlobRequest request = new BlobRequest();
            // request.SourceBlob = new BlobConfig
            // {
            //     DefaultEndpointsProtocol = "https",
            //     AccountName = "1234567890ajgx7abxnxun4",
            //     AccountKey = "EQ/Wh5AEnq+Lsy+9bKEhuFemFcIhTnAf4qGWdAfFsDbyTaqzKVq8Yv6DoD+vL9uHm2gALtMDP5JhB/zGKINpnA==",

            // };//
            // request.TargetBlobs = new List<BlobConfig>();

            // var target1=
            // new BlobConfig
            // {
            //     DefaultEndpointsProtocol = "https",
            //     AccountName = "1234567890ajgx7abxnxun4",
            //     AccountKey = "EQ/Wh5AEnq+Lsy+9bKEhuFemFcIhTnAf4qGWdAfFsDbyTaqzKVq8Yv6DoD+vL9uHm2gALtMDP5JhB/zGKINpnA==",
            //      BlobStorageContainerName= "targetblob2"
            // };//
            // var target2 =
            //new BlobConfig
            //{
            //    DefaultEndpointsProtocol = "https",
            //    AccountName = "1234567890ajgx7abxnxun4",
            //    AccountKey = "EQ/Wh5AEnq+Lsy+9bKEhuFemFcIhTnAf4qGWdAfFsDbyTaqzKVq8Yv6DoD+vL9uHm2gALtMDP5JhB/zGKINpnA==",
            //    BlobStorageContainerName = "targetblob1"
            //};//
            // request.TargetBlobs.Add(target1);
            // request.TargetBlobs.Add(target2);
            // var tmpc1 = new BlobStorageContainer
            // {
            //     ContainerName =
            //      "sourceblob"
            // };
            // var tmpc2 = new BlobStorageContainer
            // {
            //     ContainerName =
            //     "redactcontainer"
            // };
            // request.BlobStorageContainers = new List<BlobStorageContainer>();
            // request.BlobStorageContainers.Add(tmpc1);
            // request.BlobStorageContainers.Add(tmpc2);
            // var blobService = new BlobLogic(request);

            // List<FileObject> uploadedfile1 = new List<FileObject>();
            // using (StreamReader b = new StreamReader(@"F:\WorkingProjects\Teradact_new\Develop\XML Files\AdminSettings.xml"))
            // {
            //     byte[] bytes = b.CurrentEncoding.GetBytes(b.ReadToEnd());
            //     uploadedfile1.Add(new FileObject
            //     {
            //         Content = bytes,
            //         FileName = "AdminSettings.xml"
            //     }

            //     );
            // }
            // tmpc1.Blobs = uploadedfile1;
            // List<FileObject> uploadedfile2 = new List<FileObject>();
            // using (StreamReader b = new StreamReader(@"F:\WorkingProjects\dsl\docs\allsps.txt"))
            // {
            //     byte[] bytes = b.CurrentEncoding.GetBytes(b.ReadToEnd());
            //     uploadedfile2.Add(new FileObject
            //     {
            //         Content = bytes,
            //         FileName = "allsps.txt"
            //     });
            // }
            // tmpc2.Blobs = uploadedfile2;


            // var resp = blobService.B2BUpload();


            // SkypeTest test = new SkypeTest();
            //await 
            // GetAccessToken();

            // GetToken();
            // //Thread.Sleep(5000);
            //var o = new UserEmailMessageRequest();  // 
            //var listemailresponse = o.ReadEmails();

            //foreach (var emessage in listemailresponse)
            //{
            //    //functions.redactprocess(emessage.emailitemid);
            //    o.ProcessWrite(emessage.emailItemId);
            //}

            //var obj = new AzureHelper();
            //obj.ReadRuleSetsFromDB();

            //var testobj = new TeraDActRest();
            //var iList = testobj.GetAllSavedAttachment();
            //foreach (var item in iList)
            //{
            //  //  testobj.IsValidFile(item);
            //}




            //var obj = new LogHelper();
            //obj.Error("Testing log path"); 
            //obj.Info("Testing log path");

            //var obj = new EmailMessageService();
            //obj.GetEmails();
            //This method to authenticate access to your storage account and create a new container:
            //Parse the connection string and return a reference to the storage account.

            // string ConnectionString = BlobStorageAccount.Settings.BlobStorageConnectionString;

            //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("ConnectionString"));

            ////Create the blob client object.
            //CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            ////Get a reference to a container to use for the sample code, and create it if it does not exist.
            //CloudBlobContainer container = blobClient.GetContainerReference("teradactcontainer");
            //// container.CreateIfNotExists();         

            ////Generate a SAS URI for the container, without a stored access policy.
            //Console.WriteLine("Container SAS URI: " + SASTokenSecurity.GetContainerSasUri(container));

            ////Require user input before closing the console window.
            //Console.ReadLine();

        }

   


        //public static string appId = ConfigurationManager.AppSettings["ida:AppId"];
        //public static string appPassword = ConfigurationManager.AppSettings["ida:AppPassword"];
        //public static string redirectUri = ConfigurationManager.AppSettings["ida:RedirectUri"];
        //public static string[] scopes = ConfigurationManager.AppSettings["ida:AppScopes"]
        //  .Replace(' ', ',').Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

        //public void Configuration(IAppBuilder app)
        //{
        //    app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);

        //    app.UseCookieAuthentication(new CookieAuthenticationOptions());

        //    app.UseOpenIdConnectAuthentication(
        //      new OpenIdConnectAuthenticationOptions
        //      {
        //          ClientId = appId,
        //          Authority = "https://login.microsoftonline.com/common/v2.0",
        //          Scope = "openid offline_access profile email " + string.Join(" ", scopes),
        //          RedirectUri = redirectUri,
        //          PostLogoutRedirectUri = "/",
        //          TokenValidationParameters = new TokenValidationParameters
        //          {
        //              // For demo purposes only, see below
        //              ValidateIssuer = false

        //              // In a real multitenant app, you would add logic to determine whether the
        //              // issuer was from an authorized tenant
        //              //ValidateIssuer = true,
        //              //IssuerValidator = (issuer, token, tvp) =>
        //              //{
        //              //  if (MyCustomTenantValidation(issuer))
        //              //  {
        //              //    return issuer;
        //              //  }
        //              //  else
        //              //  {
        //              //    throw new SecurityTokenInvalidIssuerException("Invalid issuer");
        //              //  }
        //              //}
        //          },
        //          Notifications = new OpenIdConnectAuthenticationNotifications
        //          {
        //              AuthenticationFailed = OnAuthenticationFailed,
        //              AuthorizationCodeReceived = OnAuthorizationCodeReceived
        //          }
        //      }
        //    );
        //}


        //private static Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationResult GetToken()
        //{
        //    Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext AC = new Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext(OAuthTokenSettings.Settings.Authority);
        //    Microsoft.IdentityModel.Clients.ActiveDirectory.ClientCredential clientCred = new Microsoft.IdentityModel.Clients.ActiveDirectory.ClientCredential(OAuthTokenSettings.Settings.AppID,
        //        OAuthTokenSettings.Settings.AppPassword);
        //    var authenticationResult = AC.AcquireTokenAsync(
        //         OAuthTokenSettings.Settings.Resource, clientCred);
        //    authenticationResult.Wait();
        //    return authenticationResult.Result;
        //}

        //private Task OnAuthenticationFailed(AuthenticationFailedNotification<OpenIdConnectMessage,
        //  OpenIdConnectAuthenticationOptions> notification)
        //{
        //    notification.HandleResponse();
        //    string redirect = "/Home/Error?message=" + notification.Exception.Message;
        //    if (notification.ProtocolMessage != null && !string.IsNullOrEmpty(notification.ProtocolMessage.ErrorDescription))
        //    {
        //        redirect += "&debug=" + notification.ProtocolMessage.ErrorDescription;
        //    }
        //    notification.Response.Redirect(redirect);
        //    return Task.FromResult(0);
        //}

        //private async Task OnAuthorizationCodeReceived(AuthorizationCodeReceivedNotification notification)
        //{
        //    ConfidentialClientApplication cca = new ConfidentialClientApplication(
        //        appId, redirectUri, new ClientCredential(appPassword), null);

        //    string message;
        //    string debug;

        //    try
        //    {
        //        var result = await cca.AcquireTokenByAuthorizationCodeAsync(scopes, notification.Code);
        //        message = "See access token below";
        //        debug = result.Token;
        //    }
        //    catch (MsalException ex)
        //    {
        //        message = "AcquireTokenByAuthorizationCodeAsync threw an exception";
        //        debug = ex.Message;
        //    }

        //    notification.HandleResponse();
        //    notification.Response.Redirect("/Home/Error?message=" + message + "&debug=" + debug);
        //}
        // public static HttpContextBase httpcontext;
        // 
        //public static async Task<string> GetAccessToken()
        //{
        //    string accessToken = null;

        //    // Load the app config from web.config
        //    string appId = ConfigurationManager.AppSettings["ida:AppId"];
        //    string appPassword = ConfigurationManager.AppSettings["ida:AppPassword"];
        //    string redirectUri = ConfigurationManager.AppSettings["ida:RedirectUri"];
        //    string[] scopes = ConfigurationManager.AppSettings["ida:AppScopes"]
        //        .Replace(' ', ',').Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

        //    // Get the current user's ID
        //    string userId = ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value;

        //    if (!string.IsNullOrEmpty(userId))
        //    {
        //        // Get the user's token cache
        //        SessionTokenCache tokenCache = new SessionTokenCache(userId, httpcontext);

        //        ConfidentialClientApplication cca = new ConfidentialClientApplication(
        //            appId, redirectUri, new ClientCredential(appPassword), tokenCache);

        //        // Call AcquireTokenSilentAsync, which will return the cached
        //        // access token if it has not expired. If it has expired, it will
        //        // handle using the refresh token to get a new one.

        //        AuthenticationResult result = await cca.AcquireTokenSilentAsync(scopes);

        //        accessToken = result.Token;
        //    }

        //    return accessToken;
        //}
        //public void SignIn()
        //{

        //        // Signal OWIN to send an authorization request to Azure
        //        HttpContext.Current.GetOwinContext().Authentication.Challenge(
        //            new AuthenticationProperties { RedirectUri = "/" },
        //            OpenIdConnectAuthenticationDefaults.AuthenticationType);

        //}
    }

}
