﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeServiceTest
{
    public class TestBaseClass
    {
        public TestExchangeServerSettings ExchangeServerSettings
        {
            get
            {
                return getExchangeSettings(new TestExchangeServerSettings());
            }
           

        }
        public virtual TestExchangeServerSettings getExchangeSettings(TestExchangeServerSettings _testExchangeServerSettings)
        {
            _testExchangeServerSettings.EmailID = "Rampusa@rampgroupUS.onmicrosoft.com";
            _testExchangeServerSettings.Password = "ptg@1234";
            _testExchangeServerSettings.Domain = "rampgroupUS.onmicrosoft.com";
            _testExchangeServerSettings.ExchangeVersion = "5";
            _testExchangeServerSettings.IsApprovalEmail = "true";
            _testExchangeServerSettings.MailCount = 50;
            _testExchangeServerSettings.FromMinutes = 2000;
            _testExchangeServerSettings.EWSUrl = "https://outlook.office365.com/EWS/Exchange.asmx";

            return _testExchangeServerSettings;
        }
    }
}
