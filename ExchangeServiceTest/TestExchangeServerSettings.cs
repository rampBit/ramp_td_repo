﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeServiceTest
{
   public class TestExchangeServerSettings:TestBaseClass
    {
        public  TestExchangeServerSettings Settings { get { return ExchangeServerSettings; } }
        public string EmailID { get; set; }
        public string Password { get; set; }
        public string Domain { get; set; }
        public string ExchangeVersion { get; set; }
        public string IsApprovalEmail { get; set; }
        public int MailCount { get; set; }
        public int FromMinutes { get; set; }
        public string EWSUrl { get; internal set; }

        public override TestExchangeServerSettings getExchangeSettings(TestExchangeServerSettings testExchangeServerSettings)
        {
            TestExchangeServerSettings _exchangeServerSettings = new TestExchangeServerSettings();
            _exchangeServerSettings.EmailID = testExchangeServerSettings.EmailID;
            _exchangeServerSettings.Password = testExchangeServerSettings.Password;
            _exchangeServerSettings.Domain = testExchangeServerSettings.Domain;
            _exchangeServerSettings.ExchangeVersion = testExchangeServerSettings.ExchangeVersion;
            _exchangeServerSettings.IsApprovalEmail = testExchangeServerSettings.IsApprovalEmail;
            _exchangeServerSettings.MailCount = testExchangeServerSettings.MailCount;
            _exchangeServerSettings.FromMinutes = testExchangeServerSettings.FromMinutes;
            return testExchangeServerSettings;

        }


    }
}
