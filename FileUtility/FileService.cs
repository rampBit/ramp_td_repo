﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileUtility
{

    // using RampGroup.Logging;
    using System.Threading;
    using System.Xml;
    using System.Xml.Serialization;

    public class FileService
    {

        //    private static LogHelper _logHelper = null;

        public static string ConvertBase64ToString(string base64EncodedData)
        {
            var orgbytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.Default.GetString(orgbytes);

        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        static FileService()
        {
            //  _logHelper = new LogHelper();
        }

        public static string GetXmlPath(string FileName)
        {
            string requiredPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FileName);

            if (File.Exists(requiredPath))
            {
                return requiredPath;
            }
            else
            {
                //TODO: Need to revisit this logic and fix appropriately.
                try
                {
                    requiredPath = AppDomain.CurrentDomain.BaseDirectory;
                    requiredPath = requiredPath.Replace(@"\bin\Debug\", string.Empty).TrimEnd(new char[] { '\\' });
                    requiredPath = String.Join(@"\", requiredPath.Split('\\').Reverse().Skip(1).Reverse());
                    requiredPath = Path.Combine(requiredPath, "XML Files");
                    if (requiredPath.Contains(@"file:\"))
                        requiredPath = requiredPath.Replace(@"file:\", "");
                    if (!Directory.Exists(requiredPath))
                        Directory.CreateDirectory(requiredPath);

                    requiredPath = Path.Combine(requiredPath, FileName);

                    return requiredPath;
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
        public static string GetTemporaryDirectory()
        {
            try
            {
                string tempDirectory = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                Directory.CreateDirectory(tempDirectory);
                return tempDirectory;
            }
            catch (Exception ex)
            {
                //  _logHelper.Error(ex.Message);
            }
            return null;
        }

        public static void CreateEmptyFile(string filename)
        {
            try
            {
                File.Create(filename).Dispose();
            }
            catch (Exception ex)
            {
                //  _logHelper.Error(ex.Message);
            }
        }



        public static void CreateEmptyFile(string path, string filename)
        {
            try
            {
                File.Create(Path.Combine(path, filename)).Dispose();
            }
            catch (Exception ex)
            {
                // _logHelper.Error(ex.Message);
            }
        }

        public static void CreateDirectory(string path)
        {
            try
            {
                Directory.CreateDirectory(path);
            }
            catch (Exception ex)
            {
                //  _logHelper.Error(ex.Message);
            }
        }

        public static void CreateDirectory(string path, string childpath)
        {
            try
            {
                Directory.CreateDirectory(Path.Combine(path, childpath));
            }
            catch (Exception ex)
            {
                // _logHelper.Error(ex.Message);
            }
        }

        public static void CopyFile(string SourcePath, string TargetPath)
        {
            try
            {
                using (Stream source = File.Open(SourcePath, FileMode.Open))
                {
                    using (Stream destination = File.Create(TargetPath))
                    {
                        source.CopyTo(destination);
                    }
                }

                //File.Copy(SourcePath, TargetPath);
            }
            catch (Exception ex)
            {
                //_logHelper.Error(ex.Message);
            }
        }
        public static void CopyFile(Stream outStream, string TargetPath)
        {
            try
            {
                using (var fileStream = File.Create(TargetPath))
                {
                    outStream.Seek(0, SeekOrigin.Begin);
                    outStream.CopyTo(fileStream);
                }

            }
            catch (Exception ex)
            {
                //_logHelper.Error(ex.Message);
            }
        }
        //public static FileInfo GetFileInformation(string FilePath)
        //{
        //    string fileUrl = Path.Combine(ExchangeServerSettings.Settings.AttachmentStagingPath, FilePath);
        //    FileInfo fileInfomration = null;
        //    if (!string.IsNullOrEmpty(fileUrl))
        //    {
        //        if (File.Exists(fileUrl))
        //        {
        //            fileInfomration = new FileInfo(fileUrl);
        //        }
        //    }
        //    return fileInfomration;
        //}
        public static byte[] GetBytes(string FilePath)
        {
            return File.ReadAllBytes(FilePath);
        }
        public static byte[] GetBytesnyText(string text)
        {

            return Encoding.UTF8.GetBytes(text);
        }
        public static async Task WriteTextAsync(string filePath, string text)
        {
            byte[] encodedText = Encoding.Unicode.GetBytes(text);

            using (FileStream sourceStream = new FileStream(filePath,
                FileMode.Append, FileAccess.Write, FileShare.None,
                bufferSize: 4096, useAsync: true))
            {
                await sourceStream.WriteAsync(encodedText, 0, encodedText.Length);
            };
        }
        public static async Task<string> ReadTextAsync(string filePath)
        {
            using (FileStream sourceStream = new FileStream(filePath,
                FileMode.Open, FileAccess.Read, FileShare.Read,
                bufferSize: 4096, useAsync: true))
            {
                StringBuilder sb = new StringBuilder();

                byte[] buffer = new byte[0x1000];
                int numRead;
                while ((numRead = await sourceStream.ReadAsync(buffer, 0, buffer.Length)) != 0)
                {
                    string text = Encoding.Unicode.GetString(buffer, 0, numRead);
                    sb.Append(text);
                }

                return sb.ToString();
            }
        }

        public static string GetFileNameWithoutExtension(string FilePath)
        {
            return Path.GetFileNameWithoutExtension(FilePath);
        }
        public static string GetFileName(string FilePath)
        {
            return Path.GetFileName(FilePath);
        }
        public static Stream GetStream(byte[] content)
        {
            return new MemoryStream(content);
        }
        public static Stream GetStream(string FilePath)
        {
            return new FileStream(FilePath, FileMode.Open, FileAccess.Read);
        }

        public static void CreateStream(string path, byte[] content)
        {
            FileStream Stream = new FileStream(path, FileMode.Create);
            BinaryWriter BinaryStream = new BinaryWriter(Stream);
            BinaryStream.Write(content);
            BinaryStream.Close();

        }

        public static byte[] GetBytesFromStream(Stream content)
        {

            return StreamToByteArray(content);
        }

        private static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }
        private static byte[] StreamToByteArray(Stream stream)
        {
            if (stream is MemoryStream)
            {
                return ((MemoryStream)stream).ToArray();
            }
            else
            {
                // Jon Skeet's accepted answer 
                return ReadFully(stream);
            }
        }
        public static string getStringFromBytes(byte[] content)
        {
            return System.Text.Encoding.UTF8.GetString(content);
        }
        public static string getStringFromStream(Stream content)
        {
            using (var streamReader = new StreamReader(content))
            {
                return streamReader.ReadToEnd();
            }
        }
        public static string BytesToStringConverted(byte[] bytes)
        {
            return Convert.ToBase64String(bytes);
        }
        public static string GetXmlPathForBlobStorage()
        {
            try
            {
                Type typef = Type.GetType("Common.Repository.SQLAzureDBUtility.BaseRepository");
                //var thisType = typeof(Common.Repository.SQLAzureDBUtility.BaseRepository);//"Common.Repository.SQLAzureDBUtility.BaseRepository"
                var codeLocation = Path.GetDirectoryName(typef.Assembly.Location);
                var codeLocationPath = Path.GetDirectoryName(codeLocation);
                string clP = (codeLocationPath.Split('/'))[0].ToString();
                var appConfigPath = Path.Combine(clP, "DbSettings.xml");
                return appConfigPath;
                #region Existing Code
                //((thisType.Assembly).ManifestModule).FullyQualifiedName.ToString()
                //return GetXmlPath("BlobStorageSettings.xml");
                //if (string.IsNullOrEmpty(BlobrequiredPath))
                //{
                //    BlobrequiredPath = System.AppDomain.CurrentDomain.BaseDirectory.TrimEnd(new char[] {'\\' });
                //    BlobrequiredPath = BlobrequiredPath.Substring(0, BlobrequiredPath.LastIndexOf("\\"));
                //    BlobrequiredPath = BlobrequiredPath + @"\XML Files\";
                //    if (BlobrequiredPath.Contains(@"file:\"))
                //        BlobrequiredPath = BlobrequiredPath.Replace(@"file:\", "");
                //    if (!Directory.Exists(BlobrequiredPath))
                //        Directory.CreateDirectory(BlobrequiredPath);

                //    BlobrequiredPath = BlobrequiredPath + "BlobStorageSettings.xml";
                //}
                //return BlobrequiredPath;
                #endregion
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static string CreateXML(Object SourceObject)
        {
            XmlDocument xmlDoc = new XmlDocument();   //Represents an XML document, 
                                                      // Initializes a new instance of the XmlDocument class.          
            XmlSerializer xmlSerializer = new XmlSerializer(SourceObject.GetType());
            // Creates a stream whose backing store is memory. 
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, SourceObject);
                xmlStream.Position = 0;
                //Loads the XML document from the specified string.
                xmlDoc.Load(xmlStream);
                return xmlDoc.InnerXml;
            }
        }
    }
}
