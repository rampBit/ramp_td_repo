﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RampGroup.Config.Utility
{
    public class ActiveDirectorySettings
    {
        public string ApplicationID { get; set; }
        public string RedirectURL { get; set; }
        public string ClientID { get; set; }

        public string ClientSecret { get; set; }
        public string TenantID { get; set; }
        public string TenantName { get; set; }
        public string AuthString { get; set; }
        public bool IsActive { get; set; }
    }
}
