﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RampGroup.Config.Utility
{
    public abstract class AzuretorageAccount : BaseSettings
    {
        
        public AzuretorageAccount()
        {

        }
        internal static BlobStorageAccount BlobStorageAccountSettings { get { return BaseSettings.BlobStorageAccount; } }
        
    }
}
