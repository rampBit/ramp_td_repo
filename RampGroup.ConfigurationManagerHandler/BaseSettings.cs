﻿using FileUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace RampGroup.Config.Utility
{
    public class BaseSettings
    {
        static string AccountKey = null;
        static string AccountName = null;
        static string BlobStorageContainerName = null;
        static string EndpointSuffix = null;
        static string DefaultEndpointsProtocol = null;
        static string EmailID = null;
        static string Password = null;
        static string Domain = null;
        static string ExchangeVersion = null;
        static string IsApprovalEmail = null;
        static string EwsUrl = null;
        static string XmlPath = null;
        static string MailCount = null;
        static string FromMinutes = null;
        public BaseSettings(string AccountKey, string AccountName, string BlobStorageContainerName)
        {
            BaseSettings.AccountKey = AccountKey;
            BaseSettings.AccountName = AccountName;
            BaseSettings.BlobStorageContainerName = BlobStorageContainerName;
            BaseSettings.DefaultEndpointsProtocol = "https";
            BaseSettings.EndpointSuffix = "core.windows.net";

        }

        public BaseSettings(string EmailID, string Password, string Domain, string ExchangeVersion, string IsApprovalEmail, string EwsUrl)
        {
            BaseSettings.EmailID = EmailID;
            BaseSettings.Password = Password;
            BaseSettings.Domain = Domain;
            BaseSettings.ExchangeVersion = ExchangeVersion;
            BaseSettings.IsApprovalEmail = IsApprovalEmail;
            BaseSettings.EwsUrl = EwsUrl;
        }
        public BaseSettings()
        {

        }

        public static DBSettings DBSettings
        {
            get
            {
                return getDBSettings();
            }
        }

        public static DBSettings getDBSettings()
        {
            var teradactSettings = new DBSettings(); ;

                XElement xtr = XElement.Load(FileService.GetXmlPath("DBSettings.xml"));
                var databaseSettings = xtr.Descendants("DatabaseSettings").Single();
            teradactSettings.DataSource = databaseSettings.Element(StringConstants.DataSource).Value;
            teradactSettings.InitialCatalog = databaseSettings.Element(StringConstants.InitialCatalog).Value;
            teradactSettings.UserID = databaseSettings.Element(StringConstants.DBUserID).Value;
            teradactSettings.Password = databaseSettings.Element(StringConstants.DBPassword).Value;

            var mailSettings = xtr.Descendants("MailSettings").Single();
            teradactSettings.SMTPTargetName = mailSettings.Element(StringConstants.SMTPTargetName).Value;
            teradactSettings.SMTPEnableSsl = bool.Parse(mailSettings.Element(StringConstants.SMTPEnableSsl).Value);
            teradactSettings.SMTPHost = mailSettings.Element(StringConstants.SMTPHost).Value;
            teradactSettings.SMTPPort = int.Parse(mailSettings.Element(StringConstants.SMTPPort).Value);
            teradactSettings.SMTPUseDefaultCredentials = bool.Parse(mailSettings.Element(StringConstants.SMTPUseDefaultCredentials).Value);

            return teradactSettings;
        }

        //public static AzuretorageAccount AzuretorageAccount { get; protected set; }
        //public static ExchangeServerSettings ExchangeServerSettings
        //{
        //    get
        //    {
        //        return getExchangeSettings();
        //    }
        //}

        //public static ExchangeServerSettings getExchangeSettings()
        //{
        //    var ExchangeSettings = new ExchangeServerSettings();
        //    //if (File.Exists(FileService.GetXmlPath("AdminSettings.xml")))
        //    //{
        //    //    XElement xtr = XElement.Load(FileService.GetXmlPath("AdminSettings.xml"));
        //    //    String EmailID = xtr.Descendants("EmailID").Single().Value;
        //    //    String Password = xtr.Descendants("Password").Single().Value;
        //    //    String Domain = xtr.Descendants("Domain").Single().Value;
        //    //    ExchangeSettings.EmailID = BaseSettings.EmailID == null ? EmailID : BaseSettings.EmailID;
        //    //    ExchangeSettings.Password = BaseSettings.Password == null ? Password : BaseSettings.Password;
        //    //    ExchangeSettings.Domain = BaseSettings.Domain == null ? Domain : BaseSettings.Domain;
        //    //    ExchangeSettings.ExchangeVersion = BaseSettings.ExchangeVersion == null ? "5" : BaseSettings.ExchangeVersion;
        //    //    ExchangeSettings.EWSUrl = BaseSettings.EwsUrl == null ? "https://outlook.office365.com/EWS/Exchange.asmx" : BaseSettings.EwsUrl;
        //    //    ExchangeSettings.FromMinutes = int.Parse(BaseSettings.FromMinutes == null ? "5" : BaseSettings.FromMinutes);
        //    //    ExchangeSettings.MailCount = int.Parse(BaseSettings.MailCount == null ? "4" : BaseSettings.MailCount);
        //    //}
        //    //else
        //    //{

        //        ExchangeSettings.EmailID = BaseSettings.EmailID == null ? "Rampusa@rampgroupUS.onmicrosoft.com" : BaseSettings.EmailID;
        //        ExchangeSettings.Password = BaseSettings.Password == null ? "ptg@1234" : BaseSettings.Password;
        //        ExchangeSettings.Domain = BaseSettings.Domain == null ? "rampgroupUS.onmicrosoft.com" : BaseSettings.Domain;
        //        ExchangeSettings.ExchangeVersion = BaseSettings.ExchangeVersion == null ? "5" : BaseSettings.ExchangeVersion;
        //        ExchangeSettings.IsApprovalEmail = BaseSettings.IsApprovalEmail == null ? "true" : BaseSettings.IsApprovalEmail;
        //        ExchangeSettings.EWSUrl = BaseSettings.EwsUrl == null ? "https://outlook.office365.com/EWS/Exchange.asmx" : BaseSettings.EwsUrl;
        //        ExchangeSettings.FromMinutes = int.Parse( BaseSettings.FromMinutes == null ? "2000" : BaseSettings.FromMinutes);
        //        ExchangeSettings.MailCount = int.Parse(BaseSettings.MailCount == null ? "4" : BaseSettings.MailCount);

        //    //}

        //    return ExchangeSettings;
        //}

        //public static TeraDActConfiguration TeraDActConfiguration
        //{
        //    get
        //    {
        //        return getTeraDActSettings();

        //    }
        //}

        //private static TeraDActConfiguration getTeraDActSettings()
        //{
            
        //    return new TeraDActConfiguration
        //    {
        //        ServerURL = "http://52.205.42.150:8234/rest/",
        //        APIKey = "a8737f39-7f37-4ccf-95a4-f51c1830b54f",
        //        BaseURLRedactSuffix = "/redact?rulesetAttached=true",
        //        BaseURLRulesetSuffix = "\rulesets",
        //        RulesetXMLPathNode = "/RulesetMessageList/messageList/message/name",
        //        ServerName = "devtdc.database.usgovcloudapi.net",
        //        DataBase = "teradact",
        //    };
        //}

        public static OAuthTokenSettings TokenSetting
        {
            get
            {
                return getTokenSettings();
            }
        }


        private static OAuthTokenSettings getTokenSettings()
        {
            return new OAuthTokenSettings
            {
                AppID = "5d195cc2-09fc-47ae-a03a-f65e153b4f57",  //same for clientid
                AppPassword = "7Wi0Mp9mBfWRdmFkFpFHij0",
                AppScopes = "http://localhost:10800",
                RedirectUri = "https://outlook.office.com/mail.read",
                Authority = "https://login.windows.net/common",
                TenantID = "5a5fc493-a003-4f6e-a242-b2ab6f6a1546",
                subscriptionID = "02743f7e-e0f3-4e11-9a3e-1d75df252188",
                subscriptionname = "US Government Azure Enterprise Offer",
                Resource = "https://management.core.usgovcloudapi.net/"
            };
        }

        public static BlobStorageAccount BlobStorageAccount
        {
            get
            {
                return getStorageSettings();
            }
        }

        private static BlobStorageAccount getStorageSettings()
        {
            var StorageSettings = new BlobStorageAccount();
            //if (File.Exists(FileService.GetXmlPathForBlobStorage()))
            //{
            //    XElement xtr = XElement.Load(FileService.GetXmlPathForBlobStorage());
            //    String AccountKey = xtr.Descendants("AccountKey").Single().Value;
            //    String AccountName = xtr.Descendants("AccountName").Single().Value;
            //    String BlobStorageContainerName = xtr.Descendants("BlobStorageContainerName").Single().Value;
            //    String DefaultEndpointsProtocol = xtr.Descendants("DefaultEndpointsProtocol").Single().Value;
            //    StorageSettings.AccountKey = BaseSettings.AccountKey == null ? AccountKey : BaseSettings.AccountKey;
            //    StorageSettings.AccountName = BaseSettings.AccountName == null ? AccountName : BaseSettings.AccountName;
            //    StorageSettings.BlobStorageContainerName = BaseSettings.BlobStorageContainerName == null ? BlobStorageContainerName : BaseSettings.BlobStorageContainerName;
            //    StorageSettings.DefaultEndpointsProtocol = BaseSettings.DefaultEndpointsProtocol == null ? DefaultEndpointsProtocol : BaseSettings.DefaultEndpointsProtocol;
            //    StorageSettings.EndpointSuffix = "core.windows.net";
            //}
            //else
            //{
            //    StorageSettings.AccountKey = BaseSettings.AccountKey == null ? "EQ/Wh5AEnq+Lsy+9bKEhuFemFcIhTnAf4qGWdAfFsDbyTaqzKVq8Yv6DoD+vL9uHm2gALtMDP5JhB/zGKINpnA==" : BaseSettings.AccountKey;
            //    StorageSettings.AccountName = BaseSettings.AccountName == null ? "1234567890ajgx7abxnxun4" : BaseSettings.AccountName;
            //   // StorageSettings.BlobStorageContainerName = BaseSettings.BlobStorageContainerName == null ? "sourceblob" : BaseSettings.BlobStorageContainerName;
            //    StorageSettings.DefaultEndpointsProtocol = "https";
            //    StorageSettings.EndpointSuffix = "core.windows.net";
            //}
            return StorageSettings;
        }
    }
}