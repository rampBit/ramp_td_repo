﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RampGroup.Config.Utility
{[Serializable]
    public class BlobStorageAccount : AzuretorageAccount
    {
        public BlobStorageAccount(string AccountKey, string AccountName, string DefaultEndpointsProtocol, string Domain) 
        { }
        public BlobStorageAccount()
        {

        }
        public static BlobStorageAccount Settings { get { return AzuretorageAccount.BlobStorageAccountSettings; } }
        public string DefaultEndpointsProtocol { get; set; }
        public string AliasName { get; set; }
        public  string AccountName { get; set; }
        public string AccountKey { get; set; }
        public string BlobStorageContainerName { get; set; }
        public string Domain { get; set; }
        public string ExchangeVersion { get; set; }
        public string IsApprovalEmail { get; set; }
        public string MailCount { get; set; }
        public int FromMinutes { get; set; }
        public string Type { get; set; }
        public string EWSUrl { get; set; }
        public string Protocol { get; set; }
        public string ProductName { get; set; }

        public int ProductID { get; set; }
        public string EndpointSuffix { get; set; }
        
        public string BlobType { get; set; }
        public string TenantName { get; set; }

        public bool Selected { get; set; }

        public string BlobStorageConnectionString
        {
            get
            {
                return $"DefaultEndpointsProtocol={BlobStorageAccount.Settings.DefaultEndpointsProtocol};AccountName={BlobStorageAccount.Settings.AccountName};AccountKey={BlobStorageAccount.Settings.AccountKey};EndpointSuffix={BlobStorageAccount.Settings.EndpointSuffix}";

            }

        }

        public object RulesetDescription { get; set; }
    }
}

