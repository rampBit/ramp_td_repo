﻿

namespace RampGroup.Config.Utility
{
using System;

using System.Configuration;

    /// <summary>
    /// Helper class makes it easy to get values from AppSettings.
    /// </summary>
    public class ConfigHelper
    {
        public static string GetString(string key, string defaultValue)
        {
            if (ConfigurationManager.AppSettings[key] == null)
                return defaultValue;
            else
                return (ConfigurationManager.AppSettings[key]);
        }

        public static int GetInt32(string key, int defaultValue)
        {
            if (ConfigurationManager.AppSettings[key] == null)
                return defaultValue;
            else
                return (Int32.Parse(ConfigurationManager.AppSettings[key]));
        }

        public static double GetDouble(string key, double defaultValue)
        {
            if (ConfigurationManager.AppSettings[key] == null)
                return defaultValue;
            else
                return (Double.Parse(ConfigurationManager.AppSettings[key]));
        }

        public static bool GetBoolean(string key, bool defaultValue)
        {
            if (ConfigurationManager.AppSettings[key] == null)
                return defaultValue;
            else
                return (bool.Parse(ConfigurationManager.AppSettings[key].ToLower()));
        }


    }


}
