﻿using BusinessObjects;

namespace RampGroup.Config.Utility
{
    public class DBSettings:BaseSettings
    {

        public static DBSettings Settings { get { return DBSettings; } }

        public DBSettings()
        {

        }
        public string DataSource { get; set; }
        public string InitialCatalog { get; set; }
        public string UserID { get; set; }
        public string Password { get; set; }
        public string SMTPTargetName { get; set; }
        public bool SMTPUseDefaultCredentials { get; set; }
        public int SMTPPort { get; set; }
        public string SMTPHost { get; set; }
        public bool SMTPEnableSsl { get; set; }
    }
}
