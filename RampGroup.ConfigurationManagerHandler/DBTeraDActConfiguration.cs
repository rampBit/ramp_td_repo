﻿using BusinessObjects;
using RampGroup.Config.Utility;

namespace RampGroup.Config.Utility
{
    public class DBTeraDActConfiguration : BaseSettings
    {
        public static TeraDActConfiguration Settings { get; set; }
        public string ServerName { get; set; }
        public string DataBase { get; set; }
        public string Connectionconstr { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}