﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RampGroup.Config.Utility
{
    public class OAuthTokenSettings : BaseSettings
    {
        public static OAuthTokenSettings Settings { get { return BaseSettings.TokenSetting; } }
        public string AppID { get; set; }
        public string AppPassword { get; set; }
        public string RedirectUri { get; set; }
        public string AppScopes { get; set; }
        public string TenantID { get; set; }
        public string Authority { get; internal set; }
        public string subscriptionID { get; internal set; }
        public string subscriptionname { get; internal set; }
        public string  Resource { get; set; }
    }
}
