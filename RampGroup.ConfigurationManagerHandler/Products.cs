﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RampGroup.Config.Utility
{
   public class Products
    {
        public string ProductName { get; set; }
        public int  ProductID { get; set; }
        public bool Selected { get; set; }
    }
}
