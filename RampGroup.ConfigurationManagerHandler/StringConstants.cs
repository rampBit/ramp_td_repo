﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RampGroup.Config.Utility
{
    public class StringConstants
    {
        #region Exchange Server Config Settings
        public const string ExchangeServer = "ExchangeServer";


        public const string EmailID = "EmailID";
        public const string Password = "Password";
        public const string Domain = "Domain";
        public const string ExchangeVersion = "ExchangeVersion";
        public const string IsApprovalEmail = "IsApprovalEmail";
        public const string MailCount = "MailCount";
        public const string FromMinutes = "FromMinutes";
        // public const string EmailStagingPath = "EmailStagingPath";
        //public const string AttachmentStagingPath = "AttachmentStagingPath";
        #endregion

        #region TeraDAct Config Settings
        public const string TeraDActServer = "TeraDActServer";

        public const string ServerURL = "ServerURL";
        public const string APIKey = "APIKey";
        // public const string RuleSetPath = "RuleSetPath";
        public const string BaseURLRedactSuffix = "BaseURLRedactSuffix";
        //public const string RedactMailPath = "RedactMailPath";
        public const string RulesetXMLPathNode = "RulesetXMLPathNode";
        public const string BaseURLRulesetSuffix = "BaseURLRulesetSuffix";
        //public const string TeradactResponseLog = "TeradactResponseLog";

        #endregion

        #region  BlobStorageConnectionString
        public const string BlobStorageConnectionString = "BlobStorageConnectionString";

        public const string DefaultEndpointsProtocol = "DefaultEndpointsProtocol";
        public const string AccountName = "AccountName";
        public const string AccountKey = "AccountKey";
        public const string BlobStorageContainerName = "BlobStorageContainerName";

        public const string RedactEmailsQueue = "redactemailsqueue";

        public const string SendEmailQueue = "sendemailqueue";
        public static readonly string IPMFilter = "IPM.Note.Microsoft.Approval.Request";

        //public static readonly string RuleSetQuery = @"Select ruleset_xml From ruleset where name =@name";
        public static readonly string RuleSetQuery = @"GetExchangeRuleSet";


        public static readonly string ErrorLog = "LogException";
        public static readonly string CleanUpData = "CleanUPData";

        #endregion

        #region Database Config Settings
        public const string DataSource = "DataSource";
        public const string InitialCatalog = "InitialCatalog";
        public const string DBUserID = "UserID";
        public const string DBPassword = "Password";
        #endregion

        #region Mail Settings
        public const string SMTPTargetName = "SMTPTargetName";
        public const string SMTPUseDefaultCredentials = "SMTPUseDefaultCredentials";
        public const string SMTPPort = "SMTPPort";
        public const string SMTPHost = "SMTPHost";
        public const string SMTPEnableSsl = "SMTPEnableSsl";

        #endregion
    }
    public  enum TeraDActRuleSetType
    {
        email,
        ip,
        mac,
        phone,
        ssn
    }
}