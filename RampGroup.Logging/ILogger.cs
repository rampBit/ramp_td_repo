﻿using BusinessObjects;
using Common.Repository.SQLAzureDBUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RampGroup.Logging
{
    public interface ILogger
    {
        void Trace(string message);
        void Info(string message);
        void Error(Exception ex , ErrorSource Source);
        void Fatal(string message);
        void Error(string message, Exception ex , ErrorSource Source);
        void Fatal(string message, Exception ex);

    }
}
