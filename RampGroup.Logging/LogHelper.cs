﻿using BusinessObjects;
using Common.Repository.SQLAzureDBUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RampGroup.Logging
{
    public class LogHelper : LoggerBase, ILogger
    {
       
        public void Error(Exception ex , ErrorSource Source)
        {

            log.WriteErrorLog(ex, Source);
        }

        public void Error(string message, Exception ex , ErrorSource Source)
        {

            log.WriteErrorLog(ex==null?new Exception(message):ex, Source);
        }

        public void Fatal(string message)
        {
            Log.Fatal(message);
        }

        public void Fatal(string message, Exception ex)
        {
            Log.Fatal(ex, message);
        }

        public void Info(string message)
        {
           Info(message, ErrorSource.ExchangeOnlineServiceWebJob);
        }

        public void Info(string message , ErrorSource Source)
        {
            log.WriteErrorLog(new System.Exception(message), Source);
            //Log.Info(message);
        }

        public void Trace(string message)
        {

            Log.Trace(message);
        }
    }
}
