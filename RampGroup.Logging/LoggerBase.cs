﻿using Common.Repository.SQLAzureDBUtility;
using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RampGroup.Logging
{

  public  class LoggerBase
    {
        protected enum LogLevel1 { Warn, Normal, Hard };
        protected LoggerBase()
        {
            Log = LogManager.GetLogger(GetType().FullName);
            log = new ErrorLogRepository();
        }


        protected static Logger Log { get; private set; }

        public static ErrorLogRepository log { get; private set; }

    }
   
}
