﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RampGroup.Logging
{
    public static class NLogInformation
    {
        public const string uploadblobs = "UploadBlobs(): Exception generated while send the emails to BlobStorage.";
        public const string downloadblob = "DownloadBlob(): Parameter 'blob' is not containing any references.";
        public const string certificatevalidcallback = "CertificateValidationCallBack(): The Certificate is a valid, signed certificate, return true.";
        public const string certificatenotvalidcallback = "CertificateValidationCallBack(): The Certificate is a invalid. Return False.";
        public const string initexchangeservice = "InitExchangeService():Exchange Services initialised.";
        public const string initexchangeservicefail = "InitExchangeService(): Error while preparing asynchronous connection or AutodiscoverUrl couldn't found.";
        public const string getallexchangeemails = "GetAllExchangeEmails(): All the emails added.";
        public const string getallexchangeemailsfail = "InitExchangeService(): Error while preparing asynchronous connection or AutodiscoverUrl couldn't found.";
        public const string getrespose = "Server Config Value not set";
        public const string getmails = "GetEmails(): Error in getting the emails. {0}";
        public const string getmailsfail = "GetAllExchangeEmails(): Error in getting the emails from Exchange Server. {0}";
        public const string httppostresponse = "Request is not a http request";
        public const string putteradactresponse = "PutRedactResponse(): Attachments successfully sents.";
        public const string putteradactresponsefail = "PutRedactResponse(): Try to send the attachments again.";
        public const string getallsavedfilefatal = "GetAllSavedAttachment(): Path is not exist.";
        public const string getallsavedfileinfo = "GetAllSavedAttachment(): Getting bytes array to collection of list of bytes";
        public const string getallsavedfileerror = "GetAllSavedAttachment(): Path of the Saved attachment could not found. {0}";
        public const string getallrulesetpass = "Response successfully.";
        public const string getallrulesetfail = "Response failed";
    }
}
