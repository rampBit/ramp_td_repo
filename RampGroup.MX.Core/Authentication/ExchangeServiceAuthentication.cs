﻿namespace RampGroup.MX.Core
{
    using Authentication;
    using Microsoft.Exchange.WebServices.Data;
    using Config.Utility;
    using System;
    using Logging;
    using BusinessObjects;
    using Common.Repository.SQLAzureDBUtility;
    using System.Linq;
    using System.Configuration;

    /// <summary>
    /// 
    /// </summary>
    public class ExchangeServiceAuthentication : ExchangeServerBase
    {
        /// <summary>
        /// 
        /// </summary>
        //public ExchangeServiceAuthentication(string EmailID,
        //    string Password, string Domain, string EWSUrl)
        //{
        //    if (_instance==null )
        //    InitExchangeService(EmailID, Password, Domain, EWSUrl);
        //     if(_SMTPinstance==null)
        //        InitSMTPExchangeService(EmailID, Password, Domain, EWSUrl);
        //}



        private ExchangeServiceAuthentication() { }
        private static ExchangeServerBase _instance = null;
        private static ExchangeServerBase _SMTPinstance = null;

        public static ExchangeServerBase Instance
        {
            get
            {
                if (_instance == null)
                {
                    try
                    {
                        bool IsExchageOnPrem = bool.Parse(
                   ConfigurationManager.AppSettings["IsExchageOnPrem"] == null ? "false" :
                   ConfigurationManager.AppSettings["IsExchageOnPrem"]);

                        EmailQueueRepository _Repo = new EmailQueueRepository();
                        var setting = _Repo.getTenantEmail(IsExchageOnPrem).FirstOrDefault();
                        if (setting == null)
                            throw new Exception("Unable to connect exvhange");
                        ExchangeServiceAuthentication exch = new ExchangeServiceAuthentication();
                        exch.InitExchangeService(setting.EmailID, setting.Password, setting.Domain, setting.EWSUrl);

                    }
                    catch (Exception ex)
                    {

                        _logHelper.Error(NLogInformation.initexchangeservicefail, ex, ErrorSource.ExchangeOnlineServiceWebJob);
                    }
                }
                return _instance;
            }
        }
        public static ExchangeServerBase SMTPInstance
        {
            get
            {
                if (_SMTPinstance == null)
                {
                    try
                    {
                        bool IsExchageOnPrem = bool.Parse(
                  ConfigurationManager.AppSettings["IsExchageOnPrem"] == null ? "false" :
                  ConfigurationManager.AppSettings["IsExchageOnPrem"]);

                        EmailQueueRepository _Repo = new EmailQueueRepository();
                        var setting = _Repo.getTenantEmail(IsExchageOnPrem).FirstOrDefault();
                        if (setting == null)
                            throw new Exception("Unable to connect exvhange");
                        ExchangeServiceAuthentication exch = new ExchangeServiceAuthentication();
                        exch.InitSMTPExchangeService(setting.EmailID, setting.Password, setting.Domain, setting.EWSUrl);

                    }
                    catch (Exception ex)
                    {
                        _logHelper.Error(NLogInformation.initexchangeservicefail, ex, ErrorSource.ExchangeOnlineServiceWebJob);
                    }
                }
                return _SMTPinstance;
            }
        }


        /// <summary>
        /// Set user credentials from Configuration 
        /// </summary>

        private Microsoft.Exchange.WebServices.Data.ExchangeService InitExchangeService(string EmailID,
            string Password, string Domain, string EWSUrl)
        {

            try
            {
                CertificateCallback.Initialize();
                exchange = new ExchangeService();
                exchange.Credentials = new System.Net.NetworkCredential(
                     EmailID,
                     Password,
                   Domain);
                exchange.Url = new Uri(EWSUrl);
                exchange.Timeout = 300000;
                //exchange.AutodiscoverUrl(ExchangeServerSettings.Settings.EmailID,
                //    delegate (string url) { return true; });
                _instance = new ExchangeServiceAuthentication { exchange = exchange };

              //  exchange.TraceEnabled = true;
                _logHelper.Info(NLogInformation.initexchangeservice);
            }
            catch (Exception ex)
            {
                _logHelper.Error(NLogInformation.initexchangeservicefail, ex, ErrorSource.ExchangeOnlineServiceWebJob);
            }

            return exchange;
        }
        private Microsoft.Exchange.WebServices.Data.ExchangeService InitSMTPExchangeService(string EmailID,
          string Password, string Domain, string EWSUrl)
        {

            try
            {
                CertificateCallback.Initialize();
                var SMTPexchange = new ExchangeService();
                SMTPexchange.Credentials = new System.Net.NetworkCredential(
                     EmailID,
                     Password,
                   Domain);
                SMTPexchange.Url = new Uri(EWSUrl);
                SMTPexchange.Timeout = 300000;
                //exchange.AutodiscoverUrl(ExchangeServerSettings.Settings.EmailID,
                //    delegate (string url) { return true; });
                _SMTPinstance = new ExchangeServiceAuthentication { SMTPexchange = SMTPexchange };

                SMTPexchange.TraceEnabled = true;
                _logHelper.Info(NLogInformation.initexchangeservice);
            }
            catch (Exception ex)
            {
                _logHelper.Error(NLogInformation.initexchangeservicefail, ex, ErrorSource.ExchangeOnlineServiceWebJob);
            }

            return SMTPexchange;
        }

    }
}