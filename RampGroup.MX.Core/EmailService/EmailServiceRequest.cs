﻿using BusinessObjects;
using Common.Repository.SQLAzureDBUtility;
using Microsoft.Exchange.WebServices.Data;
using RampGroup.Config.Utility;
using System;
using System.Collections.Generic;

namespace RampGroup.MX.Core
{/// <summary>
 /// 
 /// </summary>
    public class EmailServiceRequest :   IDisposable, IEmailServiceRequest
    {
        
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            throw new NotImplementedException();
        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="uniqueID"></param>
        /// <returns></returns>
        public EmailMessage GetExchangeEmailByUniqueID(string uniqueID)
        {
            EmailMessage email = null;

            if (ExchangeServiceAuthentication.Instance.exchange != null)
            {
                email = EmailMessage.Bind(ExchangeServiceAuthentication.Instance.exchange, new ItemId(uniqueID));
            }
            return email;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<EmailMessage> GetAllExchangeEmails(BusinessObjects.ExchangeServerSettings setting)
        {
            IList<EmailMessage> messages = new List<EmailMessage>();
            try
            {
                //ExchangeServiceAuthentication exch = new Core.ExchangeServiceAuthentication
                //      (setting.EmailID, setting.Password, setting.Domain, setting.EWSUrl);
                DateTime Emaildate = DateTime.Now.AddMinutes(0 - setting.FromMinute);

                SearchFilter.SearchFilterCollection filter =
      new SearchFilter.SearchFilterCollection(LogicalOperator.And);

                SearchFilter.IsEqualTo searchFilterByIPM =
                    new SearchFilter.IsEqualTo(ItemSchema.ItemClass,
                    StringConstants.IPMFilter);

                SearchFilter.IsGreaterThanOrEqualTo searchFilterByDate =
                    new SearchFilter.IsGreaterThanOrEqualTo(ItemSchema.DateTimeReceived,
                    Emaildate);

                SearchFilter.IsEqualTo searchFilterByAttachments = new SearchFilter.IsEqualTo(ItemSchema.HasAttachments, true);
              


                SearchFilter.IsEqualTo unreadFilter = new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, false);

                filter.AddRange(new SearchFilter[] { searchFilterByDate, searchFilterByIPM, searchFilterByAttachments, unreadFilter });
               
                if (ExchangeServiceAuthentication.Instance.exchange != null)
                {
                    ItemView view = new ItemView(setting.MailCount);
                    FindItemsResults<Item> findResults = ExchangeServiceAuthentication.Instance.exchange.FindItems(WellKnownFolderName.Inbox, filter,
                    view);
                    foreach (Item item in findResults)
                    {

                        EmailMessage message = EmailMessage.Bind(ExchangeServiceAuthentication.Instance.exchange, item.Id);
                        messages.Add(message);
                    }
                    //}
                }

                //_logHelper.Info(NLogInformation.getallexchangeemails);
            }
            catch (Exception ex)
            {
                ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.ExchangeOnlineServiceWebJob);
            }
            return messages;
        }
        

    }
}
