﻿using BusinessObjects;
using Microsoft.Exchange.WebServices.Data;
using RampGroup.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RampGroup.MX.Core
{
    public interface IEmailServiceRequest
    {
        IList<EmailMessage> GetAllExchangeEmails(BusinessObjects.ExchangeServerSettings setting);
        EmailMessage GetExchangeEmailByUniqueID(string UniqueID);
       
        // AttachmentSampleServiceResponse GetAttachmentsFromExchangeServerUsingEWS(AttachmentSampleServiceRequest request);
    }
}
