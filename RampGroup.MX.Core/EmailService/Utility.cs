﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RampGroup.MX.Core.EmailService
{
  public  class Utility
    {
        public static string ProcessXmlResponse(XElement responseEnvelope)
        {
            // First, check the response for web service errors.
            var errorCodes = from errorCode in responseEnvelope.Descendants
                             ("{http://schemas.microsoft.com/exchange/services/2006/messages}ResponseCode")
                             select errorCode;
            // Return the first error code found.
            foreach (var errorCode in errorCodes)
            {
                if (errorCode.Value != "NoError")
                {
                    return string.Format("Could not process result. Error: {0}", errorCode.Value);
                }
            }

            // No errors found, proceed with processing the content.
            // First, get and process file attachments.
            var fileAttachments = from fileAttachment in responseEnvelope.Descendants
                              ("{http://schemas.microsoft.com/exchange/services/2006/types}FileAttachment")
                                  select fileAttachment;
            foreach (var fileAttachment in fileAttachments)
            {
                var fileContent = fileAttachment.Element("{http://schemas.microsoft.com/exchange/services/2006/types}Content");
                var fileData = System.Convert.FromBase64String(fileContent.Value);
                var s = new MemoryStream(fileData);
                // Process the file attachment here. 
            }

            // Second, get and process item attachments.
            var itemAttachments = from itemAttachment in responseEnvelope.Descendants
                                  ("{http://schemas.microsoft.com/exchange/services/2006/types}ItemAttachment")
                                  select itemAttachment;
            foreach (var itemAttachment in itemAttachments)
            {
                var message = itemAttachment.Element("{http://schemas.microsoft.com/exchange/services/2006/types}Message");
                if (message != null)
                {
                    // Process a message here.
                    break;
                }
                var calendarItem = itemAttachment.Element("{http://schemas.microsoft.com/exchange/services/2006/types}CalendarItem");
                if (calendarItem != null)
                {
                    // Process calendar item here.
                    break;
                }
                var contact = itemAttachment.Element("{http://schemas.microsoft.com/exchange/services/2006/types}Contact");
                if (contact != null)
                {
                    // Process contact here.
                    break;
                }
                var task = itemAttachment.Element("{http://schemas.microsoft.com/exchange/services/2006/types}Tontact");
                if (task != null)
                {
                    // Process task here.
                    break;
                }
                var meetingMessage = itemAttachment.Element("{http://schemas.microsoft.com/exchange/services/2006/types}MeetingMessage");
                if (meetingMessage != null)
                {
                    // Process meeting message here.
                    break;
                }
                var meetingRequest = itemAttachment.Element("{http://schemas.microsoft.com/exchange/services/2006/types}MeetingRequest");
                if (meetingRequest != null)
                {
                    // Process meeting request here.
                    break;
                }
                var meetingResponse = itemAttachment.Element("{http://schemas.microsoft.com/exchange/services/2006/types}MeetingResponse");
                if (meetingResponse != null)
                {
                    // Process meeting response here.
                    break;
                }
                var meetingCancellation = itemAttachment.Element("{http://schemas.microsoft.com/exchange/services/2006/types}MeetingCancellation");
                if (meetingCancellation != null)
                {
                    // Process meeting cancellation here.
                    break;
                }
            }

            return string.Empty;
        }
    }
}
