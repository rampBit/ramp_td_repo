﻿using Microsoft.Exchange.WebServices.Data;
using RampGroup.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RampGroup.MX.Core.Authentication
{/// <summary>
/// 
/// </summary>
    public abstract class ExchangeServerBase
    {
        protected ExchangeServerBase()
        {
            _logHelper = new LogHelper();
        }
        public ExchangeService exchange { get; protected set; }
        public ExchangeService SMTPexchange { get; protected set; }
        
        protected static ILogger _logHelper;

    }
}
