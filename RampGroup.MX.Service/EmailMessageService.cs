﻿using Microsoft.Exchange.WebServices.Data;
using RampGroup.Logging;
using RampGroup.MX.Core;
using RampGroup.MX.Core.Authentication;
using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel.Web;

namespace RampGroup.MX.Service
{
    [DataContract(Namespace = "")]
    public class Data
    {
        [DataMember]
        public string Message { get; set; }
    }
    [DataContract(Namespace = "")]
    public class ErrorDetails
    {
        [DataMember]
        public string ErrorMessage { get; set; }
    }

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class EmailMessageService : ExchangeServerBase ,IEmailMessageService
    {
        public Data GetResponse()
        {
            throw new WebFaultException<ErrorDetails>(
                new ErrorDetails { ErrorMessage = NLogInformation.getrespose},
                HttpStatusCode.OK 
                );
        }
        public List<object> GetEmails()
        {
            try
            {
                return null;  
            }
            catch (Exception ex)
            {
                _logHelper.Error(NLogInformation.getmails, ex);
                throw null;
            }
        }

        public IList<EmailMessage> GetAllExchangeEmails()
        {
            try
            {
                IEmailServiceRequest _mailService = new EmailServiceRequest();
                return _mailService.GetAllExchangeEmails();
            }
            catch (Exception ex)
            {
                _logHelper.Error(NLogInformation.getmailsfail, ex);
                throw null;
            }
           
        }
    }
}
