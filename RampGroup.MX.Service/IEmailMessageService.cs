﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Microsoft.Exchange.WebServices.Data;
namespace RampGroup.MX.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract(Namespace = "")]
    public interface IEmailMessageService
    {

        [OperationContract]
        [WebGet(UriTemplate = "/response")]
        [FaultContract(typeof(ErrorDetails))]
        Data GetResponse();

        [OperationContract]
        [WebGet(UriTemplate = "/ExchangeEmail")]
        [FaultContract(typeof(ErrorDetails))]
        IList<EmailMessage> GetAllExchangeEmails();
    }
   

}
