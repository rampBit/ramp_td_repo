﻿using RampGroup.Logging;
using RampGroup.MX.Service;
using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.ServiceModel.Web;
using TeraDActService;

namespace RampGroupService
{


    public class SelfServiceHost : IDisposable

    {  //hosting all the services(add ref teradact service lib)
        static ServiceHost host;
        static bool disposed = false;
        private static void BindCert()

        {
            int port = 8446;
            string certPath = @"D:\WorkingProjects\OdataCertificate\datapipe.cer";
            X509Certificate2 certificate = new X509Certificate2(certPath);
            // netsh http add sslcert ipport=0.0.0.0:<port> certhash={<thumbprint>} appid={<some GUID>}
            Process bindPortToCertificate = new Process();
            bindPortToCertificate.StartInfo.FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "netsh.exe");
            bindPortToCertificate.StartInfo.Arguments = string.Format("http add sslcert ipport=0.0.0.0:{0} certhash={1} appid={{{2}}}", port, certificate.Thumbprint, Guid.NewGuid());
            bindPortToCertificate.Start();
            bindPortToCertificate.WaitForExit();
        }
        public static void BindService()
        {
            // BindCert();

            WebServiceHost host = new WebServiceHost(typeof(EmailMessageService ));
            try
            {
                // request from: http://localhost:8000/SampleService.Service/response

                host.Open();
                
                Console.WriteLine("Press <ENTER> to terminate");
                Console.ReadLine();
                host.Close();
                LogHelper loghelper = new LogHelper();
                loghelper.Error("BindService(): Bind services working properly .");
            }
            catch (CommunicationException cex)
            {
                Console.WriteLine("An exception occurred: {0}", cex.Message);
                host.Abort();
                LogHelper loghelper = new LogHelper();
                loghelper.Error("BindService(): An communication exception occurred: {0}", cex);
            }
        }

        
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    host.Close();
                }

                disposed = true;
            }
        }

        ~SelfServiceHost()
        {
            Dispose(false);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}