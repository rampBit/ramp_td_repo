﻿using RampGroup.Config.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using BusinessObjects;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace Common.Repository.SQLAzureDBUtility
{
    public class AdminRepository : BaseRepository<AdminRepository>
    {
        public AdminRepository() { }
        private string saveEmailQueueSP = "saveAdmin";
        private readonly string ReadAdminSettings = "SELECT TOP {0} [EmailQueueID],[MessageID],[Status],[ExchangeAttachementEmailUniqueID],[RetryCount] FROM [EmailQueue]  WHERE [Status] NOT IN (@Status)  AND RetryCount<3";
        static TransactionScope CreateTransactionScope()
        {
            var transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            transactionOptions.Timeout = TransactionManager.MaximumTimeout;
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions, TransactionScopeAsyncFlowOption.Enabled);
        }
        public int AdminExchangeSettingSConfigurations(BusinessObjects.ExchangeServerSettings adminSubscription)
        {
            string SaveExchangeSettingsDetailsSP = "usp_SaveExchangeSettingsDetails";

            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] paramlst = new SqlParameter[10];

                var ExchangeSettings = new SqlParameter("@ExchangeSettings", System.Data.SqlDbType.VarChar);
                //var TenantName = new SqlParameter("@TenantName", System.Data.SqlDbType.VarChar);
                var AccountName = new SqlParameter("@AccountName", System.Data.SqlDbType.VarChar);
                var AccountKey = new SqlParameter("@AccountKey", System.Data.SqlDbType.VarChar);
                var AliasName = new SqlParameter("@AliasName", System.Data.SqlDbType.VarChar);
                var Domain = new SqlParameter("@Domain", System.Data.SqlDbType.VarChar);
                var Protocol = new SqlParameter("@Protocol", System.Data.SqlDbType.VarChar);
                var Type = new SqlParameter("@BlobType", System.Data.SqlDbType.VarChar);
                var EwsURL = new SqlParameter("@EwsURL", System.Data.SqlDbType.VarChar);
                var MailCount = new SqlParameter("@MailCount", System.Data.SqlDbType.Int);
                var RulesetDescription = new SqlParameter("@RulesetDescription", System.Data.SqlDbType.VarChar);
                ExchangeSettings.Value = adminSubscription.ProductName;
                AccountName.Value = adminSubscription.AccountName;
                AccountKey.Value = adminSubscription.AccountKey;
                AliasName.Value = adminSubscription.AliasName;
                Domain.Value = adminSubscription.Domain;
                Protocol.Value = adminSubscription.Protocal;
                Type.Value = adminSubscription.BlobType;
                EwsURL.Value = adminSubscription.EWSUrl;
                MailCount.Value = adminSubscription.MailCount;
                RulesetDescription.Value = adminSubscription.RulesetDescription;
                paramlst[0] = ExchangeSettings;
                paramlst[2] = AccountName;
                paramlst[1] = AccountKey;
                paramlst[3] = AliasName;
                paramlst[4] = Domain;
                paramlst[5] = Protocol;
                paramlst[6] = Type;
                paramlst[7] = EwsURL;
                paramlst[8] = MailCount;
                paramlst[9] = RulesetDescription;
                db.ExecNonQueryStorProc(SaveExchangeSettingsDetailsSP, paramlst);

                return 0;
            }

        }


        public int SaveGroupsandRules(List<GroupNames> GroupNames)
        {
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                string SaveRolesandGroupsDetailsDetailsSP = "usp_SaveRolesandGroupsDetails";
                SqlParameter[] paramlst = new SqlParameter[2];
                foreach (var item in GroupNames)
                {
                    string RuleSetName = "";
                    foreach (var rule in item.rulesList)//ssn,mac,ip
                    {
                        if (RuleSetName == string.Empty)
                            RuleSetName = rule.Name;
                        else
                            RuleSetName = RuleSetName + ',' + rule.Name;
                    }
                    var GroupNameList = new SqlParameter("@GroupName", System.Data.SqlDbType.VarChar);
                    var RuleSet = new SqlParameter("@RuleSet", System.Data.SqlDbType.VarChar);
                    GroupNameList.Value = item.Name;
                    RuleSet.Value = RuleSetName;
                    paramlst[0] = GroupNameList;
                    paramlst[1] = RuleSet;
                    db.ExecNonQueryStorProc(SaveRolesandGroupsDetailsDetailsSP, paramlst);
                }
            }
            return 0;
        }
        public string TenantID { get; set; }
        public string TenantName { get; set; }
        public string AuthString { get; set; }
        public bool IsActive { get; set; }

        public int SaveActiveDirectorySettings(ActiveDirectorySettings _activeDirectorySettings)
        {
            string SaveActiveDirectorySettingsDetailsSP = "usp_ActiveDirectorySettingsDetails";

            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] paramlst = new SqlParameter[8];


                //var TenantName = new SqlParameter("@TenantName", System.Data.SqlDbType.VarChar);
                var ApplicationID = new SqlParameter("@ApplicationID", System.Data.SqlDbType.VarChar);
                var RedirectURL = new SqlParameter("@RedirectURL", System.Data.SqlDbType.VarChar);
                var ClientID = new SqlParameter("@ClientID", System.Data.SqlDbType.VarChar);
                var ClientSecret = new SqlParameter("@ClientSecret", System.Data.SqlDbType.VarChar);
                var TenantID = new SqlParameter("@TenantID", System.Data.SqlDbType.VarChar);
                var TenantName = new SqlParameter("@TenantName", System.Data.SqlDbType.VarChar);
                var AuthString = new SqlParameter("@AuthString", System.Data.SqlDbType.VarChar);
                var IsActive = new SqlParameter("@IsActive", System.Data.SqlDbType.Bit);
                ApplicationID.Value = _activeDirectorySettings.ApplicationID;
                RedirectURL.Value = _activeDirectorySettings.RedirectURL;
                ClientID.Value = _activeDirectorySettings.ClientID;
                ClientSecret.Value = _activeDirectorySettings.ClientSecret;
                TenantID.Value = _activeDirectorySettings.TenantID;
                TenantName.Value = _activeDirectorySettings.TenantName;
                AuthString.Value = _activeDirectorySettings.AuthString;
                IsActive.Value = _activeDirectorySettings.IsActive;
                paramlst[0] = ApplicationID;
                paramlst[1] = RedirectURL;
                paramlst[2] = ClientID;
                paramlst[3] = ClientSecret;
                paramlst[4] = TenantID;
                paramlst[5] = TenantName;
                paramlst[6] = AuthString;
                paramlst[7] = IsActive;
                db.ExecNonQueryStorProc(SaveActiveDirectorySettingsDetailsSP, paramlst);

                return 0;
            }
        }
        public int AdminBlobSettingSConfigurations(BlobStorageAccount adminSubscription)
        {
            string SaveExchangeSettingsDetailsSP = "usp_SaveExchangeSettingsDetails";

            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] paramlst = new SqlParameter[9];

                var ExchangeSettings = new SqlParameter("@ExchangeSettings", System.Data.SqlDbType.VarChar);
                var AccountKey = new SqlParameter("@AccountKey", System.Data.SqlDbType.VarChar);
                var AccountName = new SqlParameter("@AccountName", System.Data.SqlDbType.VarChar);
                //var TenantName = new SqlParameter("@TenantName", System.Data.SqlDbType.VarChar);
                var AliasName = new SqlParameter("@AliasName", System.Data.SqlDbType.VarChar);
                var Domain = new SqlParameter("@Domain", System.Data.SqlDbType.VarChar);
                var Protocol = new SqlParameter("@Protocol", System.Data.SqlDbType.VarChar);
                var Type = new SqlParameter("@BlobType", System.Data.SqlDbType.VarChar);
                var EwsURL = new SqlParameter("@EwsURL", System.Data.SqlDbType.VarChar);
                var MailCount = new SqlParameter("@MailCount", System.Data.SqlDbType.Int);
               // var RulesetDescription = new SqlParameter("@RulesetDescription", System.Data.SqlDbType.VarChar);
                ExchangeSettings.Value = adminSubscription.ProductName;
                AccountKey.Value = adminSubscription.AccountKey;
                AccountName.Value = adminSubscription.AccountName;
                AliasName.Value = adminSubscription.AliasName;
                Domain.Value = adminSubscription.Domain;
                Protocol.Value = adminSubscription.Protocol;
                Type.Value = adminSubscription.BlobType;
                EwsURL.Value = adminSubscription.EWSUrl;
                MailCount.Value = adminSubscription.MailCount;
              //  RulesetDescription.Value = adminSubscription.RulesetDescription;
                paramlst[0] = ExchangeSettings;
                paramlst[1] = AccountKey;
                paramlst[2] = AccountName;
                paramlst[3] = AliasName;
                paramlst[4] = Domain;
                paramlst[5] = Protocol;
                paramlst[6] = Type;
                paramlst[7] = EwsURL;
                paramlst[8] = MailCount;
               // paramlst[9] = RulesetDescription;
                db.ExecNonQueryStorProc(SaveExchangeSettingsDetailsSP, paramlst);
                return 0;
            }

        }

        public class BlobSettingDetails
        {
            public BlobStorageAccount BlobStorageAccount { get; set; }
            public bool Selected { get; set; }

        }
        public List<Products> GetProductsNames()
        {
            List<Products> BlobStorageAccount = new List<Products>();
            string GetProductDetailsSp = StringConstant.GetProductsName;
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlDataReader reader = db.ExecDataReaderProc(GetProductDetailsSp);

                var Res = from Dr in reader.Enumerate()
                          select new RampGroup.Config.Utility.Products
                          {
                              ProductName = Dr["ProductName"] as string,
                              //   ProductID= (int) Dr["ProductID"],
                              Selected = false
                          };
                BlobStorageAccount.AddRange(Res);
                #region Commented

                //var Res =
                //        from Dr in reader.Enumerate()
                //        select new BlobSettingDetails
                //        {   
                //,
                //            //Selected = false
                //        };
                //blobsettings.AddRange(Res);
                #endregion
            }

            return BlobStorageAccount;
        }

        public List<GroupsAndRoles> GetRolesandGroups()
        {
            List<GroupsAndRoles> GetRolesandRuleslist = new List<GroupsAndRoles>();
            string GetRolesandGroups = StringConstant.GetRolesandGroupsNames;
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlDataReader reader = db.ExecDataReaderProc(GetRolesandGroups);
                var Res = from Dr in reader.Enumerate() select new GroupsAndRoles { GroupName = Dr["GroupName"] as string, RuleSetName = Dr["RulesetName"] as string };
                GetRolesandRuleslist.AddRange(Res);
            }
            return GetRolesandRuleslist;
        }

        public List<RampGroup.Config.Utility.ActiveDirectorySettings> GetActiveDirectoryDetails()
        {

            List<ActiveDirectorySettings> GetActiveDetails = new List<ActiveDirectorySettings>();
            string GetActiveDirectorysSp = StringConstant.ActiveDirectorySettings;
            List<BlobSettingDetails> blobsettings = new List<BlobSettingDetails>();
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlDataReader reader = db.ExecDataReaderProc(GetActiveDirectorysSp);

                var Res = from Dr in reader.Enumerate()
                          select new RampGroup.Config.Utility.ActiveDirectorySettings
                          {

                              ApplicationID = Dr["ApplicationID"] as string,
                              RedirectURL = Dr["RedirectURL"] as string,
                              ClientID = Dr["ClientID"] as string,
                              ClientSecret = Dr["ClientSecret"] as string,
                              TenantID = Dr["TenantID"] as string,
                              TenantName = Dr["TenantName"] as string,
                              AuthString = Dr["AuthString"] as string,

                          };
                GetActiveDetails.AddRange(Res);

            }
            return GetActiveDetails;
        }


        public List<BlobStorageAccount> GetBlobSettings()
        {
            List<BlobStorageAccount> GetBlobSettingsDetails = new List<BlobStorageAccount>();
            string GetBlobSettingsSp = StringConstant.GetBlobSettingssps;
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlDataReader reader = db.ExecDataReaderProc(GetBlobSettingsSp);

                var Res = from Dr in reader.Enumerate()
                          select new RampGroup.Config.Utility.BlobStorageAccount
                          {
                              AliasName = Dr["AliasName"] as string,
                              AccountName = Dr["AccountName"] as string,
                              AccountKey = Dr["AccountKey"] as string,
                              BlobType = Dr["BlobType"] as string,
                              Domain = Dr["Domain"] as string,
                              EWSUrl = Dr["EwsURL"] as string,
                              MailCount = Dr["MailCount"] as string,
                              Protocol = Dr["Protocol"] as string,
                          };
                GetBlobSettingsDetails.AddRange(Res);

            }
            return GetBlobSettingsDetails;

        }

        public List<BlobStorageAccount> GetExchangeOnlineDetails(string ProductName)
        {

            List<BlobStorageAccount> GetExchangeOnlineDetails = new List<BlobStorageAccount>();
            string GetExchangeOnlineDetailsSp = StringConstant.GetExchangeServerSettings;
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] paramlst = new SqlParameter[1];

                var ProductNameparam = new SqlParameter("@ProductName", System.Data.SqlDbType.VarChar);
                ProductNameparam.Value = ProductName;
                paramlst[0] = ProductNameparam;
                SqlDataReader reader = db.ExecDataReaderProc(GetExchangeOnlineDetailsSp,paramlst);

                var Res = from Dr in reader.Enumerate()
                          select new RampGroup.Config.Utility.BlobStorageAccount
                          {

                              AccountName = Dr["AccountName"] as string,
                              AccountKey = Dr["AccountKey"] as string,
                              Domain = Dr["Domain"] as string,
                              EWSUrl = Dr["EwsURL"] as string,
                              MailCount = Dr["MailCount"] as string,
                              Protocol = Dr["Protocol"] as string,
                              Selected = false
                          };
                GetExchangeOnlineDetails.AddRange(Res);

            }
            return GetExchangeOnlineDetails;
        }


        public List<SharePointOnlineSettings> GetViewSharePointDetails(string ProductName)
        {
            List<SharePointOnlineSettings> GetShareOnlineDetails = new List<SharePointOnlineSettings>();
            string GetSharePointOnlineDetailsSps = StringConstant.GetSharePointOnlineDetailsSp;
            List<SharePointOnlineSettings> blobsettings = new List<SharePointOnlineSettings>();
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] paramlst = new SqlParameter[1];

                var ProductNameparam = new SqlParameter("@ProductName", System.Data.SqlDbType.VarChar);
                ProductNameparam.Value = ProductName;
                paramlst[0] = ProductNameparam;
                SqlDataReader reader = db.ExecDataReaderProc(GetSharePointOnlineDetailsSps, paramlst);

                var Res = from Dr in reader.Enumerate()
                          select new SharePointOnlineSettings
                          {
                              SPFileTransferServiceURL = Dr["SPFileTransferServiceURL"] as string,
                              SPAdminUserID = Dr["SPAdminUserID"] as string,
                              SPAdminPassword = Dr["SPAdminPassword"] as string,
                              AdminCentarlSiteURL = Dr["AdminCentarlSiteURL"] as string,
                              SharedDocumentLibraryName = Dr["SharedDocumentLibraryName"] as string,
                              Domain = Dr["Domain"] as string,

                          };
                GetShareOnlineDetails.AddRange(Res);

            }
            return GetShareOnlineDetails;
        }
        public List<BlobStorageAccount> GetSourceTargetBlobSettingsDetails()
        {

            List<BlobStorageAccount> BlobStorageAccount = new List<BlobStorageAccount>();
            string SourceBlobSettingsDetailsSp = StringConstant.SourceBlobSettingsDetailsSp;
            List<BlobSettingDetails> blobsettings = new List<BlobSettingDetails>();
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlDataReader reader = db.ExecDataReaderProc(SourceBlobSettingsDetailsSp);

                var Res = from Dr in reader.Enumerate()
                          select new RampGroup.Config.Utility.BlobStorageAccount
                          {
                              AliasName = Dr["AliasName"] as string,
                              BlobType = Dr["BlobType"] as string,
                              AccountName = Dr["AccountName"] as string,
                              AccountKey = Dr["AccountKey"] as string,
                              Domain = Dr["Domain"] as string,
                              Protocol = Dr["Protocol"] as string,
                              Selected = false
                          };
                BlobStorageAccount.AddRange(Res);
                #region Commented

                //var Res =
                //        from Dr in reader.Enumerate()
                //        select new BlobSettingDetails
                //        {   
                //,
                //            //Selected = false
                //        };
                //blobsettings.AddRange(Res);
                #endregion
            }
            return BlobStorageAccount;
        }

        public int SharePointOnlineSettingsConfigurations(BusinessObjects.SharePointOnlineSettings sharepointsettings)
        {
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] param = new SqlParameter[8];
                var SPFileTransferServiceURL = new SqlParameter("@SPFileTransferServiceURL", System.Data.SqlDbType.NVarChar);
                var SPAdminUserID = new SqlParameter("@SPAdminUserID", System.Data.SqlDbType.NVarChar);
                var SPAdminPassword = new SqlParameter("@SPAdminPassword", System.Data.SqlDbType.NVarChar);
                var AdminCentarlSiteURL = new SqlParameter("@AdminCentarlSiteURL", System.Data.SqlDbType.NVarChar);
                var IsActive = new SqlParameter("@IsActive", System.Data.SqlDbType.Bit);
                var SharedDocumentLibName = new SqlParameter("@SharedDocumentLibraryName", System.Data.SqlDbType.NVarChar);
                var ProductName = new SqlParameter("@ProductName", System.Data.SqlDbType.NVarChar);
                var Domain = new SqlParameter("@Domain", System.Data.SqlDbType.NVarChar);
                SPFileTransferServiceURL.Value = sharepointsettings.SPFileTransferServiceURL;
                SPAdminUserID.Value = sharepointsettings.SPAdminUserID;
                SPAdminPassword.Value = sharepointsettings.SPAdminPassword;
                AdminCentarlSiteURL.Value = sharepointsettings.AdminCentarlSiteURL;
                IsActive.Value = sharepointsettings.IsActive;
                SharedDocumentLibName.Value = sharepointsettings.SharedDocumentLibraryName;
                ProductName.Value = sharepointsettings.ProductName;
                Domain.Value = sharepointsettings.Domain;
                param[0] = SPFileTransferServiceURL;
                param[1] = SPAdminUserID;
                param[2] = SPAdminPassword;
                param[3] = AdminCentarlSiteURL;
                param[4] = IsActive;
                param[5] = SharedDocumentLibName;
                param[6] = ProductName;
                param[7] = Domain;
                db.ExecNonQueryStorProc(StringConstant.saveSharePointOnlineSettingsSP, param);
                return 0;
            }
        }

    }
}
