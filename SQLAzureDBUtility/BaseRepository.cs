﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Common.Repository.SQLAzureDBUtility
{
    public abstract class BaseRepository
    {
        public virtual string SqlAzureConnectionString()
        {
            return ConfigurationManager.AppSettings["DBTeraDActConfiguration"];
        }
    }
    public abstract class BaseRepository<T> where T : class, new()
    {
        protected static T GetInstance()
        {
            if (_instance == null)
            {
                lock (_lockObj)
                {
                    if (_instance == null)
                        _instance = new T();
                }
            }
            return _instance;
        }

        public static T Instance
        {
            get { return GetInstance(); }
        }
        public bool DownloadXML(string fileName, string containerName)
        {

            CloudBlobContainer container = BlobUtilities.GetBlobClient.GetContainerReference(containerName);
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
            using (MemoryStream ms = new MemoryStream())
            {
                XElement xtr = XElement.Load(blockBlob.Uri.ToString());
                String DatabaseServerName = xtr.Descendants("DatabaseServerName").Single().Value;
                String DatabaseUserName = xtr.Descendants("DatabaseUserName").Single().Value;
                String DatabasePassword = xtr.Descendants("DatabasePassword").Single().Value;
                String DatabaseName = xtr.Descendants("DatabaseName").Single().Value;
                try
                {
                    System.Configuration.Configuration configuration = null;
                    if (System.Web.HttpContext.Current != null)
                    {
                        configuration =
                            System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
                    }
                    else
                    {
                        configuration =
                            ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    }

                  //  Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);                    
                    var value=$"Server={DatabaseServerName};Initial Catalog={DatabaseName};Persist Security Info=False;User ID={DatabaseUserName};Password={DatabasePassword};";
                    configuration.AppSettings.Settings["DBTeraDActConfiguration"].Value = value;
                    configuration.Save(ConfigurationSaveMode.Minimal, true);
                    ConfigurationManager.RefreshSection("appSettings");
                }
                catch (Exception ex)
                {

                }
            }


            return false;
        }

        public virtual string SqlAzureConnectionString()
        {
            if(ConfigurationManager.AppSettings["DBTeraDActConfiguration"] == string.Empty) //Debugger.IsAttached)
            {
                DownloadXML("DbSettings.xml", "configfiles");
                return ConfigurationManager.AppSettings["DBTeraDActConfiguration"];
            }
             return ConfigurationManager.AppSettings["DBTeraDActConfiguration"];
        }

        

        private volatile static T _instance;
        private  static object _lockObj = new object();
    }

    public class BlobUtilities
    {
        public static CloudBlobClient GetBlobClient
        {
            get
            {
                
                StorageCredentials credentials = new StorageCredentials("teradacttenantonboarding", "G7tLxKRdxp8eeqIak9rZwBf46K1L/NV9CN/pl0UK7d15X92bofjWcsVJqVN3hj4qc9SjVI04CdgMRGgiJT7U+Q==");
                CloudStorageAccount storageAccount = new CloudStorageAccount(credentials, "core.usgovcloudapi.net",true);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                return blobClient;
            }
        }
    }
}
