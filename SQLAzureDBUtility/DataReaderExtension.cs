﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Repository.SQLAzureDBUtility
{
    public static class DataReaderExtension
    {
        public static IEnumerable<Object[]> AsEnumerable(this System.Data.IDataReader source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            while (source.Read())
            {
                Object[] row = new Object[source.FieldCount];
                source.GetValues(row);
                yield return row;
            }
        }
        public static IEnumerable<T> Enumerate<T>(this T reader) where T : IDataReader
        {
            using (reader)
                while (reader.Read())
                    yield return reader;
        }
    }
}
