﻿using BusinessObjects;
using RampGroup.Config.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Common.Repository.SQLAzureDBUtility
{
    public class EmailQueueRepository : BaseRepository<EmailQueueRepository>
    {
        public string getExchangeRuleSet(string ProductName = "ExchangeSettings")   //for admin ui 
        {

            if (IsExchageOnPrem)
                ProductName = "ExchangeOnPremSettings";
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] param = new SqlParameter[1];
                var ProductNamep = new SqlParameter("@ProductName", System.Data.SqlDbType.VarChar);
                ProductNamep.Value = ProductName;
                param[0] = ProductNamep;
                var rulesetStringValue = db.ExecScalarProc(
                    StringConstants.RuleSetQuery, param);

                if (rulesetStringValue != null && rulesetStringValue.ToString().Length > 0)
                    return rulesetStringValue.ToString();


            }
            return null;


        }
        public byte[] ReadRuleSetsFromDB()  //email redaction
        {
            return Encoding.ASCII.GetBytes("#include,RedactRulesets," + getExchangeRuleSet());
        }
        public EmailQueueRepository() { }

        static TransactionScope CreateTransactionScope()
        {
            var transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            transactionOptions.Timeout = TransactionManager.MaximumTimeout;
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions, TransactionScopeAsyncFlowOption.Enabled);
        }
        public void saveEmailQueue(EmailQueue emailQueue, char Operation = 'I')
        {

            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] param = new SqlParameter[5];
                var MessageID = new SqlParameter("MessageID", System.Data.SqlDbType.VarChar);
                var Status = new SqlParameter("Status", System.Data.SqlDbType.VarChar);
                var OperationType = new SqlParameter("@OperationType", System.Data.SqlDbType.Char);
                var ExchangeAttachementEmailUniqueID = new SqlParameter("@ExchangeAttachementEmailUniqueID", System.Data.SqlDbType.VarChar);
                var EmailID = new SqlParameter("@EmailID", System.Data.SqlDbType.VarChar);
                MessageID.Value = emailQueue.MessageID;
                Status.Value = emailQueue.Status;
                OperationType.Value = Operation;
                EmailID.Value = emailQueue.ExchangeServerSettings.EmailID;
                ExchangeAttachementEmailUniqueID.Value = emailQueue.ExchangeAttachementEmailUniqueID;
                param[0] = MessageID;
                param[1] = Status;
                param[2] = OperationType;
                param[3] = ExchangeAttachementEmailUniqueID;
                param[4] = EmailID;
                db.ExecNonQueryStorProc(StringConstant.saveEmailQueueSP, param);
            }


        }

        public List<ExchangeServerSettings> getTenantEmail(bool IsExchageOnPrem = false)
        {
            List<ExchangeServerSettings> emails = new List<ExchangeServerSettings>();

            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlDataReader reader = db.ExecDataReaderProc(
                    IsExchageOnPrem ? StringConstant.getTenantEmailOnprem : StringConstant.getTenantEmail);
                var Res =
                        from Dr in reader.Enumerate()
                        select new ExchangeServerSettings
                        {
                            EmailID = Dr["EmailID"] as string,
                            Password = Dr["Password"] as string,
                            Domain = Dr["Domain"] as string,
                            EWSUrl = Dr["EWSURL"] as string,
                            MailCount = int.Parse(Dr["MailCount"] as string),
                            FromMinute = (int)Dr["FromMinute"]
                        };
                emails.AddRange(Res);
            }
            return emails;
        }

        public int saveBlobQueue(BlobQueue blobQueue, char Operation = 'I')
        {

            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] param = new SqlParameter[6];

                var Status = new SqlParameter("Status", System.Data.SqlDbType.VarChar);
                var OperationType = new SqlParameter("@OperationType", System.Data.SqlDbType.Char);
                var RequestFilesCount = new SqlParameter("@RequestFilesCount", System.Data.SqlDbType.BigInt);
                var identity = new SqlParameter("@identity", System.Data.SqlDbType.Int);
                var FilesProcessedCount = new SqlParameter("@FilesProcessedCount", System.Data.SqlDbType.BigInt);
                identity.Direction = ParameterDirection.Output;
                var BlobQueueID = new SqlParameter("@BlobQueueID", System.Data.SqlDbType.BigInt);
                Status.Value = blobQueue.Status;
                OperationType.Value = Operation;
                RequestFilesCount.Value = blobQueue.RequestFilesCount;
                FilesProcessedCount.Value = blobQueue.FilesProcessedCount;
                BlobQueueID.Value = blobQueue.BlobQueueID;
                param[0] = Status;
                param[1] = OperationType;
                param[2] = RequestFilesCount;
                param[3] = identity;
                param[4] = FilesProcessedCount;
                param[5] = BlobQueueID;
                db.ExecNonQueryStorProc(StringConstant.saveBlobQueueSP, param);
                if (identity.Value != DBNull.Value)
                    return Convert.ToInt32(identity.Value);
                return 0;
            }

        }
        public IList<EmailQueue> GetEmails()
        {

            List<EmailQueue> emails = new List<EmailQueue>();
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlDataReader reader = db.ExecDataReaderProc(StringConstant.ReadEmailQueue);
                var Res =
                        from Dr in reader.Enumerate()
                        select new EmailQueue
                        {
                            MessageID = Dr["MessageID"] as string,
                            Status = (RequestStatus)Enum.Parse(typeof(RequestStatus), Dr["Status"] as string),
                            EmailQueueID = (long)Dr["EmailQueueID"],
                            ExchangeAttachementEmailUniqueID = Dr["ExchangeAttachementEmailUniqueID"] as string,
                            RetryCount = Dr["RetryCount"] == DBNull.Value ? 0 : (short)(Dr["RetryCount"]),
                            EmailID = Dr["EmailID"] as string,
                            ExchangeServerSettings = new ExchangeServerSettings
                            {
                                EmailID = Dr["EmailID"] as string,
                                Password = Dr["Passwrod"] as string,
                                Domain = Dr["Domain"] as string,
                                EWSUrl = Dr["EWSURL"] as string
                            }
                        };
                emails.AddRange(Res);
            }


            EmailQueueRepository _Repo = new EmailQueueRepository();
            var setting = _Repo.getTenantEmail(IsExchageOnPrem).FirstOrDefault();

            return emails.Where(em => em.EmailID.Equals(setting.EmailID, StringComparison.OrdinalIgnoreCase)).ToList();


        }
        public bool IsExchageOnPrem
        {
            get
            {
                return bool.Parse(
                    ConfigurationManager.AppSettings["IsExchageOnPrem"] == null ? "false" :
                    ConfigurationManager.AppSettings["IsExchageOnPrem"]);
            }
        }

        public SMTPSetting GetSMTPSetting(string ProductName = "ExchangeSettings")   //for admin ui 
        {
            if (IsExchageOnPrem)
                ProductName = "ExchangeOnPremSettings";
            string smtpQuery = StringConstant.smtpQuery;
            var smtp = new SMTPSetting();
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] param = new SqlParameter[1];
                var ProductNamep = new SqlParameter("@ProductName", System.Data.SqlDbType.VarChar);
                ProductNamep.Value = ProductName;
                param[0] = ProductNamep;


                SqlDataReader reader = db.ExecDataReaderProc(smtpQuery, param);
                var Res =
                        from Dr in reader.Enumerate()
                        select new SMTPSetting
                        {
                            TargetName = Dr["TargetName"] as string,
                            Host = Dr["Host"] as string,
                            Port = (int)Dr["Port"],
                            EnableSsl = (bool)Dr["EnableSsl"]

                        };
                smtp = Res.First();
            }

            return smtp;


        }
        public int saveSMTPSettings(SMTPSetting smtpSettings)
        {

            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] param = new SqlParameter[5];
                var ProductNamep = new SqlParameter("@ProductName", System.Data.SqlDbType.VarChar);
                var TargetName = new SqlParameter("@TargetName", System.Data.SqlDbType.VarChar);
                var Host = new SqlParameter("@Host", System.Data.SqlDbType.VarChar);
                var Port = new SqlParameter("@Port", System.Data.SqlDbType.Int);
                var EnableSSL = new SqlParameter("@EnableSSL", System.Data.SqlDbType.Bit);
                ProductNamep.Value = smtpSettings.ProductName;
                TargetName.Value = smtpSettings.TargetName;
                Host.Value = smtpSettings.Host;
                Port.Value = smtpSettings.Port;
                EnableSSL.Value = smtpSettings.EnableSsl;
                param[0] = ProductNamep;
                param[1] = TargetName;
                param[2] = Host;
                param[3] = Port;
                param[4] = EnableSSL;
                db.ExecNonQueryStorProc(StringConstant.SaveSMTPSettigns, param);
            }
            return 0;
        }
    }
}
