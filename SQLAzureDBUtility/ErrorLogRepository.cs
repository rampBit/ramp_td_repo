﻿
using BusinessObjects;
using RampGroup.Config.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Repository.SQLAzureDBUtility
{
    public class ErrorLogRepository : BaseRepository<ErrorLogRepository>
    {

        

        public void WriteErrorLog(Exception ex, ErrorSource Source)
        {
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] param = new SqlParameter[4];
                var ErrorTime = new SqlParameter("ErrorTime", System.Data.SqlDbType.DateTime);
                var ErrorMessage = new SqlParameter("ErrorMessage", System.Data.SqlDbType.VarChar);
                var ExceptionType = new SqlParameter("ExceptionType", System.Data.SqlDbType.VarChar);
                var ErrorSource = new SqlParameter("ErrorSource", System.Data.SqlDbType.VarChar);
                ErrorTime.Value = DateTime.Now.ToShortDateString();
                ErrorMessage.Value = ex.ToString();
                ExceptionType.Value = ex.Source == null ? null : ex.GetType().FullName;
                ErrorSource.Value = Source;
                param[0] = ErrorTime;
                param[1] = ErrorMessage;
                param[2] = ExceptionType;
                param[3] = ErrorSource;
                db.ExecNonQueryStorProc(StringConstants.ErrorLog, param);

            }
        }

        public void DoCleanUpData()
        {
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                
                db.ExecNonQueryStorProc(StringConstants.CleanUpData);

            }


        }
    }
}
