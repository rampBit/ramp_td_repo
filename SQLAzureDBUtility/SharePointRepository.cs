﻿using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Common.Repository.SQLAzureDBUtility
{
    public class SharePointRepository : BaseRepository<SharePointRepository>
    {
        public bool IsSharePointOnPrem
        {
            get
            {
                return bool.Parse(
                                  ConfigurationManager.AppSettings["IsSharePointOnPrem"] == null ? "false" :
                                  ConfigurationManager.AppSettings["IsSharePointOnPrem"]);
            }
        }
        public List<SharePointOnlineSettings> GetSharePointSettingsDetailsSP(string ProductName = "SharePointSettings")
        {

            if (IsSharePointOnPrem)
                ProductName = "SharePointOnPremSettings";

            List<SharePointOnlineSettings> sharepointonlinesettings = new List<SharePointOnlineSettings>();
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] param = new SqlParameter[1];
                var ProductNamep = new SqlParameter("@ProductName", System.Data.SqlDbType.VarChar);
                ProductNamep.Value = ProductName;
                param[0] = ProductNamep;
                SqlDataReader reader = db.ExecDataReaderProc(StringConstant.GetSharePointOnlineDetailsSp, param);


                var Res = from Dr in reader.Enumerate()
                          select new BusinessObjects.SharePointOnlineSettings
                          {
                              SPFileTransferServiceURL = Path.Combine(Dr["SPFileTransferServiceURL"] as string, "api/TeraDActFileSericeAPI/"),
                              SPAdminUserID = Dr["SPAdminUserID"] as string,
                              SPAdminPassword = Dr["SPAdminPassword"] as string,

                              AdminCentarlSiteURL = Dr["AdminCentarlSiteURL"] as string,
                              SharedDocumentLibraryName = Dr["SharedDocumentLibraryName"] as string,
                              Domain = Dr["Domain"] as string,
                              IsActive = (bool)Dr["IsActive"]
                          };
                sharepointonlinesettings.AddRange(Res);
            }
            return sharepointonlinesettings;
        }
        public int saveSharePointQueue(SharePointQueue sharepointQueue, char Operation = 'I')
        {
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] param = new SqlParameter[11];

                var Status = new SqlParameter("Status", System.Data.SqlDbType.VarChar);
                var OperationType = new SqlParameter("@OperationType", System.Data.SqlDbType.Char);
                var RequestFilesCount = new SqlParameter("@RequestFilesCount", System.Data.SqlDbType.BigInt);
                var identity = new SqlParameter("@identity", System.Data.SqlDbType.Int);
                var FilesProcessedCount = new SqlParameter("@FilesProcessedCount", System.Data.SqlDbType.BigInt);
                var SharePointQueueID = new SqlParameter("@SharePointQueueID", System.Data.SqlDbType.BigInt);
                var FileName = new SqlParameter("@FileName", System.Data.SqlDbType.VarChar);
                var SPListID = new SqlParameter("@SPListID", System.Data.SqlDbType.VarChar);
                var SPListItemID = new SqlParameter("@SPListItemID", System.Data.SqlDbType.Int);
                var SPHostURL = new SqlParameter("@SPHostURL", System.Data.SqlDbType.VarChar);
                var EventSources = new SqlParameter("@EventSources", System.Data.SqlDbType.VarChar);
                identity.Direction = ParameterDirection.Output;

                Status.Value = sharepointQueue.Status;
                OperationType.Value = Operation;
                RequestFilesCount.Value = sharepointQueue.RequestFilesCount;
                FilesProcessedCount.Value = sharepointQueue.FilesProcessedCount;
                SharePointQueueID.Value = sharepointQueue.SharePointQueueID;
                FileName.Value = sharepointQueue.FileName;
                SPListID.Value = sharepointQueue.SPListID;
                SPListItemID.Value = sharepointQueue.SPListItemID;
                SPHostURL.Value = sharepointQueue.SPHostURL;
                EventSources.Value = sharepointQueue.EventSources + "---->" + sharepointQueue.ActionType;

                param[0] = Status;
                param[1] = OperationType;
                param[2] = RequestFilesCount;
                param[3] = identity;
                param[4] = FilesProcessedCount;
                param[5] = SharePointQueueID;
                param[6] = FileName;
                param[7] = SPListID;
                param[8] = SPListItemID;
                param[9] = SPHostURL;
                param[10] = EventSources;
                db.ExecNonQueryStorProc(StringConstant.saveSharePointQueueSP, param);
                if (identity.Value != DBNull.Value)
                    return Convert.ToInt32(identity.Value);
                return 0;
            }
        }

        public void LogSharePointRestInfo(RestLogInfo log)
        {
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] param = new SqlParameter[5];

                var StartedOn = new SqlParameter("@StartedOn", System.Data.SqlDbType.DateTime);
                var RequestMethod = new SqlParameter("@RequestMethod", System.Data.SqlDbType.VarChar);
                var RequestUrl = new SqlParameter("@RequestUrl", System.Data.SqlDbType.VarChar);
                var ResponseStatusCode = new SqlParameter("@ResponseStatusCode", System.Data.SqlDbType.VarChar);
                var ResponseData = new SqlParameter("@ResponseData", System.Data.SqlDbType.VarChar);

                StartedOn.Value = log.StartedOn;
                RequestMethod.Value = log.RequestMethod;
                RequestUrl.Value = log.RequestUrl;
                ResponseStatusCode.Value = log.ResponseStatusCode;
                ResponseData.Value = log.ResponseData;

                param[0] = StartedOn;
                param[1] = RequestMethod;
                param[2] = RequestUrl;
                param[3] = ResponseStatusCode;
                param[4] = ResponseData;
                db.ExecNonQueryStorProc(StringConstant.LogSharePointRestInfo, param);


            }
        }
        public void updatePolicyInfoStatus(string xmldata)
        {
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] param = new SqlParameter[1];
                var data = new SqlParameter("@xmldata", System.Data.SqlDbType.Xml);
                data.Value = xmldata;
                param[0] = data;

                db.ExecNonQueryStorProc(StringConstant.updatePolicyInfoStatus, param);
            }
        }
        public void updatePolicyInfoStatus(TDPolicyInfo strPolicyInfo)
        {
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] param = new SqlParameter[4];
                var POLICYNAME = new SqlParameter("@POLICYNAME", System.Data.SqlDbType.VarChar);
                var POLICYTYPE = new SqlParameter("@POLICYTYPE", System.Data.SqlDbType.VarChar);
                var POLICYKEY = new SqlParameter("@POLICYKEY", System.Data.SqlDbType.VarChar);
                var POLICYVALUE = new SqlParameter("@POLICYVALUE", System.Data.SqlDbType.VarChar);
                param[0] = POLICYNAME;
                param[1] = POLICYTYPE;
                param[2] = POLICYKEY;
                param[3] = POLICYVALUE;
                db.ExecNonQueryStorProc(StringConstant.updatePolicyInfoStatus, param);
            }
        }
        public object getPolicyInfoStatus()
        {
            List<TDPolicyInfo> pinfo = new List<TDPolicyInfo>();

            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlDataReader reader = db.ExecDataReaderProc(StringConstant.getPolicyInfoStatus);
                var res = from Dr in reader.Enumerate()
                          select new BusinessObjects.TDPolicyInfo
                          {
                              PolicyName = Dr["PolicyName"] as string,
                              PolicyKey = Dr["PolicyKey"] as string,
                              PolicyValue = Dr["PolicyValue"] as string,
                              PolicyType = Dr["PolicyType"] as string,
                              //  MasterPolicyType = Dr["MasterpolicyType"] as string,

                          };

                //PolicyTypeID = Dr["PolicyTypeID"] as string
                //pinfo = Res.FirstOrDefault();
                pinfo.AddRange(res);
            }
            return pinfo;
        }



        public List<TDRuleSetDirectiveInfo> getTDRuleSetDirectiveInfo(string rulesetname = "")
        {
            SqlParameter[] param = new SqlParameter[1];
            var rulesetn = new SqlParameter("@RuleSetName", System.Data.SqlDbType.VarChar);
            rulesetn.Value = rulesetname;
            param[0] = rulesetn;
            List<TDRuleSetDirectiveInfo> resp = new List<TDRuleSetDirectiveInfo>();
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {

                SqlDataReader reader = db.ExecDataReaderProc(StringConstant.getTDRuleSetDirectiveInfo, param);

                var Res = from Dr in reader.Enumerate()
                          select new BusinessObjects.TDRuleSetDirectiveInfo
                          {
                              RuleSetName = Dr["RuleSetName"] as string,
                              TotalRLString = Dr["RuleSetContent"] as string
                          };
                resp.AddRange(Res);
            }
            return resp;
            //label = Dr["label"] as string,
            //Pattern = Dr["Pattern"] as string, //replacementText = Dr["replacementText"] as string,  Type = Dr["Type"] as string
        }

        public void updateTDRulesetDirInfo(TDRuleSetDirectiveInfo obj)
        {
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] param = new SqlParameter[3];
                #region Commented
                //var label = new SqlParameter("@label", System.Data.SqlDbType.VarChar);
                //var replacementText = new SqlParameter("@replacementText", System.Data.SqlDbType.VarChar);
                //var Type = new SqlParameter("@Type", System.Data.SqlDbType.VarChar);
                //var Pattern = new SqlParameter("@Pattern", System.Data.SqlDbType.VarChar);
                //label.Value = obj.label;
                //replacementText.Value = obj.replacementText;
                //Type.Value = obj.Type;
                //Pattern.Value = obj.Pattern;  param[2] = Type;
                //param[3] = Pattern;
                //param[4] = RuleSetName;
                #endregion
                var RuleSetName = new SqlParameter("@RuleSetName", System.Data.SqlDbType.NVarChar);
                var RuleSetContent = new SqlParameter("@RuleSetContent", System.Data.SqlDbType.NVarChar);
                var NewRuleSetName = new SqlParameter("@NewRuleSetName", System.Data.SqlDbType.NVarChar);
                RuleSetName.Value = obj.RuleSetName;
                RuleSetContent.Value = obj.TotalRLString;
                if (obj.NewRuleSetName == null) obj.NewRuleSetName = string.Empty;//(obj.NewRuleSetName == null)? null : string.Empty;
                NewRuleSetName.Value = obj.NewRuleSetName;
                param[0] = RuleSetName;
                param[1] = RuleSetContent;
                param[2] = NewRuleSetName;
                db.ExecNonQueryStorProc(StringConstant.updateTDRulesetDirInfo, param);
            }
        }
        public List<SPUnAttendedApplyRulesSettings> getSPUnAttendedApplyRulesSettingsInfo()
        { string ProductName= "SharePointSettings";
            if (IsSharePointOnPrem)
                 ProductName = "SharePointOnPremSettings";
                List<SPUnAttendedApplyRulesSettings> info = new List<SPUnAttendedApplyRulesSettings>();
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] param = new SqlParameter[1];
                var ProductNamep = new SqlParameter("@ProductName", System.Data.SqlDbType.VarChar);
                ProductNamep.Value = ProductName;
                param[0] = ProductNamep;
                SqlDataReader reader = db.ExecDataReaderProc(StringConstant.getSPUnAttendedApplyRulesSettings, param);

                var Res = from Dr in reader.Enumerate()
                          select new BusinessObjects.SPUnAttendedApplyRulesSettings
                          {
                              ID = (int)Dr["ID"],
                              SourceSiteURL = Dr["SourceSiteURL"] as string,
                              SourceDocLib = Dr["SourceDocLib"] as string,
                              SourceFolder = Dr["SourceFolder"] as string,
                              TargetSiteURL = Dr["TargetSiteURL"] as string,
                              TargetDocLib = Dr["TargetDocLib"] as string,
                              TargetFolder = Dr["TargetFolder"] as string,
                              AppliedRuleSets = Dr["AppliedRuleSets"] as string,
                              ISActive = true

                          };
                info.AddRange(Res);
            }
            return info;
        }

        public int DeleteUnattendedFolder(int deleteID)
        {
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] param = new SqlParameter[1];
                var ID = new SqlParameter("@ID", System.Data.SqlDbType.Int);
                ID.Value = deleteID;
                param[0] = ID;
                db.ExecNonQueryStorProc(StringConstant.deleteUnattendedFolderSettings, param);
            }
            return 0;
        }

        public int saveSPUnAttendedApplyRulesSettings(SPUnAttendedApplyRulesSettings UnAttendedApplyRulesSettings)
        {
            string ProductName = "SharePointSettings";  //default sp online
            if (IsSharePointOnPrem)
                ProductName = "SharePointOnPremSettings";

            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                

                SqlParameter[] param = new SqlParameter[9];
                var ID = new SqlParameter("@ID", System.Data.SqlDbType.Int);
                var SourceSiteURL = new SqlParameter("@SourceSiteURL", System.Data.SqlDbType.VarChar);
                var SourceDocLib = new SqlParameter("@SourceDocLib", System.Data.SqlDbType.VarChar);
                var SourceFolder = new SqlParameter("@SourceFolder", System.Data.SqlDbType.VarChar);

                var TargetSiteURL = new SqlParameter("@TargetSiteURL", System.Data.SqlDbType.VarChar);
                var TargetDocLib = new SqlParameter("@TargetDocLib", System.Data.SqlDbType.VarChar);
                var TargetFolder = new SqlParameter("@TargetFolder", System.Data.SqlDbType.VarChar);

                var AppliedRuleSets = new SqlParameter("@AppliedRuleSets", System.Data.SqlDbType.VarChar);
                var ProductNamep = new SqlParameter("@ProductName", System.Data.SqlDbType.VarChar);
                ID.Value = UnAttendedApplyRulesSettings.ID;
                SourceSiteURL.Value = UnAttendedApplyRulesSettings.SourceSiteURL;
                SourceDocLib.Value = UnAttendedApplyRulesSettings.SourceDocLib;
                SourceFolder.Value = UnAttendedApplyRulesSettings.SourceFolder;
                TargetSiteURL.Value = UnAttendedApplyRulesSettings.TargetSiteURL;
                TargetDocLib.Value = UnAttendedApplyRulesSettings.TargetDocLib;
                TargetFolder.Value = UnAttendedApplyRulesSettings.TargetFolder;
                AppliedRuleSets.Value = UnAttendedApplyRulesSettings.AppliedRuleSets;
                ProductNamep.Value = ProductName;
                param[0] = ID;
                param[1] = SourceSiteURL;
                param[2] = SourceDocLib;
                param[3] = SourceFolder;
                param[4] = TargetSiteURL;
                param[5] = TargetDocLib;
                param[6] = TargetFolder;
                param[7] = AppliedRuleSets;
                param[8] = ProductNamep;
                db.ExecNonQueryStorProc(StringConstant.saveSPUnAttendedApplyRulesSettings, param);
            }
            return 0;
        }

        public string getUserRoleDirectives(string aDUserGroups)
        {


            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlParameter[] param = new SqlParameter[1];
                var GroupName = new SqlParameter("@GroupName", System.Data.SqlDbType.VarChar);
                GroupName.Value = aDUserGroups;
                param[0] = GroupName;
                return db.ExecScalarProc(StringConstant.getUserRoleDirectives, param).ToString();

            }
        }

        public string[] getAllRoleDirectives()
        {

            List<string> info = new List<string>();
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {

                SqlDataReader reader = db.ExecDataReaderProc(StringConstant.getAllRoleDirectives);

                var Res = from Dr in reader.Enumerate()
                          select new
                          {

                              labelname = Dr["LabelName"] as string,


                          };
                info.AddRange(Res.Select(l => l.labelname));
            }
            return info.ToArray();
        }

    }

}


#region Commented SQL params



//pci = Dr["pci"] as string,
//                              phi = Dr["phi"] as string,
//                              pii = Dr["pii"] as string,
//                              ranked = Dr["ranked"] as string,
//                              PCIType=Dr["PCIType"] as string,
//                              PCIKey=Dr["PCIKey"] as string,
//                              PHIType=Dr["PHIType"] as string,
//                              PHIKey=Dr["PHIKey"] as string,
//                              PIIType=Dr["PIIType"] as string,
//                              PIIKey=Dr["PIIKey"] as string,
//                              RankedType=Dr["RankedType"] as string,
//                              RankedKey=Dr["RankedKey"] as string


//var PolicyName = new SqlParameter("@pci", System.Data.SqlDbType.VarChar);
//var pci = new SqlParameter("@pci", System.Data.SqlDbType.VarChar);
//var phi = new SqlParameter("@phi", System.Data.SqlDbType.VarChar);
//var pii = new SqlParameter("@pii", System.Data.SqlDbType.VarChar);
//var ranked = new SqlParameter("@ranked", System.Data.SqlDbType.VarChar);
//var pciType = new SqlParameter("@PCIType", System.Data.SqlDbType.VarChar);
//var pciKey = new SqlParameter("@PCIKey", System.Data.SqlDbType.VarChar);
//var phiType = new SqlParameter("@PHIType", System.Data.SqlDbType.VarChar);
//var phiKey = new SqlParameter("@PHIKey", System.Data.SqlDbType.VarChar);
//var piiType = new SqlParameter("@PIIType", System.Data.SqlDbType.VarChar);
//var piiKey = new SqlParameter("@PIIKey", System.Data.SqlDbType.VarChar);
//var rankedType = new SqlParameter("@RankedType", System.Data.SqlDbType.VarChar);
//var rankedKey = new SqlParameter("@RankedKey", System.Data.SqlDbType.VarChar);
//pci.Value = strPolicyInfo.pci;
//phi.Value = strPolicyInfo.phi;
//pii.Value = strPolicyInfo.pii;
//ranked.Value = strPolicyInfo.ranked;
//pciType.Value = strPolicyInfo.PCIType;
//pciKey.Value = strPolicyInfo.PCIKey;
//phiType.Value = strPolicyInfo.PHIType;
//phiKey.Value = strPolicyInfo.PHIKey;
//piiType.Value = strPolicyInfo.PIIType;
//piiKey.Value = strPolicyInfo.PIIKey;
//rankedType.Value = strPolicyInfo.RankedType;
//rankedKey.Value = strPolicyInfo.RankedKey;


//param[4] = pciType;
//param[5] = pciKey;
//param[6] = phiType;
//param[7] = phiKey;
//param[8] = piiType;
//param[9] = piiKey;
//param[10] = rankedType;
//param[11] = rankedKey;
#endregion

