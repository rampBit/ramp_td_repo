﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Repository.SQLAzureDBUtility
{
    internal static class StringConstant
    {
        internal static readonly string getTenantEmail = "getTenantEmail";
        internal static readonly string getTenantEmailOnprem = "getTenantEmailOnPrem";
        internal static readonly string ReadEmailQueue = "ReadEmailQueue";
        internal static readonly string saveBlobQueueSP = "saveBlobQueue";
        internal static readonly string saveEmailQueueSP = "saveEmailQueue";
        internal static readonly string smtpQuery = "getSMTPSettings";
        internal static readonly string SourceBlobSettingsDetailsSp = "usp_GetBlobSettingsDetails";

        internal static readonly string GetProductsName = "usp_GetProductsNames";
        internal static readonly string saveSharePointQueueSP = "saveSharePointQueue";
        internal static readonly string saveSharePointOnlineSettingsSP = "usp_saveSharePointSettingsDetails";
        //internal static readonly string SharePointSettingsDetailsSp = "usp_GetSharePointSettingsDetails";
        internal static readonly string getTeraDActConfiguration= "getTeraDActConfiguration";
        internal static readonly string LogSharePointRestInfo= "LogSharePointRestInfo";
        internal static readonly string GetSharePointOnlineDetailsSp = "usp_GetSharePointSettingsDetails";
        internal static readonly string GetExchangeServerSettings = "usp_GetExchangeServerSettings";
        internal static readonly string GetBlobSettingssps= "usp_GetBlobSettings";
        internal static readonly string ActiveDirectorySettings = "usp_ActiveDirectorySettings";
        public static readonly string RuleSetQuery = @"select RulesetDescription from [dbo].[RuleSet]";
        public static readonly string updatePolicyInfoStatus = @"updatePolicyInfo";
        public static readonly string getPolicyInfoStatus = @"getPolicyInfo";
        public static readonly string getTDRuleSetDirectiveInfo = @"getTDRuleSetDirectiveInfo";
        public static readonly string updateTDRulesetDirInfo = @"updateTDRulesetDirInfo";
        public static readonly string getSPUnAttendedApplyRulesSettings = @"getSPUnAttendedApplyRulesSettings";
        public static readonly string saveSPUnAttendedApplyRulesSettings = @"saveSPUnAttendedApplyRulesSettings";
        public static readonly string getAllRoleDirectives = @"getAllRoleDirectives";
        public static readonly string getUserRoleDirectives = @"getUserRoleDirectives";
        public static readonly string deleteUnattendedFolderSettings = @"deletefoldesettingsSp";
        public static readonly string GetRolesandGroupsNames = @"GetRolesandGroupsNamesSp";
        public static readonly string SaveSMTPSettigns = @"SaveSMTPSettings";
    }
}
