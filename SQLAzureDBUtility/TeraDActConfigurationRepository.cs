﻿using BusinessObjects;
using System.Data.SqlClient;
using System.Linq;

namespace Common.Repository.SQLAzureDBUtility
{
    public class TeraDActConfigurationRepository : BaseRepository<TeraDActConfigurationRepository>
    {
        public void getTeraDActConfiguration()
        {
            TeraDActConfiguration config = new TeraDActConfiguration();
            using (DBHelper db = new DBHelper(SqlAzureConnectionString()))
            {
                SqlDataReader Dr = db.ExecDataReaderProc(StringConstant.getTeraDActConfiguration);
                while (Dr.Read())
                {
                        config = new TeraDActConfiguration
                        {
                            ServerURL = Dr["ServerURL"] as string,
                            APIKey = Dr["APIKey"] as string,
                            BaseURLRedactSuffix = Dr["BaseURLRedactSuffix"] as string,
                             BaseURLRulesetSuffix = Dr["BaseURLRulesetSuffix"] as string,
                            RulesetXMLPathNode = Dr["RulesetXMLPathNode"] as string,
                        };

                    }
                Dr.Close();
                

            }
            TeraDActConfiguration.Settings = config;
           
        }
    }
}
