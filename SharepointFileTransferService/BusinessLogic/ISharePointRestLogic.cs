﻿using BusinessObjects;
using System.Collections.Generic;
namespace TeraDAct.SharepointFileTransferService.BusinessLogic
{
    public interface ISharePointRestLogic
    {
        string CheckAction(byte[] FileContent);

        string CreateReviewAction(string docName, byte[] FileContent);
        string CreateReviewSessionAction(byte[]  FileContent);
        string ReleaseReviewAction(byte[] FileContent,string roles);
        string ApplyRulesetAction(byte[] FileContent, string RuleSetName);
        RuleSetResponse ListRuleSet();
        string SharedView(byte[] FileContent, string SPLoggedUserName);
        string SharedRelease(byte[] FileContent,string LoggedUserEmailID);
        string Redact(byte[] FileContent,string RuleSetNames,string roles, string SPLoggedUserName);
        object CheckAccountPolicy();
        object UpdateAccountPolicy(TDPolicyInfo policyInfo);
        object getTDRuleDirectiveInfo(string ruleSetName="");
        object createORUpdateTDRuleDirectiveInfo(List<TDRuleSetDirectiveInfo> tdrulesetinfo);
        string UnattendedRedact(byte[] FileContent, byte[] RuleSetContent);
        string GetAccountPolicyFromTDServer();
        string getAllRoleDirectives();
    }
}