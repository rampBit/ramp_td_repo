﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SharepointFileTransferService.BusinessLogic
{
   internal class JSONUtility
    {

        internal  static string XML2JSON(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
           return  JsonConvert.SerializeXmlNode(doc);
        }

        internal static XmlDocument JSON2XML(string json)
        {
            return  JsonConvert.DeserializeXmlNode(json);

        }
    }
}
