﻿using BusinessObjects;
using Common.Repository.SQLAzureDBUtility;
using FileUtility;
using Newtonsoft.Json;
using SharepointFileTransferService.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace TeraDAct.SharepointFileTransferService.BusinessLogic
{
    public class SharePointRestLogic : ISharePointRestLogic
    {

        private string RestURL = "";
        private readonly string teradactBaseURL;
        private const string ForwardSlash = @"/";

        struct ActionInvokerType
        {
            internal string Review;
            internal string UserSesstion;
            internal string RuleSets;
            internal string Redact;
            internal string Policy;
        };
        ActionInvokerType ActionType;
        ActionRequestType RequestType;
        private readonly string bucketName = "RedactReviews";
        private readonly string PolicyName = "Redact";
        private readonly string RuleSetbucketName = "RedactRulesets";

        struct ActionRequestType
        {
            internal object ApplyRuleset;
            internal string Release;
            internal string GetPolicy;
            internal string UpdatePolicy;
            internal string ValidatePolicy;
            internal string GetLabels;
        }

        public SharePointRestLogic()
        {

            SharepointHelper.SetupTeraDActConfigSetting();
            teradactBaseURL = TeraDActConfiguration.Settings.ServerURL +
                TeraDActConfiguration.Settings.APIKey + ForwardSlash;
            AssingCustomAction();
            AssignActionRequestType();
        }

        private void AssignActionRequestType()
        {
            RequestType.Release = "release";
            RequestType.ApplyRuleset = "applyRuleset";
            RequestType.GetPolicy = "getPolicy";
            RequestType.UpdatePolicy = "update";
            RequestType.ValidatePolicy = "validatePolicySet";
            RequestType.GetLabels = "getLabels";
        }

        private void AssingCustomAction()
        {
            ActionType.Review = "review";
            ActionType.UserSesstion = "user_session";
            ActionType.RuleSets = "rulesets";
            ActionType.Redact = "redact";
            ActionType.Policy = "policy";
        }

        public string ApplyRulesetAction(byte[] FileContent, string RuleSetName)
        {
            var sha2hash = SharepointHelper.SHA512Generator(FileContent);


            RestURL = $"{teradactBaseURL}{ActionType.Review}?requestType={RequestType.ApplyRuleset}&reviewId={sha2hash}&bucketName={bucketName}";
            var rulesetcontent = FileService.GetBytesnyText("#include,RedactRulesets," + RuleSetName);
            using (ISharePointRestClient client = new SharePointRestClient(RestURL,
               SharePointRequestActionType.ApplyRuleset, null, rulesetcontent))
            {
                return FileService.ConvertBase64ToString(client.MakeRequest().Result);

            }



        }
        public string RedactAction(string ReviewID)
        {


            RestURL = $"{teradactBaseURL}{ActionType.Review}?requestType={RequestType.ApplyRuleset}&reviewId={ReviewID}";
            using (ISharePointRestClient client = new SharePointRestClient(RestURL,
               SharePointRequestActionType.ApplyRuleset))
            {
                return client.MakeRequest().Result;   //requires base64 string output .. for office docs

            }



        }
        enum orderby
        {
            creationDate
        }
        public string CheckAction(byte[] FileContent)
        {

            var sha2hash = SharepointHelper.SHA512Generator(FileContent);

            RestURL = $"{teradactBaseURL}{ActionType.Review}?bucketName={bucketName}&documentHash={sha2hash}&orderBy={orderby.creationDate}";
            using (ISharePointRestClient client = new SharePointRestClient(RestURL,
               SharePointRequestActionType.Check))
            {
                return FileService.ConvertBase64ToString(client.MakeRequest().Result);


            }
        }

        public string CreateReviewAction(string docName, byte[] FileContent)
        {
            var sha2hash = SharepointHelper.SHA512Generator(FileContent);

            RestURL = $"{teradactBaseURL}{ActionType.Review}?docName={docName}&bucketName={bucketName}&docHash={sha2hash}";
            using (ISharePointRestClient client = new SharePointRestClient(RestURL,
               SharePointRequestActionType.CreateReview, FileContent))
            {
                return FileService.ConvertBase64ToString(client.MakeRequest().Result);

            }
        }

        public string CreateReviewSessionAction(byte[] FileContent)
        {
            var sha2hash = SharepointHelper.SHA512Generator(FileContent);
            RestURL = $"{teradactBaseURL}{ActionType.UserSesstion}?reviewId={sha2hash}&type={ActionType.Review}&bucketName={bucketName}";
            using (ISharePointRestClient client = new SharePointRestClient(RestURL,
               SharePointRequestActionType.CreateReviewSession, FileContent))
            {
                return FileService.ConvertBase64ToString(client.MakeRequest().Result);

            }
        }

        public string ReleaseReviewAction(byte[] FileContent, string roles)
        {
            var sha2hash = SharepointHelper.SHA512Generator(FileContent);

            RestURL = $"{teradactBaseURL}{ActionType.Review}?reviewId={sha2hash}&requestType={RequestType.Release}&bucketName={bucketName}&releaseSetAttached=true";
            using (ISharePointRestClient client = new SharePointRestClient(RestURL,
               SharePointRequestActionType.ReleaseReview, FileContent))
            {
                client.ReleaseParams = string.IsNullOrEmpty(roles) ? "" : $"--release {roles}";
                return client.MakeRequest().Result;  //requires base64 string output .. for office docs

            }
        }
        private string RemoveXmlDefinition(string xml)
        {
            XDocument xdoc = XDocument.Parse(xml);
            xdoc.Declaration = null;
            return xdoc.ToString();
        }
        public RuleSetResponse ListRuleSet()
        {
            RestURL = $"{teradactBaseURL}{ActionType.RuleSets}";
            using (ISharePointRestClient client = new SharePointRestClient(RestURL,
               SharePointRequestActionType.ListRuleSet))
            {
                try
                {
                    var res = FileService.ConvertBase64ToString(client.MakeRequest().Result);
                    res = JSONUtility.XML2JSON(RemoveXmlDefinition(res.ToString()));
                    var result = JsonConvert.DeserializeObject<RuleSetResponse>(res);
                    return result;
                }
                catch
                {

                    return new RuleSetResponse();
                }

            }
        }
        public string Redact(byte[] FileContent, string RuleSetNames, string roles, string SPLoggedUserName)  //attended redact
        {

            RestURL = $"{teradactBaseURL}{ActionType.Redact}?rulesetAttached=true&releaseSetAttached=true&releaseConfigAttached=true";
            var rulesetcontent = FileService.GetBytesnyText("#include,RedactRulesets," + RuleSetNames);

            return this.Redact(RestURL, FileContent, rulesetcontent, roles, SPLoggedUserName, true);//b 64

        }
        string Redact(string url, byte[] FileContent, byte[] RuleSetContent, string roles = "",
            string SPLoggedUserName = "", bool isAttended = false)
        {

            using (ISharePointRestClient client = new SharePointRestClient(RestURL,
               SharePointRequestActionType.Redact, FileContent, RuleSetContent))
            {

                client.RCParams = GetAccountPolicyFromTDServer(); //get from TD or db
                if (string.IsNullOrEmpty(roles) && !isAttended)
                    roles = string.Join(",", SharePointRepository.Instance.getAllRoleDirectives());
                //for unattended get full roledirectives else get from selected roles from ui
                client.ReleaseParams = string.IsNullOrEmpty(roles) ? "" : $"--release {roles}";
                return client.MakeRequest().Result;  //base 64 

                //var respstr = FileService.ConvertBase64ToString(resp);
                //string nomatchesFound = "no matches found";
                //string noredactionreq = "No Redaction Required";
                //bool respStatus = true;
                //if (string.IsNullOrEmpty(respstr) || respstr.Length == 0) respStatus = false;

                //if (respStatus)
                //{

                //    file.RedactionStatus = OperationStatus.Succeded;
                //    if (!(respstr.EndsWith(nomatchesFound, StringComparison.OrdinalIgnoreCase)
                //        || respstr.StartsWith(noredactionreq, StringComparison.OrdinalIgnoreCase)))
                //        file.Content = Convert.FromBase64String(resp);  //base 64
                //    //else
                //    //    file.Content = Convert.FromBase64String(FileService.getStringFromBytes(file.Content));
                //}
            }

        }
        public string UnattendedRedact(byte[] FileContent, byte[] RuleSetContent)
        {

            RestURL = $"{teradactBaseURL}{ActionType.Redact}?rulesetAttached=true&releaseSetAttached=true&releaseConfigAttached=true";
            return this.Redact(RestURL, FileContent, RuleSetContent);
        }
        public string SharedView(byte[] FileContent, string SPLoggedUserName)
        {
            var roles = getUserRoleDirectives(SPLoggedUserName);
            var sha2hash = FileService.getStringFromBytes(FileContent);
            RestURL = $"{teradactBaseURL}{ActionType.UserSesstion}?reviewId={sha2hash}&releaseSetAttached=true&type={ActionType.Review}";
            using (ISharePointRestClient client = new SharePointRestClient(RestURL,
               SharePointRequestActionType.SharedView, FileContent))
            {
                client.ReleaseParams = string.IsNullOrEmpty(roles) ? "" : $"--release {roles}";
                return FileService.ConvertBase64ToString(client.MakeRequest().Result);

            }
        }
        string getUserRoleDirectives(string SPLoggedUserName)
        {
            if (SharepointHelper.IsSharePointOnPrem) return null;
            try
            {
                var _repo = new AzureActiveDirectoryUtility.AzureADRepository();
                var ADUserGroups = _repo.getAllADGroups(SPLoggedUserName);
                if (ADUserGroups == null || ADUserGroups.Count == 0)
                    throw new Exception($"AD Groups not available for user:{SPLoggedUserName}");
                var allgropusNames = ADUserGroups.Select(c => c.GroupDetail.DisplayName).ToList();
                return SharePointRepository.Instance.getUserRoleDirectives(string.Join(",", allgropusNames));


            }
            catch (Exception ex)
            {

                ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.SharePointAttendedCustomAction);
            }
            return null;
        }
        public string SharedRelease(byte[] FileContent, string SPLoggedUserName)
        {
            var roles = getUserRoleDirectives(SPLoggedUserName);
            var sha2hash = FileService.getStringFromBytes(FileContent);
            RestURL = $"{teradactBaseURL}{ActionType.Review}?reviewId={sha2hash}&requestType={RequestType.Release}&bucketName={bucketName}&releaseSetAttached=true";
            using (ISharePointRestClient client = new SharePointRestClient(RestURL,
               SharePointRequestActionType.SharedRelease, FileContent))
            {
                client.ReleaseParams = string.IsNullOrEmpty(roles) ? "" : $"--release {roles}";
                return client.MakeRequest().Result;  //base64

            }
        }

        public string GetAccountPolicyFromTDServer()
        {
            var policyinfo = SharePointRepository.Instance.getPolicyInfoStatus() as List<TDPolicyInfo>;
            if (policyinfo != null && policyinfo.Count > 0)
            {
                var policydata = policyinfo.Select(pinfo =>
                      $"{pinfo.PolicyType} {pinfo.PolicyKey} {pinfo.PolicyValue}").ToList();
                return string.Join("\n", policydata);
            }

            RestURL = $"{teradactBaseURL}{ActionType.Policy}?policyName={PolicyName}& requestType={RequestType.GetPolicy}";
            using (ISharePointRestClient client = new SharePointRestClient(RestURL,
             SharePointRequestActionType.GetAccountPolicy))
            {
                return FileService.ConvertBase64ToString(client.MakeRequest().Result);
            }

        }

        public object CheckAccountPolicy()
        {
            RestURL = $"{teradactBaseURL}{ActionType.Policy}?requestType={RequestType.ValidatePolicy}&releaseOnly=false";
            using (ISharePointRestClient client = new SharePointRestClient(RestURL,
             SharePointRequestActionType.CheckAccountPolicy))
            {
                return FileService.ConvertBase64ToString(client.MakeRequest().Result);

            }
            // return GetPolicyInfoStatus();
        }

        public object UpdateAccountPolicy(TDPolicyInfo PolicyInfo)
        {
            RestURL = $"{teradactBaseURL}{ActionType.Policy}?policyName={PolicyName}&requestType={RequestType.UpdatePolicy}";
            using (ISharePointRestClient client = new SharePointRestClient(RestURL,
           SharePointRequestActionType.UpdateAccountPolicy, FileService.GetBytesnyText(
             $"{PolicyInfo.PolicyType} {PolicyInfo.PolicyKey} {PolicyInfo.PolicyValue}{Environment.NewLine}{PolicyInfo.PolicyType} {PolicyInfo.PolicyKey} {PolicyInfo.PolicyValue}{Environment.NewLine}{PolicyInfo.PolicyType} {PolicyInfo.PolicyKey} {PolicyInfo.PolicyValue}{Environment.NewLine}{PolicyInfo.PolicyType} {PolicyInfo.PolicyKey} {PolicyInfo.PolicyValue}")))
            {
                var resp = FileService.ConvertBase64ToString(client.MakeRequest().Result);
                if (resp.EndsWith("updated Successfully", StringComparison.OrdinalIgnoreCase))
                {
                    //update DB
                    updatePolicyInfoStatus(PolicyInfo);

                }
                return resp;

            }
        }

        private void updatePolicyInfoStatus(TDPolicyInfo Policyinfo)
        {


            SharePointRepository.Instance.updatePolicyInfoStatus(Policyinfo);
        }
        private Object GetPolicyInfoStatus()
        {
            return SharePointRepository.Instance.getPolicyInfoStatus();
        }
        public object createORUpdateTDRuleDirectiveInfo(List<TDRuleSetDirectiveInfo> tdrulesetinfo)
        {

            string resp = string.Empty;
            tdrulesetinfo.ForEach(e =>
            {
                //employees.ToList().Foreach(u => { u.SomeProperty = null; u.OtherProperty = null; });

                var dbResponse = getTDRuleDirectiveInfo((e.RuleSetName.ToString())) as List<TDRuleSetDirectiveInfo>;
                if (dbResponse == null || dbResponse.Count == 0)  //create new
                {
                    RestURL = $"{teradactBaseURL}{ActionType.RuleSets}?rulesetName={e.RuleSetName}&bucketName={RuleSetbucketName}";
                    using (ISharePointRestClient client = new SharePointRestClient(RestURL,
                     SharePointRequestActionType.createTDRuleDirectiveInfo, FileService.GetBytesnyText(
                     e.TotalRLString)))
                    {
                        resp = FileService.ConvertBase64ToString(client.MakeRequest().Result);
                    }

                }

                else  //update
                {
                    var rulesetName = string.Empty;
                    if (e.NewRuleSetName != null) rulesetName = e.NewRuleSetName; else rulesetName = e.RuleSetName;
                    RestURL = $"{teradactBaseURL}{ActionType.RuleSets}?rulesetName={rulesetName.ToLower()}&bucketName={RuleSetbucketName}";
                    using (ISharePointRestClient client = new SharePointRestClient(RestURL,
                     SharePointRequestActionType.updateTDRuleDirectiveInfo, FileService.GetBytesnyText(
                     e.TotalRLString)))
                    {
                        resp = FileService.ConvertBase64ToString(client.MakeRequest().Result);
                    }

                }
                if (string.IsNullOrEmpty(resp) || resp.ToString().Contains("already"))
                {
                    resp = "Ruleset Updated Successfully.";
                    //update DB
                    updateTDRulesetDirInfo(e);

                }

            });
            return resp;
        }

        private void updateTDRulesetDirInfo(TDRuleSetDirectiveInfo tdrulesetinfo)
        {
            #region Commented
            //resp = resp.Replace("tns-dm:", "").Replace(":tns-dm", "");
            //XDocument doc = XDocument.Parse(resp);
            //var labeln = doc.Descendants().Where(c => c.Name.LocalName == "label").FirstOrDefault();
            //var replacementtextn = doc.Descendants().Where(c => c.Name.LocalName == "replacementText").FirstOrDefault();
            //var searchn = doc.Descendants().Where(c => c.Name.LocalName == "search").FirstOrDefault();
            //var typen = searchn == null ? null : searchn.Attributes().Where(c => c.Name.LocalName == "type").FirstOrDefault();
            //var patternn = searchn == null ? null : searchn;
            //TDRuleSetDirectiveInfo obj = new TDRuleSetDirectiveInfo
            //{
            //    label = labeln == null ? null : labeln.Value,
            //    replacementText = replacementtextn == null ? null : replacementtextn.Value,
            //    RuleSetName = rulesetname,
            //    Type = typen == null ? null : typen.Value,
            //    Pattern = patternn == null ? null : patternn.Value

            //};
            #endregion
            SharePointRepository.Instance.updateTDRulesetDirInfo(tdrulesetinfo);
        }

        public object getTDRuleDirectiveInfo(string ruleSetName = "")
        {
            return SharePointRepository.Instance.getTDRuleSetDirectiveInfo(ruleSetName);


        }

        public string getAllRoleDirectives()
        {
            RestURL = $"{teradactBaseURL}{ActionType.Policy}?policyName={PolicyName}&requestType={RequestType.GetLabels}";
            using (ISharePointRestClient client = new SharePointRestClient(RestURL,
             SharePointRequestActionType.getLabels))
            {
                return FileService.ConvertBase64ToString(client.MakeRequest().Result);

            }
        }
    }
}

