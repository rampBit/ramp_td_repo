﻿using Microsoft.Online.SharePoint.TenantAdministration;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharepointFileTransferService.BusinessLogic
{
    internal class SharePointUtility
    {
        internal List<SiteOwner> getAllSites(ClientContext clientContext,bool IsSharePointOnPrem=false,string AdminCentarlSiteURL="")
        {
                       List<SiteOwner> so = new List<SiteOwner>();
            if (!IsSharePointOnPrem)
            {

                using (clientContext)
                {

                    var tenant = new Tenant(clientContext);
                    SPOSitePropertiesEnumerable spp = tenant.GetSiteProperties(0, true);

                    clientContext.Load(spp);
                    clientContext.ExecuteQuery();

                    return spp.Select(s => new SiteOwner { sUrl = s.Url, Owner = s.Owner }).ToList();
                }
            }
            else
            {
                try
                {

               
            //    SPSecurity.RunWithElevatedPrivileges(delegate
            //{

                using (SPSite currentSite = new SPSite(AdminCentarlSiteURL))
                {
                    string relativerURL = currentSite.ServerRelativeUrl.ToString();
                    string[] split = relativerURL.Split('/');
                    string managedPath = split[1].ToString();
                    SPSiteCollection siteColl = currentSite.WebApplication.Sites;
                    foreach (SPSite site in siteColl)
                    {
                        if (site.ServerRelativeUrl.Contains(managedPath) == true)
                        {
                            so.Add(new SiteOwner { sUrl = site.Url });
                        }
                    }
                }
          //  });
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
            return so;

        }
        internal List<string> getDocmentLibraryList(ClientContext clientContext)
        {
            List<string> lib = new List<string>();

            using (clientContext)
            {

                var Libraries = clientContext.LoadQuery(clientContext.Web.Lists.Where
                    (l => l.BaseTemplate == 101));
                clientContext.ExecuteQuery();
                foreach (var docLib in Libraries)
                {

                    lib.Add(docLib.Title);
                }

            }


            return lib;
        }
        internal List<string> getFolderList(ClientContext clientContext,string docLibTitle)
        {
             
            using (clientContext)
            {
                Web web = clientContext.Web;
                List list = web.Lists.GetByTitle(docLibTitle);
                FolderCollection folders = list.RootFolder.Folders;
                clientContext.Load(web);
                clientContext.Load(list);
                clientContext.Load(folders);
                clientContext.ExecuteQuery();
               return  folders.Select(c => c.Name).ToList();

            }


            
        }

    }

    internal class SiteOwner
    {
        public string Owner { get; internal set; }
        public string sUrl { get; internal set; }
    }
}
