﻿using BusinessObjects;
using FileUtility;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using TeraDAct.SharepointFileTransferService;
using TeraDAct.SharepointFileTransferService.BusinessLogic;

namespace SharepointFileTransferService.BusinessLogic
{
    public class UnAttendedRedactLogic
    {
        SharePointQueue spQueue = new SharePointQueue
        {
            RequestFilesCount = 1,
            FilesProcessedCount = null,
            Status = RequestStatus.Requested,
            EventSources = ErrorSource.SharepointRedactServiceUnattendedWebJob,
            ActionType = "UnAttendedRedact",
            SPListItemID = null
        };
        public void processUnattendedRedaction(SPUnAttendedApplyRulesSettings redactSetting,
            ClientContext cxt, ClientContext targetctx)
        {
            try
            {
                var transactionOptions = new TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted,
                    Timeout = TransactionManager.MaximumTimeout
                };
                using (var ts = new TransactionScope(TransactionScopeOption.Suppress, transactionOptions))
                {
                    var allSourceFiles = RetriveSourceFiles(redactSetting, cxt); //get all SP files
                    RedactUnattendedFiles(
                        redactSetting, cxt, allSourceFiles);  //send for redact

                    foreach (var file in allSourceFiles)  //delete from source and upload redacted copy to target
                    {
                        spQueue.Status = RequestStatus.Redacted;
                        spQueue.SharePointQueueID = file.spQueueID;
                        spQueue.FilesProcessedCount = 1;
                        LogQueue(spQueue, 'U');

                        var tmpfile = cxt.Web.GetFileByServerRelativeUrl(
                            file.FileUrl);

                        tmpfile.DeleteObject();
                        cxt.ExecuteQuery();
                        spQueue.Status = RequestStatus.FileDeleted;
                        LogQueue(spQueue, 'U');
                        Web destWeb = targetctx.Web;   //upload starts
                        targetctx.Load(destWeb);
                        targetctx.ExecuteQuery();

                        var fileUrl = String.Format("{0}/{1}/{2}/{3}", destWeb.ServerRelativeUrl, redactSetting.TargetDocLib, redactSetting.TargetFolder,
                            file.FileName);
                        Microsoft.SharePoint.Client.File.SaveBinaryDirect(cxt, fileUrl,
                            FileService.GetStream(file.Content), true);
                        spQueue.Status = RequestStatus.SharePointFileUpload;
                        LogQueue(spQueue, 'U');
                    }
                    if (allSourceFiles != null && allSourceFiles.Count > 0)
                    {
                        //deleted context menu to target
                        IContextMenuManager _mngr = new ContextMenuManager(targetctx);
                        _mngr.DeleteAction();
                    }
                    ts.Complete();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void RedactUnattendedFiles(SPUnAttendedApplyRulesSettings redactSetting,
            ClientContext cxt, List<UnAttendedResponseFileInfo> sourceFiles)
        {
            var logic = new SharePointRestLogic();
            foreach (var file in sourceFiles)
            {
                file.Content = Convert.FromBase64String(logic.UnattendedRedact(file.Content,
                        FileService.GetBytesnyText("#include,RedactRulesets," + redactSetting.AppliedRuleSets)));

            }

        }
        void LogQueue(SharePointQueue spQueue, char Operation = 'I')
        {

            SharepointHelper.saveSharePointQueue(spQueue, Operation);
        }
        private List<UnAttendedResponseFileInfo> RetriveSourceFiles(SPUnAttendedApplyRulesSettings
            redactSetting, ClientContext cxt)
        {
            List<UnAttendedResponseFileInfo> requestedFiles = new List<UnAttendedResponseFileInfo>();


            using (cxt)
            {
                List list = cxt.Web.Lists.GetByTitle(redactSetting.SourceDocLib);
                cxt.Load(list, d => d.Title, d => d.RootFolder.Name, d => d.Id);
                cxt.Load(list.RootFolder);
                cxt.Load(list.RootFolder.Files);
                FolderCollection fcol = list.RootFolder.Folders;
                cxt.Load(fcol);
                cxt.ExecuteQuery();
                var foundFolderList = fcol.Where(folder => folder.Name.
                Equals(redactSetting.SourceFolder)).FirstOrDefault();

                if (foundFolderList != null)
                {
                    cxt.Load(foundFolderList.Files);

                    cxt.ExecuteQuery();
                    requestedFiles = (from file in foundFolderList.Files
                                      select new UnAttendedResponseFileInfo
                                      {
                                          FileName = file.Name,
                                         // ID = file.UniqueId,
                                          FileUrl = file.ServerRelativeUrl
                                      }).ToList();
                }
                else
                {

                    cxt.ExecuteQuery();
                    requestedFiles = (from file in list.RootFolder.Files
                                      select new UnAttendedResponseFileInfo
                                      {
                                          FileName = file.Name,
                                        //  ID = file.UniqueId,
                                          FileUrl = file.ServerRelativeUrl
                                      }).ToList();


                }
                foreach (var item in requestedFiles)
                {

                    var fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(cxt, item.FileUrl);
                    using (var fileStream = new MemoryStream())
                    {
                        fileInfo.Stream.CopyTo(fileStream);
                        item.Content = FileService.GetBytesFromStream(fileStream);
                    }
                    spQueue.FileName = item.FileName;
                    spQueue.Status = RequestStatus.FileDownloaded;
                    spQueue.SPListID = item.ID.ToString();
                    spQueue.SPHostURL = item.FileUrl;

                    LogQueue(spQueue);
                    item.spQueueID = spQueue.SharePointQueueID;
                    spQueue.SharePointQueueID = null;
                }
            }
            return requestedFiles;
        }
    }

    internal class UnAttendedResponseFileInfo
    {
        internal long? spQueueID;

        public byte[] Content { get; internal set; }
        public string FileName { get; set; }
        public string FileUrl { get; internal set; }
        public Guid ID { get; set; }
    }
}
