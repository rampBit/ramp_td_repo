﻿using BusinessObjects;
using Common.Repository.SQLAzureDBUtility;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security;
using TeraDAct.SharepointFileTransferService;
using TeraDAct.SharepointFileTransferService.BusinessLogic;
using static SharepointFileTransferService.ContextMenuManager;

namespace SharepointFileTransferService
{


    public class ContextMenuManager : IContextMenuManager
    {
        public class ShareActionTypeTitle
        {
            public string Title { get; set; }
            public string ActionType { get; set; }
            public int Sequence { get; set; }
            public string PageName { get; set; }
            public string SPFileTransferServiceURL { get; set; }
        }

        public class Constant
        {
            public static string ActionURL;

            public readonly static string EditControlBlock = "EditControlBlock";
            public static readonly string RegistrationId = "0x0101";

            public static readonly string Teradact = "Teradact";
        }
        ISharePointRestLogic logic;



        string imagepath = "";
        private readonly ClientContext clientContext;
        public static int SiteCount;
        private static int processedCtr = 0;

        public string UserName
        {
            get; set;
        }

        public string SPAppWebUrl
        {
            get; set;
        }
        

        public ContextMenuManager(ClientContext ctx)
        {
            logic = new SharePointRestLogic();
            this.clientContext = ctx;

            imagepath = AppDomain.CurrentDomain.BaseDirectory;
            // imagepath = imagepath.Remove(imagepath.IndexOf("bin"));
            imagepath = Path.Combine(imagepath, "teradactlogo.jpg");
        }

        public void CreateContextMenu()
        {


        }

        public void AddCustomMenu(List<ShareActionTypeTitle> titles, bool IsLibraryReq = false)
        {
            try
            {
                var cparams = "SPListItemId={ItemId}&SPListId={ListId}&ActionType=";
               
                foreach (ShareActionTypeTitle title in titles)
                {
                    Constant.ActionURL = $"{SPAppWebUrl}/CustomeActionMenuApp/Pages/{title.PageName}?{cparams}";
                    using (clientContext)
                    {
                        Site site = clientContext.Site;
                        Web web = clientContext.Web;
                        UserCustomAction userCustomAction;
                        if (IsLibraryReq)
                        {
                            List listcol = web.Lists.GetByTitle("SharedTeradactLib");
                            userCustomAction = listcol.UserCustomActions.Add();
                        }
                        else
                            userCustomAction = site.UserCustomActions.Add();
                        userCustomAction.Location = Constant.EditControlBlock;
                        userCustomAction.RegistrationType = UserCustomActionRegistrationType.ContentType;
                        userCustomAction.ImageUrl = imagepath;
                        userCustomAction.Sequence = title.Sequence;
                        userCustomAction.RegistrationId = Constant.RegistrationId;
                        userCustomAction.Title = title.Title;
                        userCustomAction.Url = $"{Constant.ActionURL}{title.ActionType}&SPHostUrl={SPAppWebUrl}&RemoteEndPointURL={title.SPFileTransferServiceURL}";
                       // userCustomAction.Url = Uri.EscapeDataString(userCustomAction.Url);
                       BasePermissions permission = new BasePermissions();
                        permission.Set(PermissionKind.ManageWeb);
                        userCustomAction.Rights = permission;
                        userCustomAction.Update();
                        clientContext.ExecuteQuery();

                    }
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.ContextMenuService);
            }
        }



        //public void HandleShareCustomActions(List<ShareActionTypeTitle> titles, bool IsDocLibReq = false)
        //{
        //    var bgw = new BackgroundWorker();
        //    bgw.DoWork += new DoWorkEventHandler(bgw_HandleShareAction);
        //    bgw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgw_HandleShareAction_End);
        //    bgw.ProgressChanged += new ProgressChangedEventHandler(BgwReportProgress);
        //    bgw.WorkerReportsProgress = true;
        //    bgw.WorkerSupportsCancellation = true;
        //    object[] param = { titles, IsDocLibReq };

        //    bgw.RunWorkerAsync(param);


        //}

        //private void bgw_HandleShareAction_End(object sender, RunWorkerCompletedEventArgs e)
        //{
        //    Console.WriteLine("Adding new Custom Action is under Progress.");
        //    var titles = ((object[])e.Result)[0] as List<ShareActionTypeTitle>;
        //    AddCustomMenu(titles, (bool)((object[])e.Result)[1]);
        //    Console.WriteLine("Finished Add Custom Action process. ");

        //    processedCtr++;
        //    if (processedCtr == SiteCount)
        //    {
        //        Console.WriteLine("Done now...");
        //        Environment.Exit(0);
        //    }

        //}

        //private void bgw_HandleShareAction(object sender, DoWorkEventArgs e)
        //{
        //    var res = DeleteAction();
        //    //if (res)
        //    //{

        //    //    Console.WriteLine("Adding new Custom Action is under Progress.");
        //    //    var titles = ((object[])e.Argument)[0] as List<ShareActionTypeTitle>;
        //    //    AddCustomMenu(titles, (bool)((object[])e.Argument)[1]);
        //    //    Console.WriteLine("Finished Add Custom Action process. ");

        //    //    e.Result = e.Argument;
        //    //}
        //    e.Result = e.Argument;
        //}

        //private void DeleteContextMenu()
        //{
        //    var bgw = new BackgroundWorker();
        //    bgw.DoWork += new DoWorkEventHandler(bgw_DeleteAction);
        //    bgw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgw_DeleteAction_End);
        //    bgw.ProgressChanged += new ProgressChangedEventHandler(BgwReportProgress);
        //    bgw.WorkerReportsProgress = true;
        //    bgw.WorkerSupportsCancellation = true;

        //    bgw.RunWorkerAsync();

        //}

        //private void BgwReportProgress(object sender, ProgressChangedEventArgs e)
        //{

        //}

        //private void bgw_DeleteAction_End(object sender, RunWorkerCompletedEventArgs e)
        //{


        //}
        public bool DeleteAction()
        {
            Console.WriteLine("Delete Custom Action is under progress");
            bool isDeleted = false;
            using (clientContext)
            {
                try
                {
                    Site site = clientContext.Site;
                    // Web web = clientContext.Web;
                    // List listcol = web.Lists.GetByTitle(parameters[0] as string);
                    UserCustomActionCollection userCustomActionCol = site.UserCustomActions;
                    clientContext.Load(userCustomActionCol);
                    clientContext.ExecuteQuery();
                    var uccol = userCustomActionCol.Where(c =>
                   c.Location == Constant.EditControlBlock && (
                   c.Title.StartsWith(Constant.Teradact, StringComparison.OrdinalIgnoreCase)
                   )).ToList();

                    foreach (var userCustomAction in uccol)
                    {
                        userCustomAction.DeleteObject();
                        clientContext.Load(userCustomAction);
                        clientContext.ExecuteQuery();

                    }
                    isDeleted = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.ContextMenuService);


                }
                Console.WriteLine("Finished Delete Custom Action Process. ");
                return isDeleted;
            }


        }
        //public void bgw_DeleteAction(object sender, DoWorkEventArgs e)
        //{

        //    e.Result = DeleteAction();


        //}
    }
    public interface IContextMenuManager
    {
        string UserName { get; set; }
        string SPAppWebUrl { get; set; }
        



        bool DeleteAction();
        void AddCustomMenu(List<ShareActionTypeTitle> titles, bool IsDocLibReq = false);
    }

}
