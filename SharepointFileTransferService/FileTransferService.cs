﻿using BusinessObjects;
using Common.Repository.SQLAzureDBUtility;
using FileUtility;
using Microsoft.SharePoint.Client;
using SharepointFileTransferService;
using SharepointFileTransferService.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Xml.Linq;
using TeraDAct.SharepointFileTransferService.BusinessLogic;
using static SharepointFileTransferService.ContextMenuManager;

namespace TeraDAct.SharepointFileTransferService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.

    public class FileTransferService : IFileTransferService
    {
        ISharePointRestLogic logic;
        SharePointUtility utility;
        public FileTransferService()
        {
            logic = new SharePointRestLogic();
        }
        public FileTransferService(string SharePointSubscriptionUserID, string SharePointSubscriptionPassword)
        {
            logic = new SharePointRestLogic();
            this.SharePointSubscriptionUserID = SharePointSubscriptionUserID;
            var securePassword = new SecureString();
            foreach (char c in SharePointSubscriptionPassword)
                securePassword.AppendChar(c);
            this.SharePointSubscriptionPassword = securePassword;
            this.SharePointSubscriptionClearPassword = SharePointSubscriptionPassword;
            utility = new SharePointUtility();
            this. IsSharePointOnPrem = bool.Parse(
                  ConfigurationManager.AppSettings["IsSharePointOnPrem"] == null ? "false" :
                  ConfigurationManager.AppSettings["IsSharePointOnPrem"]);

        }

        public SecureString SharePointSubscriptionPassword
        {
            get; set;
        }

        public string SharePointSubscriptionUserID
        {
            get; set;
        }

        public string AdminCentarlSiteURL
        {
            get; set;
        }

        public string SPFileTransferServiceURL
        {
            get; set;
        }

        public string Domain
        {
            get; set;
        }
        public string SharePointSubscriptionClearPassword { get; set; }
        public bool IsSharePointOnPrem { get; private set; }

        public ClientContext getClientContext(SharepointContexRequest request)
        {
            ClientContext clientContext = null;
           
            if (IsSharePointOnPrem) 
            {
                var Netcreds = new NetworkCredential(SharePointSubscriptionUserID,
                    this.SharePointSubscriptionClearPassword, this.Domain);
                clientContext = new ClientContext(request.SPHostURL)
                {
                    Credentials = Netcreds
                };
            }
            else
            {
                var creds = new SharePointOnlineCredentials(SharePointSubscriptionUserID, SharePointSubscriptionPassword);
                clientContext = new ClientContext(request.SPHostURL)
                {
                    Credentials = creds
                };
            }
            request.ClientContext = clientContext;
            clientContext.ExecutingWebRequest += context_ExecutingWebRequest;
            return clientContext;
        }

        private void context_ExecutingWebRequest(object sender, WebRequestEventArgs e)
        {
            e.WebRequestExecutor.WebRequest.PreAuthenticate = true;
        }

        void LogQueue(SharePointQueue spQueue, char Operation = 'I')
        {

            SharepointHelper.saveSharePointQueue(spQueue, Operation);
        }
        public SharePointCustomActionResult ProcessFile(SharepointContexRequest request)
        {

            FileObject sharepointRequestedFile = null; ;
            var result = new SharePointCustomActionResult();
            string reviewout = string.Empty;
            string checkactionresult = "";
            switch (request.SharePointRequestActionType)
            {
                case SharePointRequestActionType.Check:
                    {
                        sharepointRequestedFile = DownloadFile(request);
                        if (!CheckAction(sharepointRequestedFile, ref checkactionresult))
                        {
                            reviewout = createreview(sharepointRequestedFile);
                            const string successreviewmessage = "Review created";

                            if (reviewout.StartsWith(successreviewmessage, StringComparison.InvariantCultureIgnoreCase))
                                result.CheckActionResult = ApplyRuleSet(
                                    sharepointRequestedFile.Content, request.RuleSetName);
                            else
                                result.CheckActionResult = reviewout;
                        }

                        else
                            result.CheckActionResult = ApplyRuleSet(sharepointRequestedFile.Content, request.RuleSetName);
                        break;
                    }
                case SharePointRequestActionType.CreateReview:
                    {
                        sharepointRequestedFile = DownloadFile(request);
                        string CreateReviewSessionout = "";
                        if (CheckAction(sharepointRequestedFile, ref checkactionresult))
                        {
                            CreateReviewSessionout = CreateReviewSession(sharepointRequestedFile.Content);
                        }
                        else
                        {
                            reviewout = createreview(sharepointRequestedFile);
                            const string successreviewmessage = "Review created";
                            if (reviewout.StartsWith(successreviewmessage, StringComparison.InvariantCultureIgnoreCase))
                                CreateReviewSessionout = CreateReviewSession(sharepointRequestedFile.Content);
                            else
                                CreateReviewSessionout = reviewout;
                        }
                        result.CreateReviewActionResult = CreateReviewSessionout;
                        break;
                    }

                case SharePointRequestActionType.ReleaseReview:
                    {
                        sharepointRequestedFile = DownloadFile(request);
                        string ReleaseReviewout = "";
                        if (CheckAction(sharepointRequestedFile, ref checkactionresult))
                        {
                            ReleaseReviewout = releasereview(sharepointRequestedFile.Content, request.Roles);
                            request.RedactedFileInfo = new FileObject
                            {
                                Content = Convert.FromBase64String(ReleaseReviewout),
                                FileName = sharepointRequestedFile.FileName
                            };

                            UploadFileToRedactedDocumentLibrary(request);
                            ReleaseReviewout = sharepointRequestedFile.FileName;

                        }
                        else
                            result.ReleaseReviewActionResult = ReleaseReviewout;
                        break;

                    }
                case SharePointRequestActionType.Redact:
                    {
                        sharepointRequestedFile = DownloadFile(request);

                        string redactedContent = this.Redact(sharepointRequestedFile.Content,
                          sharepointRequestedFile.FileName, request.RuleSetName, request.Roles, request.UserName);


                        sharepointRequestedFile.Content = Convert.FromBase64String(redactedContent);  //base 64
                        request.RedactedFileInfo = new FileObject
                        {
                            FileName = sharepointRequestedFile.FileName,
                            Content = sharepointRequestedFile.Content
                        };
                        this.UploadFileToRedactedDocumentLibrary(request);
                        request.RedactedFileInfo.Content = null;
                        result.RedactActionResult = request.RedactedFileInfo;

                        break;
                    }
                case SharePointRequestActionType.Share:
                    {
                        request.RedactedDocumentLibraryName = request.SharedDocumentLibName;
                        sharepointRequestedFile = DownloadFile(request);
                        //check for review
                        if (!CheckAction(sharepointRequestedFile, ref checkactionresult))
                            result.ShareActionResult = checkactionresult;
                        else
                        {
                            var sha = SharepointHelper.SHA512Generator(
                          sharepointRequestedFile.Content);
                            var sharb64 = FileService.GetBytesnyText(sha);
                            request.RedactedFileInfo = new FileObject
                            {
                                Content = sharb64,
                                FileName = sharepointRequestedFile.FileName + ".txt"
                            };

                            UploadFileToRedactedDocumentLibrary(request);
                            result.ShareActionResult = "";
                        }
                        break;
                    }

                case SharePointRequestActionType.SharedView:
                    {
                        sharepointRequestedFile = DownloadFile(request);
                        request.RedactedFileInfo = new FileObject
                        {
                            Content = sharepointRequestedFile.Content,
                            FileName = SharepointHelper.TrimEnd(sharepointRequestedFile.FileName, ".txt")
                        };

                        result.SharedViewActionResult = logic.SharedView(
                         sharepointRequestedFile.Content, request.UserName);
                        break;
                    }
                case SharePointRequestActionType.SharedRelease:
                    {
                        sharepointRequestedFile = DownloadFile(request);
                        request.RedactedFileInfo = new FileObject
                        {
                            Content = sharepointRequestedFile.Content,
                            FileName = SharepointHelper.TrimEnd(sharepointRequestedFile.FileName, ".txt")
                        };
                        string releasedContent = logic.SharedRelease(
                         sharepointRequestedFile.Content, request.UserName);
                        request.RedactedFileInfo = new FileObject
                        {
                            FileName = SharepointHelper.TrimEnd(sharepointRequestedFile.FileName, ".txt"),
                            Content = Convert.FromBase64String(releasedContent)
                        };
                        this.UploadFileToRedactedDocumentLibrary(request);
                        request.RedactedFileInfo.Content = null;
                        result.SharedReleaseActionResult = "";

                        break;
                    }
                case SharePointRequestActionType.CheckAccountPolicy:
                    {
                        result.CheckAccountPolicyActionResult = logic.CheckAccountPolicy();
                        break;
                    }
                case SharePointRequestActionType.GetAccountPolicy:
                    {
                        result.GetAccountPolicyActionResult = logic.GetAccountPolicyFromTDServer();
                        break;
                    }
                case SharePointRequestActionType.getTDRuleDirectiveInfo:
                    {
                        result.TDRuleDirectiveInfo = logic.getTDRuleDirectiveInfo(request.RuleSetName);
                        break;

                    }
                default:
                    break;
            }
            return result;
        }

        private string getReviewID(string checkactionresult)
        {
            var XMLDoc = XDocument.Parse(checkactionresult);
            var revNode = XMLDoc.Descendants("review_id").FirstOrDefault();
            if (revNode != null)
                return revNode.Value;
            return string.Empty;
        }
        public RuleSetResponse ListRuleSetFromTD()
        {
            logic = new SharePointRestLogic();
            return logic.ListRuleSet();

        }
        private string releasereview(byte[] FileContent, string roles)
        {
            return logic.ReleaseReviewAction(FileContent, roles);
        }

        private string CreateReviewSession(byte[] FileContent)
        {

            return logic.CreateReviewSessionAction(FileContent);

        }

        private bool CheckAction(FileObject sharepointRequestedFile, ref string checkactionresult)
        {
            const string NoReviewFoundConst = "No review found for hash";
            const string BucketNotFoundError = "Bucket not found";
            checkactionresult = logic.CheckAction(sharepointRequestedFile.Content);

            return !(checkactionresult.StartsWith(NoReviewFoundConst, StringComparison.InvariantCultureIgnoreCase)
               || checkactionresult.StartsWith(BucketNotFoundError, StringComparison.InvariantCultureIgnoreCase));

        }

        private string createreview(FileObject sharepointRequestedFile)
        {
            return logic.CreateReviewAction(
                          sharepointRequestedFile.FileName, sharepointRequestedFile.Content);
        }
        private string ApplyRuleSet(byte[] FileContent, string RuleSetName)
        {

            return logic.ApplyRulesetAction(
                            FileContent, RuleSetName);


        }
        public List<string> getAllSites(SharepointContexRequest request)
        {
            try
            {
                return utility.getAllSites(getClientContext(request),IsSharePointOnPrem, AdminCentarlSiteURL).
                    //  Where(so=>string.IsNullOrEmpty(so.Owner)).
                    Select(s => s.sUrl).ToList();
            }
            catch (Exception ex)
            {

                ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.FileTransferService);
                throw ex;
            }

        }
        public List<string> getDocmentLibraryList(SharepointContexRequest request)
        {

            try
            {
                return utility.getDocmentLibraryList(getClientContext(request)).
                    Where(lb => !lb.Equals(request.SharedDocumentLibName)).ToList();

            }
            catch (Exception ex)
            {

                ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.FileTransferService);
                throw ex;
            }

        }
        public List<string> getFolderList(SharepointContexRequest request)
        {

            try
            {
                return utility.getFolderList(getClientContext(request), request.RedactedDocumentLibraryName);

            }
            catch (Exception ex)
            {

                ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.FileTransferService);
                throw ex;
            }

        }
        private FileObject DownloadFile(SharepointContexRequest request)
        {
            FileObject RedactedFile = new FileObject();
            SharePointRepository spRepository = new SharePointRepository();
            try
            {
                using (var clientContext = getClientContext(request))

                {
                    var list = clientContext.Web.Lists.GetById(new Guid(request.SPListId));
                    var listItem = list.GetItemById(request.SPlistItemId);
                    clientContext.Load(list);
                    clientContext.Load(listItem, i => i.File);
                    clientContext.ExecuteQuery();
                    SharePointQueue spQueue = new SharePointQueue
                    {
                        RequestFilesCount = 1,
                        FilesProcessedCount = null,
                        Status = RequestStatus.Requested,
                        FileName = (string)listItem.File.Name,
                        SPListID = request.SPListId,
                        SPListItemID = request.SPlistItemId,
                        SPHostURL = request.SPHostURL,
                        EventSources = ErrorSource.FileTransferService,
                        ActionType = request.SharePointRequestActionType.ToString(),
                    };
                    request.spQueue = spQueue;
                    LogQueue(spQueue);
                    var fileRef = listItem.File.ServerRelativeUrl;
                    var fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(clientContext, fileRef);
                    spQueue.Status = RequestStatus.FileDownloaded;
                    LogQueue(spQueue, 'U');

                    using (var fileStream = new MemoryStream())
                    {
                        fileInfo.Stream.CopyTo(fileStream);
                        var fileo = new FileObject
                        {
                            Content = FileService.GetBytesFromStream(fileStream),
                            FileName = (string)listItem.File.Name
                        };
                        RedactedFile = fileo;
                    }

                    if (request.FileOperationMode == FileOperationMode.Move &&
                       (request.SharePointRequestActionType == SharePointRequestActionType.Redact ||
                      request.SharePointRequestActionType == SharePointRequestActionType.ReleaseReview))
                    {
                        try
                        {

                            listItem.DeleteObject();
                            clientContext.ExecuteQuery();
                            spQueue.Status = RequestStatus.FileDeleted;
                            LogQueue(spQueue, 'U');
                        }
                        catch (Exception ex)
                        {
                            spQueue.Status = RequestStatus.FileNotDeleted;
                            ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.FileTransferService);
                            LogQueue(spQueue, 'U');
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.FileTransferService);
            }
            return RedactedFile;
        }

        private string Redact(byte[] byteContent, string FileName, string SPRuleSetNames, string roles, string SPLoggedUserName)
        {
            return logic.Redact(byteContent, SPRuleSetNames, roles, SPLoggedUserName);

        }

        private void UploadFileToRedactedDocumentLibrary(SharepointContexRequest request)
        {
            if (!string.IsNullOrEmpty(request.SourceRedactedSiteURL))
                request.SPHostURL = request.SourceRedactedSiteURL;
            var clientContext = getClientContext(request);
            using (clientContext)
            {
                Web web = clientContext.Web;
                FileCreationInformation newFile = new FileCreationInformation();
                newFile.Overwrite = true;

                newFile.Content = request.RedactedFileInfo.Content;
                newFile.Url = request.RedactedFileInfo.FileName;
                foreach (var RedactedDocumentLibraryName in request.RedactedDocumentLibraryName.Split(new char[] { ',' }))
                {
                    SharepointHelper.CreateRedactedDocumentLib(clientContext, RedactedDocumentLibraryName);
                    var docs = web.Lists.GetByTitle(RedactedDocumentLibraryName);
                    var uploadFile = docs.RootFolder.Files.Add(newFile);
                    clientContext.Load(uploadFile);
                    clientContext.ExecuteQuery();
                }
            }

            ///////remove actions from shared site ////////
            if (request.SharePointRequestActionType == SharePointRequestActionType.Share)
            {

                HandleShareCustomActions(clientContext, request.UserName, request.SPHostURL);


            }
        }

        private void HandleShareCustomActions(ClientContext clientContext, string UserName, string SPAppWebUrl)
        {
            IContextMenuManager _manager = new ContextMenuManager(clientContext);
            _manager.UserName = UserName;
            _manager.SPAppWebUrl = SPAppWebUrl;

            var titles = new List<ShareActionTypeTitle>
                            {
                              new ShareActionTypeTitle {  Title =
                                  $"{ Constant.Teradact} Shared View",
                                  ActionType ="SharedView",
                                   Sequence=10007,PageName="ShareCustomAction.aspx",
                                    SPFileTransferServiceURL=this.SPFileTransferServiceURL
                              },

                new ShareActionTypeTitle {  Title =
                                  $"{ Constant.Teradact} Shared Release",
                                  ActionType ="SharedRelease",
                                   Sequence=10008,PageName="ShareCustomAction.aspx",
                                    SPFileTransferServiceURL=this.SPFileTransferServiceURL
                              }
                            };
            if (_manager.DeleteAction())
                _manager.AddCustomMenu(titles, false);


        }

        private object ContextMenuManager()
        {
            throw new NotImplementedException();
        }

        public static List<SharePointOnlineSettings> GetSharePointSettingsDetails()
        {
            try
            {

                var details = SharePointRepository.Instance.GetSharePointSettingsDetailsSP();
                if (details != null)
                    return details;
            }
            catch (Exception ex)
            {
                ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.SharePointAttendedCustomAction);
            }
            return null;
        }

        public object createORUpdateTDRuleDirectiveInfo(List<TDRuleSetDirectiveInfo> tdrulesetinfo)
        {
            try
            {

                return logic.createORUpdateTDRuleDirectiveInfo(tdrulesetinfo);
            }
            catch (Exception ex)
            {
                ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.FileTransferService);
            }
            return null;
        }

        public object updateAccountPolicy(TDPolicyInfo policyinfo)
        {
            try
            {

                return logic.UpdateAccountPolicy(policyinfo);
            }
            catch (Exception ex)
            {
                ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.FileTransferService);
            }
            return null;
        }

        public void processUnAttendedRedact(SPUnAttendedApplyRulesSettings redactSetting)
        {
            try
            {
                var request = new SharepointContexRequest();
                request.SPHostURL = redactSetting.SourceSiteURL;
                UnAttendedRedactLogic _logic = new UnAttendedRedactLogic();
                var targetrequest = new SharepointContexRequest();
                targetrequest.SPHostURL = redactSetting.TargetSiteURL;

                _logic.processUnattendedRedaction(redactSetting,
                  getClientContext(request), getClientContext(targetrequest));


            }
            catch (Exception ex)
            {

                ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.FileTransferService);
            }


        }

        public string getAllRoleDirectives()
        {
            try
            {

                return logic.getAllRoleDirectives();
            }
            catch (Exception ex)
            {
                ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.FileTransferService);
            }
            return null;
        }
    }
}
