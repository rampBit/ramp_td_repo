﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Security;
using BusinessObjects;

namespace TeraDAct.SharepointFileTransferService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.

    public interface IFileTransferService
    {
        SecureString SharePointSubscriptionPassword { get; set; }
        string SharePointSubscriptionUserID { get; set; }
        string AdminCentarlSiteURL { get; set; }
        string SPFileTransferServiceURL { get; set; }
        string Domain { get; set; }

        List<string> getAllSites(SharepointContexRequest clientContext);
        ClientContext getClientContext(SharepointContexRequest request);

        SharePointCustomActionResult ProcessFile(SharepointContexRequest request);
        RuleSetResponse ListRuleSetFromTD();
      List<string>   getDocmentLibraryList(SharepointContexRequest request);
        object createORUpdateTDRuleDirectiveInfo(List<TDRuleSetDirectiveInfo> tdrulesetinfo);
        object updateAccountPolicy(TDPolicyInfo policyinfo);
        List<string> getFolderList(SharepointContexRequest request);
        void processUnAttendedRedact(SPUnAttendedApplyRulesSettings redactSetting);
        string getAllRoleDirectives();
    }
    public  enum FileOperationMode
    {
        Copy,
        Move
    }
    
    public class SharepointContexRequest
    {
         
        public ClientContext ClientContext { get; set; }
        public string RedactedDocumentLibraryName { get;   set; }
        public FileObject RedactedFileInfo { get; internal set; }
        public string RuleSetName { get; set; }
        public SharePointRequestActionType SharePointRequestActionType { get;   set; }
        public string SPHostURL { get; set; }

        public string SPListId { get; set; }

        public int SPlistItemId { get; set; }
        public SharePointQueue spQueue { get; internal set; }
        public FileOperationMode FileOperationMode { get; set; }
        public string SourceRedactedSiteURL { get; set; }
        public string UserName { get; set; }
        public string SPAppWebUrl { get; set; }
        public string Roles { get;   set; }
        public string SharedDocumentLibName { get;   set; }
    }

     
}
