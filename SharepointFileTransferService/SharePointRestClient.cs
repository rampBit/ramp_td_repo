﻿using BusinessObjects;
using Common.Repository.SQLAzureDBUtility;
using FileUtility;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace TeraDAct.SharepointFileTransferService
{
    internal enum HttpVerb
    {
        GET,
        POST,
        PUT,
        DELETE
    }
    public enum SharePointRequestActionType
    {
        Check,
        CreateReview,
        CreateReviewSession,
        ReleaseReview,
        Share,
        View,
        Download,
        ApplyRuleset,
        Redact,
        ListRuleSet,
        SharedView,
        SharedRelease,
        CheckAccountPolicy,
        UpdateAccountPolicy,
        getTDRuleDirectiveInfo,
        createTDRuleDirectiveInfo,
        updateTDRuleDirectiveInfo,
        GetAccountPolicy,
        getLabels
    }
    internal class SharePointRestClient : ISharePointRestClient
    {
        public string EndPoint { get; set; }
        public byte[] content { get; set; }
        public SharePointRequestActionType SharePointRequestActionType { get; set; }
        public byte[] RuleSetContent { get; private set; }

        public string ReleaseParams
        {
            get; set;
        }
        public string RCParams { get; set; }

        public SharePointRestClient(string endpoint, SharePointRequestActionType
            SharePointRequestActionType,
           byte[] content = null, byte[] RuleSetContent = null)
        {
            EndPoint = endpoint;
            this.SharePointRequestActionType = SharePointRequestActionType;
            this.content = content;
            this.RuleSetContent = RuleSetContent;
        }
        private MultipartFormDataContent multiPartContent = new MultipartFormDataContent();

        public async Task<string> MakeRequest()
        {
            DateTime startedOn = DateTime.Now;
            using (var client = new HttpClient(new HttpClientHandler()))
            {
                var request = new HttpRequestMessage(
                   HttpMethod.Get, EndPoint);
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Encoding", "gzip, deflate");
                switch (SharePointRequestActionType)
                {
                    case SharePointRequestActionType.Check:
                        {
                            //    client.DefaultRequestHeaders.Accept.Add(
                            //new MediaTypeWithQualityHeaderValue("application/x-protobuf"));
                            break;
                        }
                    case SharePointRequestActionType.CreateReview:
                        {
                            request.Method = HttpMethod.Put;  //pass file content
                            request.Content = new StreamContent(FileService.GetStream(this.content));
                            break;
                        }
                    case SharePointRequestActionType.CreateReviewSession:
                        {
                            request.Method = HttpMethod.Put;  //no part
                                                              //multiPartContent.Headers.TryAddWithoutValidation
                                                              //      ("Content-Type", "multipart/form-data;");
                                                              //HttpContent rlsContent = new StringContent("Devgroup,User");
                                                              //multiPartContent.Add(rlsContent, "rls");

                            // request.Content = multiPartContent;
                            break;
                        }
                    case SharePointRequestActionType.SharedView:
                        {
                            request.Method = HttpMethod.Put;

                            multiPartContent.Headers.TryAddWithoutValidation
                                ("Content-Type", "multipart/form-data;");

                            HttpContent rlsContent = new StringContent(ReleaseParams);
                            multiPartContent.Add(rlsContent, "rls");




                            request.Content = multiPartContent;



                            break;
                        }
                    case SharePointRequestActionType.ReleaseReview:
                        {
                            request.Method = HttpMethod.Post;
                            multiPartContent.Headers.TryAddWithoutValidation
                                   ("Content-Type", "multipart/form-data;");
                            HttpContent fileStreamContent = new ByteArrayContent(this.content);
                            fileStreamContent.Headers.Add("Content-Type", "application/octet-stream");
                            fileStreamContent.Headers.Add("Content-Transfer-Encoding", "binary");
                            multiPartContent.Add(fileStreamContent, "doc");

                            HttpContent rlsContent = new StringContent(ReleaseParams);
                            multiPartContent.Add(rlsContent, "rls");

                            request.Content = multiPartContent;
                            break;
                        }
                    case SharePointRequestActionType.SharedRelease:
                        {
                            request.Method = HttpMethod.Post;
                            multiPartContent.Headers.TryAddWithoutValidation
                                ("Content-Type", $"multipart/form-data;");
                            HttpContent fileStreamContent = new ByteArrayContent(this.content);
                            fileStreamContent.Headers.Add("Content-Type", "application/octet-stream");
                            fileStreamContent.Headers.Add("Content-Transfer-Encoding", "binary");
                            multiPartContent.Add(fileStreamContent, "doc");

                            HttpContent rlsContent = new StringContent(ReleaseParams);
                            multiPartContent.Add(rlsContent, "rls");

                            request.Content = multiPartContent;


                            break;
                        }
                    case SharePointRequestActionType.ApplyRuleset:
                        {
                            request.Method = HttpMethod.Post;
                            request.Content = new StreamContent(FileService.GetStream(this.RuleSetContent));
                            break;
                        }
                    case SharePointRequestActionType.ListRuleSet:
                        {
                            break;
                        }
                    case SharePointRequestActionType.Redact:  //for redact and create review pass file 
                        {
                            request.Method = HttpMethod.Put;
                            multiPartContent.Headers.TryAddWithoutValidation
                                ("Content-Type", $"multipart/form-data;");

                            HttpContent fileStreamContent = new ByteArrayContent(this.content);
                            fileStreamContent.Headers.Add("Content-Type", "application/octet-stream");
                            fileStreamContent.Headers.Add("Content-Transfer-Encoding", "binary");

                            HttpContent RuleSetFileContent = new ByteArrayContent(this.RuleSetContent);
                            RuleSetFileContent.Headers.Add("Content-Type", "text/plain");
                            RuleSetFileContent.Headers.Add("Content-Transfer-Encoding", "8bit");

                            multiPartContent.Add(fileStreamContent, "doc");
                            multiPartContent.Add(RuleSetFileContent, "trs");
                            HttpContent rlsContent = new StringContent(ReleaseParams);
                            multiPartContent.Add(rlsContent, "rls");
                            HttpContent rcContent = new StringContent(RCParams);
                            multiPartContent.Add(rcContent, "rc");

                            request.Content = multiPartContent;

                            break;
                        }
                    case SharePointRequestActionType.GetAccountPolicy:
                        {
                            break;
                        }
                    case SharePointRequestActionType.CheckAccountPolicy:
                        {
                            request.Method = HttpMethod.Put;
                            request.Content = new StreamContent(FileService.GetStream(FileService.GetBytesnyText("--options")));
                            //client.DefaultRequestHeaders.TryAddWithoutValidation("options", "\r\n--options");
                            break;
                        }
                    case SharePointRequestActionType.UpdateAccountPolicy:
                        {
                            request.Method = HttpMethod.Put;
                            request.Content = new StreamContent(FileService.GetStream(this.content));
                            break;
                        }
                    case SharePointRequestActionType.getTDRuleDirectiveInfo:
                        {
                            break;
                        }
                    case SharePointRequestActionType.createTDRuleDirectiveInfo:
                        {
                            request.Method = HttpMethod.Put;
                            request.Content = new StreamContent(FileService.GetStream(this.content));
                            break;
                        }
                    case SharePointRequestActionType.updateTDRuleDirectiveInfo:
                        {
                            request.Method = HttpMethod.Post;
                            request.Content = new StreamContent(FileService.GetStream(this.content));
                            break;
                        }
                    case SharePointRequestActionType.getLabels:
                        {
                            break;
                        }
                    default:
                        break;
                }
                string resp;


                resp = SendToAsync(() => client, () => request);
                var orgresp = FileService.ConvertBase64ToString(resp);
                string nomatchesFound = "no matches found";
                string noredactionreq = "No Redaction Required";

                if ((string.IsNullOrEmpty(resp) || resp.Length == 0
                    || orgresp.EndsWith(nomatchesFound, StringComparison.OrdinalIgnoreCase)
                    || orgresp.StartsWith(noredactionreq, StringComparison.OrdinalIgnoreCase)
                    ) && (this.content != null))
                    resp = Convert.ToBase64String(this.content);
                return resp;

            }


        }

        private string SendToAsync(Func<HttpClient> client,
            Func<HttpRequestMessage> request)
        {
            string result = null;
            string result64 = null;
            DateTime startedOn = DateTime.Now;
            try
            {

                //var t = client().GetStringAsync(request().RequestUri).ConfigureAwait(false);  //await client.SendAsync(request);
                response = client().SendAsync(request()).Result;
                result = response.Content.ReadAsStringAsync().Result;
                result64 = Convert.ToBase64String(response.Content.ReadAsByteArrayAsync().Result);
                if (result.Contains("System.Net.Sockets.SocketException"))
                    throw new Exception(result);
            }
            catch (Exception ex)
            {

                ErrorLogRepository.Instance.WriteErrorLog(ex, BusinessObjects.ErrorSource.SharePointAttendedCustomAction);
                return null;
            }
            finally
            {
                multiPartContent.Dispose();
                var log = new RestLogInfo
                {
                    RequestMethod = request().Method.ToString(),
                    RequestUrl = request().RequestUri.ToString(),
                    ResponseStatusCode = response == null ? "NULL" : response.StatusCode.ToString(),
                    StartedOn = startedOn,
                    ResponseData = string.IsNullOrEmpty(result) ? null : result.Length > 100 ? result.Substring(0, 100) : result
                };
                SharePointRepository.Instance.LogSharePointRestInfo(log);
            }
            return result64;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool _disposed = false;
        private HttpResponseMessage response;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {

                }

                _disposed = true;
            }
        }
        ~SharePointRestClient()
        {
            Dispose(false);
        }

    }

    internal interface ISharePointRestClient : IDisposable
    {
        string RCParams { get; set; }
        string ReleaseParams { get; set; }

        Task<string> MakeRequest();
    }



}
