﻿using BusinessObjects;
using Common.Repository.SQLAzureDBUtility;
using Microsoft.SharePoint.Client;
using System;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace TeraDAct.SharepointFileTransferService
{
   
    public class SharepointHelper
    {
        public static bool IsSharePointOnPrem
        {
            get
            {
                return bool.Parse(
              ConfigurationManager.AppSettings["IsSharePointOnPrem"] == null ? "false" :
              ConfigurationManager.AppSettings["IsSharePointOnPrem"]);
            }
        }


        internal static string TrimEnd(string source, string value)
        {
            if (!source.EndsWith(value))
                return source;

            return source.Remove(source.LastIndexOf(value));
        }
        internal static string SHA512Generator(byte[] Buffer)
        {
            using (SHA512Managed sha1 = new SHA512Managed())
            {
                
                return Uri.EscapeDataString(Convert.ToBase64String(sha1.ComputeHash(Buffer)));
            }
        }
        internal static void CreateRedactedDocumentLib(ClientContext clientCTX,string RedactedDocumentLibraryName)
        {
            
            using (clientCTX)
            {
                ListCreationInformation lci = new ListCreationInformation();
                lci.Description = RedactedDocumentLibraryName ??
                   "SharedDocumentLibName";
                lci.Title = lci.Description;
                lci.TemplateType = 101;
                ListCollection listCollection = clientCTX.Web.Lists;
                clientCTX.Load(listCollection, lists =>
                lists.Include(list => list.Title).
                Where(list => list.Title == lci.Title));
                clientCTX.ExecuteQuery();
                if (listCollection.Count == 0)
                {
                    List newLib = clientCTX.Web.Lists.Add(lci);
                    clientCTX.Load(newLib);
                    clientCTX.ExecuteQuery();
                }
            }
        }
        private static long? tempSharePointQueueID { get; set; }
        internal static void saveSharePointQueue(SharePointQueue spQueue, char Operation)
        {
            //return Task.Run(()=>
            //{
            spQueue.SharePointQueueID = SharePointRepository.Instance.saveSharePointQueue(spQueue, Operation);
            //if (Operation == 'I')
            //{
            //    tempSharePointQueueID = null;
            //    tempSharePointQueueID = spQueue.SharePointQueueID;
            //}
            //else
            //{
            //    spQueue.SharePointQueueID = tempSharePointQueueID;
            //}
            //spQueue.SharePointQueueID = Operation == 'I' ? SharePointRepository.Instance.saveSharePointQueue(spQueue, Operation) : spQueue.SharePointQueueID;
        }
        internal static void SetupTeraDActConfigSetting()
        {

            if (null == TeraDActConfiguration.Settings)
                TeraDActConfigurationRepository.Instance.getTeraDActConfiguration();
        }
    }
}
