﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using System.Configuration;
using System.Threading;
using System.Transactions;
using Common.Repository.SQLAzureDBUtility;
using BusinessObjects;
using TeraDAct.SharepointFileTransferService;

namespace SharepointIntegration.UI.UnAttended.SharepointRedactServiceUnattendedWebJob
{
    public class Functions
    {
        private static SharePointOnlineSettings spvalues;


        [NoAutomaticTriggerAttribute]
        public static async Task CleanUpData()
        {
            try
            {
                string interval = ConfigurationManager.AppSettings
                    ["CleanUpDBDataIntervalInDays"] == null ? "7" : ConfigurationManager.AppSettings["CleanUpDBDataIntervalInDays"];

                var dueTime = TimeSpan.FromDays(int.Parse(interval)).TotalMilliseconds;
                var timer = new System.Timers.Timer(dueTime);
                timer.Elapsed += (sender, e) =>
                {
                    ErrorLogRepository.Instance.DoCleanUpData();

                };
                timer.Start();
            }
            catch (Exception ex)
            {
                ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.SharepointRedactServiceUnattendedWebJob);

            }

        }
        [NoAutomaticTriggerAttribute]
        public static async Task ProcessRedact()
        {

            while (true)
            {


                try
                {
                    using (var ts = CreateAsyncTransactionScope())
                    {

                        IList<SPUnAttendedApplyRulesSettings> allSPSettings = null;

                        allSPSettings = SharePointRepository.Instance.getSPUnAttendedApplyRulesSettingsInfo();


                        await ProcessQueue(allSPSettings).ConfigureAwait(false);
                        ts.Complete();
                    }
                }
                catch (Exception ex)
                {


                    ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.SharepointRedactServiceUnattendedWebJob);
                }
                finally
                {

                }
                string interval = ConfigurationManager.AppSettings["SPRedactedPullingIntervalinSecond"] == null ? "200" : ConfigurationManager.AppSettings["SPRedactedPullingIntervalinSecond"];
                Thread.Sleep(TimeSpan.FromSeconds(int.Parse(interval)));
            }
        }

        private static TransactionScope CreateAsyncTransactionScope(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = isolationLevel,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Suppress, transactionOptions, TransactionScopeAsyncFlowOption.Enabled);
        }
        static async Task doWork(IList<SPUnAttendedApplyRulesSettings> allSPSettings)
        {

            List<Task> trackedTasks = new List<Task>();

            foreach (SPUnAttendedApplyRulesSettings spSetting in allSPSettings)
            {




                try
                {
                    if (null == spvalues)
                        spvalues = FileTransferService.GetSharePointSettingsDetails()[0];
                    IFileTransferService _service = new FileTransferService(
                        spvalues.SPAdminUserID, spvalues.SPAdminPassword
                        );
                    _service.Domain = spvalues.Domain;
                    _service.AdminCentarlSiteURL = spvalues.AdminCentarlSiteURL;
                    _service.processUnAttendedRedact(spSetting);

                }
                catch (Exception ex)
                {
                    ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.SharepointRedactServiceUnattendedWebJob);
                }
                finally
                {

                }

            }

        }


        private static async Task ProcessQueue(IList<SPUnAttendedApplyRulesSettings> allSPSettings)
        {
            await Task.WhenAll(doWork(allSPSettings));


        }
    }
}
