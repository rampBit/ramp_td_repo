﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;

namespace SharepointIntegration.UI.UnAttended.SharepointRedactServiceUnattendedWebJob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    class Program : WebJobBase
    {

        static void Main()
        {
            var host = GetJobHost("SharepointRedactServiceUnattendedWebJob");
            host.CallAsync(typeof(Functions).GetMethod("ProcessRedact"));
            host.CallAsync(typeof(Functions).GetMethod("CleanUpData"));

            host.Start();
            Console.WriteLine("Host started. press return key to stop.");
            Console.ReadLine();
        }
    }
    public abstract class WebJobBase
    {
        protected static JobHost GetJobHost(string webJobName)
        {
            JobHost host = new JobHost(GetConfiguration(webJobName));

            return host;
        }
        protected static JobHostConfiguration GetConfiguration(string webJobName)
        {
            JobHostConfiguration config = new JobHostConfiguration
            {
                DashboardConnectionString = "", //BlobStorageAccount.Settings.BlobStorageConnectionString,
                StorageConnectionString = ""// BlobStorageAccount.Settings.BlobStorageConnectionString
            };
            config.HostId = Guid.NewGuid().ToString("N");

            var host = new JobHost(config);
            return config;
        }

    }
}
