﻿using BusinessObjects.SkypeIntegration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeIntegrationService.Skype
{
  public  interface ISkypeService
    {
        bool AuthenticateAndLogin(SkypeUserCredentials authCreds);
    }
}
