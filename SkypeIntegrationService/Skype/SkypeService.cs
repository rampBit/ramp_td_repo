﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessObjects.SkypeIntegration;
using Skype4Sharp;
using System.Threading;
using Skype4Sharp.Helpers;
using Skype4Sharp.Enums;
using Common.Repository.SQLAzureDBUtility;
using BusinessObjects;

namespace SkypeIntegrationService.Skype
{
    public class SkypeService : ISkypeService
    {
        Skype4Sharp.Skype4Sharp mainSkype;
        SkypeUserCredentials authCreds;
        static string triggerString = "!";
        
        public bool AuthenticateAndLogin(SkypeUserCredentials authCreds)
        {
            this.authCreds = authCreds;
            mainSkype = new Skype4Sharp.Skype4Sharp(new Skype4Sharp.Auth.SkypeCredentials(authCreds.Username, authCreds.Password));
            Console.WriteLine("[DEBUG]: Logging in with {0}:{1}", authCreds.Username, string.Join("", Enumerable.Repeat("*", authCreds.Password.Length)));
            bool sucess = mainSkype.Login();
            if (sucess)
            {
                mainSkype.messageReceived += MainSkype_messageReceived;
            }
            return sucess;
        }

        private void MainSkype_messageReceived(ChatMessage pMessage)
        {
            new Thread(() =>
            {
                try
                {
                    Console.WriteLine("[EVENT]: MESSAGE_RECEIVED > {0} ({2}): {1}", pMessage.Sender.Username, pMessage.Body, pMessage.Chat.ID);
                    string[] commandArgs = pMessage.Body.Split(' ');
                    if (commandArgs[0].ToLower().StartsWith(triggerString))
                    {
                        ChatMessage rMessage = pMessage.Chat.SendMessage("Processing your command...");
                        rMessage.Type = MessageType.RichText;

                        switch (commandArgs[0].Remove(0, triggerString.Length).ToLower())
                        {
                            case "help":
                                {
                                    string commandPrefix = Environment.NewLine + "    " + triggerString;


                                    break;
                                }
                            case "ping":
                                {
                                    rMessage.Body = "<b>@" + pMessage.Sender.Username + " (" + pMessage.Sender.DisplayName + ")</b> Pong!";

                                    break;
                                }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.SkypeForBusiness);
                }
            }).Start();
        }
    }

}