﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Exchange.WebServices;
using System.Threading.Tasks;
using RampGroup.Config.Utility;

using RampGroup.MX.Core;
using BusinessObjects;
using FileUtility;
using TeraDActService.Logic;
using System.Security.Cryptography.Pkcs;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using Common.Repository.SQLAzureDBUtility;
using System.Text;
using System.ComponentModel;
using System.Configuration;

namespace RampGroup.TeraDActBusinessLogic.ExchangeServiceRequests
{
    public class MailUtility
    {
        private static readonly string EmailContents = "EmailContents";
        private static readonly string DateTimeFormat = "yyyyMMddHHmmss_";
        private static EmailMessageResponse currentMessage;
        private List<MailAttachment> mailattchmentList = null;
        public MailUtility() { mailattchmentList = new List<MailAttachment>(); }

        public List<MailAttachment> TraverseItemAttachment(Microsoft.Exchange.WebServices.Data.Attachment attachment)
        {

            if (attachment is Microsoft.Exchange.WebServices.Data.FileAttachment)
            {
                Microsoft.Exchange.WebServices.Data.FileAttachment fileAttachment = attachment as Microsoft.Exchange.WebServices.Data.FileAttachment;
                fileAttachment.Load();
                MailAttachment mailattchment = new MailAttachment();
                mailattchment.AttachmentName = fileAttachment.Name;
                mailattchment.AttachmentContent = fileAttachment.Content;
                mailattchment.FileType = fileAttachment.ContentType;
                mailattchmentList.Add(mailattchment);
                //FileService.CreateStream(getAttachmentPath(mailattchment.AttachmentName), mailattchment.AttachmentContent);
                //nestedattachment.Add(File.ReadAllBytes(getAttachmentPath(mailattchment.AttachmentName)));
            }
            else if (attachment is Microsoft.Exchange.WebServices.Data.ItemAttachment)
            {

                Microsoft.Exchange.WebServices.Data.ItemAttachment itemAttachment = attachment as Microsoft.Exchange.WebServices.Data.ItemAttachment;
                if (itemAttachment.Id.IsNormalized())//&& itemAttachment.ContentLocation != null
                {
                    itemAttachment.Load(new PropertySet(ItemSchema.Attachments));
                    try
                    {
                        itemAttachment.Load(new PropertySet(ItemSchema.MimeContent));
                        if (itemAttachment.Item is EmailMessage)
                        {
                            EmailMessage attachMessage = itemAttachment.Item as EmailMessage;
                            MailAttachment mailattchment = new MailAttachment();
                            mailattchment.AttachmentName = attachMessage.Subject + ".mht";  //if attachem start with ATT000 then continue
                            mailattchment.AttachmentContent = attachMessage.MimeContent.Content;
                            mailattchment.FileType = "application/octet-stream";
                            mailattchmentList.Add(mailattchment);
                            if (attachMessage.HasAttachments)
                            {
                                // FileService.CreateStream(getAttachmentPath(mailattchment.AttachmentName), mailattchment.AttachmentContent);
                                foreach (Microsoft.Exchange.WebServices.Data.Attachment subAttachment in attachMessage.Attachments)
                                {
                                    TraverseItemAttachment(subAttachment);
                                    //foreach (MailAttachment subEntry in TraverseItemAttachment(subAttachment))
                                    //{

                                    //}
                                }

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.MailUtility);
                    }

                }

            }

            return mailattchmentList;

        }

        public static bool IsExchageOnPrem
        {
            get
            {
                return bool.Parse(
                       ConfigurationManager.AppSettings["IsExchageOnPrem"] == null ? "false" :
                       ConfigurationManager.AppSettings["IsExchageOnPrem"]);
            }
        }
        public static async Task<OperationStatus> sendMailUsingSMTP(AttachmentSampleServiceResponse result,
            string ID, OperationStatus RedactedStatus)
        {
            var _repo = new EmailQueueRepository();
            var smtpSetting = _repo.GetSMTPSetting();
            OperationStatus status = OperationStatus.Succeded;
            if (result.attachments == null && result.EmailMessageResponse == null) status = OperationStatus.Failed;
            //await System.Threading.Tasks.Task.Run(() =>
            //{
            var ParentEmail = result.EmailMessageResponse.Where(c => c.IsAttachment == false).First();
            byte[] bodyContent = ParentEmail.Content;
            var Text = bodyContent != null ? Encoding.Default.GetString(bodyContent) : null;

            MailMessage msg = new MailMessage();
            foreach (var toAddress in ParentEmail.ToAddress)
            {
                msg.To.Add(toAddress);
            }
            foreach (var ccAddress in ParentEmail.CCAddress)
            {
                msg.To.Add(ccAddress);
            }
            foreach (var bccAddress in ParentEmail.BCCAddress)
            {
                msg.To.Add(bccAddress);
            }
            msg.From = new MailAddress(ParentEmail.fromAddress);
            if (!IsExchageOnPrem)
            {
                var sender = new EmailAddress(result.ExchangeServerSettings.EmailID,
                    result.ExchangeServerSettings.EWSUrl);
                msg.Sender = new MailAddress(sender.Name);
            }
            msg.Subject = ParentEmail.subject;  //+ "{Redacted}";  added message header
            msg.Body = Text;
            msg.IsBodyHtml = true; 
            foreach (AttachmentDetails att in result.attachments)
            {
                MemoryStream stream = new MemoryStream(att.FileContent);
                System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(stream, att.name);
                msg.Attachments.Add(attachment);
            }

            #region Extended Property for Redacted Email Filter
             msg.Headers.Add("X-TeradactRedacted", "TeradactRedacted");
            #endregion

            SmtpClient client = new SmtpClient();

            if (!IsExchageOnPrem) client.TargetName = smtpSetting.TargetName; // "STARTTLS/smtp.office365.com";
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(result.ExchangeServerSettings.EmailID,
                result.ExchangeServerSettings.Password, result.ExchangeServerSettings.Domain);
            client.Port = smtpSetting.Port; // You can use Port 25 if 587 is blocked
            client.Host = smtpSetting.Host;// "smtp.office365.com";
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = smtpSetting.EnableSsl;
            client.SendCompleted += (s, e) =>
            {



                if (e.Error == null)
                    status = OperationStatus.Succeded;
                else
                {
                    status = OperationStatus.Failed;
                    ErrorLogRepository.Instance.WriteErrorLog(e.Error, ErrorSource.MailUtility);
                }
                client.Dispose();
                msg.Dispose();
               // if (RedactedStatus == OperationStatus.Succeded)
                    UpdateEmailQueueStatus(status == OperationStatus.Succeded ? RequestStatus.MailDelivered :
                        RequestStatus.MailUndelivered,
                            ID, result.ExchangeServerSettings);

            };

            client.SendAsync(msg, OperationStatus.Failed);
            UpdateEmailQueueStatus(  RequestStatus.MailDelivered ,               
                          ID, result.ExchangeServerSettings);  //to avoid duplicate email from grmail to exchange account
            //});
            return await System.Threading.Tasks.Task<OperationStatus>.FromResult(status);

        }
        private static void UpdateEmailQueueStatus(RequestStatus status, string ID, BusinessObjects.ExchangeServerSettings setting)
        {


            EmailQueueRepository QueueRepo = new EmailQueueRepository();
            EmailQueue emqueue = new EmailQueue
            {
                MessageID = ID,
                Status = status,
                ExchangeServerSettings = setting
            };
            QueueRepo.saveEmailQueue(emqueue, 'U');


        }

        public static async System.Threading.Tasks.Task<OperationStatus> sendMail(AttachmentSampleServiceResponse result,
            string ID, OperationStatus RedactedStatus)
        {

            OperationStatus status = OperationStatus.Succeded; ;
            #region previous working code
            if (result.attachments == null && result.EmailMessageResponse == null) status = (OperationStatus)OperationStatus.Failed;
            //return await System.Threading.Tasks.Task.Run(() =>
            //{
            var ParentEmail = result.EmailMessageResponse.Where(c => c.IsAttachment == false).First();
           
            if (ParentEmail.MailBoxtype != ExchangeMailboxType.Mailbox)
                return sendMailUsingSMTP(result, ID, RedactedStatus).Result;
            ExchangeService service = ExchangeServiceAuthentication.SMTPInstance.SMTPexchange;
            //    var exchangeEmailID = ((System.Net.NetworkCredential)((Microsoft.Exchange.WebServices.Data.WebCredentials)service.Credentials).Credentials).UserName;
            service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, ParentEmail.fromAddress);
            // Filedownload failed marked when webjob running continuously with above commented line.
            byte[] bodyContent = ParentEmail.Content;
            var Text = bodyContent != null ? Encoding.Default.GetString(bodyContent) : null;

            var message = new EmailMessage(service)
            {
                Subject = ParentEmail.subject,  // + "{Redacted}",  added message header
                Body = new MessageBody { Text = Text, BodyType = BodyType.HTML }
            };

            var sender = new EmailAddress(result.ExchangeServerSettings.EmailID,
                result.ExchangeServerSettings.EWSUrl);
            message.Sender = sender;
            message.From = ParentEmail.fromAddress;
            message.ToRecipients.AddRange(ParentEmail.ToAddress);
            message.CcRecipients.AddRange(ParentEmail.CCAddress);
            message.BccRecipients.AddRange(ParentEmail.BCCAddress);
            foreach (AttachmentDetails attachment in result.attachments)
            {

                message.Attachments.AddFileAttachment(attachment.name, attachment.FileContent);
            }

            try
            {
                #region Extended Property for Redacted Email Filter
                ExtendedPropertyDefinition xExperimentalHeader = new ExtendedPropertyDefinition(DefaultExtendedPropertySet.InternetHeaders,
                                                                                                 "X-TeradactRedacted",
                                                                                                 MapiPropertyType.String);
                message.SetExtendedProperty(xExperimentalHeader, "TeradactRedacted");
                #endregion
                message.Send();
                status = (OperationStatus)OperationStatus.Succeded;


            }
            catch (Exception ex)
            {
                ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.MailUtility);
                status = (OperationStatus)OperationStatus.Failed;

            }
            finally
            {
                //  service.ImpersonatedUserId = null;


            }
            // if (RedactedStatus == OperationStatus.Succeded)
            UpdateEmailQueueStatus(status == OperationStatus.Succeded ? RequestStatus.MailDelivered :
                RequestStatus.MailUndelivered,
                    ID, result.ExchangeServerSettings);

            return await System.Threading.Tasks.Task<OperationStatus>.FromResult(status);

            //});

            #endregion

        }

        //public static async Task<OperationStatus> sendMail(string ItemID)
        //{
        //    await System.Threading.Tasks.Task<OperationStatus>.Run(() =>
        //    {

        //        string[] LoadAttachments = new string[1];
        //        LoadAttachments[0] = ItemID;
        //        PropertySet PropSet = new PropertySet(BasePropertySet.IdOnly, ItemSchema.MimeContent);
        //        ServiceResponseCollection<GetAttachmentResponse> getAttachmentresps2 =
        //                    ExchangeServiceAuthentication.Instance.exchange.GetAttachments(LoadAttachments, BodyType.HTML, PropSet);
        //        var attach = ((ItemAttachment)getAttachmentresps2[0].Attachment).Item as EmailMessage;

        //        var message = new EmailMessage(ExchangeServiceAuthentication.Instance.exchange)
        //        {
        //            Subject = attach.Subject,

        //        };
        //        message.ToRecipients.Add(attach.From);
        //        message.ToRecipients.Add("vijay.saxena@ptgindia.com");
        //        message.Attachments.AddFileAttachment(attach.Subject + ".eml", attach.MimeContent.Content);

        //        message.ItemClass = "IPM.Note";
        //        message.IsRead = true;
        //        // message.Update(ConflictResolutionMode.AlwaysOverwrite);

        //        try
        //        {
        //            message.SendAndSaveCopy();
        //            return (OperationStatus)OperationStatus.Succeded;
        //        }
        //        catch (Exception ex)
        //        {
        //            ErrorLogRepository.Instance.WriteErrorLog(ex,ErrorSource.MailUtility);
        //            return (OperationStatus)OperationStatus.Failed;

        //        }

        //    });
        //    return (OperationStatus)OperationStatus.Succeded;
        //}
        //public static void sendMail(string ItemID)
        //{

        //    var message = (EmailMessage)Item.Bind(MX.Core.ExchangeServiceAuthentication.Instance.exchange, new ItemId(ItemID), PropertySet.FirstClassProperties);
        //    var reply = message.CreateReply(false);
        //    reply.BodyPrefix = message.Body;
        //    var replyMessage = reply.Save(WellKnownFolderName.Drafts);
        //    PropertySet psPropSet = new PropertySet(BasePropertySet.FirstClassProperties);
        //    psPropSet.Add(ItemSchema.MimeContent);
        //    message.Load(psPropSet);
        //    ItemAttachment emAttach = replyMessage.Attachments.AddItemAttachment<EmailMessage>();
        //    emAttach.Name = message.ConversationTopic;
        //    message.ItemClass = "IPM.Note";
        //    message.IsRead = true;
        //    message.Update(ConflictResolutionMode.AlwaysOverwrite);
        //    emAttach.Item.MimeContent = message.MimeContent;
        //    reply.SendAndSaveCopy();
        //}

        //private static string getMailBodyPath(string UniqueId)
        //{
        //    string EmailPath = Path.Combine(ExchangeServerSettings.Settings.EmailStagingPath,
        //            String.Format("{0}", EmailContents));
        //    if (!Directory.Exists(EmailPath))
        //    {
        //        Directory.CreateDirectory(EmailPath);
        //    }
        //    return Path.Combine(EmailPath, UniqueId + ".eml");
        //}

        //private static string getAttachmentPath(string attachmentname)
        //{
        //    string attachmentPath = ExchangeServerSettings.Settings.AttachmentStagingPath;//
        //    FileService.CreateDirectory(attachmentPath);
        //    return Path.Combine(attachmentPath, attachmentname);
        //    // return Path.Combine(attachmentPath);
        //}
    }


}
