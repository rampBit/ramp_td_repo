﻿using BusinessObjects;
using RampGroup.MX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TeraDActService.Logic;
using Microsoft.Exchange.WebServices.Data;
using FileUtility;
using RampGroup.MX.Core.Authentication;
using Common.Repository.SQLAzureDBUtility;

namespace RampGroup.TeraDActBusinessLogic.ExchangeServiceRequests
{
    public class UserEmailMessageRequest : ExchangeServerBase
    {


        private List<AttachmentDetails> attachmentsdet = null;
        private List<EmailMessageResponse> emlMSGResp = null;
        private AttachmentSampleServiceResponse resp = null;

        // public List<byte[]> nestedattachment = new List<byte[]>();
        public UserEmailMessageRequest()
            : base()
        {
        }

        TeraDActRest ne = new TeraDActRest();



        private PropertySet setProperty()
        {
            PropertySet PropSet = new PropertySet();
            PropSet.Add(ItemSchema.HasAttachments);
            PropSet.Add(ItemSchema.Body);
            PropSet.Add(ItemSchema.DisplayTo);
            PropSet.Add(ItemSchema.IsDraft);
            PropSet.Add(ItemSchema.DateTimeCreated);
            PropSet.Add(ItemSchema.DateTimeReceived);
            PropSet.Add(ItemSchema.Attachments);
            return PropSet;
        }
        
        public AttachmentSampleServiceResponse ProcessWrite(AttachmentDetails emlMsg)
        {
            //  return await System.Threading.Tasks.Task<AttachmentSampleServiceResponse>.Run(async () =>
            //{
            // FolderId folder = WellKnownFolderName.Inbox;
            string[] LoadAttachments = new string[1];
            LoadAttachments[0] = emlMsg.id;
            SearchFilter searchFilter = new SearchFilter.IsEqualTo(ItemSchema.Id, LoadAttachments[0]);
            var view = new ItemView(1);
            view.PropertySet = new PropertySet(PropertySet.IdOnly);
            PropertySet PropSet = setProperty();
            view.Traversal = ItemTraversal.Shallow;
            ServiceResponseCollection<GetAttachmentResponse> getAttachmentresps2 =
                            ExchangeServiceAuthentication.Instance.exchange.GetAttachments(LoadAttachments, BodyType.HTML, null);
            if (getAttachmentresps2.OverallResult == ServiceResult.Error) return resp;
            var OrginalEmail = ((ItemAttachment)getAttachmentresps2[0].Attachment).Item as EmailMessage;
            resp.EmailMessageResponse = emlMSGResp;
            string parentId = Guid.NewGuid().ToString();

            emlMSGResp.Add(new EmailMessageResponse
            {
                emailItemId = parentId,
                Content = FileService.GetBytesnyText(OrginalEmail.Body),
                ConversationTopic = OrginalEmail.ConversationTopic,
                emailBody = OrginalEmail.Body,
                subject = OrginalEmail.Subject,
                fromAddress = OrginalEmail.From.Address,
                ToAddress = OrginalEmail.ToRecipients.Select(ad => ad.Address).ToList(),
                CCAddress = OrginalEmail.CcRecipients.Select(ad => ad.Address).ToList(),
                BCCAddress = OrginalEmail.BccRecipients.Select(ad => ad.Address).ToList(),
                MailBoxtype = (ExchangeMailboxType)Enum.Parse(typeof(ExchangeMailboxType),
                 OrginalEmail.From.MailboxType.ToString())
            });

            resp.EmailMessageResponse = emlMSGResp;
            resp.attachments = attachmentsdet;


            foreach (var attachment in OrginalEmail.Attachments)
            {
                var tmpAttachmentDetail = new AttachmentDetails();
                tmpAttachmentDetail.id = attachment.Id;
                tmpAttachmentDetail.name = attachment.Name;
                if (attachment is ItemAttachment)
                {
                    tmpAttachmentDetail.IsItemAttachment = true;
                    ProcessWrite(tmpAttachmentDetail);
                }
                else if (attachment is FileAttachment)
                {
                    try
                    {
                        attachment.Load();
                        tmpAttachmentDetail.ParentID = parentId;
                        tmpAttachmentDetail.FileContent = ((FileAttachment)attachment).Content;
                        resp.attachments.Add(tmpAttachmentDetail);
                    }
                    catch (Exception ex)
                    {
                        ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.TeraDActServiceEmailWebJob);
                        throw ex;
                    }

                }

            }
             
            return resp;
            // });
        }



        public AttachmentSampleServiceResponse ReadEmails(BusinessObjects.ExchangeServerSettings setting)
        {
            AttachmentSampleServiceResponse resp = new AttachmentSampleServiceResponse();

            IList<AttachmentDetails> allMessages = new List<AttachmentDetails>();

            MailUtility util = new ExchangeServiceRequests.MailUtility();
            IEmailServiceRequest Exchangeobj = new EmailServiceRequest();
            // EmailQueueRepository _Repo = new EmailQueueRepository();

            //List< BusinessObjects.ExchangeServerSettings> setting = _Repo.getTenantEmail();


            IList<EmailMessage> emails = (Exchangeobj.GetAllExchangeEmails(setting));
            resp.ExchangeServerSettings = setting;
            emails.ToList().ForEach(
        em =>
        {


            Microsoft.Exchange.WebServices.Data.ItemAttachment itemAttachment = em.Attachments[0] as Microsoft.Exchange.WebServices.Data.ItemAttachment;
            itemAttachment.Load();
            AttachmentDetails tempemail = new AttachmentDetails();
            tempemail.id = itemAttachment.Id;
            tempemail.name = itemAttachment.Name;
            tempemail.ExchangeAttachementEmailUniqueID = em.Id.UniqueId;

            allMessages.Add(tempemail);

            // sendTestMail(em.Id.UniqueId, itemAttachment.Id);
        }
        );
            resp.attachments = allMessages.ToList();
            return resp;
        }
        private OperationStatus RedactProcess(AttachmentSampleServiceResponse result)
        {
             
              
            
            if (result.attachments == null && result.EmailMessageResponse == null) return OperationStatus.Failed;

            var resp = ne.Redact(result);

            return resp;


        }
        private void UpdateEmailQueueStatus(RequestStatus status, string ID, BusinessObjects.ExchangeServerSettings setting)
        {


            EmailQueueRepository QueueRepo = new EmailQueueRepository();
            EmailQueue emqueue = new EmailQueue
            {
                MessageID = ID,
                Status = status,
                ExchangeServerSettings = setting
            };
            QueueRepo.saveEmailQueue(emqueue, 'U');


        }
        public void redactTest()
        {


            for (int i = 0; i < 100; i++)
            {
                var respemil = ne.RedactFilestoBlob(new BusinessObjects.FileObject
                {
                    Content = FileService.GetBytesnyText("IP: [14.142.119.123]"),
                    FileName = "test.txt"
                });

                if (respemil == null || respemil.FileStream == null || respemil.Content.Length == 0)
                    Console.WriteLine(BusinessObjects.OperationStatus.Failed.ToString());
            }

        }
        public async System.Threading.Tasks.Task processAll(AttachmentSampleServiceRequest serviceRequest)
        {
            resp = new AttachmentSampleServiceResponse();
            var cred = ((System.Net.NetworkCredential)((Microsoft.Exchange.WebServices.Data.WebCredentials)ExchangeServiceAuthentication.Instance.exchange.Credentials).Credentials);
            resp.ExchangeServerSettings = new BusinessObjects.ExchangeServerSettings
            {
                Domain = cred.Domain,
                EmailID = cred.UserName,
                AccountName = cred.UserName,
                Password = cred.Password
            };

            attachmentsdet = new List<AttachmentDetails>();
            emlMSGResp = new List<EmailMessageResponse>();

            var exchangeResult = ProcessWrite(serviceRequest.attachment);

            if (exchangeResult.attachments == null && exchangeResult.EmailMessageResponse == null)
            {
                UpdateEmailQueueStatus(RequestStatus.FileDownloadedFailed, serviceRequest.attachment.id, resp.ExchangeServerSettings);
                return;
            }
            UpdateEmailQueueStatus(RequestStatus.FileDownloaded, serviceRequest.attachment.id, resp.ExchangeServerSettings);
            var RedactResult = RedactProcess(exchangeResult);
            UpdateEmailQueueStatus(RedactResult == OperationStatus.Succeded ? RequestStatus.Redacted :
                      RequestStatus.RedactionFailed, serviceRequest.attachment.id, resp.ExchangeServerSettings);
            //send mail..
            if ((serviceRequest.attachment.RetryCount >= 2 && RedactResult == OperationStatus.Failed) || RedactResult == OperationStatus.Succeded)
            {
                await MailUtility.sendMail(exchangeResult, serviceRequest.attachment.id,
                     RedactResult);
            }


        }
        public System.Threading.Tasks.Task UpdateIPM(string exchangeAttachementEmailUniqueID, string orgMsgID)
        {
            return System.Threading.Tasks.Task.Run(() =>
            {
                try
                {
                    EmailMessage exMsg = EmailMessage.Bind(ExchangeServiceAuthentication.Instance.exchange,
                   new ItemId(exchangeAttachementEmailUniqueID),
                   new PropertySet(EmailMessageSchema.ItemClass, EmailMessageSchema.IsRead));

                    if (exMsg != null)
                    {
                        exMsg.ItemClass = "IPM.Note";
                        exMsg.IsRead = true;
                        exMsg.Update(ConflictResolutionMode.AlwaysOverwrite);
                        //  UpdateEmailQueueStatus(RequestStatus.IPMUpdated, orgMsgID);
                       // _logHelper.Info("IPM Updated");
                    }
                }
                catch (Exception ex)
                {

                    ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.TeraDActServiceEmailWebJob);

                }


            });

        }


    }
}
