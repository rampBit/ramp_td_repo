﻿using AzureActiveDirectoryUtility;
using BusinessObjects;
using Common.Repository.SQLAzureDBUtility;
using Microsoft.SharePoint.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Xml;
using System.Xml.Linq;
using TeraDAct.SharepointFileTransferService;
using FileUtility;
namespace TeraDAct.FileTransferServiceAPI.Controllers
{

    public class TeraDActFileSericeAPIController : ApiController
    {
        private readonly string RedactedDocumentLibraryName;
        private readonly string SharePointSubscriptionPassword;
        private readonly string SharePointSubscriptionUserID;
        private static SharePointOnlineSettings spvalues;

        public string AdminCentarlSiteURL { get; private set; }
        public string SharedDocumentLibName { get; private set; }
        public string Domain { get; private set; }

        public TeraDActFileSericeAPIController()
        {
            try
            {
                if (null == spvalues)
                    spvalues = FileTransferService.GetSharePointSettingsDetails()[0];
                this.SharePointSubscriptionUserID = spvalues.SPAdminUserID;
                this.SharePointSubscriptionPassword = spvalues.SPAdminPassword;
                this.AdminCentarlSiteURL = spvalues.AdminCentarlSiteURL;
                this.SharedDocumentLibName = spvalues.SharedDocumentLibraryName;
                this.Domain = spvalues.Domain;
            }
            catch
            {

                throw;
            }

        }

        [HttpPost]
        public object hello(testhello SharepointContexRequest)
        {
            //IFileTransferService fileServiceRequest = new FileTransferService(SharePointSubscriptionUserID,
            //         SharePointSubscriptionPassword);

            //  var result = fileServiceRequest.ProcessFile(SharepointContexRequest);
            return "hello" + SharepointContexRequest.SPHostURL; //+ SharepointContexRequest.RuleSetName+ SharepointContexRequest.SPListId+ SharepointContexRequest.SPlistItemId+ SharepointContexRequest.SharePointRequestActionType.ToString();
        }
        [HttpPost]
        public object ProcessFile(SharepointContexRequest SharepointContexRequest)
        {


            try
            {
                //SharepointContexRequest.SharePointRequestActionType = (SharePointRequestActionType)
                //   Enum.Parse(typeof(SharePointRequestActionType), SharepointContexRequest.ActionType);

                IFileTransferService fileServiceRequest = new FileTransferService(SharePointSubscriptionUserID,
                    SharePointSubscriptionPassword);
                fileServiceRequest.Domain = this.Domain;
                fileServiceRequest.SPFileTransferServiceURL = spvalues.SPFileTransferServiceURL;
                SharepointContexRequest.SharedDocumentLibName = this.SharedDocumentLibName;
                if(!string.IsNullOrEmpty(SharepointContexRequest.UserName))
                SharepointContexRequest.UserName = SharepointContexRequest.UserName.Replace("backslash", "\\");
                var result = fileServiceRequest.ProcessFile(SharepointContexRequest);

                switch (SharepointContexRequest.SharePointRequestActionType)
                {
                    case SharePointRequestActionType.Check:
                        return result.CheckActionResult;
                    case SharePointRequestActionType.CreateReview:
                        return result.CreateReviewActionResult;
                    case SharePointRequestActionType.CreateReviewSession:
                        return result.CreateReviewSessionActionResult;
                    case SharePointRequestActionType.ReleaseReview:
                        return result.ReleaseReviewActionResult;
                    case SharePointRequestActionType.Share:
                        return result.ShareActionResult;

                    case SharePointRequestActionType.SharedView:
                        return result.SharedViewActionResult;

                    case SharePointRequestActionType.SharedRelease:
                        return result.SharedReleaseActionResult;
                    case SharePointRequestActionType.ApplyRuleset:
                        return result.ApplyRulesetActionResult;
                    case SharePointRequestActionType.Redact:
                        return result.RedactActionResult;
                    case SharePointRequestActionType.GetAccountPolicy:
                        return result.GetAccountPolicyActionResult;
                    case SharePointRequestActionType.CheckAccountPolicy:
                        return result.CheckAccountPolicyActionResult;
                    case SharePointRequestActionType.UpdateAccountPolicy:
                        return result.UpdateAccountPolicyActionResult;
                    case SharePointRequestActionType.getTDRuleDirectiveInfo:
                        return result.TDRuleDirectiveInfo;
                    default:
                        break;
                }
                return null;
            }
            catch (Exception ex)
            {
                ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.SharePointAttendedCustomAction);
                throw;
            }

        }


        [HttpGet]

        public void getAllAADRoles(string SPLoggedUserName)
        {

            AzureADRepository _repo = new AzureADRepository();
            _repo.getAllAADRoles(SPLoggedUserName);
        }
        [HttpGet]

        public string[] getAllRoleDirectives()
        {
            // return SharePointRepository.Instance.getAllRoleDirectives();
            IFileTransferService _service = new FileTransferService();
            var LabelInfos= _service.getAllRoleDirectives().Split(new char[] { ',' });
            List<string> Labels=new List<string>();
            foreach (var label in LabelInfos)
            {
                Labels.AddRange(label.Split(new char[] { ';' }));
            }
            return Labels.ToArray();
        }
        [HttpGet]

        public RuleSetResponse getListRuleSetsFromTDServer()
        {

            IFileTransferService fileServiceRequest = new FileTransferService();
            return fileServiceRequest.ListRuleSetFromTD();


            //  return Request.CreateResponse(HttpStatusCode.OK, result); ;
        }

        [HttpGet]
        public List<string> getDocmentLibraryList(string SPHostURL)
        {

            IFileTransferService fileServiceRequest = new FileTransferService(SharePointSubscriptionUserID,
                    SharePointSubscriptionPassword);
            SharepointContexRequest SharepointContexRequest = new SharepointContexRequest
            {
                SPHostURL = SPHostURL,
                SharedDocumentLibName = this.SharedDocumentLibName
            };
            return fileServiceRequest.getDocmentLibraryList(SharepointContexRequest);


            //  return Request.CreateResponse(HttpStatusCode.OK, result); ;
        }
        public List<string> getAllSites()
        {

            IFileTransferService fileServiceRequest = new FileTransferService(SharePointSubscriptionUserID,
                    SharePointSubscriptionPassword);
            fileServiceRequest.Domain = this.Domain;
            SharepointContexRequest SharepointContexRequest = new SharepointContexRequest { SPHostURL = this.AdminCentarlSiteURL };
            fileServiceRequest.AdminCentarlSiteURL = this.AdminCentarlSiteURL;
            return fileServiceRequest.getAllSites(SharepointContexRequest);


        }
        [HttpGet]
        public object CheckAccountPolicy()
        {
            //string[] tmp = { "category", "replace", "release" };
            //return tmp;

            var resp = ProcessFile(new SharepointContexRequest
            {
                SharePointRequestActionType =
                SharePointRequestActionType.CheckAccountPolicy
            });
            if (resp != null)
                return resp.ToString().Split(new char[] { ',' });
            return "";

        }
        [HttpGet]
        public object GetAccountPolicy()
        {
            return SharePointRepository.Instance.getPolicyInfoStatus();




        }
        [HttpGet]
        public object GetAccountPolicyFromTDServer()
        {

            return ProcessFile(new SharepointContexRequest
            {
                SharePointRequestActionType =
                SharePointRequestActionType.GetAccountPolicy
            });


        }
        [HttpPost]
        public object updateAccountPolicy(List<TDPolicyInfo> policyinfo)
        {
            var xmldata = FileService.CreateXML(policyinfo);



            //    IFileTransferService _service = new FileTransferService();
            //    return _service.updateAccountPolicy(policyinfo);

            //return ProcessFile(new SharepointContexRequest
            //{
            //    SharePointRequestActionType = SharePointRequestActionType.UpdateAccountPolicy,
            //    TDPolicyInfo = new TDPolicyInfo { pci = policyinfo.pci, phi = policyinfo.phi, pii = policyinfo.pii, ranked = policyinfo.ranked }
            //});
            SharePointRepository.Instance.updatePolicyInfoStatus(xmldata);
            return "";

        }


        public object getTDRuleDirectiveInfo(string rulesetName = "")
        {
            return ProcessFile(new SharepointContexRequest
            {
                SharePointRequestActionType = SharePointRequestActionType.getTDRuleDirectiveInfo,
                RuleSetName = rulesetName

            });

        }
        [HttpPost]
        public object createORUpdateTDRuleDirectiveInfo(List<TDRuleSetDirectiveInfo> tdrulesetinfo)
        {

            IFileTransferService _service = new FileTransferService();
            return _service.createORUpdateTDRuleDirectiveInfo(tdrulesetinfo);

        }
        // [Route("getFolderList/{SPHostURL}/{docLibName}")]
        [HttpGet]
        public List<string> getFolderList(string SPHostURL, string docLibName)
        {

            IFileTransferService fileServiceRequest = new FileTransferService(SharePointSubscriptionUserID,
                    SharePointSubscriptionPassword);
            SharepointContexRequest SharepointContexRequest = new SharepointContexRequest
            {
                SPHostURL = SPHostURL,
                RedactedDocumentLibraryName = docLibName
            };
            return fileServiceRequest.getFolderList(SharepointContexRequest).Where(
                fol => !fol.Equals("Forms")).ToList();
        }

    }
    public class testhello
    {
        //public ClientContext ClientContext { get; set; }
        //public string RedactedDocumentLibraryName { get; set; }
        //public FileObject RedactedFileInfo { get; internal set; }
        //public string selectedReviewID { get; internal set; }
        //public SharePointRequestActionType SharePointRequestActionType { get; set; }
        public string SPHostURL { get; set; }

        //public string SPListId { get; set; }

        //public int SPlistItemId { get; set; }
        //public SharePointQueue spQueue { get; internal set; }

    }

}
