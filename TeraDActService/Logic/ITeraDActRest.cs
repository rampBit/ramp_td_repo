﻿using BusinessObjects;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace TeraDActService.Logic
{
    public interface ITeraDActRest
    {
        void Authenticate();
        void TestRest();
        OperationStatus Redact(AttachmentSampleServiceResponse emailListToRedact);
        void GetAllRuleSet();
        FileObject RedactFilestoBlob(FileInfo fileInfo);
        FileObject RedactFilestoBlob(FileObject fileInfo);
        FileObject RedactFilesForSharePoint(FileObject fileInfo, byte[] SPRuleSetContent);

    }
}