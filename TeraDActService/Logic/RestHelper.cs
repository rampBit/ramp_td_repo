﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using FileUtility;
using RampGroup.Config.Utility;

namespace TeraDActService.Logic
{
    using BusinessObjects;
    using Common.Repository.SQLAzureDBUtility;
    using RampGroup.Logging;
    using RampGroup.MX.Core.Authentication;
    using System.IO;
    using System.Net.Http;
    internal abstract class RestHelper : ExchangeServerBase
    {
        internal static HttpClient _httpClient;


        internal static async Task<HttpResponseMessage> GetResponse(string requestURL)
        {
            HttpClientHandler handler = new HttpClientHandler();
            using (var httpClient = new HttpClient(handler, false))
            {
                return await httpClient.GetAsync(requestURL);
            }
        }
        internal static Stream PutRedactResponse(string requestURL, Stream fileStream, Stream fileBytes)
        {
            HttpClientHandler handler = new HttpClientHandler();
            using (var httpClient = new HttpClient(handler, false))
            {
                var multiPartContent = new MultipartFormDataContent();

                var fileStreamContent = new StreamContent(fileStream);
                fileStreamContent.Headers.Add("Content-Type", "application/octet-stream");
                fileStreamContent.Headers.Add("Content-Transfer-Encoding", "utf-8");

                multiPartContent.Add(fileStreamContent, "doc");
                
                var textContent = new StreamContent(fileBytes);
                textContent.Headers.Add("Content-Type", "text/plain");
                textContent.Headers.Add("Content-Transfer-Encoding", "8bit");

                multiPartContent.Add(textContent, "trs");

                requestURL = requestURL + "?rulesetAttached=true";

                var request = new HttpRequestMessage(HttpMethod.Put, requestURL);
                try
                {
                    request.Content = multiPartContent;

                    var response = httpClient.SendAsync(request).Result;

                    if (!response.IsSuccessStatusCode)
                    {
                        return null;
                    }

                    //    _log.WriteErrorLog(new Exception (NLogInformation.putteradactresponse));
                    return response.Content.ReadAsStreamAsync().Result;
                }
                catch (Exception ex)
                {
                    ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.TeraDActServiceEmailWebJob);
                    throw null;
                }

            }
        }

        internal static FileObject PutRedactResponse(string url, byte[] SourceFileByte, byte[] RuleSetByte)
        {

            string unredactedContent = "--includeUnredacted true";
            //byte[] unredactedContentBytes = FileService.GetBytesnyText(unredactedContent);
            HttpContent unredactedStreamContent = null;
            unredactedStreamContent = new StringContent(unredactedContent);

            HttpContent fileStreamContent = new ByteArrayContent(SourceFileByte);
            HttpContent RuleSetFileContent = new ByteArrayContent(RuleSetByte);


            HttpClientHandler handler = new HttpClientHandler();
            HttpResponseMessage response = null;
            using (HttpClient client = new HttpClient(handler, false))
            {
                using (MultipartFormDataContent multiPartContent = new MultipartFormDataContent())
                {
                    fileStreamContent.Headers.Add("Content-Type", "application/octet-stream");
                    fileStreamContent.Headers.Add("Content-Transfer-Encoding", "utf-8");
                    RuleSetFileContent.Headers.Add("Content-Type", "text/plain");
                    RuleSetFileContent.Headers.Add("Content-Transfer-Encoding", "8bit");
                    //unredactedStreamContent.Headers.Add("Content-Type", "text/plain");
                    unredactedStreamContent.Headers.Add("Content-Transfer-Encoding", "8bit");
                    multiPartContent.Add(unredactedStreamContent, "rc");
                    multiPartContent.Add(fileStreamContent, "doc");
                    multiPartContent.Add(RuleSetFileContent, "trs");
                    var request = new HttpRequestMessage(HttpMethod.Put, url);
                    DateTime startedOn = DateTime.Now;
                    try
                    {
                        request.Content = multiPartContent;
                        response = client.SendAsync(request).Result;
                        
                    }
                    catch (Exception ex)
                    {
                        ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.TeraDActServiceEmailWebJob);
                        // _logHelper.Error(NLogInformation.putteradactresponsefail, ex);
                        return new FileObject();
                    }

                    if (response == null || !response.IsSuccessStatusCode)
                    {
                        return new FileObject();
                    }
                    MemoryStream _ms = new MemoryStream();

                    var res = response.Content.ReadAsByteArrayAsync().Result;
                    var cndres =FileService.getStringFromBytes(res);
                    if (cndres.Contains("System.Net.Sockets.SocketException"))
                    {
                        ErrorLogRepository.Instance.WriteErrorLog(new Exception(cndres), ErrorSource.TeraDActServiceEmailWebJob);
                        return new FileObject();
                    }

                    _ms.Write(res, 0, res.Length);

                    FileObject RedactedFile = new FileObject
                    {
                        Content = res,
                        FileStream = _ms,
                        OperationStatus = OperationStatus.Succeded
                    };

                    string result = FileService.getStringFromBytes(res);
                    var log = new RestLogInfo
                    {
                        RequestMethod = request.Method.ToString(),
                        RequestUrl = request.RequestUri.ToString(),
                        ResponseStatusCode = response == null ? "NULL" : response.StatusCode.ToString(),
                        StartedOn = startedOn,
                        ResponseData = string.IsNullOrEmpty(result) ? null : result.Length > 100 ? result.Substring(0, 100) : result
                    };
                    SharePointRepository.Instance.LogSharePointRestInfo(log);
                    return RedactedFile;

                }

            }
        }

        public static async Task<FileObject> PutRedactResponseMultipleAttachments(string url, List<byte[]> listNested, byte[] Ruleset)
        {
            List<HttpContent> fileStreamContentArray = new List<HttpContent>();
            HttpContent fileStreamContent = null;
            fileStreamContent = new ByteArrayContent(listNested[0]);
            HttpContent RuleSetFileContent = new ByteArrayContent(Ruleset);
            HttpClientHandler handler = new HttpClientHandler();
            using (HttpClient client = new HttpClient(handler, false))
            {
                using (MultipartFormDataContent multiPartContent = new MultipartFormDataContent())
                {
                    RuleSetFileContent.Headers.Add("Content-Type", "text/plain");
                    RuleSetFileContent.Headers.Add("Content-Transfer-Encoding", "8bit");
                    multiPartContent.Add(RuleSetFileContent, "trs");
                    fileStreamContent.Headers.Add("Content-Type", "application/octet-stream");
                    fileStreamContent.Headers.Add("Content-Transfer-Encoding", "utf-8");
                    multiPartContent.Add(fileStreamContent, "doc");

                    var request = new HttpRequestMessage(HttpMethod.Put, url);
                    request.Content = multiPartContent;
                    var response = await client.SendAsync(request);

                    if (!response.IsSuccessStatusCode)
                    {
                        return null;
                    }
                    MemoryStream _ms = new MemoryStream();
                    Task task = response.Content.ReadAsStreamAsync().ContinueWith(t =>
                    {
                        var res = FileService.GetBytesFromStream(t.Result);
                        _ms.Write(res, 0, res.Length);

                    });


                    FileObject RedactedFile = new FileObject
                    {

                        FileStream = _ms,
                        OperationStatus = OperationStatus.Succeded
                    };
                    return RedactedFile;
                }
            }
        }
    }
}