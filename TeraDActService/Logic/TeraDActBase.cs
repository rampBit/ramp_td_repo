﻿using BusinessObjects;
using Common.Repository.SQLAzureDBUtility;
using RampGroup.Config.Utility;
using RampGroup.Logging;
using System.IO;

namespace TeraDActService.Logic
{
    public abstract class TeraDActBase
    {
        public static LogHelper _logHelper = null;
        static  TeraDActBase()
        {
            _logHelper = new LogHelper();
            if(TeraDActConfiguration.Settings==null)
                TeraDActConfigurationRepository.Instance.getTeraDActConfiguration();
            EmailContent = TeradactConstants.EmailContents;
            baseURL = TeradactConstants.baseURL;
            TeradactURL = TeradactConstants.TeradactURL;

        }
        public static readonly string EmailContent;

        private static string baseURL  ;
        public  static string TeradactURL;
        //internal static readonly string rulesetfileName = TeradactConstants.rulesetfileName;
     //   internal static readonly string RulesetPath = TeradactConstants.RulesetPath;

       //internal static string RedactMailPath = TeraDActConfiguration.Settings.RedactMailPath;
       // internal static string EmailStagingPath = TeraDActConfiguration.Settings.EmailStagingPath;


    }
}