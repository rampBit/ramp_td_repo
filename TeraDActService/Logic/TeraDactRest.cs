﻿using BusinessObjects;
using FileUtility;
using RampGroup.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using TeraDActEventHandler;

namespace TeraDActService.Logic
{
    public class TeraDActRest : TeraDActBase, ITeraDActRest
    {


        public TeraDActRest() : base()
        {

            TeradactRedactEventHandler redactHandler = new TeradactRedactEventHandler();
            RestHelper._httpClient = new System.Net.Http.HttpClient();
            // redactHandler.RedactProcessing += RedactHandler_RedactProcessing;
        }



        public void RedactHandler_RedactProcessing(object sender, CustomEventHandler.CustomEventArgs<EmailMessageResponse> e)
        {  //send to teradact e will have entire desc..
            //TeradactHelper.validateFromTeradact();
        }

        public void Authenticate()
        {
            throw new NotImplementedException();
        }


        public void GetAllRuleSet()
        {
            string result;
            var response = RestHelper.GetResponse(TeradactURL).Result;
            if (response.IsSuccessStatusCode)
            {
                result = response.Content.ReadAsStringAsync().Result;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(result);
                XmlNodeList nodeList = doc.DocumentElement.SelectNodes(TeraDActConfiguration.Settings.RulesetXMLPathNode);
                List<string> rulesets = new List<string>();
                foreach (XmlNode node in nodeList)
                {
                    string name = node.InnerText;
                    rulesets.Add(name);
                }
                _logHelper.Info(NLogInformation.getallrulesetpass);
            }
            else
            {
                _logHelper.Fatal(NLogInformation.getallrulesetfail);
                result = string.Format("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }
        }


        public OperationStatus Redact(AttachmentSampleServiceResponse emailListToRedact)
        {

            return createStreamOnServer(emailListToRedact);
        }

        public void TestRest()
        {

        }

        private OperationStatus createStreamOnServer(AttachmentSampleServiceResponse emailListToRedact)
        {
            OperationStatus status = OperationStatus.Succeded;
            var emailListToRedactDup = emailListToRedact.Clone() as AttachmentSampleServiceResponse;
            string nomatchesFound = "no matches found";
            string noredactionrequired = "No Redaction Required";
            for (int indx = 0; indx < emailListToRedactDup.EmailMessageResponse.Count; indx++)
            {
                //Redacting Subject
                var Originalsubject = FileService.GetBytesnyText(emailListToRedactDup.EmailMessageResponse[indx].subject);
                var respSubject = TeradactHelper.validateFromTeradact(Originalsubject);

                string NonRedactedSubject = respSubject.Content == null ? noredactionrequired : FileService.getStringFromBytes(respSubject.Content);

                if (respSubject == null || respSubject.FileStream == null || respSubject.Content.Length == 0)
                {
                    status = OperationStatus.Failed; 
                }


                else if (NonRedactedSubject.EndsWith(nomatchesFound, StringComparison.OrdinalIgnoreCase)
                   || NonRedactedSubject.StartsWith(noredactionrequired, StringComparison.OrdinalIgnoreCase)) { }

                else
                {
                    emailListToRedact.EmailMessageResponse[indx].subject = FileService.getStringFromBytes(respSubject.Content);

                }

                //Redacting Email Body
                var respemil = TeradactHelper.validateFromTeradact(emailListToRedactDup.EmailMessageResponse[indx].Content);
                string NonRedactedContent = respemil.Content==null? noredactionrequired : FileService.getStringFromBytes(respemil.Content);
                if (respemil == null || respemil.FileStream == null || respemil.Content.Length == 0)
                {
                    status = OperationStatus.Failed; 
                }
                else if (NonRedactedContent.EndsWith(nomatchesFound, StringComparison.OrdinalIgnoreCase)
                      || NonRedactedContent.StartsWith(noredactionrequired, StringComparison.OrdinalIgnoreCase))
                {
                    // Don't do anything. since we are going to send the original email back.
                }
                else
                {
                    emailListToRedact.EmailMessageResponse[indx].Content = respemil.Content;
                }

            }

            //Redacting Attachments
            for (int indx = 0; indx < emailListToRedactDup.attachments.Count; indx++)
            {

                var respAtt = TeradactHelper.validateFromTeradact(emailListToRedactDup.attachments[indx].FileContent);
                string NonRedactedAttContent = respAtt.Content==null?noredactionrequired: FileService.getStringFromBytes(respAtt.Content);
                if (respAtt == null || respAtt.FileStream == null || respAtt.Content.Length == 0)
                {
                    status = OperationStatus.Failed;  
                }

                else if (NonRedactedAttContent.EndsWith(nomatchesFound, StringComparison.OrdinalIgnoreCase)
                      || NonRedactedAttContent.StartsWith(noredactionrequired, StringComparison.OrdinalIgnoreCase))
                {
                    // Don't do anything. since we are going to send the same attachment back.
                }
                else
                {
                    emailListToRedact.attachments[indx].FileContent = respAtt.Content;
                }
            }

            return status;
        }

        public async Task<FileObject> SendBytestoServer(List<byte[]> content)  //multiple attachment at once along with body
        {
            return await TeradactHelper.validateFromTeradact(content);

        }
        public FileObject RedactFilestoBlob(FileInfo fileInfo)
        {


            return TeradactHelper.validateFromTeradact(FileService.GetBytes(fileInfo.FullName));

        }

        public FileObject RedactFilestoBlob(FileObject fileInfo)
        {

            return TeradactHelper.validateFromTeradact(fileInfo.Content);

        }
        public FileObject RedactFilesForSharePoint(FileObject fileInfo, byte[] SPRuleSetContent)
        {

            return TeradactHelper.validateFromTeradact(fileInfo.Content, SPRuleSetContent);

        }

    }
}