﻿using BusinessObjects;
using Common.Repository.SQLAzureDBUtility;
using System.IO;

namespace TeraDActService.Logic
{
    internal static class TeradactConstants
    {
        //  internal static readonly string rulesetfileName = "RuleSet1.txt";
        // internal static readonly string RulesetPath =  Path.Combine( TeraDActConfiguration.Settings.RuleSetPath , rulesetfileName);

            static TeradactConstants()
        {
            if (TeraDActConfiguration.Settings == null) TeraDActConfigurationRepository.Instance.getTeraDActConfiguration();

        }
        internal static readonly string EmailContents = "EmailContents";
        internal static string baseURL = Path.Combine(TeraDActConfiguration.Settings.ServerURL,
            TeraDActConfiguration.Settings.APIKey);
        internal static string TeradactURL = baseURL + TeraDActConfiguration.Settings.BaseURLRedactSuffix;
        //private static string RedactMailPath = TeraDActConfiguration.Settings.RedactMailPath;
       
    }
}
