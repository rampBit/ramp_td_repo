﻿using AzureUtility;
using BusinessObjects;
using FileUtility;
using RampGroup.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeraDActService.Logic
{
    internal class TeradactHelper : TeraDActBase
    {
        public static async Task<FileObject> validateFromTeradact(List<byte[]> content)  //multiple attachment
        {
            return await RestHelper.PutRedactResponseMultipleAttachments(TeradactURL,
                  content, new AzureHelper().ReadRuleSetsFromDB());


        }
        internal static FileObject validateFromTeradact(byte[] content,byte[]SPRuleSetContent=null)
        {
            if(SPRuleSetContent==null)
            return RestHelper.PutRedactResponse(TeradactURL,
                content, new AzureHelper().ReadRuleSetsFromDB());
            else
                return RestHelper.PutRedactResponse(TeradactURL,   //for sharepoint redact
                content, SPRuleSetContent);


        }
        //internal static Stream validateFromTeradact(byte[] content)
        //{
        //    Stream response = null;


        //    response = RestHelper.PutRedactResponse(TeradactURL,
        //       FileService.GetStream(content), FileService.GetStream(RulesetPath));
        //    //return new MemoryStream(response);
        //    return response;


        //}
        //internal static List<byte[]> GetAllSavedAttachment() {
        //     List<byte[]> Directorybyte = new List<byte[]>();
        //    string EmailPath = Path.Combine(EmailStagingPath,
        //             String.Format("{0}", EmailContents));

        //        // Check if the path is exist or not.
        //        if (!Directory.Exists(EmailPath))
        //        {
        //            _logHelper.Fatal(NLogInformation.getallsavedfilefatal);
        //            throw null;
        //        }
        //        // Get all files from the given directory and added in the collection of bytes"
        //        string[] dirs = Directory.GetFiles(EmailPath);
        //        foreach (string dir in dirs)
        //        {
        //            //Extract the filename from the path.
        //            string fileName = Path.GetFileName(dir);
        //            //Reading the file into bytes and stored them into bytes array
        //            byte[] bytes = System.IO.File.ReadAllBytes(dir);
        //            //Now adding those bytes array to collection of list of bytes.
        //            Directorybyte.Add(bytes);
        //            //If necessary to move the file to different path then uncomment below code.
        //            //File.WriteAllBytes(@"D:\" + fileName, bytes);
        //        }
        //        _logHelper.Info(NLogInformation.getallsavedfileinfo);
        //        return Directorybyte;
        //    }}
    }
}
