﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Common.Repository.SQLAzureDBUtility;
using BusinessObjects;
using System.Configuration;
using System.Threading;
using RampGroup.TeraDActBusinessLogic.ExchangeServiceRequests;
using System.Transactions;
using RampGroup.MX.Core.Authentication;
using RampGroup.MX.Core;

namespace TeraDActServiceEmailWebJob
{
    public class Functions
    {

        [NoAutomaticTriggerAttribute]
        public static async Task ProcessEmails()
        {

            while (true)
            {


                try
                {
                    using (var ts = CreateAsyncTransactionScope())
                    {
                        EmailQueueRepository emailServiceRequest = new EmailQueueRepository();
                        IList<EmailQueue> allmail = null;

                        allmail = emailServiceRequest.GetEmails();

                        //await Task.Delay(30000).ConfigureAwait(false);
                        await ProcessQueue(allmail).ConfigureAwait(false);
                        ts.Complete();
                    }
                }
                catch (Exception ex)
                {
                   

                    ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.TeraDActServiceEmailWebJob);
                }
                finally
                {
                    // SlowStuffSemaphore.Release();
                }
                string interval = ConfigurationManager.AppSettings["EmailPullingIntervalinSecond"] == null ? "60" : ConfigurationManager.AppSettings["EmailPullingIntervalinSecond"];
                Thread.Sleep(TimeSpan.FromSeconds(int.Parse(interval)));
            }
        }

        private static TransactionScope CreateAsyncTransactionScope(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = isolationLevel,
                Timeout = TransactionManager.MaximumTimeout
            };
            return new TransactionScope(TransactionScopeOption.Suppress, transactionOptions, TransactionScopeAsyncFlowOption.Enabled);
        }
        static async Task doWork(IList<EmailQueue> allmail)
        {
            UserEmailMessageRequest req1 = new UserEmailMessageRequest();
           
            List<Task> trackedTasks = new List<Task>();
            
            foreach (EmailQueue mail in allmail)
            {
              
               
                var serviceRequest = new AttachmentSampleServiceRequest();
                UserEmailMessageRequest req = new UserEmailMessageRequest();
                serviceRequest.RequestID = Guid.NewGuid();
                serviceRequest.attachment = new AttachmentDetails
                {
                    id = mail.MessageID,
                    ExchangeAttachementEmailUniqueID = mail.ExchangeAttachementEmailUniqueID,
                    RetryCount = mail.RetryCount ?? 0
                };
                
               
                   try
                   {
                   // req1.redactTest();
                    await    req.processAll(serviceRequest) ;

                   }
                   catch (Exception ex)
                   {
                       ErrorLogRepository.Instance.WriteErrorLog(ex, ErrorSource.TeraDActServiceEmailWebJob);
                   }
                   finally
                   {
                      
                   }
              
            }
           
        }


        private static async Task ProcessQueue(IList<EmailQueue> allmail)
        {
            await Task.WhenAll(doWork(allmail));
            ////IMP update
            foreach (EmailQueue mail in allmail)
            {
                UserEmailMessageRequest req = new UserEmailMessageRequest();
                await req.UpdateIPM(mail.ExchangeAttachementEmailUniqueID, mail.MessageID);
            }

        }
    }
}
