﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;

namespace TeraDActServiceEmailWebJob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    class Program: WebJobBase
    {
         
        static void Main()
        {
            var host = GetJobHost("TeraDActServiceEmailWebJob");
            host.CallAsync(typeof(Functions).GetMethod("ProcessEmails"));
             
            host.Start();
            Console.WriteLine("Host started. press return key to stop.");
            Console.ReadLine();
        }
    }
}
