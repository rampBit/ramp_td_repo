﻿using Microsoft.Azure.WebJobs;
using System;
 

namespace TeraDActServiceEmailWebJob
{
    public abstract class WebJobBase
    {
        protected static JobHost GetJobHost(string webJobName)
        {
            JobHost host = new JobHost(GetConfiguration(webJobName));

            return host;
        }
        protected static JobHostConfiguration GetConfiguration(string webJobName)
        {
            JobHostConfiguration config = new JobHostConfiguration
            {
                DashboardConnectionString = "", //BlobStorageAccount.Settings.BlobStorageConnectionString,
                StorageConnectionString = ""// BlobStorageAccount.Settings.BlobStorageConnectionString
            };
            config.HostId = Guid.NewGuid().ToString("N");
           
            var host = new JobHost(config);
            return config;
        }

    }
}
