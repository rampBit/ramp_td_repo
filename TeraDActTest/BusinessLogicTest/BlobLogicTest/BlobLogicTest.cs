﻿using System;
using Moq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessObjects;
using BlobBusinessLogic;
using System.Collections.Generic;

namespace BusinessLogicTest.Tests
{
    [TestClass()]
    public class BlobLogicTest
    {
        [TestMethod()]
        public void B2BUploadTest()
        {
            //=====Arrange: Setup the unit being tested
            List<FileObject> FileObjectBlobs = new List<FileObject>();
            FileObject FileObjectBlob = new FileObject()
            {
                FileName = "",
                ContentType = "",
                FileLength = 4565,
                FilePath = "",
            };
            FileObjectBlobs.Add(FileObjectBlob);
            List<BlobDirectory> BlobDirectoryList = new List<BlobDirectory>();
            BlobDirectory BlobDirectoryLists = new BlobDirectory()
            {
                DirectoryName = "",
                //ParentID = 0000-0000-0000-0000,
            };
            BlobDirectoryLists.Blobs = FileObjectBlobs;
            BlobDirectoryList.Add(BlobDirectoryLists);
            List<BlobStorageContainer> blobstoragecontainer = new List<BlobStorageContainer>();
            BlobStorageContainer blobstoragecontainers = new BlobStorageContainer()
            {
                ContainerName = "Source",
            };
            blobstoragecontainer.Add(blobstoragecontainers);
            BlobResponse retObj = new BlobResponse();
            retObj.BlobStorageContainers = blobstoragecontainer;
            blobstoragecontainers.BlobDirectoryList = BlobDirectoryList;

            //=====Act: Exercise the unit under test and capture results
            //We are using the Moq concept to mocking the interface 
            //in order to prevent the logic not hit to backend code.
            //It will return the result as per the your response.
            var mockRepo = new Mock<IBlobLogic>();
            mockRepo.Setup(x => x.B2BUpload());

            //=====Assert: Verify the behavior between the 
            //actual response and expected response.
            if (retObj != null)
            {
                bool chk = true;
                Assert.AreEqual(true, chk ,"Actual value is equal to the Excepted value.");
            }
            else
            {
                Assert.Fail("Test Fail beacause object is NUll.");     
            }
        }

        [TestMethod()]
        public void DowloadBlobTest()
        {
            //=====Arrange: Setup the unit being tested
            BlobResponse retObj = new BlobResponse();
            //=====Act: Exercise the unit under test and capture results
            //We are using the Moq concept to mocking the interface 
            //in order to prevent the logic not hit to backend code.
            //It will return the result as per the your response.


            //This step will tell your mock what to return
            //var b2bObject = new BlobResponse();
            //var retrnData = companyObject.InsertCompany(company);

            //=====Assert: Verify the behavior between the 
            //actual response and expected response.
            Assert.AreEqual(true, retObj);

        }

        [TestMethod()]
        public void UploadBlobsTest()
        {
            //=====Arrange: Setup the unit being tested
            BlobResponse retObj = new BlobResponse();
            //=====Act: Exercise the unit under test and capture results
            //We are using the Moq concept to mocking the interface 
            //in order to prevent the logic not hit to backend code.
            //It will return the result as per the your response.


            //This step will tell your mock what to return
            //var b2bObject = new BlobResponse();
            //var retrnData = companyObject.InsertCompany(company);

            //=====Assert: Verify the behavior between the 
            //actual response and expected response.
            Assert.AreEqual(true, retObj);

        }

        [TestMethod()]
        public void GetAllContainerListTest()
        {
            //=====Arrange: Setup the unit being tested
            BlobResponse retObj = new BlobResponse();
            //=====Act: Exercise the unit under test and capture results
            //We are using the Moq concept to mocking the interface 
            //in order to prevent the logic not hit to backend code.
            //It will return the result as per the your response.


            //This step will tell your mock what to return
            //var b2bObject = new BlobResponse();
            //var retrnData = companyObject.InsertCompany(company);

            //=====Assert: Verify the behavior between the 
            //actual response and expected response.
            Assert.AreEqual(true, retObj);
        }
    }
}