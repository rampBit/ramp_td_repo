﻿CREATE PROCEDURE [dbo].[CleanUPData]
	 
	As
	SET NOCOUNT ON 
	begin
		DELETE EmailQueue 
		DELETE BlobQueue
		DELETE ErrorLog 
		DELETE RestLogInfo
        DELETE SharePointRequestQueue

	end