﻿
CREATE Procedure [dbo].[ExceptionLoggingToDataBase]  
(  
@ErrorTime date,
@ErrorMessage varchar(100)=null,  
@ExceptionType varchar(100)=null,
@ErrorSource varchar(200)=null
)  
as  
begin  
Insert into ErrorLog
(  
ErrorTime ,  
ErrorMessage,
ExceptionType,
ErrorSource  
)  
select  
@ErrorTime,  
@ErrorMessage,  
@ExceptionType,
@ErrorSource  
End