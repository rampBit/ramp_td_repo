﻿CREATE PROC [dbo].[GetExchangeRuleSet]
@ProductName nvarchar(100)='ExchangeSettings'
AS
BEGIN
	SELECT RulesetDescription from [dbo].[RuleSet] RS
	JOIN PRODUCTS P ON P.PRODUCTID=RS.PRODUCTID where P.ProductName=@ProductName
END