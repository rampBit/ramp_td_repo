﻿CREATE PROCEDURE LogException
(  
@ErrorTime VARCHAR(20),
@ErrorMessage VARCHAR(4000)=null,  
@ExceptionType VARCHAR(100)=null,
@ErrorSource VARCHAR(200)=null

)  
AS  
	BEGIN  
		INSERT INTO [ErrorLog](ErrorTime,ErrorMessage,ExceptionType,ErrorSource)
		SELECT  
		GETDATE(),
		@ErrorMessage,  
		@ExceptionType,  
		@ErrorSource
	End  
