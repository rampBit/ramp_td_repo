﻿
CREATE PROC [dbo].[LogSharePointRestInfo](
@StartedOn datetime,
@RequestMethod varchar(6),
@RequestUrl varchar(max),
@ResponseStatusCode varchar(5),
@ResponseData nvarchar(max)=null
) 
AS 
BEGIN
	INSERT RestLogInfo
	SELECT @StartedOn,@RequestMethod,@RequestUrl,@ResponseStatusCode,GETDATE(),@ResponseData
END
