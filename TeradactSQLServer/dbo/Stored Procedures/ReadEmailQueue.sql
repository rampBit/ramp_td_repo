﻿
CREATE PROCEDURE [dbo].[ReadEmailQueue]  
--@TenantName varchar(80)=null
AS
BEGIN
	DECLARE @EmailBatchSize INT
	SELECT  @EmailBatchSize=MailCount from ProductSettings PS where productID=1
	 --JOIN TENANT T ON PS.TenantID=t.id AND IsActive=1 AND NAME=@TenantName 
	
	SELECT TOP (ISNULL(@EmailBatchSize,50)) [EmailQueueID],[MessageID],[Status],[ExchangeAttachementEmailUniqueID],[RetryCount],[EmailID],
	[AccountKey] As Passwrod, [Domain],	EwsURL
	FROM [EmailQueue] eq 
	JOIN ProductSettings PS 
	ON
	PS.AccountName=eq.EmailID 
	--JOIN TENANT T 
	--ON 
	--PS.TenantID=t.id 
	--AND IsActive=1  
	WHERE [Status] NOT IN ('MailDelivered','MailUndelivered')  AND ISNULL(RetryCount,0)<3
END
