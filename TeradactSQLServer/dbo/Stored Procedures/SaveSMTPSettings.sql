﻿ 
CREATE PROCEDURE [dbo].[SaveSMTPSettings]
	-- Add the parameters for the stored procedure here
	@ProductName nvarchar(100)='ExchangeSettings',
	@TargetName nvarchar(50)=null,
	@Host varchar(50),
	@Port int=25,
	@EnableSSL bit=1
	

AS
BEGIN
	DECLARE @ProductID BIGINT
 
	SELECT  @ProductID=ProductID	
	FROM Products
	WHERE ProductName = @ProductName
	
	
	  IF EXISTS(SELECT 1 FROM [SMTPSettings] WHERE ProductID=@ProductID)   
		BEGIN
			UPDATE [SMTPSettings] SET TargetName=@TargetName, 
			[Host]=@Host,[Port]=@Port,[EnableSSL]=@EnableSSL
			 WHERE ProductID=@ProductID
			
		END
	 ELSE
	 		BEGIN
				INSERT [SMTPSettings]
				SELECT  
				@TargetName,@Host,@Port,@EnableSSL,@ProductID,1
		  END
END