﻿create proc getSMTPSettings
@ProductName nvarchar(100)='ExchangeSettings'
AS
BEGIN
SELECT TOP 1 [TargetName], [Host],[Port], [EnableSsl]
 FROM [SMTPSettings] sp
 join products p on p.productID=sp.ProductID
	where IsActive = '1' AND p.productName=@ProductName
END