﻿

CREATE PROCEDURE [dbo].[getSPUnAttendedApplyRulesSettings]
@ProductName nvarchar(100)='SharePointSettings'
	 AS
	 SELECT rs.ID ,rs.SourceSiteURL,rs.SourceDocLib,rs.SourceFolder,
	 rs.TargetSiteURL,rs.TargetDocLib,rs.TargetFolder,
	 rs.AppliedRuleSets  FROM SPUnAttendedApplyRulesSettings rs 
	 join products p on p.productID=rs.ProductID
	 WHERE IsActive =1 AND p.productName=@ProductName