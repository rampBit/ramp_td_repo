﻿ 
create PROCEDURE [dbo].[getTDRuleSetDirectiveInfo]
	@RuleSetName nvarchar(50)=''
AS
if len(@RuleSetName)>0
	SELECT rl.RuleSetContent,rl.RuleSetName  from TDRuleSetDirectiveInfo rl
	where IsActive=1 
	AND RuleSetName=@RuleSetName 
 else
	SELECT rl.RuleSetContent,rl.RuleSetName from TDRuleSetDirectiveInfo rl
	where IsActive=1


