﻿CREATE PROCEDURE getUserRoleDirectives  
(     
    @GroupName NVARCHAR(1000)  
)  
AS  
BEGIN     
    SET NOCOUNT ON;  
  
 DECLARE @RSN NVARCHAR(MAX)  
  
 SET @RSN=STUFF(   
    (SELECT DISTINCT   
     ',' + RulesetName    
    FROM [dbo].[RulesandGroups]  
    WHERE GroupName IN (SELECT value FROM STRING_SPLIT(@GroupName, ',') WHERE RTRIM(value) <> '')  
    FOR XML PATH ('')), 1, 1, '');  
  
 SET @RSN=STUFF(   
    (SELECT DISTINCT   
     ',' + ColumnData    
    FROM [fn_CSVToTable](@RSN)   
    FOR XML PATH ('')), 1, 1, '');  
  
 SELECT @RSN AS RuleSetName;  
  
 SET NOCOUNT OFF;  
END  
