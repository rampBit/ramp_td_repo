﻿ 
create PROCEDURE [dbo].[saveBlobQueue]
	@BlobQueueID bigint=0,
	@Status varchar(20),
	@OperationType char(1),
	@RequestFilesCount bigint =null,
	@FilesProcessedCount bigint =null,
	@identity int OUTPUT
AS
	BEGIN
	IF EXISTS(SELECT 1 FROM BlobQueue where BlobQueueID=@BlobQueueID AND @OperationType='I') return;
		IF @OperationType ='I'
			 BEGIN
				 INSERT BlobQueue (RequestedTime,[Status],[RequestFilesCount])
				 SELECT GETDATE(),@Status,@RequestFilesCount
				 SELECT @identity = SCOPE_IDENTITY()
			 END
		ELSE IF @OperationType ='U'
			BEGIN
				UPDATE BlobQueue SET ProcessedTime=GETDATE(),
				[Status]=@Status,[FilesProcessedCount]= @FilesProcessedCount WHERE BlobQueueID=@BlobQueueID
			END
	END

 