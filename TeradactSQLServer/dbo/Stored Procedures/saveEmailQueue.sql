﻿

 CREATE PROCEDURE [dbo].[saveEmailQueue]
	@MessageID varchar(200),
	@Status varchar(20),
	@OperationType char(1),
	@ExchangeAttachementEmailUniqueID varchar(200)=null,
	@EmailID  varchar(200)=null
AS
	BEGIN
	IF EXISTS(SELECT 1 FROM EmailQueue where MessageID=@MessageID AND @OperationType='I') return;
		IF @OperationType ='I'
			 BEGIN
				 INSERT EmailQueue (MessageID,RequestedTime,[Status],[ExchangeAttachementEmailUniqueID],[EmailID])
				 SELECT @MessageID,GETDATE(),@Status,@ExchangeAttachementEmailUniqueID,@EmailID
			 END
		ELSE IF @OperationType ='U'
			BEGIN
				UPDATE EmailQueue SET ProcessedTime=GETDATE(),[Status]=@Status,[RetryCount]=
				case when @Status='RedactionFailed' then ISNULL([RetryCount],0)+1   else  RetryCount
				end  WHERE MessageID=@MessageID
			END
	END
	 