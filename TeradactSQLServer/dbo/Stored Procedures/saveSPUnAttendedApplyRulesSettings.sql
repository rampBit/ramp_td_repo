﻿

CREATE PROCEDURE [dbo].[saveSPUnAttendedApplyRulesSettings]
    @ID INT =NULL, 
    @SourceSiteURL NVARCHAR(200) , 
    @SourceDocLib NVARCHAR(100)  , 
    @SourceFolder NVARCHAR(100)  =NULL, 
    @TargetSiteURL NVARCHAR(200) , 
    @TargetDocLib NVARCHAR(100)  , 
    @TargetFolder NVARCHAR(100) =NULL  , 
	@AppliedRuleSets  NVARCHAR(200)  ,
	@ProductName nvarchar(100)='SharePointSettings'

AS
BEGIN
DECLARE @productID int 
	 
	SELECT @productID=   ProductID from products WHERE productName=@ProductName

IF EXISTS(SELECT 1 FROM SPUnAttendedApplyRulesSettings WHERE 
 ProductID=@productID)
	UPDATE SPUnAttendedApplyRulesSettings SET SourceSiteURL=@SourceSiteURL,SourceDocLib=@SourceDocLib,SourceFolder=@SourceFolder,TargetSiteURL=@TargetSiteURL,
	TargetDocLib=@TargetDocLib,TargetFolder=@TargetFolder,
	AppliedRuleSets=@AppliedRuleSets WHERE 
	 ProductID=@productID
	 
ELSE
INSERT INTO SPUnAttendedApplyRulesSettings(SourceSiteURL,
SourceDocLib,SourceFolder,TargetSiteURL,TargetDocLib,TargetFolder,
AppliedRuleSets,ProductID)VALUES
(@SourceSiteURL,@SourceDocLib,@SourceFolder,@TargetSiteURL,@TargetDocLib,
@TargetFolder,@AppliedRuleSets,@productID)
END 

	 
	 