﻿ CREATE PROCEDURE [dbo].[saveSharePointQueue]
	@SharePointQueueID bigint=0,
	@Status varchar(20),
	@OperationType char(1),
	@RequestFilesCount bigint =null,
	@FilesProcessedCount bigint =null,
    @FileName varchar(200) =NULL,
    @SPListID varchar(50) =NULL,
    @SPListItemID int =NULL,
    @SPHostURL varchar(100) =NULL,
	@EventSources varchar(200)= NULL,
	@identity int OUTPUT
AS
	BEGIN
	IF EXISTS(SELECT 1 FROM SharePointRequestQueue where SharePointQueueID=@SharePointQueueID AND @OperationType='I') return;
		IF @OperationType ='I'
			 BEGIN
				 INSERT SharePointRequestQueue (RequestedTime,[Status],[RequestFilesCount],[FileName],[SPListID],[SPListItemID],[SPHostURL],[EventSources])
				 SELECT GETDATE(),@Status,@RequestFilesCount,@FileName,@SPListID,@SPListItemID,@SPHostURL,@EventSources
				 SELECT @identity = SCOPE_IDENTITY()
			 END
		ELSE IF @OperationType ='U'
			BEGIN
				UPDATE SharePointRequestQueue SET ProcessedTime=GETDATE(),
				[Status]=@Status,[FilesProcessedCount]= @FilesProcessedCount WHERE SharePointQueueID=@SharePointQueueID
			 SELECT @identity =@SharePointQueueID
			END
	END

 