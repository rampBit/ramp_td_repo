﻿CREATE PROCEDURE [dbo].[updatePolicyInfo]
@xmldata XML
--@POLICYNAME NVARCHAR(100),@POLICYTYPE NVARCHAR(50),@POLICYKEY NVARCHAR(50),@POLICYVALUE NVARCHAR(50)	 
AS
	DELETE TDPolicyInfo

	INSERT TDPolicyInfo (PolicyName,PolicyType,PolicyKey,PolicyValue,IsActive,ModifiedDate)
	SELECT 
		ref.value('PolicyName[1]', 'NVARCHAR (100)') AS PolicyName ,
		ref.value('PolicyType[1]', 'NVARCHAR (100)') AS PolicyType ,		
		ref.value('PolicyKey[1]', 'NVARCHAR (100)') AS PolicyKey,
		ref.value('PolicyValue[1]', 'NVARCHAR (100)') AS PolicyValue ,1,GETDATE()
	FROM 
		@xmldata.nodes('/ArrayOfTDPolicyInfo/TDPolicyInfo')  xmlData( ref )
	
	
	--SELECT 'Redact' ,@POLICYTYPE,@POLICYKEY,@POLICYVALUE,1,getdate()
	--SELECT  'Redact',@pci,@phi,@pii,@ranked,@PCIType, @PCIKey,@PHIType ,@PHIKey,@PIIType,@PIIKey,@RankedType,
	--@RankedKey,1,getdate()




	--@pci NVARCHAR(100),@PCIType nvarchar(50),@PCIKey NVARCHAR(50),
	--			@PHIType NVARCHAR(50), @PHIKey NVARCHAR(50),
	--			@PIIType NVARCHAR(50), @PIIKey NVARCHAR(50),
	--			@RankedType NVARCHAR(50),@RankedKey NVARCHAR(50),@PolicyName NVARCHAR(30)='',
	--@phi NVARCHAR(100),
	--@pii NVARCHAR(100),
	--@ranked NVARCHAR(100) select * from TDPolicyInfo
