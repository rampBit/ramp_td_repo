﻿CREATE PROCEDURE [dbo].[updateTDRulesetDirInfo]
	@NewRuleSetName nvarchar(50) null,@RuleSetName nvarchar(50),@RuleSetContent NvarCHAR(max)
AS

--DELETE TDRuleSetDirectiveInfo WHERE RuleSetName=@RuleSetName
if(len(@NewRuleSetName)=0 AND @RuleSetName not in (SELECT RuleSetName FROM TDRuleSetDirectiveInfo)) 
INSERT TDRuleSetDirectiveInfo SELECT @RuleSetName,@RuleSetContent,1,GETDATE()
else
update TDRuleSetDirectiveInfo set RuleSetName=@NewRuleSetName,RuleSetContent=@RuleSetContent where RuleSetName=@RuleSetName


--[ID] [int] IDENTITY(1,1) NOT NULL,
--@RuleSetName] [varchar](50) NULL,
--	[RuleSetContent] [varchar](Max) NULL,
--	[IsActive] [bit] NULL,
--	[ModifiedDate] [datetime] NULL,@replacementText  NVARCHAR(200),
	--@Type NvarCHAR(30),@Pattern NVARCHAR(200),
