﻿

CREATE PROCEDURE [dbo].[usp_ActiveDirectorySettingsDetails]
	-- Add the parameters for the stored procedure here
	@ApplicationID NVARCHAR(100)=NULL,  
	@RedirectURL VARCHAR(50)=NULL,
	@ClientID VARCHAR(100)=NULL,
	@ClientSecret VARCHAR(100)=NULL,
	@TenantID VARCHAR(100)=NULL,
	@TenantName VARCHAR(100)=NULL,
	@AuthString VARCHAR(100)=NULL,
	@IsActive bit
AS
BEGIN
 
		DELETE [ActiveDirectorySettings] 
		
		INSERT INTO [dbo].[ActiveDirectorySettings]
				   ([ApplicationID]
				   ,[RedirectURL]
				   ,[ClientID]
				   ,[ClientSecret]
				   ,[TenantID]
				   ,[TenantName]
				   ,[AuthString]
				   ,[IsActive]				 
				   )

			 VALUES (  
				@ApplicationID,	
				@RedirectURL,
				@ClientID,
				@ClientSecret,
				@TenantID,
				@TenantName,
				@AuthString ,
				@IsActive

				)
			
END