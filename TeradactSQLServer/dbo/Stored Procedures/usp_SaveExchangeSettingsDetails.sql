﻿
 
 
CREATE PROCEDURE [dbo].[usp_SaveExchangeSettingsDetails]
	-- Add the parameters for the stored procedure here
	@ExchangeSettings NVARCHAR(50),  
	@AccountKey NVARCHAR(200),
	@AccountName NVARCHAR(200),
	@AliasName NVARCHAR(200)=NULL,
	@Domain NVARCHAR(50)=NULL,
	@Protocol NVARCHAR(5)=NULL,
	@BlobType VARCHAR(30)=NULL,
	@EwsURL VARCHAR(100)=NULL,
	@MailCount int=null,
	@RulesetDescription NVARCHAR(1000)=NULL

AS
BEGIN
	DECLARE @ProductID BIGINT
	DECLARE @TENANTID BIGINT
	SELECT  @ProductID=ProductID	
	FROM Products
	WHERE ProductName = @ExchangeSettings
	
	
	  IF EXISTS(SELECT 1 FROM [ProductSettings] WHERE ProductID=@ProductID)   
		BEGIN
			UPDATE [ProductSettings] SET [AccountKey]=@AccountKey, 
			[Domain]=@Domain ,[AliasName]=@AliasName, AccountName=@AccountName,
			[Protocol]=@Protocol,BlobType=@BlobType,EwsURL=@EwsURL,MailCount=@MailCount
			 WHERE ProductID=@ProductID
			IF @ExchangeSettings ='ExchangeSettings'  OR @ExchangeSettings='ExchangeOnPremSettings'
				UPDATE RULESET SET RulesetDescription=@RulesetDescription, ModifiedOn=getdate() WHERE PRODUCTID=@ProductID 
		END
	 ELSE
	 		BEGIN
			INSERT INTO [dbo].[ProductSettings]
				   ([ProductID]
				   ,[AccountName]
				   ,[AccountKey]
				   ,[AliasName]
				   ,[Domain]
				   ,[Protocol]
				   ,[BlobType]
				   ,CreatedDate
				   ,CreatedBy
				   ,[EwsURL]
				   ,[MailCount]
				   --,[TenantID]
				   )

			SELECT  
				@ProductID,	
				@AccountName,
				@AccountKey,
				@AliasName,
				@Domain ,
				@Protocol,
				@BlobType,
				getdate(),
				'System',
				@EwsURL,
				@MailCount
				--,@TENANTID
			DECLARE @MAXRID INT
			SELECT @MAXRID=ISNULL(MAX(RID),0)+1 FROM RULESET 
			IF @ExchangeSettings ='ExchangeSettings'  OR @ExchangeSettings='ExchangeOnPremSettings'
			BEGIN
				INSERT RULESET 
				SELECT @MAXRID,'RULESET_' +CONVERT(NVARCHAR(40), NEWID()),
					@RulesetDescription,getdate(),null,1,@productID
			END
		  END
END
GO