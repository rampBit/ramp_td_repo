﻿CREATE PROCEDURE [dbo].[usp_SaveRolesandGroupsDetails]  
  
@GroupName VARCHAR(50)=NULL,    
@RuleSet VARCHAR(200)=NULL  
AS  
BEGIN

DELETE   [RulesandGroups] WHERE GROUPNAME=@GroupName 
INSERT INTO [dbo].[RulesandGroups]  
                   ([GroupName]  
       ,[RulesetName]  
       )  
       VALUES  
      ( @GroupName,@RuleSet)  
END