﻿-- =============================================
-- Author:		Pavan
-- Create date: 
-- Description:	
-- exec usp_getProductID  'ExchangeSettings'
-- =============================================
CREATE PROCEDURE [dbo].[usp_getProductID] 
	-- Add the parameters for the stored procedure here
	@ExchangeSettings NVARCHAR(50)  
	--,
	--@ProductID INT OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT  ProductID	
	FROM Products
	WHERE ProductName = @ExchangeSettings
END