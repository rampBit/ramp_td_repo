﻿ 


CREATE PROCEDURE [dbo].[usp_saveSharePointSettingsDetails]
	-- Add the parameters for the stored procedure here
	@SPFileTransferServiceURL NVARCHAR(1000),  
	@SPAdminUserID NVARCHAR(200),
	@SPAdminPassword NVARCHAR(200),
	@AdminCentarlSiteURL NVARCHAR(1000),
	@SharedDocumentLibraryName NVARCHAR(200) =NULL,
	@IsActive Bit,
	@Domain nvarchar(100)=null,
	@ProductName nvarchar(100)='SharePointSettings'

	As
BEGIN
	DECLARE @productID int 
	DECLARE @maxPId int
	SELECT @productID=   ProductID from products WHERE productName=@ProductName
	 IF EXISTS(SELECT 1 FROM [SharePointOnlineSettings] WHERE  
	  ProductID=@productID )
	  BEGIN
		
		UPDATE [SharePointOnlineSettings] SET [SPFileTransferServiceURL]=@SPFileTransferServiceURL, 
		[SPAdminUserID]=@SPAdminUserID ,[SPAdminPassword]=@SPAdminPassword,
		[AdminCentarlSiteURL]=@AdminCentarlSiteURL,
		[SharedDocumentLibraryName]=@SharedDocumentLibraryName ,
		[Domain]=@Domain
		WHERE ProductID=@productID
	END

	 ELSE
		BEGIN
		IF @productID IS NULL
			BEGIN
				SELECT @maxPId= Isnull( Max(ProductID),0)+1 from products
				INSERT Products  SELECT @maxPId , @ProductName
			END
		SELECT @productID=   ProductID from products WHERE productName=@ProductName
			INSERT INTO [dbo].[SharePointOnlineSettings]
				   ([SPFileTransferServiceURL]
				   ,[SPAdminUserID]
				   ,[SPAdminPassword]
				   ,[AdminCentarlSiteURL]
				   ,[SharedDocumentLibraryName]
				   ,[IsActive],
				   [Domain],[ProductID])
			SELECT  
				@SPFileTransferServiceURL,	
				@SPAdminUserID,
				@SPAdminPassword,
				@AdminCentarlSiteURL,
				@SharedDocumentLibraryName,
				'1',@Domain,@productID
		  END
	END



GO


