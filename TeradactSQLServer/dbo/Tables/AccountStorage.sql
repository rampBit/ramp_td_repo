﻿
GO

CREATE TABLE [dbo].[AccountStorage](
	[SID] [int] NOT NULL,
	[AccountKey] [nvarchar](50) NOT NULL,
	[AccountName] [nvarchar](50) NOT NULL,
	[Flag] [int] NOT NULL,
	[FKID_ContainerId] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [SID] PRIMARY KEY CLUSTERED 
(
	[SID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO


