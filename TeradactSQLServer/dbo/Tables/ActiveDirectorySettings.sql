﻿CREATE TABLE [dbo].[ActiveDirectorySettings] (
    [ID]            INT           IDENTITY (1, 1) NOT NULL,
    [ApplicationID] VARCHAR (100) NULL,
    [RedirectURL]   VARCHAR (50)  NULL,
    [ClientID]      VARCHAR (100) NULL,
    [ClientSecret]  VARCHAR (100) NULL,
    [TenantID]      VARCHAR (100) NULL,
    [TenantName]    VARCHAR (100) NULL,
    [AuthString]    VARCHAR (100) NULL,
    [IsActive]      BIT           NULL
);

