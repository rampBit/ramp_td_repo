﻿CREATE TABLE [dbo].[BlobQueue](
	[BlobQueueID] [bigint] IDENTITY(1,1) NOT NULL,
	[RequestedTime] [datetime] NOT NULL,
	[ProcessedTime] [datetime] NULL,
	[Status] [nchar](20) NOT NULL,
	[RequestFilesCount] [bigint] NULL,
	[FilesProcessedCount] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[BlobQueueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
