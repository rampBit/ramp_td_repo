﻿CREATE TABLE [dbo].[EmailQueue]
(
	[EmailQueueID] BIGINT NOT NULL identity(1,1) PRIMARY KEY, 
    [MessageID] VARCHAR(200) NOT NULL, 
    [RequestedTime] DATETIME NOT NULL, 
    [Status] NCHAR(20) NOT NULL, 
    [ProcessedTime] DATETIME NULL, 
    [ExchangeAttachementEmailUniqueID] VARCHAR(200) NULL, 
    [RetryCount] SMALLINT NULL,
	EmailID varchar(50) NULL
)
