﻿CREATE TABLE [dbo].[ErrorLog] (
    [ErrorLogID]    INT             IDENTITY (1, 1) NOT NULL,
    [ErrorTime]     NVARCHAR (20)   NOT NULL,
    [ErrorMessage]  NVARCHAR (4000) NOT NULL,
    [ExceptionType] VARCHAR (100)   NULL,
    [ErrorSource]   NVARCHAR (200)  NULL,
    CONSTRAINT [PK_ErrorLog_ErrorLogID] PRIMARY KEY CLUSTERED ([ErrorLogID] ASC)
);



GO

