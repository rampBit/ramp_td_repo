﻿CREATE TABLE [dbo].[FileStorage] (
    [FileID]          INT            IDENTITY (1, 1) NOT NULL,
    [FileContent]     NVARCHAR (MAX) NULL,
    [FileType]        VARCHAR (50)   NOT NULL,
    [FileDescription] VARCHAR (50)   NOT NULL,
    CONSTRAINT [FileID] PRIMARY KEY CLUSTERED ([FileID] ASC)
);

