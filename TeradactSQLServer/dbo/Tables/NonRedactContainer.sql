﻿
GO

CREATE TABLE [dbo].[NonRedactContainer](
	[NRItemId] [int] NOT NULL,
	[NRItemName] [nvarchar](50) NULL,
	[NRItemDescription] [nvarchar](max) NULL,
	[NRItemPath] [nvarchar](50) NULL,
	[NRItemSize] [nchar](10) NULL,
	[NRItemType] [nchar](10) NULL,
	[FKID_ContainerId] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [NRItemId] PRIMARY KEY CLUSTERED 
(
	[NRItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO


