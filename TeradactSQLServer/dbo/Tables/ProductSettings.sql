﻿CREATE TABLE [dbo].[ProductSettings] (
    [ID]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [ProductID]   BIGINT         NULL,
    [AccountName] NVARCHAR (200) NOT NULL,
    [AccountKey]  NVARCHAR (200) NOT NULL,
    [AliasName]   NVARCHAR (200) NULL,
    [Domain]      NVARCHAR (50)  NULL,
    [Protocol]    NVARCHAR (5)   NULL,
    [BlobType]    VARCHAR (30)   NULL,
    [CreatedDate] DATETIME       NOT NULL,
    [CreatedBy]   NVARCHAR (30)  NOT NULL,
    [EwsURL]      NVARCHAR (100) NULL,
    [MailCount]   NVARCHAR (50)  NULL,
    [FromMinute]  INT            NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);




go




