﻿
GO

CREATE TABLE [dbo].[RedactContainer](
	[RItemId] [int] NOT NULL,
	[RItemName] [nvarchar](50) NULL,
	[RItemDescription] [nvarchar](max) NULL,
	[RItemPath] [nvarchar](50) NULL,
	[RItemSize] [nchar](10) NULL,
	[RItemType] [nchar](10) NULL,
	[FKID_ConainerId] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [RItemId] PRIMARY KEY CLUSTERED 
(
	[RItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO


