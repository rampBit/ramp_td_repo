﻿CREATE TABLE  RestLogInfo
(
	[Id] bigint NOT NULL PRIMARY KEY identity(1,1),
	StartedOn datetime,
	RequestMethod varchar(6),
	RequestUrl nvarchar(max),
	ResponseStatusCode varchar(5),
	FinishedOn datetime, 
    [ResponseData] NVARCHAR(MAX) NULL

)
