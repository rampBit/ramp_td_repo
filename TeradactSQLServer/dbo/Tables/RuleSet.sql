﻿GO

CREATE TABLE [dbo].[RuleSet] (
    [RID]                INT            NOT NULL,
    [RuleSetName]        NVARCHAR (50)  NULL,
    [RuleSetDescription] NVARCHAR (MAX) NULL,
    [CreatedOn]          DATETIME       NULL,
    [ModifiedOn]         DATETIME       NULL,
    [TenantID]           BIGINT         NULL,
    [ProductID]          SMALLINT       NULL,
    CONSTRAINT [RID] PRIMARY KEY CLUSTERED ([RID] ASC)
);




