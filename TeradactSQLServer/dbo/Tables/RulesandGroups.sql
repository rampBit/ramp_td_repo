﻿CREATE TABLE [dbo].[RulesandGroups] (
    [ID]          INT           IDENTITY (1, 1) NOT NULL,
    [GroupName]   VARCHAR (50)  NULL,
    [RulesetName] VARCHAR (200) NULL
);

