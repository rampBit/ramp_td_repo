﻿CREATE TABLE [dbo].[SMTPSettings] (
    [Id]         SMALLINT      IDENTITY (1, 1) NOT NULL,
    [TargetName] NVARCHAR (50) NOT NULL,
    [Host]       VARCHAR (50)  NOT NULL,
    [Port]       INT           NOT NULL,
    [EnableSsl]  BIT           NOT NULL,
    [ProductId]  SMALLINT      NULL,
    [IsActive]   BIT           NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


