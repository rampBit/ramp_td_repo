﻿CREATE TABLE [dbo].[SPUnAttendedApplyRulesSettings] (
    [ID]              INT            IDENTITY (1, 1) NOT NULL,
    [SourceSiteURL]   NVARCHAR (200) NULL,
    [SourceDocLib]    NVARCHAR (100) NULL,
    [SourceFolder]    NVARCHAR (100) NULL,
    [TargetSiteURL]   NVARCHAR (200) NULL,
    [TargetDocLib]    NVARCHAR (100) NULL,
    [TargetFolder]    NVARCHAR (100) NULL,
    [AppliedRuleSets] NVARCHAR (200) NULL,
    [ISActive]        BIT            DEFAULT ((1)) NULL,
    [CreatedDate]     DATETIME       DEFAULT (getdate()) NULL,
    [ModifiedDate]    DATETIME       NULL,
    [ProductID]       SMALLINT       NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);


