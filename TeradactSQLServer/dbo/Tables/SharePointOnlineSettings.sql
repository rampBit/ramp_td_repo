﻿CREATE TABLE [dbo].[SharePointOnlineSettings] (
    [Id]                        SMALLINT        IDENTITY (1, 1) NOT NULL,
    [SPFileTransferServiceURL]  NVARCHAR (1000) NOT NULL,
    [SPAdminUserID]             NVARCHAR (100)  NOT NULL,
    [SPAdminPassword]           NVARCHAR (200)  NOT NULL,
    [AdminCentarlSiteURL]       NVARCHAR (1000) NOT NULL,
    [IsActive]                  BIT             NOT NULL,
    [SharedDocumentLibraryName] NVARCHAR (200)  NULL,
    [Domain]                    NVARCHAR (100)  NULL,
    [ProductID]                 SMALLINT        NULL,
    CONSTRAINT [PK_SharePointOnlineSettings] PRIMARY KEY CLUSTERED ([Id] ASC)
);



