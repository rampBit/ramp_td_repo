﻿
GO
CREATE TABLE [dbo].[SharePointRequestQueue](
	[SharePointQueueID] [bigint] IDENTITY(1,1) NOT NULL,
	[RequestedTime] [datetime] NOT NULL,
	[ProcessedTime] [datetime] NULL,
	[Status] [nchar](20) NOT NULL,
	[RequestFilesCount] [bigint] NULL,
	[FilesProcessedCount] [bigint] NULL,
	[FileName] varchar(200) NULL,
	[SPListID] varchar(50) NULL,
	[SPListItemID] int NULL,
	[SPHostURL] varchar(100) NULL,
	[EventSources] [nvarchar] (200) NULL
PRIMARY KEY CLUSTERED 
(
	[SharePointQueueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO


