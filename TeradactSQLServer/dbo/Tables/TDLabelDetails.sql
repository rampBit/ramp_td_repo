﻿CREATE TABLE [dbo].[TDLabelDetails]
(
	[ID] INT NOT NULL PRIMARY KEY identity(1,1), 
    [LabelName] NVARCHAR(50) NOT NULL, 
    [IsActive] BIT NULL DEFAULT 1, 
    [CreatedDate] DATETIME NULL DEFAULT getdate(),

)
