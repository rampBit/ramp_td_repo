﻿CREATE TABLE [dbo].[TDPolicyInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PolicyName] [nvarchar](30) NULL,
	[PolicyType] [nvarchar](30) NULL,
	[PolicyKey] [nvarchar](30) NULL,
	[PolicyValue] [nvarchar](30) NULL,

	[IsActive] [bit] NOT NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [dbo].[TDPolicyInfo] ADD  DEFAULT ((1)) FOR [IsActive]
GO
