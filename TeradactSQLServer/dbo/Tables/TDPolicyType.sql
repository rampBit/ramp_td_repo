﻿CREATE TABLE [dbo].[TDPolicyType](
	[PolicyTypeID] [bigint] IDENTITY(1,1) NOT NULL,
	[PolicyType] [nvarchar](30) NULL
)
