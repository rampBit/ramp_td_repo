﻿
CREATE TABLE [dbo].[TDRuleSetDirectiveInfo] (
    [ID]             INT           IDENTITY (1, 1) NOT NULL,
    [RuleSetName]    VARCHAR (50)  NULL,
    [RuleSetContent] VARCHAR (MAX) NULL,
    [IsActive]       BIT           DEFAULT ((1)) NULL,
    [ModifiedDate]   DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);



GO


GO