﻿CREATE TABLE  TeraDActConfiguration
(
	[Id] INT NOT NULL PRIMARY KEY identity(1,1),
	ServerURL nvarchar(100) not null,
	APIKey nvarchar(100) not null,
	 
	BaseURLRedactSuffix nvarchar(100) not null,
	BaseURLRulesetSuffix  nvarchar(100) not null,
	  RulesetXMLPathNode  nvarchar(100) not null,

	  IsActive bit default 1 not null
)
