﻿
GO

CREATE TABLE [dbo].[UserAccount](
	[Id] [int] NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[PasswordHash] [nvarchar](50) NULL,
	[FullName] [nvarchar](50) NULL,
	[EmailId] [nvarchar](50) NULL,
	[UserType] [char](10) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedOn] [datetime] NULL,
	[Flag] [int] NULL,
	[FKIDRoleId] [int] NULL,
 CONSTRAINT [Id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)




