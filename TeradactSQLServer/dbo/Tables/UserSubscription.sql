﻿GO

CREATE TABLE [dbo].[UserSubscription](
	[UserSId] [int] NOT NULL,
	[SubscriptionId] [nvarchar](50) NULL,
	[SubscriptionName] [nvarchar](50) NULL,
	[ResourceGroup] [nvarchar](50) NULL,
	[DBName] [nvarchar](50) NULL,
	[DBSource] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [UserSId] PRIMARY KEY CLUSTERED 
(
	[UserSId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)