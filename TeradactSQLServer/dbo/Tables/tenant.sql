﻿CREATE TABLE [dbo].[Tenant] (
    [ID]         BIGINT        IDENTITY (1, 1) NOT NULL,
    [CreatedOn]  DATETIME2 (7) NOT NULL,
    [ModifiedOn] DATETIME      NULL,
    [Name]       VARCHAR (80)  NULL,
    [IsActive]   BIT           NULL,
    CONSTRAINT [PK__tenant__3213E83FD34EC8AE] PRIMARY KEY CLUSTERED ([ID] ASC)
);


